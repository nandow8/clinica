<?php

// Route::get('/', function (){
//     return view('welcome');
// });
Route::get('/', 'Usuarios\\WelcomeController@index');
Route::post('/preagendamento', 'Usuarios\\WelcomeController@preagendamento');
Route::post('/preagenda', 'Usuarios\\WelcomeController@preagenda');
Route::get('/consultaRegistro', 'Usuarios\\PreagendamentoController@consultaRegistro');
Route::get('/gerarRegistro', 'Usuarios\\PreagendamentoController@gerarRegistro');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('admin/usuarios', 'Admin\\UserController');
Route::get('admin/usuarios/{id}/edita', 'Admin\\UserController@edita');

Route::get('/agendamentos', 'AgendamentosController@index')->name('agendamentos');
Route::get('/eventos', 'EventController@index')->name('agendamentos');
Route::get('/eventos/ajaxprod', 'EventController@ajaxagendamento')->name('agendamentos');
Route::get('/eventos/xeventos', 'EventController@ajaxeventos')->name('agendamentos');
Route::get('/eventos/xeventosdetalhes', 'EventController@ajaxeventosdetalhes')->name('agendamentos');
Route::get('/eventos/xeventosesclui', 'EventController@ajaxeventosexclui')->name('agendamentos');
Route::get('/eventos/xeventosedita', 'EventController@ajaxeventosupdate')->name('agendamentos');
Route::get('/eventos/xeventosclinicas', 'EventController@ajaxeventosclinicas')->name('agendamentos');

Route::resource('funcionarios/funcionarios', 'Funcionarios\\FuncionariosController');
Route::get('funcionarios/xmedicos', 'Funcionarios\\FuncionariosController@retornamedico');
Route::get('funcionarios/xdisponibilidadeagenda', 'Funcionarios\\FuncionariosController@disponibilidadaagendaajax');
Route::get('funcionarios/xdiastrabalho', 'Funcionarios\\FuncionariosController@getDiasTrabalhoMedico');
Route::get('funcionarios/xdisponibilidademedico', 'Funcionarios\\FuncionariosController@disponibilidademedicoajax');


Route::resource('usuarios/estados', 'Usuarios\\EstadosController');
Route::resource('usuarios/cidades', 'Usuarios\\CidadesController');
Route::resource('usuarios/bairros', 'Usuarios\\BairrosController');

Route::resource('usuarios/pacientes', 'Usuarios\\PacientesController');
Route::resource('financeiro/contasreceber', 'Financeiro\\ContasreceberController');//->middleware('role:contasreceber');
Route::get('financeiro/contasreceber/{id}/boleto', 'Financeiro\\ContasreceberController@boleto');
Route::get('financeiro/contasreceber/{id}/retornadadosservico', 'Financeiro\\ContasreceberController@retornadadosservico');
Route::get('financeiro/contasreceber/{id}/retornadadosdescacre', 'Financeiro\\ContasreceberController@retornadadosdescacre');
Route::get('financeiro/contasreceber/wssantander/{id}/boleto', 'Financeiro\\ContasreceberController@wsboleto');
Route::get('financeiro/contasreceber/ajaxexibirservicoexamerf/{str}', 'Financeiro\\ContasreceberController@ajaxexibirservicoexamerf');
Route::get('financeiro/contasreceber/ajaxindexnomepaciente/{str}/{cob}/{tip}', 'Financeiro\\ContasreceberController@ajaxindexnomepaciente');
Route::get('financeiro/contasreceber/{id}/createorcamento', 'Financeiro\\ContasreceberController@createorcamento');
Route::get('financeiro/contasreceber/{id}/receberPDF','Financeiro\\ContasreceberController@generatePDF');
Route::get('financeiro/contasreceber/{str}/remessa','Financeiro\\ContasreceberController@remessa');
Route::get('financeiro/contasreceber/{str}/retorno','Financeiro\\ContasreceberController@retorno');
Route::get('financeiro/contasreceber/{id},{param}/marcarremessaajax','Financeiro\\ContasreceberController@marcarremessaajax');
Route::get('financeiro/contasreceber/{id},{parc},{param}/contratogerareceber','Financeiro\\ContasreceberController@contratogerareceber');
Route::post('financeiro/contasreceber/upload', 'Financeiro\\ContasreceberController@upload')->name('financeiro.contasreceber.upload');

Route::resource('financeiro/orcamento', 'Financeiro\\OrcamentosController');//->middleware('role:orcamento');
Route::get('financeiro/orcamento/{id}/boleto', 'Financeiro\\OrcamentosController@boleto');
Route::get('financeiro/orcamento/{id}/retornadadosservico', 'Financeiro\\OrcamentosController@retornadadosservico');
Route::get('financeiro/orcamento/{id}/retornadadosdescacre', 'Financeiro\\OrcamentosController@retornadadosdescacre');
Route::get('financeiro/orcamento/wssantander/{id}/boleto', 'Financeiro\\OrcamentosController@wsboleto');
Route::get('financeiro/orcamento/ajaxexibirservicoexamerf/{str}', 'Financeiro\\OrcamentosController@ajaxexibirservicoexamerf');
Route::get('financeiro/orcamento/ajaxindexnomepaciente/{str}/{cob}/{tip}', 'Financeiro\\OrcamentosController@ajaxindexnomepaciente');
Route::get('financeiro/orcamento/{id}/createorcamento', 'Financeiro\\OrcamentosController@createorcamento');
Route::get('financeiro/orcamento/{id}/receberPDF','Financeiro\\OrcamentosController@generatePDF');
Route::get('financeiro/orcamento/{str}/remessa','Financeiro\\OrcamentosController@remessa');
Route::get('financeiro/orcamento/{str}/retorno','Financeiro\\OrcamentosController@retorno');
Route::get('financeiro/orcamento/{id},{param}/marcarremessaajax','Financeiro\\OrcamentosController@marcarremessaajax');
Route::get('financeiro/orcamento/{id},{parc},{param}/contratogerareceber','Financeiro\\OrcamentosController@contratogerareceber');
Route::post('financeiro/orcamento/upload', 'Financeiro\\OrcamentosController@upload')->name('financeiro.orcamento.upload');
Route::get('financeiro/orcamento/{id}/exportarReceber','Financeiro\\OrcamentosController@exportarReceber');


Route::resource('financeiro/contaspagar', 'Financeiro\\ContaspagarController');
Route::resource('financeiro/fluxocaixa', 'Financeiro\\FluxocaixaController');

Route::resource('financeiro/contrato', 'Financeiro\\ContratoController');
Route::get('financeiro/contrato/{id}/retornadadostitular', 'Financeiro\\ContratoController@retornadadostitular');
Route::get('financeiro/contrato/{id}/retornadadosdependentes', 'Financeiro\\ContratoController@retornadadosdependentes');
Route::get('financeiro/contrato/{id}/retornamodelodocumento', 'Financeiro\\ContratoController@retornamodelodocumento');
Route::get('financeiro/contrato/{id}/contratoPDF','Financeiro\\ContratoController@generatePDF');
Route::get('financeiro/contrato/{id}/copiacontrato','Financeiro\\ContratoController@copiacontrato');

Route::resource('financeiro/recibo', 'Financeiro\\ReciboController');
Route::resource('admin/perfil', 'Admin\\PerfilController');

Route::resource('cadastro/fornecedor', 'Cadastro\\FornecedorController');

Route::resource('admin/tabelasparapopularperfil', 'Admin\\TabelasparapopularperfilController');
Route::resource('cadastro/clinica', 'Cadastro\\ClinicaController');

Route::resource('cadastro/tusscapitulo', 'Cadastro\\TUSSCapituloController');
Route::resource('cadastro/tussgrupo', 'Cadastro\\TUSSGrupoController');
Route::resource('cadastro/tusssubgrupo', 'Cadastro\\TUSSSubgrupoController');
Route::resource('cadastro/tuss', 'Cadastro\\TUSSController');
Route::resource('cadastro/tabela-servico', 'Cadastro\\TabelaServicoController');


Route::resource('preagendamento', 'Usuarios\\PreagendamentoController');
Route::resource('especialidades/especialidades', 'Especialidades\\EspecialidadesController');

Route::resource('cadastro/cid', 'Cadastro\\CidController');

Route::resource('medicos/medicofichaclinica', 'Medicos\\MedicofichaclinicaController');
Route::resource('cadastro/tabela-servico', 'Cadastro\\TabelaServicoController');
Route::resource('cadastro/pacientesconvenio', 'Usuarios\\PacientesconvenioController');
Route::get('/cadastro/ajaxpacienteconvenio/{convenio_id}', 'Usuarios\\PacientesController@ajaxpacienteconvenio');

Route::post('/getCidades', 'Usuarios\\PacientesController@getCidades');

Route::resource('cadastro/salas', 'Salas\\SalasController');

Route::resource('cadastro/planos', 'Cadastro\\PlanosController');

Route::get('/medicos/ajaxexcluiranexo/{anexo_id}', 'Medicos\\MedicofichaclinicaController@ajaxexcluirAnexo');
Route::get('/medicos/ajaxexibirdescricaocid/{descricao_cid}', 'Medicos\\MedicofichaclinicaController@ajaxexibirdescricaocid');
Route::get('/medicos/ajaxexibirmedicamento/{medicamento}/{prescricoes_tipo}', 'Medicos\\MedicofichaclinicaController@ajaxexibirmedicamento');
Route::get('/medicos/ajaxultimareceita/{medicofichaclinica_id}/{user_id}', 'Medicos\\MedicofichaclinicaController@ajaxultimareceita');
Route::get('/medicos/ajaxultimoexame/{medicofichaclinica_id}/{user_id}', 'Medicos\\MedicofichaclinicaController@ajaxultimoexame');
Route::get('/medicos/ajaxexibirexamerf/{codigooudescricao}', 'Medicos\\MedicofichaclinicaController@ajaxexibirexamerf');
Route::get('/medicos/ajaxhistoricoprescricoespdf/{id_prescricao}', 'Medicos\\MedicofichaclinicaController@ajaxhistoricoprescricoespdf');
Route::get('/medicos/ajaxhistoricoexamespdf/{id_exame}', 'Medicos\\MedicofichaclinicaController@ajaxhistoricoexamespdf');
Route::get('/medicos/ajaxhistoricoatestadospdf/{id_atestado}', 'Medicos\\MedicofichaclinicaController@ajaxhistoricoatestadospdf');
Route::get('/medicos/ajaxhistoricoatestadosretificacao/{historico_id}/{retificacao}', 'Medicos\\MedicofichaclinicaController@ajaxhistoricoatestadosretificacao');
Route::get('medicos/abrirficha/{id_paciente}', 'Medicos\\MedicofichaclinicaController@abrirfichaclinica');
Route::get('medicos/medicofichaclinica/{medicofichaclinica}/{agendamento}', 'Medicos\\MedicofichaclinicaController@edit');
Route::get('/medicos/buscaanamnese/{id}', 'Medicos\\MedicofichaclinicaController@buscaanamnese');

Route::resource('cadastro/medicamento', 'Cadastro\\MedicamentoController');
Route::resource('cadastro/fabricante', 'Cadastro\\FabricanteController');

Route::resource('relatorios/mapa', 'Relatorios\\MapaController');
Route::get('/mapa', 'Relatorios\\MapaController@mapa')->name('localizacao');
Route::get('/mapa/findpacientes', 'Relatorios\\MapaController@findPacientes')->name('localizacao');
Route::get('/findpacientebyid', 'Relatorios\\MapaController@findPacienteById');
Route::get('/mapa/findDependente', 'Relatorios\\MapaController@findDependente')->name('localizacao');
// Route::get('/eventos', 'EventController@index')->name('agendamentos');

Route::resource('relatorios/contasreceberrel', 'Relatorios\\ContasreceberrelController');

Route::resource('medicos/medicomodeloatestado', 'Medicos\\MedicomodeloatestadoController');//->middleware('role:medicomodeloatestado');

Route::get('/logout', 'Auth\LoginController@logout');

Route::get('tela/', 'TelaController@index');

Route::resource('cadastro/modelos-documentos', 'Cadastro\\ModelosDocumentosController');

Route::resource('cadastro/examesrf', 'Cadastro\\ExamesrfController');

Route::resource('cadastro/tabela-desconto-acrescimo', 'Cadastro\\TabelaDescontoAcrescimoController');

Route::resource('cadastro/grupoempresarial', 'Cadastro\\GrupoempresarialController');
Route::resource('cadastro/tipos-pagamentos', 'Cadastro\\TiposPagamentosController');
Route::resource('cadastro/bancos', 'Cadastro\\BancosController');
Route::resource('cadastro/despesas', 'Cadastro\\DespesasController');