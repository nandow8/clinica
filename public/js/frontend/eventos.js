$(function () {
    //mascara
    $('#telefone').mask('(00) 0000-00009');
    $('#telefone').blur(function(event) {
    if($(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
        $('#telefone').mask('(00) 00000-0009');
    } else {
        $('#telefone').mask('(00) 0000-00009');
    }
    });


    $('#dataconsulta').datepicker({
        dateFormat: "dd/mm/yy",                
        numberOfMonths: 1,
        minDate: 0,
        monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
        dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        onSelect: function(selectedDate) {                  
        }
    });
  
  function setOption(name){
    $(".agendamento__radio-button").css("display","none");
    $("[name="+name+"]").css("display", "block");
  }

  setOption("agendamento-tipo-consulta");
  
  changeOption = function(obj){     
        valor = $(obj).val();
        switch(valor){
            case 'exame':
                $("#box-dataconsulta").fadeOut('fast', function(){
                    $("#box-exame").fadeIn();
                    $("#exame").val("");
                });
            break;
            case 'consulta':
                $("#box-exame").fadeOut('fast', function(){
                    $("#box-dataconsulta").fadeIn();
                    $("#dataconsulta").val("");
                });
            break;
        }

        radio = $(obj).attr("id");          
        setOption(radio);
  }

$("#sendMessageButton").on("click", function(ev){
    ev.preventDefault();
    //dados
    var dados = $("#contactForm").serialize();
   
     $.post("../mail/contact_me.php", dados, function(data){
         if(data.status=="FALHA"){
             $("#errormessage").html(data.mensagem).fadeIn();

            setTimeout(function(){
                $("#errormessage").fadeOut();
            }, '3000');
         }
     }, "jSON");
});

$("#btn-send").on("click", function(e){
    e.preventDefault();    
    var dados = $('#agendaForm').serialize();//dados vindo do formulário

    $.ajax({
        //antes de executar
        //comando acrescentado para corrigir problemas com o Laravel
        beforeSend: function(xhr, type) {
            if (!type.crossDomain) {               
                xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
            }
        },
        url: '/preagenda',
        type: 'POST',
        dataType: 'json',
        data: dados,
        success: function(retorno){
            //erro
            if(retorno.status=="FALHA"){
                $("#boxerror").fadeIn();
                $("#errormensagem").html(retorno.mensagem);

                setTimeout(function(){
                    $("#boxerror").fadeOut();//esconder box de erro
                }, '3000');
            }else{
                $("#boxsucesso").fadeIn();
                $("#sucessomensagem").html(retorno.mensagem);
               
                $('#agendaForm').trigger("reset");//apagar todos os valores do formulário
               
                setTimeout(function(){
                    $("#boxsucesso").fadeOut();//esconder box de sucesso
                }, '5000');
            }
        }
    });
})

});