jQuery(function () {   
    
    var jAgenda = jQuery.noConflict();

    changeOption = function(obj){
        valor = jAgenda(obj).val();
        switch(valor){
            case 'exame':
                jAgenda("#box-dataconsulta").fadeOut('fast', function(){
                    jAgenda("#box-exame").fadeIn();
                    jAgenda("#exame").val("");
                });
            break;
            case 'consulta':
                jAgenda("#box-exame").fadeOut('fast', function(){
                    jAgenda("#box-dataconsulta").fadeIn();
                    jAgenda("#dataconsulta").val("");
                });
            break;
        }
    }

    jAgenda("#btn-registro").on("click", function(ev){
        ev.preventDefault();

        var dados = jAgenda("#form-edit").serialize();
        jAgenda.ajax({
            url: '/gerarRegistro',
            type: 'GET',
            dataType: 'json',
            data: dados,
            success: function(retorno){           
                if(retorno.status === 'OKAY'){
                    window.location.replace('/usuarios/pacientes/');
                }else{
                    // window.location.replace('/usuarios/pacientes/');
                }
            },
            error: function(){
                console.log('Houve um erro');
            }
        });
    });

    function consultaRegistroPorCPF(cpf, nome){ 
      
       if(cpf.length >= 11){
            jAgenda.ajax({
                //antes de executar
                //comando acrescentado para corrigir problemas com o Laravel
                beforeSend: function(xhr, type) {
                    if (!type.crossDomain) {               
                        xhr.setRequestHeader('X-CSRF-Token', jAgenda('meta[name="csrf-token"]').attr('content'));
                    }
                },
                url: '/consultaRegistro',
                type: 'GET',
                dataType: 'json',
                data: {cpf:cpf, nome:nome},
                success: function(retorno){
                   
                    if(retorno.status==="OKAY"){
                        jAgenda("#statusregistro").removeClass("alert-warning").addClass("alert-info").text("Paciente já possui cadastro");                              
                        jAgenda("#boxstatusregistro").show();
                        jAgenda("#btn-registro").hide();
                    }else{
                        jAgenda("#btn-registro").show();
                        jAgenda("#statusregistro").removeClass("alert-info").addClass("alert-warning").text(retorno.mensagem + '! Você pode gerar o registro do paciente'); 
                        jAgenda("#boxstatusregistro").fadeIn();                           
                    }
                }
            });
        }
    }
    jAgenda('#cpf').mask('000.000.000-00');
    consultaRegistroPorCPF(jAgenda("#cpf").val(), jAgenda("#nome").val());

    jAgenda("#cpf").keyup(function(){
        var cpf = jAgenda(this).val();
        
        if(cpf.length > 13){
            consultaRegistroPorCPF(cpf, jAgenda("#nome").val());
        }else{
            jAgenda("#boxstatusregistro").hide();
            jAgenda("#btn-registro").hide();
        }
    });

});