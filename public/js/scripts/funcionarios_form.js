/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var unsaved = false;

jQuery("input,textarea,select").change(function () {
    unsaved = true;
});

jQuery(document).ready(function () {
    window.onbeforeunload = function () {
        if (unsaved) {
            return "";
        }
    };

    jQuery('#especialidade').select2({
        maximumInputLength: 20
      });
    $especialidades = $form_especialidades.split(',');
    jQuery('#especialidade').val($especialidades).change();

    jQuery('.telefone').mask('(00) 0000-00009');
    jQuery('.celular').mask('(00) 00000-0009');
    jQuery('#cep').mask('00000-000');
    jQuery('#rg').mask('00.000.000-0');
    jQuery('#cpf').mask('000.000.000-00');
    jQuery('#cnpj').mask('00.000.000/0000-00');
    jQuery('#cpfcnpj').mask('00.000.000/0000-00');
    jQuery('[name=crm]').mask("99999999-99", {reverse: true});

});

$('[name=areaatuacao]').change(function(){
    if(jQuery(this).val() == 'medico'){
        jQuery('#categoria').text('CRM');
        jQuery('#categoriaemissao').text('CRM Emissão');
    }else{
        jQuery('#categoria').text('CRO');
        jQuery('#categoriaemissao').text('CRO Emissão');
    }
});
function limpaCampos() {
        // Limpa valores do formulário de cep.
        jQuery("#rua").val("");
        jQuery("#bairro").val("");
        jQuery("#cidade").val("");
        jQuery("#uf").val("");
        jQuery("#estado").val("");
    }
function buscacep(){
    var cep = jQuery('[name=cep]').val();
    if(cep.length>=9){
        jQuery.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
            if (!("erro" in dados)) {
                
                jQuery('[name=rua]').val(dados.logradouro);
                jQuery('[name=bairro]').val(dados.bairro);
                jQuery('[name=cidade]').val(dados.localidade);
                jQuery('.'+dados.uf).attr("selected", true);
                
                jQuery('[name=numero]').focus();
            } //end if.
            else {
                limpaCampos();
            }
        });
    }
}//end function buscacep

$('[name=filhos]').change(function(){ 
    if(jQuery(this).val() == 'nao'){
        jQuery('#totalfilhos').val('0');
    }
});