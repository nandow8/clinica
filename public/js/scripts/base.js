/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var unsaved = false;

jQuery("input,textarea,select").change(function () {
    unsaved = true;
});

jQuery(document).ready(function () {

    var cpfMascara = function (val) {
       return val.replace(/\D/g, '').length > 11 ? '00.000.000/0000-00' : '000.000.000-009';
    };

    // jQuery('#telefone').mask('(00) 0000-0009');
    jQuery('#telefone, #telefonedois').mask('(00) 0000-00009');
    jQuery('#telefone, telefonedois').blur(function(event) {
        if(jQuery(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            jQuery('#telefone, #telefonedois').mask('(00) 00000-0009');
        } else {
            jQuery('#telefone, #telefonedois').mask('(00) 0000-00009');
        }
    });

    jQuery('#celular').mask('(00) 00000-0009');
    jQuery('#cep').mask('00000-000');
    jQuery('#rg').mask('00.000.000-0');


    jQuery('#cpf').mask(cpfMascara);
    jQuery('#cnpj').mask(cpfMascara);
    jQuery('#cpfcnpj').mask(cpfMascara);

    function limpaCampos() {
        // Limpa valores do formulário de cep.
        jQuery("#endereco").val("");
        jQuery("#bairro").val("");
        jQuery("#cidade").val("");
        jQuery("#uf").val("");
    }

    buscarCEP = function(cep = ''){
        if(!cep){
            cep = jQuery('#cep').val();
        }
        //CEP - somente dígitos
        cep = cep.replace(/\D/g, '');

        jQuery("#endereco").val("...");
        jQuery("#bairro").val("...");
        jQuery("#cidade").val("...");
        jQuery("#uf").val("...");

        jQuery.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

            if (!("erro" in dados)) {
                jQuery(".errorcep").html("").hide();
                //Atualiza os campos com os valores da consulta.
                jQuery("#endereco").val(dados.logradouro);
                jQuery("#bairro").val(dados.bairro);
                jQuery("#cidade").val(dados.localidade);
                jQuery("#uf").val(dados.uf);

                jQuery("#numero").focus();
            } //fim do if
            else {
                //CEP pesquisado não foi encontrado.
                limpaCampos();
                jQuery(".errorcep").html("CEP não encontrado").show();
            }
        });

    }
    jQuery('#cep').keyup(function(){
        cep = jQuery(this).val();
        if(cep.length>=9){
            buscarCEP(cep);
        }else{
            limpaCampos();
            jQuery(".errorcep").html("CEP não encontrado").show();
        }
    });

});
