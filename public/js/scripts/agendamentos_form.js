
  jQuery(document).ready(function(){

    jQuery('.fc-unthemed td.fc-today').css('background-color', '#5dc1df !important');

	function load_calendar(){

		$('#calendar').fullCalendar({
      locale: 'pt-br',
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			ignoreTimezone: false,
			monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
			monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
			dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
			dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
			slotDuration: '00:15:00',
			slotLabelInterval: 15,
			slotLabelFormat: 'H(:mm)',
			eventLimit: true,
			minTime: '7:00',
			maxTime: '23:15',
			timeFormat: 'H(:mm)',
			defaultTimedEventDuration : '00:15:00',
			buttonText: {
				today: "Hoje",
				month: "Mês",
				week: "Semana",
				day: "Dia",
			},
		dayClick: function(date, jsEvent, view){

      var dayselected = moment(date.format());

      jQuery("#diasemana").val(dayselected.day());
      zeraformulario();
      setsalasclinicas('','');

      jQuery('#_data').val(date.format());
      jQuery('#data').val(date);

      var $strdata = date.format().split('T');
      var hora =  date.format().split('T')[1];
      var data = new Date();

      var dataAtual = (data.getFullYear() + '-' + (data.getMonth()+1) + '-' + data.getDate());

      if(tipoperfil=='geral' && $strdata.length > 1){

			  $("#boxredireciona").fadeOut();
        jQuery('#exampleModal').modal('show');

        if($strdata.length == 1){
        $hora = "Agendamento Diário";
        document.getElementById('str_hora').innerHTML = $hora;

        }else{
        $hora = date.format().split('T')[1];
        document.getElementById('str_hora').innerHTML = $hora;
        }

        document.getElementById('str_data').innerHTML = dataconvert($strdata[0]);

        $('#pacx').append("<option value='10'>teste</option>");

        $('#footer').html('');
        if( !((date < moment()) && (!(moment().format('YYYY-MM-DD') == date.format('YYYY-MM-DD')))) ){
        $('#footer').append("<button type='button' class='btn btn-success' onclick='salva()'>Salvar</button>");
        $('#footer').append("<button type='button' class='btn btn-danger'  onclick='exclui()' data-dismiss='modal'>Excluir</button>");
        }else{
        $('#alerta').css('display','block').animate({opacity: 1});
        $('#alerta').html('Agendamentos Indisponíveis para datas anteriores');
        }
        $('#footer').append("<button type='button' class='btn btn-primary' data-dismiss='modal'>Fechar</button>");
      }
    },
		eventClick: function(calEvent, jsEvent, view){
			$("#boxredireciona").fadeOut();

			$.ajax({
			url: 'eventos/xeventosdetalhes',
			Type: 'GET',
			data: {
				id: calEvent.obs
			},
			success: function(doc){
        result = JSON.parse(doc);
        evento = result.dados;
        ficha = result.ficha;

				if(evento){
					$("#boxredireciona").fadeIn();
				}

				document.getElementById('str_data').innerHTML = dataconvert(evento.dataconsulta);
				document.getElementById('str_hora').innerHTML = retornahorario(evento.startconsulta, evento.endconsulta);

        var href='';

        if(evento.status === "finalizado"){
            href = '/medicos/medicofichaclinica/'+ficha.id;
        }else{
            href = '/medicos/medicofichaclinica/'+ficha.id+'/'+evento.id;
        }

			$('#redireciona').removeAttr('href');
			$('#redireciona').attr('href', href);

			$('#alerta').css('display','none');
			$('#alerta').html('');
			$('[name=atividade]').val(evento.atividade);
      $('[name=obs]').val(evento.obs);

			//$('[name=medico] option').eq(evento.medico).prop('selected', true);
			$('[name=medico]').val(evento.medico);
			$('[name=paciente]').val(evento.paciente);

			$('[name=status]').val(evento.status);
			$('[name=clinica]').val(evento.idclinica);

			setespecialidades(evento.medico);
			setsalasclinicas(evento.idclinica,evento.sala);

			$('#footer').html('');
			$('#footer').append("<input id='calevent' type='hidden' value='" + calEvent._id + "' >");
			$('#footer').append("<input id='idagendamento' type='hidden' value='" + calEvent.obs + "' >");

			if(!(evento.status == 'finalizado')){
				if(tipoperfil!=='Médico'){
					$('#footer').append("<button type='button' class='btn btn-success' onclick='edita()'>Editar</button>");
					$('#footer').append("<button type='button' class='btn btn-danger'  onclick='exclui()' data-dismiss='modal'>Excluir</button>");
				}
        }
				$('#footer').append("<button type='button' class='btn btn-primary' data-dismiss='modal'>Fechar</button>");

				jQuery('#exampleModal').modal('show');
			}
			});
		},
		navLinks: true,   // can click day/week names to navigate views
		eventLimit: false, // allow "more" link when too many events
		editable: false,
		events: function(start, end, timezone, callback) {

			//var filtros = [];
			salas = getSalas();
			medicos = getMedicos();
			grupoStatus = getStatus();

			$.ajax({
				url: 'eventos/xeventos',
				type: 'GET',
				data: {
					salas : salas,
					status: grupoStatus,
					medicos : medicos
				},
				success: function(doc){
				events = JSON.parse(doc);
				callback(events);

				}
			});
		},
		});
	}

	load_calendar();
/** Begin Filtro Salas */
	function marcarCampo(obj){
		if($(obj).is(':checked')){
			$(obj).parent().parent().addClass('checked');
			$(obj).prev().addClass('fa-check');
		}else{
			$(obj).parent().parent().removeClass('checked');
			$(obj).prev().removeClass('fa-check');
		}
	}
    $('[name=sala]').attr('disabled', true);

    $("#todassalas").change(function(){
		marcarCampo($(this));
		if($(this).is(':checked')){
			$(".each-room .checksala").each(function(){
				$(this).parent().parent().addClass('checked');
				$(this).prev().addClass('fa-check');
				$(this).prop('checked', true);
			});
		}else{
			$(".each-room .checksala").each(function(){
				$(this).parent().parent().removeClass('checked');
				$(this).prev().removeClass('fa-check');
				$(this).prop('checked', false);
			});
		}
		$('#calendar').fullCalendar('refetchEvents');
	});

  $(".checksala").change(function(){
		marcarCampo($(this));
		$('#calendar').fullCalendar('refetchEvents');
  });

/** End Filtro Salas */

/**Begin Filtro Médicos */
	$("#todosmedicos").change(function(){
		marcarCampo($(this));
		if($(this).is(':checked')){
			$(".each-doctor .checkmedico").each(function(){
				$(this).parent().parent().addClass('checked');
				$(this).prev().addClass('fa-check');
				$(this).prop('checked', true);
			});
		}else{
			$(".each-doctor .checkmedico").each(function(ind, el){
				$(this).parent().parent().removeClass('checked');
				$(this).prev().removeClass('fa-check');
				$(this).prop('checked', false);
			});
		}
		$('#calendar').fullCalendar('refetchEvents');
	});

	$(".checkmedico").change(function(){
		marcarCampo($(this));
		$('#calendar').fullCalendar('refetchEvents');
	});
  /** End Filtro Médicos */

  /** Begin Filtro Status */
  $("#todosStatus").change(function(){

		marcarCampo($(this));
		if($(this).is(':checked')){
			$(".each-status .checkstatus").each(function(){
				$(this).parent().parent().addClass('checked');
				$(this).prev().addClass('fa-check');
				$(this).prop('checked', true);
			});
		}else{
			$(".each-status .checkstatus").each(function(ind, el){
				$(this).parent().parent().removeClass('checked');
				$(this).prev().removeClass('fa-check');
				$(this).prop('checked', false);
			});
		}
		$('#calendar').fullCalendar('refetchEvents');
	});

	$(".checkstatus").change(function(){
		marcarCampo($(this));
		$('#calendar').fullCalendar('refetchEvents');
	});
  /** End Filtro Status */

	function getMedicos(){
		//pegar salas marcadas
		var medicos = $("input[name='medicos[]']:checked");

    var idsmedicos = new Array();//array que recebe os ids dos médicos

		$.each(medicos, function(){
			idsmedicos.push($(this).val());
		});

		//desmarcar filtro TODAS caso não haja nenhuma sala preenchida
		if(idsmedicos.length===0) $("#todosmedicos").prop('checked', false);

		return idsmedicos;
  }

  function getStatus(){
    //pegar status marcadas
	var status = $("input[name='status[]']:checked");
	// console.table(status);
    var idsStatus = new Array();//array que recebe os ids dos status

	$.each(status, function(){
		idsStatus.push($(this).val());
	});

	//desmarcar filtro TODAS caso não haja nenhuma sala preenchida
	if(idsStatus.length===0) $("#todosStatus").prop('checked', false);

	return idsStatus;
  }

	function getSalas(){
		//pegar salas marcadas
		var salas = $("input[name='salas[]']:checked");

		var idssalas = [];//array que recebe os ids das salas
		$.each(salas, function(){
			idssalas.push($(this).val());
		});

		//desmarcar filtro TODAS caso não haja nenhuma sala preenchida
		if(idssalas.length===0) $("#todassalas").prop('checked', false);

		return idssalas;
	}

  });

  function salva(){

    var $data = jQuery('#_data').val();
    var $hora = jQuery('[name=hora]').val();
    var $paciente = jQuery('[name=paciente]').val();
    var $medico = jQuery('[name=medico]').val();
    var $nomepaciente =  jQuery('[name=paciente] option:selected').text();
    var $nomemedico =  jQuery('[name=medico] option:selected').text();
    var $sala = jQuery('[name=sala]').val();
    var $atividade = jQuery('[name=atividade]').val();
    var $obs = jQuery('[name=obs]').val();
    var $status = jQuery('[name=status]').val();
    var $nomeclinica = jQuery('[name=clinica] option:selected').text();
    var $clinica = jQuery('[name=clinica] option:selected').val();
    var $diasemana = jQuery('[name=diasemana]').val();

    if($data.indexOf("T") != -1){
      // Dia todo
      var newEvent = {
            title: $('[name=paciente] option:selected').text(),
            start: $data
          };
    }else{
      // horario específico
      var newEvent = {
          title: $('[name=paciente] option:selected').text(),
          start: $data,
          end: $data
      };
    }
    //verificacamposincorretos($medico,$paciente,$clinica,$sala,$atividade);
    if(!(verificacamposincorretos($medico,$paciente,$clinica,$sala,$atividade))){
       	verificadisponibilidade($data, $medico, $sala, $diasemana);
    }

  }//end function


  function edita(){

    var evento = $('#calevent').val();
    var idagendamento = $('#idagendamento').val();
    var $paciente = jQuery('[name=paciente]').val();
    var $medico = jQuery('[name=medico]').val();
    var $nomepaciente =  jQuery('[name=paciente] option:selected').text();
    var $nomemedico =  jQuery('[name=medico] option:selected').text();
    var $sala = jQuery('[name=sala]').val();
    var $atividade = jQuery('[name=atividade]').val();
    var $obs = jQuery('[name=obs]').val();
    var $status = jQuery('[name=status]').val();
    var $nomeclinica = jQuery('[name=clinica] option:selected').text();
    var $idclinica = jQuery('[name=clinica] option:selected').val();

    jQuery.ajax({
      type: "GET",
      data: {
        id: idagendamento,
        paciente: $paciente,
        medico: $medico,
        sala: $sala,
        nomepaciente: $nomepaciente,
        nomemedico: $nomemedico,
        atividade: $atividade,
        obs: $obs,
        status: $status,
        nomeclinica: $nomeclinica,
        idclinica: $idclinica
      },
      url: "/eventos/xeventosedita",
      success: function(s){
        jQuery('#exampleModal').modal('hide');
      }
    }) // end ajax


  }

  function exclui(){
      evento = $('#calevent').val();
      idagendamento = $('#idagendamento').val();
      jQuery.ajax({
        type: "GET",
        data: {
          id: idagendamento
        },
        url: "/eventos/xeventosesclui",
        success: function(s){
          $('#calendar').fullCalendar('removeEvents',evento);
        }
      }) // end ajax
  }

  function dataconvert(data){

    var dia  = data.split("-")[2];
    var mes  = data.split("-")[1];
    var ano  = data.split("-")[0];

    return (dia + '/' + mes + '/' + ano);
  }

  function zeraformulario(){

    $('[name=paciente] option').eq('').prop('selected', true);
    $('[name=medico] option').eq('').prop('selected', true);
    $('[name=sala] option').eq('').prop('selected', true);
    $('[name=clinica] option').eq('').prop('selected', true);
    $('#alerta').css('display','none');
    $('#alerta').css('opacity','0');
    $('#mostrador').html('');

    $('[name=atividade]').val('');
    $('[name=obs]').val('');
  }

  function retornahorario(start, end){

    $start = start.split(' ');
    $end = end.split(' ');

    if(($start[1] == '00:00:00') && ($end[1] == '00:00:00')){
        $horario = 'Total diário';
    }else{
        $aux = $start[1].split(':');
        $horario = $aux['0'] + ":" + $aux['1'];
    }

    return $horario

  }//end function retornahorario

  changeOption = function(obj){
    valor = $(obj).val();
    switch(valor){
        case 'exame':
            $("#box-dataconsulta").fadeOut('fast', function(){
                $("#box-exame").fadeIn();
                $("#exame").val("");
            });
        break;
        case 'consulta':
            $("#box-exame").fadeOut('fast', function(){
                $("#box-dataconsulta").fadeIn();
                $("#dataconsulta").val("");
            });
        break;
    }
}

function consultaRegistroPorCPF(cpf, nome){
   	if(cpf.length >= 13){
      	$.ajax({
            //antes de executar
            //comando acrescentado para corrigir problemas com o Laravel
            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'));
                }
            },
            url: '/consultaRegistro',
            type: 'GET',
            dataType: 'json',
            data: {cpf:cpf, nome:nome},
            success: function(retorno){
                if(retorno.status==="OKAY"){
                    $("#statusregistro").removeClass("alert-warning").addClass("alert-info").text("Paciente já possui cadastro");
                    $("#boxstatusregistro").show();
                    $("#btn-registro").hide();
                }else{
                    $("#btn-registro").show();
                    $("#statusregistro").removeClass("alert-info").addClass("alert-warning").text(retorno.mensagem + '! Você pode gerar o registro do paciente');
                    $("#boxstatusregistro").fadeIn();
                }
            }
        });
    }

}
  $('[name=medico]').change(function(){
    $id = $(this).val();
    if($id != ''){
      setDiaTrabalho($id);
      setespecialidades($id);
    }else{
      $('#mostrador').html('');
    }
  });

  function setespecialidades($id){

    jQuery.ajax({
      type: "GET",
      data: {
        id: $id
      },
      url: "funcionarios/xmedicos",
      success: function(s){
        especialidades = JSON.parse(s);
        jQuery('#mostrador').html('');
        jQuery.each(especialidades, function(i, val){
            jQuery('#mostrador').append("<span class='badge badge-info'>" + val[0].titulo + "</span> &nbsp;");
        });
      }
    }) // end ajax

  }//end function setaespecialidades

  function setDiaTrabalho($id){

    jQuery.ajax({
      type: "GET",
      data: {
        id: $id
      },
      url: "funcionarios/xdiastrabalho",
      success: function(s){
        diastrabalho = JSON.parse(s);
      }
    }) // end ajax

  }//end function setaespecialidades

  function verificadisponibilidade($data, $medico, $sala, $diasemana){

    jQuery.ajax({
      type: "GET",
      data: {
        data: $data,
        medico: $medico,
        sala: $sala
      },
      url: "funcionarios/xdisponibilidadeagenda",
      success: function(s){
			resultado = JSON.parse(s);
			if(resultado.ocupado){
				$('#alerta').css('display','block').animate({opacity: 1});

				if(resultado.medico && resultado.sala){
				$('#alerta').text('Médico e sala já atribuidos');
				}else{
				if(resultado.medico){
					$('#alerta').text('Médico já atribuído para este horário');
				}else{
					$('#alerta').text('Sala já atribuída para este horário');
				}
				}
			}else{
				$("#alerta").fadeOut();
				verificadisponibilidademedico($medico, $diasemana);
			}
      	}
	}); // end ajax

  }//end function verificadisponibilidade

function verificadisponibilidademedico($medico, $diasemana){

	jQuery.ajax({
		type: "GET",
		data: {
		medico: $medico,
		diasemana: $diasemana
		},
		url: "funcionarios/xdisponibilidademedico",
		success: function(s){

		resultado = JSON.parse(s);

			if(resultado.error){
				$('#alertamedico').css('display','block').animate({opacity: 1});
				$('#alertamedico').text(resultado.mensagem);
			}else{
				ajaxsave();
			}
		}

  	}); // end ajax
}

  function ajaxsave(){
    var $data = jQuery('#_data').val();
    var $hora = jQuery('[name=hora]').val();
    var $paciente = jQuery('[name=paciente]').val();
    var $medico = jQuery('[name=medico]').val();
    var $nomepaciente =  jQuery('[name=paciente] option:selected').text();
    var $nomemedico =  jQuery('[name=medico] option:selected').text();
    var $sala = jQuery('[name=sala]').val();
    var $atividade = jQuery('[name=atividade]').val();
    var $obs = jQuery('[name=obs]').val();
    var $status = jQuery('[name=status]').val();
    var $nomeclinica = jQuery('[name=clinica] option:selected').text();
    var $idclinica = jQuery('[name=clinica] option:selected').val();


    if($data.indexOf("T") != -1){
      // Dia todo
      var newEvent = {
            title: $('[name=paciente] option:selected').text(),
            start: $data
          };
    }else{
      // horario específico
      var newEvent = {
          title: $('[name=paciente] option:selected').text(),
          start: $data,
          end: $data
      };
    }

    jQuery.ajax({
        type: "GET",
        data: {
          data: $data,
          hora: $hora,
          paciente: $paciente,
          medico: $medico,
          sala: $sala,
          nomepaciente: $nomepaciente,
          nomemedico: $nomemedico,
          atividade: $atividade,
          obs: $obs,
          status: $status,
          nomeclinica: $nomeclinica,
          idclinica: $idclinica
        },
        url: "/eventos/ajaxprod",

        success: function(s){

          var newEvent = {
               title: $('[name=paciente] option:selected').text(),
               start: $data
           };

          $('#calendar').fullCalendar('renderEvent', newEvent);
          jQuery('#exampleModal').modal('hide');

        }
      }) // end ajax


  }//end function ajaxsave

  $('[name=clinica]').change(function(){
      setsalasclinicas($(this).val());
  });

  function setsalasclinicas($idclinica, $salaset){

    $('[name=sala]').html('');

    if($idclinica == ''){
      $('[name=sala]').attr('disabled', true);
      $('[name=sala]').append("<option value=''>Selecione uma Clínica</option>");
    }else{

      $('[name=sala]').append("<option value=''>Selecione uma Sala</option>");
      $('[name=sala]').attr('disabled', false);

        jQuery.ajax({
          type: "GET",
          data: {
            idclinica: $idclinica,
          },
          url: "/eventos/xeventosclinicas",
          success: function(s){

              salas = JSON.parse(s);
              if(salas.length < 1){
                $('[name=sala]').html('');
                $('[name=sala]').append("<option value=''>Sem Salas</option>");
                $('[name=sala]').attr('disabled', true);
              }else{
                jQuery.each(salas, function(i, val){
                    $('[name=sala]').append("<option value='" + val.id + "'>" + val.titulo + "</option>");
                });
                $('[name=sala]').attr('disabled', false);

                $('[name=sala]').val($salaset);
              }
          }
        }) // end ajax

    }// end if/else
  }//end function

  function verificacamposincorretos($medico,$paciente,$clinica,$sala,$atividade){

    var strincorretos = '<br>';
    var flag = false;

    if($medico == ''){
      flag=true;
      strincorretos += ' - médico <br>';
    }

    if($paciente == ''){
      flag=true;
      strincorretos += ' - paciente <br>';
    }

    if($clinica == ''){
      flag=true;
      strincorretos += ' - clinica <br>';
    }

    if($sala == ''){
      flag=true;
      strincorretos += ' - sala <br>';
    }

    if($atividade == ''){
      flag=true;
      strincorretos += ' - atividade <br>';
    }

    if(flag){
      $('#alerta').css('display','block').animate({opacity: 1});
      $('#alerta').html('Verifique os seguintes campos: ' + strincorretos);
    }

    return flag;

  }// end function
