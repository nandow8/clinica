<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Welcome extends Model
{
    
    protected $fillable = [
        'nome',
        'telefone',
        'email',
        'cpf',
        'dataconsulta',
        'exame',
        'tipoconsulta',
        'status'
    ];
    protected $table = 'preagendamentos';
    protected $primaryKey = 'id';
}
