<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicoprescricoes extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medicoprescricoes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'medicofichaclinica_id',
        'prescricoes_tipo',
        'id_medicamento',
        'tipo_receita',
        'medicamento',
        'descricao',
        'posologia',
        'user_id',
        'tipomedicamento'
    ];

    public function paciente()
    {
        return $this->belongsTo('App\Models\Medicofichaclinica');
    }
    
}
