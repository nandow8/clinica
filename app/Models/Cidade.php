<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cidade extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cidades';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'estados_id'];

    public function bairros()
    {
        return $this->hasMany('App\Models\Bairro', 'cidades_id');
    }
    public function estados()
    {
        return $this->belongsTo('App\Models\Estado', 'estados_id');
    }
    
}
