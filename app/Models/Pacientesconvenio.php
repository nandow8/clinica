<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pacientesconvenio extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pacientesconvenios';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['convenio', 'plano', 'numerocarteira', 'datavalidade', 'cnpj', 'contato', 'telefone', 'telefonedois', 'email'];

    public function paciente()
    {
        return $this->hasOne('App\Models\Pacientes');
    }

    public static function getConvenio($idconvenio){
        return Pacientesconvenio::select('convenio')->where('id', '=', $idconvenio)->get();
            
        // return $convenio;
    }
    
}
