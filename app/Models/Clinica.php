<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Clinica extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'clinicas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['razao', 'fantasia', 'endereco', 'cep', 'uf', 'cidade', 'cnpj', 'conta', 'carteira', 'agencia', 'codigocliente', 'multa', 'juros', 'instrucao1', 'instrucao2', 'instrucao3'];

    
}
