<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banco extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bancos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['codigo', 'agencia', 'agenciadig', 'conta', 'contadig', 'cedente', 'cedentedig', 'nome', 'carteira', 'variacao', 'numero', 'convenio', 'instrucao1', 'instrucao2', 'instrucao3', 'status', 'geraboleto'];

    
}
