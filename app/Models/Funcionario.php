<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Funcionario extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'funcionarios';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'especialidade', 'areaatuacao', 'membro', 'crm', 'crmemissao', 'identidade', 'cpf', 'cep', 'rua', 'numero', 'bairro', 'cidade', 'estado', 'complemento', 'telefone', 'recado', 'celular', 'email', 'nascimento', 'nacionlalidade', 'estadocivil', 'sexo', 'filhos', 'totalfilhos', 'obs', 'status', 'image', 'agenda', 'diastrabalho', 'user_id'];


}
