<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agendamentos extends Model
{
    //
    protected $fillable = [
        'dataconsulta',
        'horaconsulta',
        'startconsulta',
        'endconsulta',
        'paciente',
        'medico',
        'sala',
        'idclinica',
        'nomeclinica',
        'nomepaciente',
        'nomemedico',
        'atividade',
        'obs',
        'status'
    ];
    protected $table = 'agendamentos';
    protected $primaryKey = 'id';
}
