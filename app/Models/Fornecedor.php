<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fornecedor extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fornecedors';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['razao', 'fantasia', 'cpfcnpj', 'cep', 'endereco', 'complemento', 'bairro', 'cidade', 'uf', 'telefone', 'celular', 'ie', 'im', 'email', 'contato', 'status'];

    
}
