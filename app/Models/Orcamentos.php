<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orcamentos extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'orcamentos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['paciente_id', 'vencimento', 'pagamento', 'valor', 'numero', 'numerodocumento', 'descricao', 'status', 'clinica_id', 'orcamento','remessa','retorno','tipopagto_id'];
    protected $hidden = ['clinica_id'];
    
}
