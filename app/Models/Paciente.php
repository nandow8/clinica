<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Estado;
use App\Models\Cidade;
use App\Models\Bairro;
use App\Models\Pacientesconvenio;
use App\Models\Plano;

class Paciente extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pacientes';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tipocobranca','nome', 'indicacao', 'sexo', 'rg', 'cpf', 'estadocivil', 'datanascimento', 'naturalidade', 'nacionalidade', 'telefone', 'celular', 'email', 'cep', 'estados_id', 'cidades_id', 'bairros_id', 'rua', 'numero', 'complemento','plano', 'numerocarteira', 'datavalidade', 'observacoes', 'tipopaciente', 'paciente_titular_id', 'image', 'pacientesconvenio_id', 'status', 'empresa_id'];

    public function index($request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pacientes = Paciente::where('nome', 'LIKE', "%$keyword%")
                ->orWhere('indicacao', 'LIKE', "%$keyword%")
                ->orWhere('sexo', 'LIKE', "%$keyword%")
                ->orWhere('rg', 'LIKE', "%$keyword%")
                ->orWhere('cpf', 'LIKE', "%$keyword%")
                ->orWhere('estadocivil', 'LIKE', "%$keyword%")
                ->orWhere('datanascimento', 'LIKE', "%$keyword%")
                ->orWhere('naturalidade', 'LIKE', "%$keyword%")
                ->orWhere('nacionalidade', 'LIKE', "%$keyword%")
                ->orWhere('telefone', 'LIKE', "%$keyword%")
                ->orWhere('celular', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('cep', 'LIKE', "%$keyword%")
                ->orWhere('estados_id', 'LIKE', "%$keyword%")
                ->orWhere('cidades_id', 'LIKE', "%$keyword%")
                ->orWhere('bairros_id', 'LIKE', "%$keyword%")
                ->orWhere('rua', 'LIKE', "%$keyword%")
                ->orWhere('numero', 'LIKE', "%$keyword%")
                ->orWhere('complemento', 'LIKE', "%$keyword%")
                ->orWhere('observacoes', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pacientes = Paciente::latest()->paginate($perPage);
        }


        return $pacientes;
    }

    public function listaEstados(){
        return Estado::orderBy('nome', 'asc')->select('id','nome', 'uf')->get();
    }

    public function listaCidades(){
        return Cidade::orderBy('nome', 'asc')->select('id','nome')->get();
    }

    public function listaBairros(){
        return Bairro::orderBy('nome', 'asc')->select('id','nome')->get();
    }

    public function listaPacientes(){
        return Paciente::orderBy('nome', 'asc')->select('id', 'nome')->get();
    }

    public function listaConvenios(){
        return Pacientesconvenio::select('id', 'convenio', 'plano', 'numerocarteira', 'datavalidade')->get();
    }

    public function listaPlanos(){
        return Plano::orderBy('nomeplano')->get();
    }

    public function listaTitulares(){
        return Paciente::where('tipopaciente', '=', '1')->orderBy('nome', 'asc')->select('id', 'nome')->get();
    }

}
