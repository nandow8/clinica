<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Cidade;

class Bairro extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bairros';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nome', 'cidades_id'];

    public function cidades()
    {
        return $this->belongsTo('App\Models\Cidade');
    }

    public function listAll($request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $bairros = Bairro::where('nome', 'LIKE', "%$keyword%")
                ->orWhere('cidades_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $bairros = Bairro::latest()->paginate($perPage);
        }

        return $bairros;
    }

    public function listaCidades(){
        return Cidade::distinct('nome')->orderBy('nome','asc')->get();
    }
    
}
