<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicofichaclinica extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medicofichaclinicas';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['paciente_id'];

    public function pacientes()
    {
        return $this->belongsTo('App\Models\Paciente');
    }
    public function anamnese()
    {
        return $this->hasMany('App\Models\Medicoanamnese');
    }
    public function evolucao()
    {
        return $this->hasMany('App\Models\Medicoevolucao');
    }
    public function diagnostico()
    {
        return $this->hasMany('App\Models\Medicodiagnostico');
    }
    public function prescricoes()
    {
        return $this->hasMany('App\Models\Medicoprescricoes');
    }
    public function exames()
    {
        return $this->hasMany('App\Models\Medicoexames');
    }
    public function atestadosecartas()
    {
        return $this->hasMany('App\Models\Medicoatestado');
    }
    public function anexos()
    {
        return $this->hasMany('App\Models\Medicoanexos');
    }
    

    public function selectdepacientes(){
        return Paciente::all();
    }
    
    public function selectdepacienteanamnese($id){
        return Medicoanamnese::where('medicofichaclinica_id', '=', $id)->get();
    }
    
    public function selectdepacienteevolucao($id){
        return Medicoevolucao::where('medicofichaclinica_id', '=', $id)->get();
    }
    
    public function selectdepacientediagnostico($id){
        return Medicodiagnostico::where('medicofichaclinica_id', '=', $id)->get();
    }
    
    public function selectdepacienteprescricoes($id){
        return Medicoprescricoes::where('medicofichaclinica_id', '=', $id)->get();
    }
    
    public function selectdepacienteexames($id){
        return Medicoexames::where('medicofichaclinica_id', '=', $id)->get();
    }
    
    public function selectdepacienteatestados($id){
        return Medicoatestado::where('medicofichaclinica_id', '=', $id)->get();
    }
    
    public function selectdepacienteanexos($id){
        return Medicoanexos::where('medicofichaclinica_id', '=', $id)->get();
    }
    
    public function selectdepacientehistorico($id){
        return Medicohistorico::where('paciente_id', '=', $id)->orderBy('created_at', 'DESC')->get();
    }
    
    public function selectdepacienteresultados($id){
        return Medicoresultado::where('medicofichaclinica_id', '=', $id)->get();
    }
   
    public function selectdeanamnese($id, $editORshow){
        return Medicoanamnese::join('medicofichaclinicas', 'medicoanamnese.medicofichaclinica_id', '=', 'medicofichaclinicas.id')
                            ->orderBy('medicoanamnese.id', $editORshow)
                            ->where('medicofichaclinicas.paciente_id', '=', $id)
                            ->limit(1)
                            ->get();
    }

    public function selecthorarioagenda($id){
        return Paciente::join('agendamentos', 'pacientes.id', '=', 'agendamentos.paciente')
                        ->where('agendamentos.paciente', '=' , $id)
                        ->get();
    }
    
}
