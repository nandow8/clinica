<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicohistorico extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medicohistoricos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['tempo_atendimento', 'paciente_id', 'medicoresultado_id', 'user_id', 'medicofichaclinica_id','medicoanamnese_id','medicoevolucao_id','medicoexames_id','medicoatestado_id','medicoanexos_id','medicoprescricoes_id','medicodiagnostico_id', ];

    
}
