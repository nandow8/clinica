<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicamento extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medicamentos';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['princicipoativo', 'laboratorio_id', 'codigoggrem', 'registro', 'ean1', 'ean2', 'ean3', 'produto', 'apresentacao', 'classeterapeutica', 'tipodeproduto', 'pfsemimpostos', 'pf0', 'pf12', 'pf17', 'pf17alc', 'pf175', 'pf175alc', 'pf18', 'pf18alc', 'pf20', 'pmc0', 'pmc12', 'pmc17', 'pmc17alc', 'pmc175', 'pmc175alc', 'pmc18', 'pmc18alc', 'pmc20', 'restricaohospitalar', 'cap', 'confaz87', 'analiserecursal', 'listadeconcessaodecreditotributario', 'comercializacao2017', 'tarja', 'tipomedicamento', 'status'];

    
}
