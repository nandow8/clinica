<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicoatestado extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'medicoatestado';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'medicofichaclinica_id',
        'titulo',
        'atestado',
        'user_id'
    ];

    public function paciente()
    {
        return $this->belongsTo('App\Models\Medicofichaclinica');
    }
    
}
