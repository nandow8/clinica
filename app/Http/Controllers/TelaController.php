<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Event;

class TelaController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
      $agendadia = DB::select('
        SELECT
          t.id,
        	t.paciente_id, p.nome AS paciente, p.image AS foto,
        	t.funcionario_id, f.nome AS profissional, f.areaatuacao,
        	t.sala_id, s.titulo AS sala, s.cor,
        	t.clinica_id, c.fantasia AS clinica, t.dataconsulta, t.status
        FROM
        	telachamados t
        	INNER JOIN pacientes p ON (t.paciente_id = p.id)
        	INNER JOIN funcionarios f ON (t.funcionario_id = f.id)
        	INNER JOIN clinicas c ON (t.clinica_id = c.id)
        	INNER JOIN salas s ON (s.id = t.clinica_id)
        WHERE
          t.created_at BETWEEN CAST(CONCAT(CAST(NOW() AS DATE)," 00:00:00") as CHAR) and CAST(CONCAT(CAST(NOW() AS DATE)," 23:59:00") as CHAR)
        ORDER BY t.id DESC
        LIMIT 6;
      ');


      return view('tela.index', compact('agendadia'));
    }
}
