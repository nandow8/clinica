<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Medicoprescricoes;
use Illuminate\Http\Request;

class MedicoprescricoesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicoprescricoes = Medicoprescricoes::latest()->paginate($perPage);
        } else {
            $medicoprescricoes = Medicoprescricoes::latest()->paginate($perPage);
        }

        return view('medicos.medicoprescricoes.index', compact('medicoprescricoes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicoprescricoes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicoprescricoes::create($requestData);

        return redirect('medicos/medicoprescricoes')->with('flash_message', 'Medicoprescricoes added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicoprescrico = Medicoprescricoes::findOrFail($id);

        return view('medicos.medicoprescricoes.show', compact('medicoprescrico'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicoprescrico = Medicoprescricoes::findOrFail($id);

        return view('medicos.medicoprescricoes.edit', compact('medicoprescrico'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicoprescrico = Medicoprescricoes::findOrFail($id);
        $medicoprescrico->update($requestData);

        return redirect('medicos/medicoprescricoes')->with('flash_message', 'Medicoprescricoes updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicoprescricoes::destroy($id);

        return redirect('medicos/medicoprescricoes')->with('flash_message', 'Medicoprescricoes deleted!');
    }
}
