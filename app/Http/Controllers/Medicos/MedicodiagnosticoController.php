<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Medicodiagnostico;
use Illuminate\Http\Request;

class MedicodiagnosticoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicodiagnostico = Medicodiagnostico::latest()->paginate($perPage);
        } else {
            $medicodiagnostico = Medicodiagnostico::latest()->paginate($perPage);
        }

        return view('medicos.medicodiagnostico.index', compact('medicodiagnostico'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicodiagnostico.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicodiagnostico::create($requestData);

        return redirect('medicos/medicodiagnostico')->with('flash_message', 'Medicodiagnostico added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicodiagnostico = Medicodiagnostico::findOrFail($id);

        return view('medicos.medicodiagnostico.show', compact('medicodiagnostico'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicodiagnostico = Medicodiagnostico::findOrFail($id);

        return view('medicos.medicodiagnostico.edit', compact('medicodiagnostico'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicodiagnostico = Medicodiagnostico::findOrFail($id);
        $medicodiagnostico->update($requestData);

        return redirect('medicos/medicodiagnostico')->with('flash_message', 'Medicodiagnostico updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicodiagnostico::destroy($id);

        return redirect('medicos/medicodiagnostico')->with('flash_message', 'Medicodiagnostico deleted!');
    }
}
