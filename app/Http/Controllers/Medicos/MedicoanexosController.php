<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Medicoanexos;
use Illuminate\Http\Request;

class MedicoanexosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicoanexos = Medicoanexos::latest()->paginate($perPage);
        } else {
            $medicoanexos = Medicoanexos::latest()->paginate($perPage);
        }

        return view('medicos.medicoanexos.index', compact('medicoanexos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicoanexos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicoanexos::create($requestData);

        return redirect('medicos/medicoanexos')->with('flash_message', 'Medicoanexos added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicoanexo = Medicoanexos::findOrFail($id);

        return view('medicos.medicoanexos.show', compact('medicoanexo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicoanexo = Medicoanexos::findOrFail($id);

        return view('medicos.medicoanexos.edit', compact('medicoanexo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicoanexo = Medicoanexos::findOrFail($id);
        $medicoanexo->update($requestData);

        return redirect('medicos/medicoanexos')->with('flash_message', 'Medicoanexos updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicoanexos::destroy($id);

        return redirect('medicos/medicoanexos')->with('flash_message', 'Medicoanexos deleted!');
    }
}
