<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Medicofichaclinica;
use Illuminate\Http\Request;
use Session;
use App\Models\Paciente;
use App\Models\Medicoanamnese;
use App\Models\Medicoevolucao;
use App\Models\Medicodiagnostico;
use App\Models\Medicoprescricoes;
use App\Models\Medicoexames;
use App\Models\Medicoatestado;
use App\Models\Medicoanexos;
use Image;
use Auth;
use App\Models\Cid;
use Illuminate\Support\Facades\Input;
use App\Models\TUSS;
use App\Models\Examesrf;
use App\Models\Medicomodeloatestado;
use App\Models\Medicohistorico;
use App\Models\Medicoresultado;
use App\Agendamentos;

class MedicofichaclinicaController extends Controller
{
    private $medicofichaclinicas;
    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct(Medicofichaclinica $medicofichaclinica)
    {
        $this->medicofichaclinicas = $medicofichaclinica;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $collection = collect(0);

        if (!empty($keyword)) {
            $medicoByName = Paciente::where('nome', 'LIKE', "%$keyword%")->get();
            for ($i=0; $i < COUNT($medicoByName); $i++) {
                $collection->push($medicoByName[$i]['id']);
            }
             
            $medicofichaclinica = Medicofichaclinica::whereIn('paciente_id', $collection->all())
            ->latest()->paginate($perPage);
            
        } else {
            $medicofichaclinica = \DB::select( \DB::raw('select medicofichaclinicas.id, medicofichaclinicas.paciente_id, P.dataconsulta from medicofichaclinicas inner join (select paciente, MAX(dataconsulta) as dataconsulta from agendamentos group by paciente) P on medicofichaclinicas.paciente_id = P.paciente WHERE medicofichaclinicas.deleted_at is NULL order by P.dataconsulta')); 
        }

        return view('medicos.medicofichaclinica.index', compact('medicofichaclinica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $listadepacientes = $this->medicofichaclinicas->selectdepacientes();

        return view('medicos.medicofichaclinica.create', compact('listadepacientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $medicofichaclinica = Medicofichaclinica::create($requestData);
        $requestData['user_id'] = Auth::user()->id;
        $requestData['medicofichaclinica_id'] = $medicofichaclinica['id'];

        Medicoanamnese::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('medicos/medicofichaclinica/' . $medicofichaclinica['id'] . '/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicofichaclinica = Medicofichaclinica::findOrFail($id);
        $listadepacientes = $this->medicofichaclinicas->selectdepacientes();
       // $pacienteanamnese = $this->medicofichaclinicas->selectdepacienteanamnese($medicofichaclinica['id']);
        $pacienteevolucao = $this->medicofichaclinicas->selectdepacienteevolucao($medicofichaclinica['id']);
        $pacientediagnostico = $this->medicofichaclinicas->selectdepacientediagnostico($medicofichaclinica['id']);
        $pacienteprescricoes = $this->medicofichaclinicas->selectdepacienteprescricoes($medicofichaclinica['id']);
        $pacienteexames = $this->medicofichaclinicas->selectdepacienteexames($medicofichaclinica['id']);
        $pacienteatestados = $this->medicofichaclinicas->selectdepacienteatestados($medicofichaclinica['id']);
        $pacienteanexos = $this->medicofichaclinicas->selectdepacienteanexos($medicofichaclinica['id']);
        $pacientehistorico = $this->medicofichaclinicas->selectdepacientehistorico($medicofichaclinica['paciente_id']);
        $pacienteresultados = $this->medicofichaclinicas->selectdepacienteresultados($medicofichaclinica['id']);
        $pacienteanamnese = $this->medicofichaclinicas->selectdeanamnese($medicofichaclinica['paciente_id'], 'DESC');
        $pacientehorariosagenda = $this->medicofichaclinicas->selecthorarioagenda($medicofichaclinica['paciente_id'], 'ASC');

        $medico_modeloatestado = Medicomodeloatestado::all();

        $dados_paciente = Paciente::find($medicofichaclinica['paciente_id']);

        return view('medicos.medicofichaclinica.show', compact('pacientehorariosagenda', 'pacienteanamnese', 'pacienteresultados', 'pacientehistorico', 'medico_modeloatestado', 'dados_paciente', 'medicofichaclinica', 'listadepacientes','pacienteanamnese', 'pacienteevolucao', 'pacientediagnostico', 'pacienteprescricoes','pacienteexames','pacienteatestados','pacienteanexos'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id, $idagenda = 11)
    {
        date_default_timezone_set('America/Sao_Paulo');
        $medicofichaclinica = Medicofichaclinica::findOrFail($id);
        $listadepacientes = $this->medicofichaclinicas->selectdepacientes();
        // $pacienteanamnese = $this->medicofichaclinicas->selectdepacienteanamnese($medicofichaclinica['id']);
        $pacienteevolucao = $this->medicofichaclinicas->selectdepacienteevolucao($medicofichaclinica['id']);
        $pacientediagnostico = $this->medicofichaclinicas->selectdepacientediagnostico($medicofichaclinica['id']);
        $pacienteprescricoes = $this->medicofichaclinicas->selectdepacienteprescricoes($medicofichaclinica['id']);
        $pacienteexames = $this->medicofichaclinicas->selectdepacienteexames($medicofichaclinica['id']);
        $pacienteatestados = $this->medicofichaclinicas->selectdepacienteatestados($medicofichaclinica['id']);
        $pacienteanexos = $this->medicofichaclinicas->selectdepacienteanexos($medicofichaclinica['id']);
        $pacientehistorico = $this->medicofichaclinicas->selectdepacientehistorico($medicofichaclinica['paciente_id']);
        $pacienteresultados = $this->medicofichaclinicas->selectdepacienteresultados($medicofichaclinica['id']);
        $pacienteanamnese = $this->medicofichaclinicas->selectdeanamnese($medicofichaclinica['paciente_id'], 'DESC');
        $pacientehorariosagenda = $this->medicofichaclinicas->selecthorarioagenda($medicofichaclinica['paciente_id'], 'ASC');
        $agendamento = Agendamentos::findOrFail($idagenda);

        $medico_modeloatestado = Medicomodeloatestado::all();
        $dados_paciente = Paciente::find($medicofichaclinica['paciente_id']);
        return view('medicos.medicofichaclinica.edit', compact('pacientehorariosagenda', 'pacienteanamnese', 'pacienteresultados', 'pacientehistorico', 'medico_modeloatestado', 'dados_paciente', 'medicofichaclinica', 'listadepacientes','pacienteanamnese', 'pacienteevolucao', 'pacientediagnostico', 'pacienteprescricoes','pacienteexames','pacienteatestados','pacienteanexos', 'agendamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $nomesImages = array();
        if ($request->hasFile('file')) {
            foreach ($request->file as $key => $file) {

                $image = $request->file[$key];
                $filename = $file->getClientOriginalName();
                $filesize = $file->getClientSize();
                $file = public_path('/uploads/fichaclinicaanexos/', $filename);
                $image->move($file, $filename);

                $nomesImages[$key] = $filename;
            }
        }

        $requestData = $request->all();
        $requestData['user_id'] = Auth::user()->id;
        if($requestData['idagendamento']){
            $agendamento = Agendamentos::findOrFail($requestData['idagendamento']);

            $agendamento['status'] = "finalizado";

            $agendamento->update($requestData);
            //$agendamento = Agendamentos::create($requestData);

        }
        //dd($requestData['idagendamento']);

        $medicofichaclinica = Medicofichaclinica::findOrFail($id);
        $medicofichaclinica->update($requestData);

        $medicoanamnese = Medicoanamnese::create($requestData);

        if($requestData['evolucao']){
            $medicoevolucao = Medicoevolucao::create($requestData);
        }

        if($requestData['resultados']){
            $medicoresultado = Medicoresultado::create($requestData);
        }

        if($requestData['codigo']){
            $medicoexames = Medicoexames::create([
                'user_id' => Auth::user()->id,
                'medicofichaclinica_id' => $medicofichaclinica['id'],
                'introducao_pedido' => $requestData['introducao_pedido'],
                'info_adicional' => $requestData['info_adicional'],
                'exame' => implode("---", $requestData['exame']),
                'codigo' => implode("---", $requestData['codigo']),
                'observacoes' => implode("---", $requestData['observacoes']),
            ]);
        }

        if($requestData['atestado']){
            $medicoatestado = Medicoatestado::create($requestData);
        }

        foreach ($nomesImages as $key => $images) {
            $medicoanexos = Medicoanexos::whereRaw(
                    "nomeanexo = '$images'"
                )
                ->updateOrCreate([
                    'medicofichaclinica_id' => $medicofichaclinica['id'],
                    'nomeanexo' => $images
                ]);
        }

        if($requestData['medicamento']){
            $medicoprescricoes = Medicoprescricoes::create([
                'user_id' => Auth::user()->id,
                'medicofichaclinica_id' => $medicofichaclinica['id'],
                'id_medicamento' => implode("---", $requestData['id_medicamento']),
                'prescricoes_tipo' => implode("---", $requestData['prescricoes_tipo']),
                'tipo_receita' => implode("---", $requestData['tipo_receita']),
                'medicamento' => implode("---", $requestData['medicamento']),
                'tipomedicamento' => implode("---", $requestData['tipomedicamento']),
                'descricao' => implode("---", $requestData['descricao']),
                'posologia' => implode("---", $requestData['posologia']),
            ]);
        }

        if(isset($requestData['cid'][0])){
            $medicodiagnostico = Medicodiagnostico::create([
                'user_id' => Auth::user()->id,
                'medicofichaclinica_id' => $medicofichaclinica['id'],
                'cid' => implode("---", $requestData['cid']),
                'informacoesadicionais' => $requestData['informacoesadicionais']
            ]);
        }

        // Medicohistorico

        $medicohistorico = Medicohistorico::create([
            'user_id' => Auth::user()->id,
            'paciente_id' => (isset($medicofichaclinica['paciente_id'])) ? $medicofichaclinica['paciente_id'] : NULL,
            'medicofichaclinica_id' => (isset($medicofichaclinica['id'])) ? $medicofichaclinica['id'] : NULL,
            'medicoanamnese_id' => (isset($medicoanamnese['id'])) ? $medicoanamnese['id'] : NULL,
            'medicoevolucao_id' => (isset($medicoevolucao['id'])) ? $medicoevolucao['id'] : NULL,
            'medicoexames_id' => (isset($medicoexames['id'])) ? $medicoexames['id'] : NULL,
            'medicoatestado_id' => (isset($medicoatestado['id'])) ? $medicoatestado['id'] : NULL,
            'medicoanexos_id' => (isset($medicoanexos['id'])) ? $medicoanexos['id'] : NULL,
            'medicoprescricoes_id' => (isset($medicoprescricoes['id'])) ? $medicoprescricoes['id'] : NULL,
            'medicodiagnostico_id' => (isset($medicodiagnostico['id'])) ? $medicodiagnostico['id'] : NULL,
            'medicoresultado_id' => (isset($medicoresultado['id'])) ? $medicoresultado['id'] : NULL,
            'tempo_atendimento' => $requestData['tempo_atendimento']
        ]);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('medicos/medicofichaclinica');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicofichaclinica::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('medicos/medicofichaclinica');
    }

    public function ajaxexcluirAnexo($anexo_id){

        $anexo = Medicoanexos::find($anexo_id);
        $anexo->delete();

        die();

    }

    public function ajaxexibirdescricaocid($descricao_cid){
        $cids = Cid::select('codigo', 'descricao')
                    ->where('codigo', 'LIKE', "%$descricao_cid%")
                    ->orWhere('descricao', 'LIKE', "%$descricao_cid%")->limit(10)->get();

        foreach ($cids as $key => $cid) {
            $cid_descricao[] = $cid->codigo . ' ' .$cid->descricao;
        }

        echo json_encode($cid_descricao);

    }

    public function abrirfichaclinica($id_paciente){
        $medicofichaclinica = Medicofichaclinica::where('paciente_id', '=', $id_paciente)->first();
        if($medicofichaclinica){
            //redireciona para a ficha clínica
            return redirect("/medicos/medicofichaclinica/$id_paciente/edit");
        }else{
            //senão cria e redireciona
            $requestData['paciente_id'] = $id_paciente;
            Medicofichaclinica::create($requestData);
            return redirect("/medicos/medicofichaclinica/$id_paciente/edit");
        }
    }

    public function ajaxexibirmedicamento($medicamento, $prescricoes_tipo){
        $medicamentos = \App\Models\Medicamento::select('id', 'produto', 'apresentacao','pf18')
                    ->where('tipomedicamento', '=', "$prescricoes_tipo")
                    ->where(function($query) use ($medicamento){
                        $query->where('produto', 'LIKE', "%$medicamento%")
                            ->orWhere('apresentacao', 'LIKE', "%$medicamento%");
                    })
                    ->limit(10)->get();

        foreach ($medicamentos as $key => $medicamento) {
            $medicamento_apresentacao[] = $medicamento->id . '---' .$medicamento->produto . '---' . $medicamento->apresentacao . '---' . $medicamento->pf18;
        }

        echo json_encode($medicamento_apresentacao);

    }

    public function ajaxexibirdescricaocmedicamento($descricao_medicamento){
        $descricao_medicamentos = \App\Models\Medicamento::select('apresentacao')
                    ->where('produto', '=', $descricao_medicamento)->get();

        echo json_encode($descricao_medicamentos);

    }

    public function ajaxultimareceita($medicofichaclinica_id, $user_id){
        $ultimareceita = \App\Models\Medicoprescricoes::
                    where(function($query) use($medicofichaclinica_id, $user_id) {
                        $query->where('medicofichaclinica_id', '=', $medicofichaclinica_id );
                            // ->where('user_id', '=', $user_id);  SE PRECISAR EXIBIR ULTIMA RECEITA POR MEDICO, DESCOMENTA AQUI
                    })->orderBy('created_at', 'DESC')->first();

        echo json_encode($ultimareceita);

    }

    public function ajaxexibirexametuss($codigooudescricao){
        $tuss = TUSS::select('codigo', 'descricao')
                    ->where('codigo', 'LIKE', "%$codigooudescricao%")
                    ->orWhere('descricao', 'LIKE', "%$codigooudescricao%")->limit(10)->get();

        foreach ($tuss as $key => $tus) {
            $tus_descricao[] = $tus->codigo . '---' .$tus->descricao;
        }

        echo json_encode($tus_descricao);

    }

    public function ajaxexibirexamerf($mneumonicocodigooudescricao){
        $examerf = Examesrf::select('mneumonico','codigo', 'nome')
                    ->where('mneumonico', 'LIKE', "%$mneumonicocodigooudescricao%")
                    ->orwhere('codigo', 'LIKE', "%$mneumonicocodigooudescricao%")
                    ->orWhere('nome', 'LIKE', "%$mneumonicocodigooudescricao%")->limit(10)->get();

        foreach ($examerf as $key => $rf) {
            $rf_exame[] = $rf->mneumonico . ' -- ' .$rf->codigo . ' -- ' .$rf->nome;
        }

        echo json_encode($rf_exame);

    }

    public function ajaxultimoexame($medicofichaclinica_id, $user_id){
        $ultimareceita = \App\Models\Medicoexames::
                    where(function($query) use($medicofichaclinica_id, $user_id) {
                        $query->where('medicofichaclinica_id', '=', $medicofichaclinica_id - 1 );
                            // ->where('user_id', '=', $user_id); SE PRECISAR EXIBIR ULTIMA RECEITA POR MEDICO, DESCOMENTA AQUI
                    })->orderBy('created_at', 'DESC')->first();

        echo json_encode($ultimareceita);

    }

    public function ajaxhistoricoprescricoespdf($id_prescricao){
        $prescricoes = \App\Models\Medicoprescricoes::
                    where('id', '=', $id_prescricao)->get();

        $returnHTMLprescricoes = view('medicos.medicofichaclinica.pdf.prescricao',['prescricoes'=> $prescricoes])->render();
        return response()->json( array('success' => true, 'html'=>$returnHTMLprescricoes) );
    }

    public function ajaxhistoricoexamespdf($id_exame){
        $exames = \App\Models\Medicoexames::
                    where('id', '=', $id_exame)->get();

        $returnHTMLexames = view('medicos.medicofichaclinica.pdf.exame',['exames'=> $exames])->render();
        return response()->json( array('success' => true, 'html'=>$returnHTMLexames) );
    }

    public function ajaxhistoricoatestadospdf($id_atestado){
        $atestados = \App\Models\Medicoatestado::
                    where('id', '=', $id_atestado)->get();

        $returnHTMLatestados = view('medicos.medicofichaclinica.pdf.atestado',['atestados'=> $atestados])->render();
        return response()->json( array('success' => true, 'html'=>$returnHTMLatestados) );
    }

    public function ajaxhistoricoatestadosretificacao($historico_id, $retificacao){
        $atestados = \App\Models\Medicohistorico::
                    where('id', '=', $historico_id)
                    ->update(['retificacao' => $retificacao]);
    }

    public function buscaanamnese($id){
        $buscaAnamnese = Medicoanamnese::where( 
                                'medicoanamnese.id', '=', $id
                            )
                            ->get();
                
        $returnHTMLanamnese = view('medicos.medicofichaclinica.pdf.anamnese',['buscaAnamnese'=> $buscaAnamnese])->render();
        return response()->json( array('success' => true, 'html'=>$returnHTMLanamnese) );
    }
}
