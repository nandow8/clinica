<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Medicoatestado;
use Illuminate\Http\Request;

class MedicoatestadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicoatestado = Medicoatestado::latest()->paginate($perPage);
        } else {
            $medicoatestado = Medicoatestado::latest()->paginate($perPage);
        }

        return view('medicos.medicoatestado.index', compact('medicoatestado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicoatestado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicoatestado::create($requestData);

        return redirect('medicos/medicoatestado')->with('flash_message', 'Medicoatestado added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicoatestado = Medicoatestado::findOrFail($id);

        return view('medicos.medicoatestado.show', compact('medicoatestado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicoatestado = Medicoatestado::findOrFail($id);

        return view('medicos.medicoatestado.edit', compact('medicoatestado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicoatestado = Medicoatestado::findOrFail($id);
        $medicoatestado->update($requestData);

        return redirect('medicos/medicoatestado')->with('flash_message', 'Medicoatestado updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicoatestado::destroy($id);

        return redirect('medicos/medicoatestado')->with('flash_message', 'Medicoatestado deleted!');
    }
}
