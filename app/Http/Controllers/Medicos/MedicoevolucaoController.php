<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Medicoevolucao;
use Illuminate\Http\Request;

class MedicoevolucaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicoevolucao = Medicoevolucao::latest()->paginate($perPage);
        } else {
            $medicoevolucao = Medicoevolucao::latest()->paginate($perPage);
        }

        return view('medicos.medicoevolucao.index', compact('medicoevolucao'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicoevolucao.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicoevolucao::create($requestData);

        return redirect('medicos/medicoevolucao')->with('flash_message', 'Medicoevolucao added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicoevolucao = Medicoevolucao::findOrFail($id);

        return view('medicos.medicoevolucao.show', compact('medicoevolucao'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicoevolucao = Medicoevolucao::findOrFail($id);

        return view('medicos.medicoevolucao.edit', compact('medicoevolucao'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicoevolucao = Medicoevolucao::findOrFail($id);
        $medicoevolucao->update($requestData);

        return redirect('medicos/medicoevolucao')->with('flash_message', 'Medicoevolucao updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicoevolucao::destroy($id);

        return redirect('medicos/medicoevolucao')->with('flash_message', 'Medicoevolucao deleted!');
    }
}
