<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Medicoresultado;
use Illuminate\Http\Request;

class MedicoresultadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicoresultados = Medicoresultado::latest()->paginate($perPage);
        } else {
            $medicoresultados = Medicoresultado::latest()->paginate($perPage);
        }

        return view('medicos.medicoresultados.index', compact('medicoresultados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicoresultados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicoresultado::create($requestData);

        return redirect('medicos/medicoresultados')->with('flash_message', 'Medicoresultado added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicoresultado = Medicoresultado::findOrFail($id);

        return view('medicos.medicoresultados.show', compact('medicoresultado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicoresultado = Medicoresultado::findOrFail($id);

        return view('medicos.medicoresultados.edit', compact('medicoresultado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicoresultado = Medicoresultado::findOrFail($id);
        $medicoresultado->update($requestData);

        return redirect('medicos/medicoresultados')->with('flash_message', 'Medicoresultado updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicoresultado::destroy($id);

        return redirect('medicos/medicoresultados')->with('flash_message', 'Medicoresultado deleted!');
    }
}
