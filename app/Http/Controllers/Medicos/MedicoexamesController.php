<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Medicoexames;
use Illuminate\Http\Request;

class MedicoexamesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicoexames = Medicoexames::latest()->paginate($perPage);
        } else {
            $medicoexames = Medicoexames::latest()->paginate($perPage);
        }

        return view('medicos.medicoexames.index', compact('medicoexames'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicoexames.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicoexames::create($requestData);

        return redirect('medicos/medicoexames')->with('flash_message', 'Medicoexames added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicoexame = Medicoexames::findOrFail($id);

        return view('medicos.medicoexames.show', compact('medicoexame'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicoexame = Medicoexames::findOrFail($id);

        return view('medicos.medicoexames.edit', compact('medicoexame'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicoexame = Medicoexames::findOrFail($id);
        $medicoexame->update($requestData);

        return redirect('medicos/medicoexames')->with('flash_message', 'Medicoexames updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicoexames::destroy($id);

        return redirect('medicos/medicoexames')->with('flash_message', 'Medicoexames deleted!');
    }
}
