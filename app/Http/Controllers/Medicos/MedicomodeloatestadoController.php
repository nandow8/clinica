<?php

namespace App\Http\Controllers\Medicos;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Medicomodeloatestado;
use Illuminate\Http\Request;

class MedicomodeloatestadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicomodeloatestado = Medicomodeloatestado::where('title', 'LIKE', "%$keyword%")
                ->orWhere('texto', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $medicomodeloatestado = Medicomodeloatestado::latest()->paginate($perPage);
        }

        return view('medicos.medicomodeloatestado.index', compact('medicomodeloatestado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('medicos.medicomodeloatestado.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Medicomodeloatestado::create($requestData);

        return redirect('medicos/medicomodeloatestado')->with('flash_message', 'Medicomodeloatestado added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicomodeloatestado = Medicomodeloatestado::findOrFail($id);

        return view('medicos.medicomodeloatestado.show', compact('medicomodeloatestado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicomodeloatestado = Medicomodeloatestado::findOrFail($id);

        return view('medicos.medicomodeloatestado.edit', compact('medicomodeloatestado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $medicomodeloatestado = Medicomodeloatestado::findOrFail($id);
        $medicomodeloatestado->update($requestData);

        return redirect('medicos/medicomodeloatestado')->with('flash_message', 'Medicomodeloatestado updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicomodeloatestado::destroy($id);

        return redirect('medicos/medicomodeloatestado')->with('flash_message', 'Medicomodeloatestado deleted!');
    }
}
