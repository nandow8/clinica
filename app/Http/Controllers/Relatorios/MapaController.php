<?php

namespace App\Http\Controllers\Relatorios;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Relatorios;
use App\Models\Paciente;
use Illuminate\Http\Request;

class MapaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        // $keyword = $request->get('search');
        // $perPage = 25;

        // if (!empty($keyword)) {
        //     $mapa = Relatorios::latest()->paginate($perPage);
        // } else {
        //     $mapa = Relatorios::latest()->paginate($perPage);
        // }
            
        return view('relatorios.mapa.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('relatorios.mapa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Relatorios::create($requestData);

        return redirect('relatorios/mapa')->with('flash_message', 'Relatorios added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $mapa = Relatorios::findOrFail($id);

        return view('relatorios.mapa.show', compact('mapa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $mapa = Relatorios::findOrFail($id);

        return view('relatorios.mapa.edit', compact('mapa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $mapa = Relatorios::findOrFail($id);
        $mapa->update($requestData);

        return redirect('relatorios/mapa')->with('flash_message', 'Relatorios updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Relatorios::destroy($id);

        return redirect('relatorios/mapa')->with('flash_message', 'Relatorios deleted!');
    }

    public function mapa(){        
        return view('relatorios.mapa.localizacao');
    }

    public function findPacienteById(Request $request){
        $id = $request->get("id");

        //busca todos os pacientes
        $pacientes = DB::table('pacientes')->where('id', $id)->select()->get();
        $dados = array();
        foreach($pacientes as $key => $paciente){           
            
            $count = DB::table('pacientes')->select(DB::raw("COUNT(*) as total"))->where('paciente_titular_id', '=', $paciente->id)->get();
          
            $titular = DB::table('pacientes')->select("nome")->where('id', '=', $paciente->paciente_titular_id)->get();
            
            $paciente->total = $count[0]->total;
            $paciente->titular = (isset($titular[0])) ? $titular[0]->nome : '';
            array_push($dados, $paciente);
           
        }
        echo json_encode($dados);
    }

    public function findPacientes(){
        //busca todos os pacientes
        $pacientes = DB::table('pacientes')->where('status', 'Ativo')->where('tipopaciente','=','1')->select()->get();
        $dados = array();
        foreach($pacientes as $key => $paciente){           
            
            $count = DB::table('pacientes')->select(DB::raw("COUNT(*) as total"))->where('paciente_titular_id', '=', $paciente->id)->get();
          
            $titular = DB::table('pacientes')->select("nome")->where('id', '=', $paciente->paciente_titular_id)->get();
            
            $paciente->total = $count[0]->total;
            $paciente->titular = (isset($titular[0])) ? $titular[0]->nome : '';
            array_push($dados, $paciente);
           
        }
        echo json_encode($dados);
    }

    public function findDependente(Request $request){
        $id = $request->get('id');
        
        $dependentes = DB::table('pacientes')->select(DB::raw("COUNT(*) as total"))->where('paciente_titular_id', '=', $id)->get();
        // dd($dependentes$dependentes[0]->total);
        echo json_encode($dependentes);
    }
}
