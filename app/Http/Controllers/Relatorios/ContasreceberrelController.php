<?php

namespace App\Http\Controllers\Relatorios;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use PHPJasper\PHPJasper;
use App\Relatorios;
use App\Models\Paciente;
use App\Models\TiposPagamento;

class ContasreceberrelController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    /*
      public function index(Request $request) {
      $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();

      $tiporel = isset($request->schtiporel) ? $request->schtiporel : 'A';
      $tipodata = isset($request->schpordata) ? $request->schpordata : 'vecto';
      $dataini = isset($request->schdataini) ? $request->schdataini : '2000-01-01';
      $datafim = isset($request->schdatafim) ? $request->schdatafim : '3000-01-01';
      $origem = isset($request->schorigem) ? $request->schorigem : 'TODOS';
      $status = isset($request->schstatus) ? $request->schstatus : 'TODOS';
      $tipopagto = isset($request->schtipopagto) ? implode(',', $request->schtipopagto) : '';

      //dd($tiporel, $tipodata, $dataini, $datafim, $origem, $status, $tipopagto);


      $relcontasreceber = DB::select(DB::raw('CALL fn_relcontasreceber(?,?,?,?,?,?,?)'),
      [
      $tiporel,
      $tipodata,
      $dataini,
      $datafim,
      $origem,
      $status,
      $tipopagto
      ]);

      return view('relatorios.contasreceberrel.index', compact('tipospagamentos', 'relcontasreceber'));
      }

      /**
     * Reporna um array com os parametros de conexão
     * @return Array
     */
    public function getDatabaseConfig() {
        return [
            'driver' => env('DB_CONNECTION'),
            'host' => env('DB_HOST'),
            'port' => env('DB_PORT'),
            'username' => env('DB_USERNAME'),
            'password' => env('DB_PASSWORD'),
            'database' => env('DB_DATABASE'),
            'jdbc_dir' => base_path() . env('JDBC_DIR', '/vendor/lavela/phpjasper/src/JasperStarter/jdbc'),
        ];
    }

    public function index() {
        // coloca na variavel o caminho do novo relatório que será gerado
        $output = public_path() . '/reports/' . time() . '_receber';
        // instancia um novo objeto JasperPHP

        $report = new PHPJasper;
        // chama o método que irá gerar o relatório
        // passamos por parametro:
        // o arquivo do relatório com seu caminho completo
        // o nome do arquivo que será gerado
        // o tipo de saída
        // parametros ( nesse caso não tem nenhum)         
        $report->process(
                public_path() . '/reports/relcontasreceber.jrxml',
                $output,
                ['pdf'],
                [],
                $this->getDatabaseConfig()
        )->execute();
        
        $file = $output . '.pdf';
        $path = $file;

        // caso o arquivo não tenha sido gerado retorno um erro 404
        if (!file_exists($file)) {
            abort(404);
        }
        //caso tenha sido gerado pego o conteudo
        $file = file_get_contents($file);
        //deleto o arquivo gerado, pois iremos mandar o conteudo para o navegador
        unlink($path);
        // retornamos o conteudo para o navegador que íra abrir o PDF
        return response($file, 200)
                        ->header('Content-Type', 'application/pdf')
                        ->header('Content-Disposition', 'inline; filename="receber.pdf"');
    }

}
