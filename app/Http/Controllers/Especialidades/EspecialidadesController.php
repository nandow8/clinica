<?php

namespace App\Http\Controllers\Especialidades;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Especialidade;
use Illuminate\Http\Request;
use Session;

class EspecialidadesController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $especialidades = Especialidade::where('titulo', 'LIKE', "%$keyword%")
                ->orWhere('descricao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $especialidades = Especialidade::latest()->paginate($perPage);
        }

        return view('especialidades.index', compact('especialidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('especialidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'titulo' => 'required',
			'descricao' => 'required'
		]);
        $requestData = $request->all();
        
        Especialidade::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('especialidades/especialidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $especialidade = Especialidade::findOrFail($id);

        return view('especialidades.show', compact('especialidade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $especialidade = Especialidade::findOrFail($id);

        return view('especialidades.edit', compact('especialidade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'titulo' => 'required',
			'descricao' => 'required'
		]);
        $requestData = $request->all();
        
        $especialidade = Especialidade::findOrFail($id);
        $especialidade->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('especialidades/especialidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Especialidade::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('especialidades/especialidades');
    }
}
