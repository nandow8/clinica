<?php

namespace App\Http\Controllers\Funcionarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\DB;
use App\Models\Funcionario;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;

use Session;
use function GuzzleHttp\json_encode;
use Illuminate\Support\Facades\Hash;

class FuncionariosController extends Controller
{
    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $funcionarios = Funcionario::where('nome', 'LIKE', "%$keyword%")
                ->orWhere('especialidade', 'LIKE', "%$keyword%")
                ->orWhere('areaatuacao', 'LIKE', "%$keyword%")
                ->orWhere('membro', 'LIKE', "%$keyword%")
                ->orWhere('crm', 'LIKE', "%$keyword%")
                ->orWhere('crmemissao', 'LIKE', "%$keyword%")
                ->orWhere('identidade', 'LIKE', "%$keyword%")
                ->orWhere('cpf', 'LIKE', "%$keyword%")
                ->orWhere('cep', 'LIKE', "%$keyword%")
                ->orWhere('rua', 'LIKE', "%$keyword%")
                ->orWhere('bairro', 'LIKE', "%$keyword%")
                ->orWhere('cidade', 'LIKE', "%$keyword%")
                ->orWhere('estado', 'LIKE', "%$keyword%")
                ->orWhere('complemento', 'LIKE', "%$keyword%")
                ->orWhere('telefone', 'LIKE', "%$keyword%")
                ->orWhere('recado', 'LIKE', "%$keyword%")
                ->orWhere('celular', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('nascimento', 'LIKE', "%$keyword%")
                ->orWhere('nacionlalidade', 'LIKE', "%$keyword%")
                ->orWhere('estadocivil', 'LIKE', "%$keyword%")
                ->orWhere('sexo', 'LIKE', "%$keyword%")
                ->orWhere('filhos', 'LIKE', "%$keyword%")
                ->orWhere('totalfilhos', 'LIKE', "%$keyword%")
                ->orWhere('obs', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->join('contacts', 'users.id', '=', 'contacts.user_id')
                ->latest()->paginate($perPage);
        } else {
            $funcionarios = Funcionario::latest()->paginate($perPage);
        }

        return view('funcionarios.index', compact('funcionarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
      $usuarios = User::where('invisible', '=', "N")
                        ->WhereNotIn('id', function($query)
                            {
                              $query->select(DB::raw('user_id'))
                                    ->from('funcionarios')
                                    ->join('users', 'funcionarios.user_id', '=', 'users.id');
                            })->get();

        return view('funcionarios.create', compact('usuarios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $img = $_POST['imagecam'];
        if($img){
            $folderPath = "'/uploads/fotosfuncionarios/'";

            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("imagecam/", $image_parts[0]);
            $image_type = $image_type_aux[0];

            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';

            $foto = Image::make($image_base64)->resize(600, 450)->save( public_path('/uploads/fotosfuncionarios/' . $fileName));
        }

        $requestData = $request->all();

        if($img) $requestData['image'] = $foto->basename;

        if(isset($requestData['especialidade'])){
            $requestData['especialidade'] = implode(',',$requestData['especialidade']);
        }else{
            $requestData['especialidade'] = '';
        }

        $requestData['diastrabalho'] = isset($requestData['diastrabalho']) ? implode(',', $requestData['diastrabalho']) : '';

        $funcionario = Funcionario::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('funcionarios/funcionarios');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $funcionario = Funcionario::findOrFail($id);
        $iduser = $funcionario->user_id;
        $usuarios = User::where('invisible', '=', "N")
                          ->WhereNotIn('id', function($query)
                              {
                                $query->select(DB::raw('user_id'))
                                      ->from('funcionarios')
                                      ->join('users', 'funcionarios.user_id', '=', 'users.id')
                                      ->where('users.id', '<>', 'iduser');
                              })->get();

        return view('funcionarios.show', compact('funcionario', 'usuarios'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $funcionario = Funcionario::findOrFail($id);
        $iduser = $funcionario->user_id;

        // O script comentado não traz o usuário corresponde ao funcionário
        // É preciso trazer todos os usuários aqui
        // Descomentar caso preciso

        // $usuarios = User::where('invisible', '=', "N")
        //                   ->WhereNotIn('id', function($query)
        //                       {
        //                         $query->select(DB::raw('user_id'))
        //                               ->from('funcionarios')
        //                               ->join('users', 'funcionarios.user_id', '=', 'users.id')
        //                               ->where('users.id', '<>', 'iduser');
        //                       })->get();
        $usuarios = User::where('invisible', '=', 'N')->get();
                    
        return view('funcionarios.edit', compact('funcionario', 'usuarios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request, [
        //     'nome' => 'required'
        // ]);

        $img = $_POST['imagecam'];
        if($img){
            $folderPath = "'/uploads/fotosfuncionarios/'";

            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("imagecam/", $image_parts[0]);
            $image_type = $image_type_aux[0];

            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';

            $foto = Image::make($image_base64)->resize(600, 450)->save( public_path('/uploads/fotosfuncionarios/' . $fileName));
        }

        $requestData = $request->all();

        if($requestData['areaatuacao'] !== 'Funcionário'){
          $requestData['especialidade'] = implode(',',$requestData['especialidade']);
        }

        if($img) $requestData['image'] = $foto->basename;

        $requestData['diastrabalho'] = isset($requestData['diastrabalho']) ? implode(',', $requestData['diastrabalho']) : '';


        $funcionario = Funcionario::findOrFail($id);
        $funcionario->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('funcionarios/funcionarios');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Funcionario::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('funcionarios/funcionarios');
    }

    public function retornamedico(){
        /*
           Ao ver esta função me responda
           Se você já deu a bundinha dê uma risadinha
        */
        $id = Input::get('id');
        $funcionarios = DB::table('funcionarios')->select('especialidade')->where('id',$id)->get();
        $retorno = array();

        foreach($funcionarios[0] as $aux):
            $especialidades = explode(',',$aux);
            foreach($especialidades as $index):
                $titulo = DB::table('especialidades')->select('titulo')->where('id',$index)->get();
                $retorno[] = isset($titulo) ? $titulo : null;
            endforeach;
        endforeach;

        echo JSON_encode($retorno);
    }//end function

public function diasdasemana($dia){
    $dias = [
        0 => 'Domingo',
        1 => 'Segunda-feira',
        2 => 'Terça-feira',
        3 => 'Quarta-feira',
        4 => 'Quinta-feira',
        5 => 'Sexta-feira',
        6 => 'Sábado'
    ];

    foreach($dias as $key => $day){
        if($key == $dia){
            return $day;
        }
    }
}

public function getDiasTrabalhoMedico(){
    $id = Input::get('id');

    $diastrabalho = DB::table('funcionarios')->select('diastrabalho')->where('id',$id)->get();
        $retorno = array();

        $dias = explode(',', $diastrabalho);

        // foreach($dias as $index){
        //     $retorno[] = diasdasemana($index);
        // }

        // foreach($funcionarios[0] as $aux):
        //     $especialidades = explode(',',$aux);
        //     foreach($especialidades as $index):
        //         $titulo = DB::table('especialidades')->select('titulo')->where('id',$index)->get();
        //         $retorno[] = $titulo;
        //     endforeach;
        // endforeach;

        echo JSON_encode($retorno);
        die();
}

    public function disponibilidadaagendaajax(){

        $data = Input::get('data');
        $medico = Input::get('medico');
        $sala = Input::get('sala');
        $flgdiario = true;

        // saber se é o dia todo
        $_data = explode('T', $data);

        $retorno = array();
        $retorno['diario'] = true;   // diario ou nao
        $retorno['ocupado'] = false; // ocupado ou nao
        $retorno['medico'] = false;  // medico
        $retorno['sala'] = false;    // sala

        if(count($_data) > 1)
            $flgdiario = false;

        if($flgdiario){

            $row = DB::table('agendamentos')->where('dataconsulta', '=', $_data[0])->where(
              function ($que) use ( $medico, $sala ){
                $que->where('medico',$medico)->orWhere(['sala'=>$sala]);
            })->first();

            if(isset($row)){
                if($row->medico == $medico)
                    $retorno['medico'] = true;

                if($row->sala == $sala)
                    $retorno['sala'] = true;

                $retorno['ocupado'] = true;
            }

        }else{

            $hora = $_data[0] . ' ' . $_data[1];

            $row = DB::table('agendamentos')->where('startconsulta', '=', $hora)->where(
                function ($que) use ($medico, $sala){
                    $que->where('medico',$medico)->orWhere(['sala'=>$sala]);
            })->first();


            if(isset($row)){
                if($row->medico == $medico)
                    $retorno['medico'] = true;

                if($row->sala == $sala)
                    $retorno['sala'] = true;

                $retorno['ocupado'] = true;
            }

            $rowdiario = DB::table('agendamentos')->where('dataconsulta', '=', $_data[0])->where('horaconsulta','=','00:00:00')->where(
                function ($que) use ( $medico, $sala ){
                  $que->where('medico',$medico)->orWhere(['sala'=>$sala]);
            })->first();

            if(isset($rowdiario)){
                if($rowdiario->medico == $medico)
                    $retorno['medico'] = true;

                if($rowdiario->sala == $sala)
                    $retorno['sala'] = true;

                $retorno['ocupado'] = true;
            }



        }// end if/else


        echo json_encode($retorno);

    }

    public function disponibilidademedicoajax(){
        $medico = Input::get('medico');
        $diasemana = Input::get('diasemana');

        $diastrabalho = DB::table('funcionarios')->select('diastrabalho')->where('id',$medico)->first();

        $arrayDias = array();
        $arrayDias = explode(',', $diastrabalho->diastrabalho);

        if(in_array($diasemana, $arrayDias)){
            echo json_encode(array('error'=>false, 'dia'=>true, 'mensagem'=>'Médico trabalha nesse dia'));
        }else{
            echo json_encode(array('error'=>true, 'dia'=>false, 'mensagem'=> 'Médico não trabalha nesse dia'));
        }
    }
}
