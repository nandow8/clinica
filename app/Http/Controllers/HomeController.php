<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User; 
use DB;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Session::flash('info', 'Bem vindo ' . \Auth::user()->name);
        // Role::create(['name' => 'clientes']);
        // $permission = Permission::create(['name' => 'editar']);

        // $role = Role::findById(1);
        // $permission = Permission::findById(1);

        // $user = User::find(1);

        // // $user->givePermissionTo($permission);
        // $user->assignRole($role);

        $particular = DB::select("select count(id) as total from pacientes where COALESCE(tipocobranca, 0) = 1 and COALESCE(tipopaciente, 0) = 1 and deleted_at is null and status = 'Ativo'");
        $particulares = $particular[0]->total ?? 0;
        
        $titular = DB::select("select count(id) as total from pacientes where COALESCE(tipocobranca, 0) = 0 and COALESCE(tipopaciente, 0) = 1 and deleted_at is null and status = 'Ativo'");
        $titulares = $titular[0]->total ?? 0;
        
        $dependente = DB::select("select count(id) as total from pacientes where COALESCE(tipocobranca, 0) = 0 and COALESCE(tipopaciente, 0) = 0 and deleted_at is null and status = 'Ativo'");
        $dependentes = $dependente[0]->total ?? 0;
        
        return view('home', compact('particulares', 'titulares', 'dependentes'));
    }
}
