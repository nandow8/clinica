<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Plano;
use Illuminate\Http\Request;
use Session;
use App\Models\Pacientesconvenio;

class PlanosController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $planos = Plano::where('nomeplano', 'LIKE', "%$keyword%")
                ->orWhere('idconvenio', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $planos = Plano::latest()->paginate($perPage);
        }

        return view('cadastro.planos.index', compact('planos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $convenios = Pacientesconvenio::All();
        return view('cadastro.planos.create', compact('convenios'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Plano::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/planos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $plano = Plano::findOrFail($id);

        return view('cadastro.planos.show', compact('plano'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $plano = Plano::findOrFail($id);
        $convenios = Pacientesconvenio::All();

        return view('cadastro.planos.edit', compact('plano', 'convenios'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $plano = Plano::findOrFail($id);
        $plano->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/planos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Plano::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/planos');
    }
    public function getConvenio($idconvenio){
        $convenio = Pacientesconvenio::select('convenio')->where('id', '=', $idconvenio)->get();

        return $convenio;
    }

}
