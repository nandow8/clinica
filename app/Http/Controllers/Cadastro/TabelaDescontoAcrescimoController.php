<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TabelaDescontoAcrescimo;
use Illuminate\Http\Request;
use Session;

class TabelaDescontoAcrescimoController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tabeladescontoacrescimo = TabelaDescontoAcrescimo::where('titulo', 'LIKE', "%$keyword%")
                ->orWhere('descricao', 'LIKE', "%$keyword%")
                ->orWhere('valor', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tabeladescontoacrescimo = TabelaDescontoAcrescimo::latest()->paginate($perPage);
        }

        return view('cadastro.tabela-desconto-acrescimo.index', compact('tabeladescontoacrescimo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.tabela-desconto-acrescimo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TabelaDescontoAcrescimo::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tabela-desconto-acrescimo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
        
//        $tabeladescontoacrescimo = TabelaDescontoAcrescimo::findOrFail($id);
//        return view('cadastro.tabela-desconto-acrescimo.show', compact('tabeladescontoacrescimo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tabeladescontoacrescimo = TabelaDescontoAcrescimo::findOrFail($id);

        return view('cadastro.tabela-desconto-acrescimo.edit', compact('tabeladescontoacrescimo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tabeladescontoacrescimo = TabelaDescontoAcrescimo::findOrFail($id);
        $tabeladescontoacrescimo->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tabela-desconto-acrescimo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TabelaDescontoAcrescimo::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/tabela-desconto-acrescimo');
    }
}
