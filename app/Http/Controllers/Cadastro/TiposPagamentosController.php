<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TiposPagamento;
use Illuminate\Http\Request;
use Session;

class TiposPagamentosController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tipospagamentos = TiposPagamento::where('descricao', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tipospagamentos = TiposPagamento::latest()->paginate($perPage);
        }

        return view('cadastro.tipos-pagamentos.index', compact('tipospagamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.tipos-pagamentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TiposPagamento::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tipos-pagamentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tipospagamento = TiposPagamento::findOrFail($id);

        return view('cadastro.tipos-pagamentos.show', compact('tipospagamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tipospagamento = TiposPagamento::findOrFail($id);

        return view('cadastro.tipos-pagamentos.edit', compact('tipospagamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tipospagamento = TiposPagamento::findOrFail($id);
        $tipospagamento->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tipos-pagamentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TiposPagamento::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/tipos-pagamentos');
    }
}
