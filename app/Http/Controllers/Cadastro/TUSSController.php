<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TUSS;
use App\Models\TUSSCapitulo;
use App\Models\TUSSGrupo;
use App\Models\TUSSSubgrupo;

use Illuminate\Http\Request;
use Session;

class TUSSController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tuss = TUSS::where('codigo', 'LIKE', "%$keyword%")
                ->orWhere('descricao', 'LIKE', "%$keyword%")
                ->orWhere('correlacao', 'LIKE', "%$keyword%")
                ->orWhere('rol', 'LIKE', "%$keyword%")
                ->orWhere('subgrupo_id', 'LIKE', "%$keyword%")
                ->orWhere('grupo_id', 'LIKE', "%$keyword%")
                ->orWhere('capitulo_id', 'LIKE', "%$keyword%")
                ->orWhere('od', 'LIKE', "%$keyword%")
                ->orWhere('amb', 'LIKE', "%$keyword%")
                ->orWhere('hco', 'LIKE', "%$keyword%")
                ->orWhere('hso', 'LIKE', "%$keyword%")
                ->orWhere('pac', 'LIKE', "%$keyword%")
                ->orWhere('dut', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tuss = TUSS::latest()->paginate($perPage);
        }

        return view('cadastro.tuss.index', compact('tuss'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $capitulos = TUSSCapitulo::all();
        $grupos = TUSSGrupo::all();
        $subgrupos = TUSSSubgrupo::all();

        return view('cadastro.tuss.create', compact('capitulos', 'grupos', 'subgrupos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TUSS::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tuss');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return $this->edit($id);        
//        $tuss = TUSS::findOrFail($id);
//
//        return view('cadastro.tuss.show', compact('tuss'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tuss = TUSS::findOrFail($id);
        $capitulos = TUSSCapitulo::all();
        $grupos = TUSSGrupo::all();
        $subgrupos = TUSSSubgrupo::all();

        return view('cadastro.tuss.edit', compact('tuss', 'capitulos', 'grupos', 'subgrupos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tuss = TUSS::findOrFail($id);
        $tuss->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tuss');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TUSS::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/tuss');
    }
}
