<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TabelaServico;
use Illuminate\Http\Request;
use Session;

class TabelaServicoController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tabelaservico = TabelaServico::where('descricao', 'LIKE', "%$keyword%")
                ->orWhere('valor', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tabelaservico = TabelaServico::latest()->paginate($perPage);
        }

        return view('cadastro.tabela-servico.index', compact('tabelaservico'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.tabela-servico.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TabelaServico::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tabela-servico');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tabelaservico = TabelaServico::findOrFail($id);

        return view('cadastro.tabela-servico.show', compact('tabelaservico'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tabelaservico = TabelaServico::findOrFail($id);

        return view('cadastro.tabela-servico.edit', compact('tabelaservico'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tabelaservico = TabelaServico::findOrFail($id);
        $tabelaservico->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tabela-servico');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TabelaServico::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/tabela-servico');
    }
    
}
