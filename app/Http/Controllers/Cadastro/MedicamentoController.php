<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Medicamento;
use Illuminate\Http\Request;

class MedicamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $medicamento = Medicamento::where('princicipoativo', 'LIKE', "%$keyword%")
                ->orWhere('produto', 'LIKE', "%$keyword%")
                ->orWhere('apresentacao', 'LIKE', "%$keyword%")
                ->orWhere('classeterapeutica', 'LIKE', "%$keyword%")
                ->orWhere('tipodeproduto', 'LIKE', "%$keyword%")
                ->orWhere('restricaohospitalar', 'LIKE', "%$keyword%")
                ->orWhere('comercializacao2017', 'LIKE', "%$keyword%")
                ->orWhere('tarja', 'LIKE', "%$keyword%")
                ->orWhere('tipomedicamento', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $medicamento = Medicamento::latest()->paginate($perPage);
        }

        return view('cadastro.medicamento.index', compact('medicamento'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.medicamento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Medicamento::create($requestData);

        return redirect('cadastro/medicamento')->with('flash_message', 'Medicamento added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $medicamento = Medicamento::findOrFail($id);

        return view('cadastro.medicamento.show', compact('medicamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $medicamento = Medicamento::findOrFail($id);

        return view('cadastro.medicamento.edit', compact('medicamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $medicamento = Medicamento::findOrFail($id);
        $medicamento->update($requestData);

        return redirect('cadastro/medicamento')->with('flash_message', 'Medicamento updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Medicamento::destroy($id);

        return redirect('cadastro/medicamento')->with('flash_message', 'Medicamento deleted!');
    }
}
