<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TUSSGrupo;
use Illuminate\Http\Request;
use Session;

class TUSSGrupoController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tussgrupo = TUSSGrupo::where('descricao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tussgrupo = TUSSGrupo::latest()->paginate($perPage);
        }

        return view('cadastro.tussgrupo.index', compact('tussgrupo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.tussgrupo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TUSSGrupo::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tussgrupo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tussgrupo = TUSSGrupo::findOrFail($id);

        return view('cadastro.tussgrupo.show', compact('tussgrupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tussgrupo = TUSSGrupo::findOrFail($id);

        return view('cadastro.tussgrupo.edit', compact('tussgrupo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tussgrupo = TUSSGrupo::findOrFail($id);
        $tussgrupo->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tussgrupo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TUSSGrupo::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/tussgrupo');
    }
}
