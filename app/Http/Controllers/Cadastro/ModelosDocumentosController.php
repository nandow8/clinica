<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\ModelosDocumento;
use Illuminate\Http\Request;
use Session;

class ModelosDocumentosController extends Controller
{
    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $modelosdocumentos = ModelosDocumento::where('descricao', 'LIKE', "%$keyword%")
                ->orWhere('corpodocumento', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $modelosdocumentos = ModelosDocumento::latest()->paginate($perPage);
        }

        return view('cadastro.modelos-documentos.index', compact('modelosdocumentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.modelos-documentos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        ModelosDocumento::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/modelos-documentos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
        // $modelosdocumento = ModelosDocumento::findOrFail($id);
        //
        // return view('cadastro.modelos-documentos.show', compact('modelosdocumento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $modelosdocumento = ModelosDocumento::findOrFail($id);

        return view('cadastro.modelos-documentos.edit', compact('modelosdocumento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $modelosdocumento = ModelosDocumento::findOrFail($id);
        $modelosdocumento->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/modelos-documentos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        ModelosDocumento::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/modelos-documentos');
    }
}
