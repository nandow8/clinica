<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TUSSCapitulo;
use Illuminate\Http\Request;
use Session;

class TUSSCapituloController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tusscapitulo = TUSSCapitulo::where('descricao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tusscapitulo = TUSSCapitulo::latest()->paginate($perPage);
        }

        return view('cadastro.tusscapitulo.index', compact('tusscapitulo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.tusscapitulo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TUSSCapitulo::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tusscapitulo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tusscapitulo = TUSSCapitulo::findOrFail($id);

        return view('cadastro.tusscapitulo.show', compact('tusscapitulo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tusscapitulo = TUSSCapitulo::findOrFail($id);

        return view('cadastro.tusscapitulo.edit', compact('tusscapitulo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tusscapitulo = TUSSCapitulo::findOrFail($id);
        $tusscapitulo->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tusscapitulo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TUSSCapitulo::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/tusscapitulo');
    }
}
