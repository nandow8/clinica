<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Clinica;
use Illuminate\Http\Request;
use Session;

class ClinicaController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $clinica = Clinica::where('razao', 'LIKE', "%$keyword%")
                ->orWhere('endereco', 'LIKE', "%$keyword%")
                ->orWhere('cep', 'LIKE', "%$keyword%")
                ->orWhere('uf', 'LIKE', "%$keyword%")
                ->orWhere('cidade', 'LIKE', "%$keyword%")
                ->orWhere('cnpj', 'LIKE', "%$keyword%")
                ->orWhere('conta', 'LIKE', "%$keyword%")
                ->orWhere('carteira', 'LIKE', "%$keyword%")
                ->orWhere('agencia', 'LIKE', "%$keyword%")
                ->orWhere('codigocliente', 'LIKE', "%$keyword%")
                ->orWhere('multa', 'LIKE', "%$keyword%")
                ->orWhere('juros', 'LIKE', "%$keyword%")
                ->orWhere('instrucao1', 'LIKE', "%$keyword%")
                ->orWhere('instrucao2', 'LIKE', "%$keyword%")
                ->orWhere('instrucao3', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $clinica = Clinica::latest()->paginate($perPage);
        }

        return view('cadastro.clinica.index', compact('clinica'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.clinica.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Clinica::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/clinica');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
//        $clinica = Clinica::findOrFail($id);
//
//        return view('cadastro.clinica.show', compact('clinica'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $clinica = Clinica::findOrFail($id);

        return view('cadastro.clinica.edit', compact('clinica'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $clinica = Clinica::findOrFail($id);
        $clinica->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/clinica');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Clinica::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/clinica');
    }
}
