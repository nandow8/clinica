<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\TUSSSubgrupo;
use Illuminate\Http\Request;
use Session;

class TUSSSubgrupoController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tusssubgrupo = TUSSSubgrupo::where('descricao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tusssubgrupo = TUSSSubgrupo::latest()->paginate($perPage);
        }

        return view('cadastro.tusssubgrupo.index', compact('tusssubgrupo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.tusssubgrupo.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        TUSSSubgrupo::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tusssubgrupo');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tusssubgrupo = TUSSSubgrupo::findOrFail($id);

        return view('cadastro.tusssubgrupo.show', compact('tusssubgrupo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tusssubgrupo = TUSSSubgrupo::findOrFail($id);

        return view('cadastro.tusssubgrupo.edit', compact('tusssubgrupo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tusssubgrupo = TUSSSubgrupo::findOrFail($id);
        $tusssubgrupo->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/tusssubgrupo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        TUSSSubgrupo::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/tusssubgrupo');
    }
}
