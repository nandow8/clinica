<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Despesa;
use Illuminate\Http\Request;
use Session;

class DespesasController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $despesas = Despesa::where('descricao', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $despesas = Despesa::latest()->paginate($perPage);
        }

        return view('cadastro.despesas.index', compact('despesas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.despesas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Despesa::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/despesas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $despesa = Despesa::findOrFail($id);

        return view('cadastro.despesas.show', compact('despesa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $despesa = Despesa::findOrFail($id);

        return view('cadastro.despesas.edit', compact('despesa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $despesa = Despesa::findOrFail($id);
        $despesa->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/despesas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Despesa::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/despesas');
    }
}
