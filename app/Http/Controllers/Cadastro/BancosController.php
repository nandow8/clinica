<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banco;
use Illuminate\Http\Request;
use Session;

class BancosController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $bancos = Banco::where('codigo', 'LIKE', "%$keyword%")
                ->orWhere('agencia', 'LIKE', "%$keyword%")
                ->orWhere('agenciadig', 'LIKE', "%$keyword%")
                ->orWhere('conta', 'LIKE', "%$keyword%")
                ->orWhere('contadig', 'LIKE', "%$keyword%")
                ->orWhere('cedente', 'LIKE', "%$keyword%")
                ->orWhere('cedentedig', 'LIKE', "%$keyword%")
                ->orWhere('nome', 'LIKE', "%$keyword%")
                ->orWhere('carteira', 'LIKE', "%$keyword%")
                ->orWhere('variacao', 'LIKE', "%$keyword%")
                ->orWhere('numero', 'LIKE', "%$keyword%")
                ->orWhere('convenio', 'LIKE', "%$keyword%")
                ->orWhere('instrucao1', 'LIKE', "%$keyword%")
                ->orWhere('instrucao2', 'LIKE', "%$keyword%")
                ->orWhere('instrucao3', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('geraboleto', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $bancos = Banco::latest()->paginate($perPage);
        }

        return view('cadastro.bancos.index', compact('bancos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.bancos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Banco::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/bancos');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $banco = Banco::findOrFail($id);

        return view('cadastro.bancos.show', compact('banco'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $banco = Banco::findOrFail($id);

        return view('cadastro.bancos.edit', compact('banco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $banco = Banco::findOrFail($id);
        $banco->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/bancos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Banco::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/bancos');
    }
}
