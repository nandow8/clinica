<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Examesrf;
use Illuminate\Http\Request;
use Session;

class ExamesrfController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $examesrf = Examesrf::where('tabela', 'LIKE', "%$keyword%")
                ->orWhere('mneumonico', 'LIKE', "%$keyword%")
                ->orWhere('codigo', 'LIKE', "%$keyword%")
                ->orWhere('nome', 'LIKE', "%$keyword%")
                ->orWhere('ch', 'LIKE', "%$keyword%")
                ->orWhere('preco', 'LIKE', "%$keyword%")
                ->orWhere('percentual', 'LIKE', "%$keyword%")
                ->orWhere('venda', 'LIKE', "%$keyword%")
                ->orWhere('lucro', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $examesrf = Examesrf::latest()->paginate($perPage);
        }

        return view('cadastro.examesrf.index', compact('examesrf'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.examesrf.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Examesrf::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/examesrf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
        //$examesrf = Examesrf::findOrFail($id);

        //return view('cadastro.examesrf.show', compact('examesrf'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $examesrf = Examesrf::findOrFail($id);

        return view('cadastro.examesrf.edit', compact('examesrf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $examesrf = Examesrf::findOrFail($id);
        $examesrf->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/examesrf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Examesrf::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/examesrf');
    }
}
