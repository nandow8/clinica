<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Bairro;
use Illuminate\Http\Request;
use Session;

class BairrosController extends Controller
{

    private $bairro;

    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct(Bairro $bairros)
    {
        $this->bairro = $bairros;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $bairros = $this->bairro->listAll($request);
        return view('usuarios.bairros.index', compact('bairros'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $cidades = $this->bairro->listaCidades();
        return view('usuarios.bairros.create', compact('cidades'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Bairro::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/bairros');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $bairro = Bairro::findOrFail($id);

        return view('usuarios.bairros.show', compact('bairro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $bairro = Bairro::findOrFail($id);

        return view('usuarios.bairros.edit', compact('bairro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $bairro = Bairro::findOrFail($id);
        $bairro->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/bairros');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Bairro::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('usuarios/bairros');
    }
}
