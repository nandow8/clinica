<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Pacientesconvenio;
use Illuminate\Http\Request;
use Session;

class PacientesconvenioController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $pacientesconvenio = Pacientesconvenio::where('convenio', 'LIKE', "%$keyword%")
                ->orWhere('plano', 'LIKE', "%$keyword%")
                ->orWhere('numerocarteira', 'LIKE', "%$keyword%")
                ->orWhere('datavalidade', 'LIKE', "%$keyword%")
                ->orWhere('contato', 'LIKE', "%$keyword%")
                ->orWhere('cnpj', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $pacientesconvenio = Pacientesconvenio::latest()->paginate($perPage);
        }

        return view('cadastro.pacientesconvenio.index', compact('pacientesconvenio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('cadastro.pacientesconvenio.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Pacientesconvenio::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/pacientesconvenio');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pacientesconvenio = Pacientesconvenio::findOrFail($id);

        return view('cadastro.pacientesconvenio.show', compact('pacientesconvenio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pacientesconvenio = Pacientesconvenio::findOrFail($id);

        return view('cadastro.pacientesconvenio.edit', compact('pacientesconvenio'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $pacientesconvenio = Pacientesconvenio::findOrFail($id);
        $pacientesconvenio->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('cadastro/pacientesconvenio');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Pacientesconvenio::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('cadastro/pacientesconvenio');
    }

}
