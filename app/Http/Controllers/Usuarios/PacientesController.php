<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Paciente;
use App\Models\Cidade;
use Illuminate\Http\Request;
use Session;
use Carbon\Carbon;
use Image;
use Illuminate\Support\Facades\Input;
use App\Models\Pacientesconvenio;
use App\Models\Plano;
use Response;
use App\Models\Grupoempresarial;

class PacientesController extends Controller
{
    private $pacientes;
    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct(Paciente $paciente)
    {
        $this->pacientes = $paciente;
        $this->middleware('auth');
    }

    public function validacoes($request){
        $this->validate($request, [
			'nome' => 'required|max:255',
			// 'rg' => 'required|max:255',
			// 'cpf' => 'required|max:255',
            'estados_id' => 'required',
			// 'email' => 'required|email',
			'tipopaciente' => 'required'
		]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $pacientes = $this->pacientes->index($request);
        return view('usuarios.pacientes.index', compact('pacientes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $estados = $this->pacientes->listaEstados();
        $cidades = $this->pacientes->listaCidades();
        $bairros = $this->pacientes->listaBairros();
        $pacientes = $this->pacientes->listaPacientes();
        $convenios = $this->pacientes->listaConvenios();
        $planos = $this->pacientes->listaPlanos();
        $titulares = $this->pacientes->listaTitulares();
        $empresa = Grupoempresarial::all();

        return view('usuarios.pacientes.create', compact('estados', 'cidades', 'bairros', 'pacientes', 'convenios', 'planos', 'titulares', 'empresa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $img = $_POST['imagecam'];
        if($img){
            $folderPath = "'/uploads/fotospacientes/'";

            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("imagecam/", $image_parts[0]);
            $image_type = $image_type_aux[0];

            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';

            $foto = Image::make($image_base64)->resize(600, 450)->save( public_path('/uploads/fotospacientes/' . $fileName));
        }

        $this->validacoes($request);

        $requestData = $request->all();

        $requestData['datanascimento'] = (isset($request->datanascimento)) ? Carbon::createFromFormat('d/m/Y', $request->datanascimento)->format('Y-m-d') : '';
        $requestData['datavalidade'] = (isset($request->datavalidade)) ? Carbon::createFromFormat('d/m/Y', $request->datavalidade)->format('Y-m-d') : '';

        if($img) $requestData['image'] = $foto->basename;
        $p = Paciente::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/pacientes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paciente = Paciente::findOrFail($id);

        $paciente['datavalidade'] = Carbon::parse($paciente['datavalidade'])->format('d/m/Y');
        $paciente['datanascimento'] = Carbon::parse($paciente['datanascimento'])->format('d/m/Y');

        $estados = $this->pacientes->listaEstados();
        $cidades = $this->pacientes->listaCidades();
        $bairros = $this->pacientes->listaBairros();
        $pacientes = $this->pacientes->listaPacientes();
        $convenios = $this->pacientes->listaConvenios();
        $planos = $this->pacientes->listaPlanos();
        $titulares = $this->pacientes->listaTitulares();
        $empresa = Grupoempresarial::all();

        return view('usuarios.pacientes.show', compact('paciente','estados', 'cidades', 'bairros', 'pacientes','convenios', 'planos', 'titulares', 'empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paciente = Paciente::findOrFail($id);
        // dd($paciente);
        $paciente['datavalidade'] = Carbon::parse($paciente['datavalidade'])->format('d/m/Y');
        $paciente['datanascimento'] = Carbon::parse($paciente['datanascimento'])->format('d/m/Y');

        $estados = $this->pacientes->listaEstados();
        $cidades = $this->pacientes->listaCidades();
        $bairros = $this->pacientes->listaBairros();
        $pacientes = $this->pacientes->listaPacientes();
        $convenios = $this->pacientes->listaConvenios();
        $planos = $this->pacientes->listaPlanos();
        $titulares = $this->pacientes->listaTitulares();
        $empresa = Grupoempresarial::all();
        // dd($planos);

        return view('usuarios.pacientes.edit', compact('paciente','estados', 'cidades', 'bairros', 'pacientes','convenios', 'planos', 'titulares', 'empresa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $img = $_POST['imagecam'];
        if($img){
            $folderPath = "'/uploads/fotospacientes/'";
            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("imagecam/", $image_parts[0]);
            $image_type = $image_type_aux[0];

            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';

            $foto = Image::make($image_base64)->resize(600, 450)->save( public_path('/uploads/fotospacientes/' . $fileName));
        }

        $this->validacoes($request);

        $requestData = $request->all();

        // $requestData['datavalidade'] = Carbon::createFromFormat('d/m/Y', $request->datavalidade)->format('Y-m-d');
        $requestData['datanascimento'] = Carbon::createFromFormat('d/m/Y', $request->datanascimento)->format('Y-m-d');
        $requestData['datavalidade'] = (isset($request->datavalidade)) ? Carbon::createFromFormat('d/m/Y', $request->datavalidade)->format('Y-m-d') : '';

        if($img) $requestData['image'] = $foto->basename;

        $paciente = Paciente::findOrFail($id);
        $paciente->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/pacientes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Paciente::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('usuarios/pacientes');
    }

    public function ajaxpacienteconvenio($convenio_id){

        $convenio = Pacientesconvenio::where('id', '=', $convenio_id)->get();

        // return Response::json($convenio);
        $returnHTML = view('usuarios.pacientes.ajax.convenios',['convenio'=> $convenio])->render();// or method that you prefere to return data + RENDER is the key here
        return response()->json( array('success' => true, 'html'=>$returnHTML) );
    }

    public function getCidades(Request $request){
        $estado_id = $request->estado_id;

        $cidades = Cidade::where('estados_id', '=', $estado_id)->get();

        echo json_encode($cidades);
        die();
    }
}
