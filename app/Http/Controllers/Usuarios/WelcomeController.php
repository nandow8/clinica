<?php

namespace App\Http\Controllers\Usuarios;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Models\Welcome;
use Carbon\Carbon;

class WelcomeController extends Controller
{
    public function index(){
        return view('welcome');
    }

    public function store(Request $request){
        $data = ($request->dataconsulta) ? Carbon::parse($request->dataconsulta)->format('Y-m-d') : null;


        $preagenda = new Welcome;
        $preagenda->nome = $request->nome;
        $preagenda->telefone = $request->telefone;
        $preagenda->email = $request->email;
        $preagenda->tipoconsulta = $request->opcao;
        $preagenda->cpf = $request->cpf;
        $preagenda->dataconsulta = $data;
        $preagenda->exame = $request->exame;
        
        $preagenda->save();
    }

    public function limpacpf($cpf){        
        if(isset($cpf)){
            $cpf = str_replace('.', '', $cpf);
            $cpf = str_replace('-', '', $cpf);
        }
        return $cpf;
    }

    public function preagenda(Request $request){
              
        $nome = $request->nome;
        $opcao = $request->opcao;
        $telefone = $request->telefone;
        $email = $request->email;
        $horario = $request->horario;
        $cpf = $request->cpf;

        if($cpf){
            $cpf = $this->limpacpf($cpf);
        }

        $exame = null;
        $data = null;
        
        if(isset($email)){
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                echo json_encode(array('status'=>'FALHA', 'mensagem'=>'Email inválido. Informe um email válido'));
                die();    
            }
        }

        if($opcao){
            if($opcao=='consulta'){
                $dataconsulta = $request->dataconsulta;
                $data = ($dataconsulta) ? Carbon::createFromFormat('d/m/Y', $request->dataconsulta)->format('Y-m-d') : null;
            }else{
                $exame = $request->exame;
            }
        }
        
        if(empty($nome)){
            echo json_encode(array('status'=>'FALHA', 'mensagem'=>'Nome é campo obrigatório. Informe o seu nome'));
            die();
        }
        if(empty($telefone)){
            echo json_encode(array('status'=>'FALHA', 'mensagem'=>'Telefone é campo obrigatório. Informe um telefone'));
            die();
        }
        
        if(empty($horario)){
            echo json_encode(array('status'=>'FALHA', 'mensagem'=>'Informe um horário para ser atendido'));
            die();
        }

        $preagenda = new Welcome;
        $preagenda->nome = $nome;
        $preagenda->telefone = $telefone;
        $preagenda->email = $email;
        $preagenda->tipoconsulta = $opcao;
        $preagenda->cpf = $cpf;
        $preagenda->dataconsulta = $data;
        $preagenda->horario = $horario;
        $preagenda->exame = $exame;
        $preagenda->status = 'Não agendado';

        if($preagenda->save()){
            echo json_encode(array('status'=>'OK', 'mensagem'=>'Dados salvos com sucesso. Entraremos em contato em breve'));
            die();
        }
    }
}
