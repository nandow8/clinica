<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Estado;
use Illuminate\Http\Request;
use Session;

class EstadosController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $estados = Estado::where('nome', 'LIKE', "%$keyword%")
                ->orWhere('uf', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $estados = Estado::latest()->paginate($perPage);
        }

        return view('usuarios.estados.index', compact('estados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('usuarios.estados.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Estado::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/estados');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $estado = Estado::findOrFail($id);

        return view('usuarios.estados.show', compact('estado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $estado = Estado::findOrFail($id);

        return view('usuarios.estados.edit', compact('estado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $estado = Estado::findOrFail($id);
        $estado->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/estados');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Estado::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('usuarios/estados');
    }
}
