<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Cidade;
use Illuminate\Http\Request;
use Session;

class CidadesController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $cidades = Cidade::where('nome', 'LIKE', "%$keyword%")
                ->orWhere('estados_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $cidades = Cidade::latest()->paginate($perPage);
        }

        return view('usuarios.cidades.index', compact('cidades'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('usuarios.cidades.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Cidade::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/cidades');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $cidade = Cidade::findOrFail($id);

        return view('usuarios.cidades.show', compact('cidade'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $cidade = Cidade::findOrFail($id);

        return view('usuarios.cidades.edit', compact('cidade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $cidade = Cidade::findOrFail($id);
        $cidade->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('usuarios/cidades');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Cidade::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('usuarios/cidades');
    }
}
