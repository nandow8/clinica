<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Preagendamento;
use App\Models\Paciente;
use Illuminate\Http\Request;
use Session;

class PreagendamentoController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $preagendamento = Preagendamento::where('nome', 'LIKE', "%$keyword%")
                ->orWhere('telefone', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('cpf', 'LIKE', "%$keyword%")
                ->orWhere('dataconsulta', 'LIKE', "%$keyword%")
                ->orWhere('exame', 'LIKE', "%$keyword%")
                ->orWhere('horario', 'LIKE', "%$keyword%")
                ->orWhere('tipoconsulta', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $preagendamento = Preagendamento::latest()->paginate($perPage);
        }

        return view('preagendamento.index', compact('preagendamento'));
    }

    public function gerarRegistro(Request $request){
        $nome = $request->nome;
        $email = $request->email;
        $telefone = $request->telefone;
        $cpf = $request->cpf;
        $estado = '24';
        $cidade = '2.400';
        $bairro = '1';

        if(empty($nome)){ 
            echo json_encode(array("status"=>"OKAY", "mensagem"=>"Nome é obrigatório. Informe o nome completo"));
            die();
        }
        if(empty($telefone)){ 
            echo json_encode(array("status"=>"OKAY", "mensagem"=>"Telefone é obrigatório. Informe o telefone"));
            die();
        }

        $paciente = new Paciente;
        $paciente->nome = $nome;
        $paciente->email = $email;
        $paciente->cpf = $cpf;
        $paciente->telefone = $telefone;
        $paciente->estados_id = $estado;
        $paciente->cidades_id = $cidade;
        $paciente->bairros_id = $bairro;

        if($paciente->save()){
            echo json_encode(array("status"=>"OKAY"));
        }else{
            echo json_encode(array("status"=>"FALHA"));
        }
        die();
    }

    public function consultaRegistro(Request $request){
        $keyword = $request->cpf;
        $nome = $request->nome;

        if(!empty($keyword)){
            $paciente = Paciente::where([
                ['cpf', '=', "$keyword"],
                ['nome', 'LIKE', "%$nome%"],
            ])->get();
                      
            if(count($paciente) > 0){
                echo json_encode(array("status"=>"OKAY", "paciente"=>$paciente[0]));
                die();
            }else{
                echo json_encode(array("status"=>"FALHA", "mensagem"=>'Paciente não encontrado'));
                die();
            }
        }else{
            echo json_encode(array("status"=>"FALHA", "mensagem"=>'Paciente não encontrado'));
            die();
        }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('preagendamento.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Preagendamento::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('preagendamento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $preagendamento = Preagendamento::findOrFail($id);

        return view('preagendamento.show', compact('preagendamento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $preagendamento = Preagendamento::findOrFail($id);

        return view('preagendamento.edit', compact('preagendamento'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $preagendamento = Preagendamento::findOrFail($id);
        $preagendamento->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('preagendamento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Preagendamento::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('preagendamento');
    }
}
