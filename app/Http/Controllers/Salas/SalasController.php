<?php

namespace App\Http\Controllers\Salas;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Sala;
use Illuminate\Http\Request;

class SalasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $salas = Sala::where('clinica', 'LIKE', "%$keyword%")
                ->orWhere('titulo', 'LIKE', "%$keyword%")
                ->orWhere('cor', 'LIKE', "%$keyword%")
                ->orWhere('descricao', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $salas = Sala::latest()->paginate($perPage);
        }

        return view('salas.index', compact('salas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('salas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'clinica' => 'required',
			'titulo' => 'required',
			'descricao' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Sala::create($requestData);

        return redirect('cadastro/salas')->with('flash_message', 'Sala added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $sala = Sala::findOrFail($id);

        return view('salas.show', compact('sala'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sala = Sala::findOrFail($id);

        return view('salas.edit', compact('sala'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'clinica' => 'required',
			'titulo' => 'required',
			'descricao' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $sala = Sala::findOrFail($id);
        $sala->update($requestData);

        return redirect('cadastro/salas')->with('flash_message', 'Sala updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Sala::destroy($id);

        return redirect('cadastro/salas')->with('flash_message', 'Sala deleted!');
    }
}
