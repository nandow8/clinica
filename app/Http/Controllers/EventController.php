<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Event;
use App\Agendamentos;
use App\Models\Funcionario;
use App\Models\Welcome;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;
use Illuminate\Support\Facades\Input;
use App\Models\Medicofichaclinica;
use Auth;
class EventController extends Controller
{
       public function index()
            {

                return view('fullcalendar');
            }

        public function ajaxagendamento()
            {
                $data = Input::get('data');
                $hora =  Input::get('hora');
                $paciente =  Input::get('paciente');
                $medico =  Input::get('medico'); 
                $nomepaciente = Input::get('nomepaciente');
                $nomemedico = Input::get('nomemedico');
                $sala = Input::get('sala');
                $atividade = Input::get('atividade');
                $obs = Input::get('obs');
                $status = Input::get('status');
                $nomeclinica = Input::get('nomeclinica');
                $idclinica = Input::get('idclinica');

                $_data = explode('T',$data);
                
                if(isset($_data[1])){
                    $hora = $_data[1];
                    $start = $_data[0] . ' ' . $_data[1];
                }else{
                    $hora = '00:00:00';
                    $start = $data . ' ' . $hora;
                }

                $agendamentos = new Agendamentos;
                
                $agendamentos->dataconsulta = $_data[0];
                $agendamentos->horaconsulta = $hora;
                $agendamentos->startconsulta = $start;
                $agendamentos->endconsulta = $start;
                $agendamentos->paciente = $paciente;
                $agendamentos->medico = $medico;
                $agendamentos->sala = $sala;
                $agendamentos->nomepaciente = $nomepaciente;
                $agendamentos->nomemedico = $nomemedico;
                $agendamentos->nomeclinica = $nomeclinica;
                $agendamentos->idclinica = $idclinica;
                $agendamentos->atividade = isset($atividade)? $atividade : 'não adicionado';
                $agendamentos->obs = isset($obs)? $obs : 'não adicionado';
                $agendamentos->status = $status;

                $agendamentos->save();

            }

            public function ajaxeventos(){         
                $filtro_sala = Input::get('salas');
                $filtro_medico = Input::get('medicos');
                $filtro_status = Input::get('status');
                              
                //Inicio do filtro
                $where = array();
               
                if(isset($filtro_sala)) {
                    $filtro_sala = implode(',', $filtro_sala);
                    array_push($where, " ag.sala IN ($filtro_sala)");                
                } 

                if(isset($filtro_medico)) {
                    $filtro_medico = implode(',', $filtro_medico);
                    array_push($where, " ag.medico IN ($filtro_medico)");                
                }    
                
                if(isset($filtro_status)) {
                    $grupo = array();
                    foreach($filtro_status as $filter => $status){
                        //garantir que seja uma string                       
                        array_push($grupo, "'".$status."'");                        
                    }

                    $arrayStatus = implode(',', $grupo);
                    array_push($where, " ag.status IN ($arrayStatus)");                    
                }
                
                $iduser = Auth::user()->id;
                $tipoperfil = 'geral';
                if($iduser){
                    $user = DB::table('users')->join('funcionarios', 'users.id', '=', 'funcionarios.user_id')->select()->where('users.id','=',$iduser)->get();
                    if(count($user) > 0){     
                        $tipoperfil = ($user[0]->areaatuacao) ? $user[0]->areaatuacao : 'geral';
                    }   
                }
               
                if($tipoperfil == 'Médico'){
                    $idmed = $user[0]->id;                   
                    array_push($where, " ag.medico = $idmed");
                    array_push($where, " ag.status IN ('finalizado', 'aguardando')");
                }

                $w = "";
                foreach ($where as $k=>$v) {
                    if ($k>0) $w .= " AND ";
                    $w .= $v;
                }
                //$s = implode(',', $salas);
                if ($w!="") $w = "AND ($w)";

                //$agendamentos = DB::table('agendamentos')->join('salas', 'agendamentos.sala', '=', 'salas.id')->select('agendamentos.*', 'salas.cor')->get();
                
                $agendamentos = DB::select("SELECT ag.*, s.cor FROM agendamentos ag INNER JOIN salas s ON(ag.sala = s.id) WHERE ag.id IS NOT NULL $w");                
                //print_r("SELECT ag.*, s.cor FROM agendamentos ag INNER JOIN salas s ON(ag.sala = s.id) WHERE ag.id IS NOT NULL $w");
                $events = [];
                
                foreach($agendamentos as $k=>$aux):

                    $start = explode(' ',$aux->startconsulta); 
                    $event = [];

                    $event['obs'] = $aux->id;
                    $event['title'] = $aux->nomepaciente;
                    $event['start'] = $start[0] . 'T' . $start[1];                    
                    
                    if($start[1] == '00:00:00'){ 
                        $event['allDay'] = true;
                    }else{
                        $event['allDay'] = false;
                    }

                    $event['color'] = $aux->cor;

                    array_push($events, $event);

                endforeach;

                echo json_encode($events);
            }

            public function ajaxeventosdetalhes(){
                $id = Input::get('id');
                $evento = DB::table('agendamentos')->select()->where('agendamentos.id', '=', $id)->get();
                
                if(isset($evento)){
                    $idpaciente = $evento[0]->paciente;
                    $ficha = DB::table('medicofichaclinicas')->select()->where('paciente_id', '=', $idpaciente)->get();
                    
                    if(count($ficha)==0){
                        $dados = array();
                        $dados["create_at"] = date("Y:m:d");
                        $dados["update_at"] = date("Y:m:d");
                        $dados["deleted_at"] = date("Y:m:d");
                        $dados["paciente_id"] = $idpaciente;
                        
                        $fichaclinica = Medicofichaclinica::create($dados);

                        $ficha = DB::table('medicofichaclinicas')->select()->where('paciente_id', '=', $idpaciente)->get();
                    }
                    $dadosficha = (count($ficha)>0) ? $ficha[0] : '';
                
                    echo json_encode(array('ficha'=>$dadosficha, 'dados'=>$evento[0]));                   
                }
            }

            public function ajaxeventosupdate(){
                $id = Input::get('id');
                $paciente = Input::get('paciente');
                $medico = Input::get('medico');
                $sala = Input::get('sala');
                $nomepaciente = Input::get('nomepaciente');
                $nomemedico = Input::get('nomemedico');
                $atividade = Input::get('atividade');
                $obs = Input::get('obs');
                $status = Input::get('status');
                $nomeclinica = Input::get('nomeclinica');
                $idclinica = Input::get('idclinica');
               
                $event = array();

                $event['paciente'] = $paciente;
                $event['medico'] = $medico;
                $event['sala'] = $sala;

                $event['nomepaciente'] = $nomepaciente;
                $event['nomemedico'] = $nomemedico;

                $event['atividade'] = $atividade;
                $event['obs'] = $obs;
                $event['status'] = $status;

                $event['nomeclinica'] = $nomeclinica;
                $event['idclinica'] = $idclinica;

                $event['updated_at'] = date("Y-m-d H:i:s");

                DB::table('agendamentos')->where('id', $id)->update($event);
            } 

            public function ajaxeventosexclui(){
                $id = Input::get('id');
                DB::table('agendamentos')->where('id', '=', $id)->delete();
                echo $id;
            }

            public function ajaxeventosclinicas(){
                $idclinica = Input::get('idclinica');

                $row = DB::table('salas')->where('clinica',$idclinica)->get();
                echo json_encode($row);
            }
}