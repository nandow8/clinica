<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Tabelasparapopularperfil;
use Illuminate\Http\Request;
use Session;
use App\Models\Perfil;

class TabelasparapopularperfilController extends Controller
{
    private $tabelasparapopularperfil;

    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct(Tabelasparapopularperfil $tabelasperfil)
    {
        $this->tabelasparapopularperfil = $tabelasperfil;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $tabelasparapopularperfil = Tabelasparapopularperfil::where('modulo', 'LIKE', "%$keyword%")
                ->orWhere('tabela', 'LIKE', "%$keyword%")
                ->orWhere('posicao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $tabelasparapopularperfil = Tabelasparapopularperfil::latest()->paginate($perPage);
        }

        return view('admin.tabelasparapopularperfil.index', compact('tabelasparapopularperfil'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $tabelas = $this->tabelasparapopularperfil->exibeTabelas();
        return view('admin.tabelasparapopularperfil.create', compact('tabelas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $modulo_id = 0;
        if($requestData['modulo'] == 'Admin') $modulo_id = 1;
        if($requestData['modulo'] == 'Financeiro') $modulo_id = 2;
        if($requestData['modulo'] == 'Cadastro') $modulo_id = 3;

        Tabelasparapopularperfil::create([
            'modulo_id' => $modulo_id,
            'modulo' => $requestData['modulo'],
            'tabela' => $requestData['tabela'],
        ]);
        // Perfil::create([
        //     'modulo_id' => $modulo_id,
        //     'modulo' => $requestData['modulo'],
        //     'tabela' => $requestData['tabela']
        // ]); 

        Session::flash('success', 'Salvo com sucesso');

        return redirect('admin/tabelasparapopularperfil');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $tabelasparapopularperfil = Tabelasparapopularperfil::findOrFail($id);

        return view('admin.tabelasparapopularperfil.show', compact('tabelasparapopularperfil'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $tabelasparapopularperfil = Tabelasparapopularperfil::findOrFail($id);
        $tabelas = $this->tabelasparapopularperfil->exibeTabelas();
        return view('admin.tabelasparapopularperfil.edit', compact('tabelasparapopularperfil','tabelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $tabelasparapopularperfil = Tabelasparapopularperfil::findOrFail($id);
        $tabelasparapopularperfil->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('admin/tabelasparapopularperfil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Tabelasparapopularperfil::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('admin/tabelasparapopularperfil');
    }
}
