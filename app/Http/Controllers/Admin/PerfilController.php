<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Perfil;
use Illuminate\Http\Request;
use Session;
use Illuminate\Support\Facades\Input;
use App\Models\Tabelasparapopularperfil;
use Spatie\Permission\Models\Permission;
use App\User;

class PerfilController extends Controller
{
    /**
     * Middleware de autenticação (AUTH) 
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $perfil = Tabelasparapopularperfil::where('modulo', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $perfil = Tabelasparapopularperfil::selectRaw('count(*) AS cnt, modulo, modulo_id')->groupBy('modulo','modulo_id')->orderBy('modulo', 'ASC')->paginate($perPage);
        }

        return view('admin.perfil.index', compact('perfil'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $perfil = Perfil::findOrFail($id);
        $listadePerfil = Perfil::all();

        $perfillista ;
        foreach ($listadePerfil as $value) {
            $perfillista[$value['modulo']][] = $value;
        }

        return view('admin.perfil.show', compact('perfil', 'perfillista'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $listadePerfil = Perfil::all();

        $perfillista ;
        foreach ($listadePerfil as $value) {
            $perfillista[$value['modulo']][] = $value;
        }

        // dd($perfillista);

        return view('admin.perfil.create', compact('perfillista'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $insertPerfil = Input::except(['_method', '_token']);
       
        $qtd = collect($insertPerfil)->count();

        for($i = 0; $i < $qtd - 1; $i++) {
            $perfil= new Perfil();
            
            $perfil->id = isset($insertPerfil['id'][$i]) ? $insertPerfil['id'][$i] : NULL;
            $perfil->tabela = isset($insertPerfil['tabela'][$i]) ? $insertPerfil['tabela'][$i] : NULL;
            $perfil->visualizar = isset($insertPerfil['visualizar'][$i]) ? $insertPerfil['visualizar'][$i] : NULL;
            $perfil->adicionar=isset($insertPerfil['adicionar'][$i]) ? $insertPerfil['adicionar'][$i] : NULL;
            $perfil->editar = isset($insertPerfil['editar'][$i]) ? $insertPerfil['editar'][$i] : NULL;
            $perfil->excluir = isset($insertPerfil['excluir'][$i]) ? $insertPerfil['excluir'][$i] : NULL;
          
            // $perfil->save();

            $perfil->where('id', $perfil->id)->update([
                'modulo' => $perfil->modulo,
                'tabela' => $perfil->tabela,
                'visualizar' => $perfil->visualizar,
                'adicionar' => $perfil->adicionar,
                'editar' => $perfil->editar,
                'excluir' => $perfil->excluir,
            ]);
         }

        Session::flash('success', 'Salvo com sucesso');

        return redirect('admin/perfil');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        // $perfil = Perfil::findOrFail($id);
        $listadePerfil = Tabelasparapopularperfil::orderBy('id', 'ASC')->get();
        $perfils = Perfil::where('modulo_id','=',$id)->orderBy('id', 'ASC')->get();

        // $collectTabelas = collect($listadePerfil);
        // $perfils->union($collectTabelas);

        $perfillista ;
        foreach ($listadePerfil as $value) {
            $perfillista[$value['modulo']][] = $value;
        }

        // dd($perfillista);

        return view('admin.perfil.edit', compact('perfils', 'id', 'perfillista'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $insertPerfil = Input::except(['_method', '_token']);

        $qtd = collect($insertPerfil)->count();

        for($i = 1; $i < COUNT($insertPerfil['id']) + 1; $i++) {
            $perfil= new Perfil();
            
            $perfil->id = isset($insertPerfil['id'][$i]) ? $insertPerfil['id'][$i] : 'NULL';
            $perfil->modulo = isset($insertPerfil['modulo'][$i]) ? $insertPerfil['modulo'][$i] : 'NULL';
            $perfil->tabela = isset($insertPerfil['tabela'][$i]) ? $insertPerfil['tabela'][$i] : 'NULL';
            $perfil->visualizar = isset($insertPerfil['visualizar'][$i]) ? $insertPerfil['visualizar'][$i] : 'NULL';
            $perfil->adicionar=isset($insertPerfil['adicionar'][$i]) ? $insertPerfil['adicionar'][$i] : 'NULL';
            $perfil->editar = isset($insertPerfil['editar'][$i]) ? $insertPerfil['editar'][$i] : 'NULL';
            $perfil->excluir = isset($insertPerfil['excluir'][$i]) ? $insertPerfil['excluir'][$i] : 'NULL';

            $perfil->updateOrCreate(
                [
                    'modulo_id' => $id,
                    'modulo' => $perfil->modulo,
                    'tabela' => $perfil->tabela,
                ],
                [ 
                    'modulo' => $perfil->modulo,
                    'tabela' => $perfil->tabela,
                    'visualizar' => $perfil->visualizar,
                    'adicionar' => $perfil->adicionar,
                    'editar' => $perfil->editar,
                    'excluir' => $perfil->excluir,
                ]
            );

           
           if($perfil->visualizar != 'NULL'){
               $check_permission = Permission::where('name', '=', $perfil->visualizar)->get();
               if($check_permission->isEmpty()){
                   Permission::create(['name' => $perfil->visualizar]);
               }

               $users = User::where('perfil_id', '=', $id)->get();
               $permission = ($perfil->visualizar != 'NULL') ? Permission::findByName($perfil->visualizar) : NULL;
               
               if($permission){
                   foreach($users as $user){
                       $user->givePermissionTo($permission);
                    }
                } 
            }

            if($perfil->adicionar != 'NULL'){
                $check_permission = Permission::where('name', '=', $perfil->adicionar)->get();
                if($check_permission->isEmpty()){
                    Permission::create(['name' => $perfil->adicionar]);
                }
 
                $users = User::where('perfil_id', '=', $id)->get();
                $permission = ($perfil->adicionar != 'NULL') ? Permission::findByName($perfil->adicionar) : NULL;
                
                if($permission){
                    foreach($users as $user){
                        $user->givePermissionTo($permission);
                     }
                 } 
             }

             if($perfil->editar != 'NULL'){
                $check_permission = Permission::where('name', '=', $perfil->editar)->get();
                if($check_permission->isEmpty()){
                    Permission::create(['name' => $perfil->editar]);
                }
 
                $users = User::where('perfil_id', '=', $id)->get();
                $permission = ($perfil->editar != 'NULL') ? Permission::findByName($perfil->editar) : NULL;
                
                if($permission){
                    foreach($users as $user){
                        $user->givePermissionTo($permission);
                     }
                 } 
             }

             if($perfil->excluir != 'NULL'){
                $check_permission = Permission::where('name', '=', $perfil->excluir)->get();
                if($check_permission->isEmpty()){
                    Permission::create(['name' => $perfil->excluir]);
                }
 
                $users = User::where('perfil_id', '=', $id)->get();
                $permission = ($perfil->excluir != 'NULL') ? Permission::findByName($perfil->excluir) : NULL;
                
                if($permission){
                    foreach($users as $user){
                        $user->givePermissionTo($permission);
                     }
                 } 
             }
           
          
        }
         
        Session::flash('success', 'Salvo com sucesso');

        return redirect('admin/perfil');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Perfil::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('admin/perfil');
    }
}
