<?php

namespace App\Http\Controllers\Financeiro;

use App\Models\OrcamentosDescontos;
use Illuminate\Http\Request;

class OrcamentosDescontosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrcamentosDescontos  $orcamentoDescontos
     * @return \Illuminate\Http\Response
     */
    public function show(OrcamentosDescontos $orcamentoDescontos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrcamentosDescontos  $orcamentoDescontos
     * @return \Illuminate\Http\Response
     */
    public function edit(OrcamentosDescontos $orcamentoDescontos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrcamentosDescontos  $orcamentoDescontos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrcamentosDescontos $orcamentoDescontos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrcamentosDescontos  $orcamentoDescontos
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrcamentosDescontos $orcamentoDescontos)
    {
        //
    }
}
