<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\Fluxocaixa;
use Illuminate\Http\Request;
use Session;

class FluxocaixaController extends Controller
{
    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $dia = $request->get('dia');

        $fluxocaixa  = DB::select(DB::raw('CALL fn_fluxodecaixa(?)'),[$dia]);

        return view('financeiro.fluxocaixa.index', compact('fluxocaixa'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function indexz(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $fluxocaixa = Fluxocaixa::where('descricao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $fluxocaixa = Fluxocaixa::latest()->paginate($perPage);
        }

        return view('financeiro.fluxocaixa.index', compact('fluxocaixa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('financeiro.fluxocaixa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Fluxocaixa::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/fluxocaixa');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $fluxocaixa = Fluxocaixa::findOrFail($id);

        return view('financeiro.fluxocaixa.show', compact('fluxocaixa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $fluxocaixa = Fluxocaixa::findOrFail($id);

        return view('financeiro.fluxocaixa.edit', compact('fluxocaixa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $fluxocaixa = Fluxocaixa::findOrFail($id);
        $fluxocaixa->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/fluxocaixa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Fluxocaixa::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('financeiro/fluxocaixa');
    }
}
