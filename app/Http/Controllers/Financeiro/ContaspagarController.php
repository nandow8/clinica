<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Contaspagar;
use App\Models\Fornecedor;
use Illuminate\Http\Request;
use Session;

class ContaspagarController extends Controller
{
    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $contaspagar = Contaspagar::where('fornecedor_id', 'LIKE', "%$keyword%")
                ->orWhere('vencimento', 'LIKE', "%$keyword%")
                ->orWhere('pagamento', 'LIKE', "%$keyword%")
                ->orWhere('valor', 'LIKE', "%$keyword%")
                ->orWhere('numerodocumento', 'LIKE', "%$keyword%")
                ->orWhere('numero', 'LIKE', "%$keyword%")
                ->orWhere('descricao', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $contaspagar = Contaspagar::latest()->paginate($perPage);
        }

        return view('financeiro.contaspagar.index', compact('contaspagar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
      $fornecedores = Fornecedor::all();
      return view('financeiro.contaspagar.create', compact('fornecedores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Contaspagar::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/contaspagar');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $contaspagar = Contaspagar::findOrFail($id);
        $fornecedores = Fornecedor::all();

        return view('financeiro.contaspagar.show', compact('contaspagar', 'fornecedores'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $contaspagar = Contaspagar::findOrFail($id);
        $fornecedores = Fornecedor::all();

        return view('financeiro.contaspagar.edit', compact('contaspagar', 'fornecedores'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $contaspagar = Contaspagar::findOrFail($id);
        $contaspagar->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/contaspagar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Contaspagar::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('financeiro/contaspagar');
    }
}
