<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Contasreceber;
use App\Models\Clinica;
use App\Models\Paciente;
use App\Models\TabelaServico;
use App\Models\Examesrf;
use App\Models\ContasreceberDetalhe;
use App\Models\ContasreceberDescontos;
use App\Models\TabelaDescontoAcrescimo;
use App\Models\Contrato;
use App\Models\TiposPagamento;
use Illuminate\Http\Request;
use Session;
use PDF;

class ContasreceberController extends Controller {

    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $pacientes = Paciente::all();
        $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();

        $keywordtipopagto = $request->get('schtipopagto');
        $keywordclienteid = $request->get('schclienteid');
        $keywordpordata = $request->get('schpordata');
        $keyworddataini = $request->get('schdataini');
        $keyworddatafim = $request->get('schdatafim');
        $keywordstatus = $request->get('schstatus');
        
        if (empty($keyworddataini)){
            $keyworddataini = date('Y/m') . '/01';
        }
        
        if (empty($keyworddatafim)){
            $keyworddatafim = date('Y/m/t');
        }

        $keyword = $request->get('search');
        $perPage = 10;

        $Sql = [];
        $query = '';

        if (!empty($keywordtipopagto))
            array_push($Sql, "tipopagto_id IN(".implode(',',$keywordtipopagto).")");

        if (!empty($keywordclienteid))
            array_push($Sql, "paciente_id = $keywordclienteid");
        
        if($keywordpordata == 'vecto'){
            if (!empty($keyworddataini) && !empty($keyworddatafim))
                array_push($Sql, "vencimento between '$keyworddataini 00:00:00' and '$keyworddatafim 23:59:59'");
        }else{
            if (!empty($keyworddataini) && !empty($keyworddatafim))
                array_push($Sql, "pagamento between '$keyworddataini 00:00:00' and '$keyworddatafim 23:59:59'");
        }
        if (!empty($keywordstatus))
            array_push($Sql, "status LIKE '%$keywordstatus%'");

        foreach ($Sql as $item) {
            if ($item == end($Sql)) {
                $query = $query . ' ' . $item;
            } else {
                $query = $query . ' ' . $item . ' and';
            }
        }

        if (!empty($query)) {
            $contasreceber = DB::table('contasrecebers')->select("*")
                    ->whereRaw("$query")
                    ->orderBy("id", "desc")
                    ->latest()
                    ->paginate($perPage);
        } else {
            $contasreceber = Contasreceber::orderBy("id", "desc")->latest()->paginate($perPage);
        }
        
        return view('financeiro.contasreceber.index', compact('contasreceber', 'pacientes', 'tipospagamentos', 'keywordtipopagto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $pacientes = Paciente::all();
        $servicos = TabelaServico::all();
        $detalhes = ContasreceberDetalhe::all();
        $descontos = ContasreceberDescontos::all();
        $tabeladescontos = TabelaDescontoAcrescimo::where('tipo', '=', 'Desconto')->where('status', '=', 'Ativo')->get();
        $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();

        return view('financeiro.contasreceber.create', compact('pacientes', 'servicos', 'detalhes', 'descontos', 'tabeladescontos', 'tipospagamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function createorcamento() {
        $pacientes = Paciente::all();
        $servicos = TabelaServico::all();
        $detalhes = ContasreceberDetalhe::all();
        $descontos = ContasreceberDescontos::all();
        $tabeladescontos = TabelaDescontoAcrescimo::where('tipo', '=', 'Desconto')->where('status', '=', 'Ativo')->get();
        $createorcamento = 'Orçamento';

        return view('financeiro.contasreceber.create', compact('pacientes', 'servicos', 'detalhes', 'descontos', 'tabeladescontos', 'createorcamento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $requestData = $request->all();

        $novoreceber = Contasreceber::create($requestData);

        /* Inicia a verificação dos detalhes do receber */
        if (isset($requestData['id_detalhe'])) {
            // Insiro os novos Registros.
            foreach ($requestData['id_detalhe'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $contasreceberdetalhe = new ContasreceberDetalhe;
                    $contasreceberdetalhe->contasreceber_id = $novoreceber->id;
                    $contasreceberdetalhe->tabela_servico_id = $requestData['tabela_servico_id_detalhe'][$k];
                    $contasreceberdetalhe->descricao = $requestData['descricao_detalhe'][$k];
                    $contasreceberdetalhe->valor = $requestData['valor_detalhe'][$k];
                    
                    $mneumonico = $requestData['descricao_detalhe'][$k];
                    $mneumonico = explode(') ', $mneumonico);
                    $mneumonico = $mneumonico[0];
                    $mneumonico = str_replace('(','',$mneumonico);
                    
                    $valorpago = DB::select("SELECT preco "
                                           ."FROM examesrf WHERE id = ". $requestData['tabela_servico_id_detalhe'][$k]. " "
                                           ."AND mneumonico = '$mneumonico' ");

                    $contasreceberdetalhe->origem = $requestData['origem_detalhe'][$k];
                    $contasreceberdetalhe->valorpago = $valorpago[0]->preco ?? 0;
                    $contasreceberdetalhe->save();
                }
            }
        }
        /* Finaliza a verificação dos detalhes do receber */

        /* Inicia a verificação dos descontos do receber */
        if (isset($requestData['id_desconto'])) {
            // Insiro os novos Registros.
            foreach ($requestData['id_desconto'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $contasreceberdesconto = new ContasreceberDescontos;
                    $contasreceberdesconto->contasreceber_id = $novoreceber->id;
                    $contasreceberdesconto->tabela_descacre_id = $requestData['tabela_descacre_id_desconto'][$k];
                    $contasreceberdesconto->descricao = $requestData['descricao_desconto'][$k];
                    $contasreceberdesconto->valor = $requestData['valor_desconto'][$k];
                    $contasreceberdesconto->save();
                }
            }
        }
        /* Finaliza a verificação dos descontos do receber */
       
        /* Update de campos */
        DB::update("update contasrecebers set numerodocumento = ?, numero = ? where id = ".$novoreceber['id'], [(1000 + $novoreceber['id']), 1]) ;
        
        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/contasreceber');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        return $this->edit($id);
//        $contasreceber = Contasreceber::findOrFail($id);
//
//        return view('financeiro.contasreceber.show', compact('contasreceber'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $contasreceber = Contasreceber::findOrFail($id);
        $pacientes = Paciente::all();
        $servicos = TabelaServico::all();
        $detalhes = ContasreceberDetalhe::all();
        $descontos = ContasreceberDescontos::all();
        $tabeladescontos = TabelaDescontoAcrescimo::where('tipo', '=', 'Desconto')->where('status', '=', 'Ativo')->get();
        $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();
        
        return view('financeiro.contasreceber.edit', compact('contasreceber', 'pacientes', 'servicos', 'detalhes', 'descontos', 'tabeladescontos', 'tipospagamentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        if ($requestData['numerodocumento'] === null)
            $requestData['numerodocumento'] = (1000 + $requestData['id']);
        if ($requestData['numero'] === null)
            $requestData['numero'] = 1;

        $contasreceber = Contasreceber::findOrFail($id);
        $contasreceber->update($requestData);

        /* Inicia a verificação dos detalhes do receber */
        if (!isset($requestData['id_detalhe'])) {
            // Verifico se existe apenas um detalhe para excluir
            foreach (ContasreceberDetalhe::where('contasreceber_id', '=', $id)->get() as $k => $f) {
                $contasreceberdetalhe = ContasreceberDetalhe::findOrFail($f->id);
                $contasreceberdetalhe->deleted_at = date('Y-m-d H:i:s');
                $contasreceberdetalhe->save();
            }
        } elseif (isset($requestData['id_detalhe'])) {
            // Verifico se houveram registros deletados.
            $deletados = ContasreceberDetalhe::where('contasreceber_id', $requestData['id'])->whereNotIn('id', $requestData['id_detalhe'])->get();

            // Deleto os registros
            foreach ($deletados as $k => $r) {
                $contasreceberdetalhe = ContasreceberDetalhe::findOrFail($r->id);
                $contasreceberdetalhe->deleted_at = date('Y-m-d H:i:s');
                $contasreceberdetalhe->save();
            }

            // Insiro os novos Registros.
            foreach ($requestData['id_detalhe'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $contasreceberdetalhe = new ContasreceberDetalhe;
                    $contasreceberdetalhe->contasreceber_id = $requestData['contasreceber_id_detalhe'][$k];
                    $contasreceberdetalhe->tabela_servico_id = $requestData['tabela_servico_id_detalhe'][$k];
                    $contasreceberdetalhe->descricao = $requestData['descricao_detalhe'][$k];
                    $contasreceberdetalhe->valor = $requestData['valor_detalhe'][$k];

                    $mneumonico = $requestData['descricao_detalhe'][$k];
                    $mneumonico = explode(') ', $mneumonico);
                    $mneumonico = $mneumonico[0];
                    $mneumonico = str_replace('(','',$mneumonico);
                    
                    $valorpago = DB::select("SELECT preco "
                                           ."FROM examesrf WHERE id = ". $requestData['tabela_servico_id_detalhe'][$k]. " "
                                           ."AND mneumonico = '$mneumonico' ");
                    
                    $contasreceberdetalhe->origem = $requestData['origem_detalhe'][$k];
                    $contasreceberdetalhe->valorpago = $valorpago[0]->preco ?? 0;                  
                    $contasreceberdetalhe->save();
                }
            }
        }
        /* Finaliza a verificação dos detalhes do receber */

        /* Inicia a verificação dos descontos do receber */
        if (!isset($requestData['id_desconto'])) {
            // Verifico se existe apenas um desconto para excluir
            foreach (ContasreceberDescontos::where('contasreceber_id', '=', $id)->get() as $k => $f) {
                $contasreceberdesconto = ContasreceberDetalhe::findOrFail($f->id);
                $contasreceberdesconto->deleted_at = date('Y-m-d H:i:s');
                $contasreceberdesconto->save();
            }
        } elseif (isset($requestData['id_desconto'])) {
            // Verifico se houveram registros deletados.
            $deletados = ContasreceberDescontos::where('contasreceber_id', $requestData['id'])->whereNotIn('id', $requestData['id_desconto'])->get();

            // Deleto os registros
            foreach ($deletados as $k => $r) {
                $contasreceberdesconto = ContasreceberDescontos::findOrFail($r->id);
                $contasreceberdesconto->deleted_at = date('Y-m-d H:i:s');
                $contasreceberdesconto->save();
            }

            // Insiro os novos Registros.
            foreach ($requestData['id_desconto'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $contasreceberdesconto = new ContasreceberDescontos;
                    $contasreceberdesconto->contasreceber_id = $requestData['contasreceber_id_desconto'][$k];
                    $contasreceberdesconto->tabela_descacre_id = $requestData['tabela_descacre_id_desconto'][$k];
                    $contasreceberdesconto->descricao = $requestData['descricao_desconto'][$k];
                    $contasreceberdesconto->valor = $requestData['valor_desconto'][$k];
                    $contasreceberdesconto->save();
                }
            }
        }
        /* Finaliza a verificação dos desconto do receber */

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/contasreceber');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Contasreceber::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('financeiro/contasreceber');
    }

    /**
     * Show the form for boleting the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function boleto($id) {
        $contasreceber = Contasreceber::findOrFail($id);

        if (isset($contasreceber) && isset($contasreceber->clinica_id) && isset($contasreceber->paciente_id)) {

            $clinica = Clinica::findOrFail($contasreceber->clinica_id);
            $paciente = Paciente::findOrFail($contasreceber->paciente_id);
            $detalhes = ContasreceberDetalhe::where('contasreceber_id', '=', $contasreceber->id)->get();

            return view('financeiro.contasreceber.boleto', compact('contasreceber', 'clinica', 'paciente', 'detalhes'));
        } else {
            return 'Não disponível';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return array $servico, informações cadastradas referente ao serviço.
     */
    public function retornadadosservico($id) {
        $servico = TabelaServico::findOrFail($id);

        return compact('servico');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return array $servico, informações cadastradas referente ao serviço.
     */
    public function retornadadosdescacre($id) {
        $descacre = TabelaDescontoAcrescimo::findOrFail($id);

        return compact('descacre');
    }

    /**
     * Show the form for boleting the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function wsboleto($id) {
        $contasreceber = Contasreceber::findOrFail($id);

        if (isset($contasreceber) && isset($contasreceber->clinica_id) && isset($contasreceber->paciente_id)) {

            $clinica = Clinica::findOrFail($contasreceber->clinica_id);
            $paciente = Paciente::findOrFail($contasreceber->paciente_id);
            $detalhes = ContasreceberDetalhe::where('contasreceber_id', '=', $contasreceber->id)->get();

            return view('financeiro.contasreceber.wssantander.boleto', compact('contasreceber', 'clinica', 'paciente', 'detalhes'));
        } else {
            return 'Não disponível';
        }
    }

    public function ajaxexibirservicoexamerf($str) {

        $tabserv = TabelaServico::select('id', 'descricao as nome', 'valor as venda', 'origem')
                        ->where('descricao', 'LIKE', "%$str%")
                        ->limit(10)->get();

        $examerf = Examesrf::select('id', 'mneumonico', 'codigo', 'nome', 'venda', 'origem')
                        ->where('mneumonico', 'LIKE', "%$str%")
                        ->orwhere('codigo', 'LIKE', "%$str%")
                        ->orWhere('nome', 'LIKE', "%$str%")->limit(10)->get();

        $results = $tabserv->union($examerf);

        foreach ($results as $key => $res) {
            $result[] = $res->mneumonico . ' --- ' . $res->codigo . ' --- ' . $res->nome . ' --- ' . number_format($res->venda, 2, ',', '.') . ' --- ' . $res->id . ' --- ' . $res->origem;
        }

        echo json_encode($result);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF($id) {
        $contasreceber = Contasreceber::findOrFail($id);
        $pacientes = Paciente::where('id', '=', $contasreceber->paciente_id)->get();
        $detalhes = ContasreceberDetalhe::where('contasreceber_id', '=', $contasreceber->id)->get();
        $descontos = ContasreceberDescontos::where('contasreceber_id', '=', $contasreceber->id)->get();

        $data = [
            'title' => 'Contas a Receber',
            'receber' => $contasreceber,
            'paciente' => $pacientes,
            'detalhe' => $detalhes,
            'desconto' => $descontos
        ];

        //return view('financeiro.contasreceber.receberPDF', $data);

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);

        $pdf = PDF::loadView('financeiro.contasreceber.receberPDF', $data);

        return $pdf->download('Receber-' . $id . '.pdf');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function remessa($str, Request $request) {
        $perPage = 10;
        $keyword = $request->get('search');
        
        $dados = DB::table('contasrecebers')->select("*")
                ->whereRaw("tipopagto = 'BO' and retorno IS NULL and deleted_at is NULL")
                ->orderBy("id", "desc")
                ->latest()
                ->paginate($perPage);
        
        return view('financeiro.contasreceber.remessa', compact('dados', 'request'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function marcarremessaajax($id, $param, Request $request) {

        
        if($id == -1){
            DB::update("update contasrecebers set remessa = 'BX' WHERE tipopagto = 'BO' and remessa = 'CH' ");
        }elseif($id == 0){
            $var = DB::select("SELECT MAX(remessa) as remessa FROM contasrecebers WHERE tipopagto = 'BO' and (remessa IN ('CH','RM') OR remessa IS NULL)");
            
            $var = $var[0]->remessa;
            
            if ($var === null){
                DB::update("update contasrecebers set remessa = 'CH' WHERE tipopagto = 'BO' and remessa <> 'BX' ");
            }elseif ($var === 'CH'){
                DB::update("update contasrecebers set remessa = 'RM' WHERE tipopagto = 'BO' and remessa <> 'BX' ");
            }elseif ($var === 'RM'){
                DB::select("update contasrecebers set remessa = null WHERE tipopagto = 'BO' and remessa <> 'BX'");
            }            
        }else{
            $var = DB::update("update contasrecebers set remessa = $param where id = $id");
        }
        return $this->remessa('envio', $request);
    }
    
    public function contratogerareceber($id, $parc, $param) {
        
        $contrato = Contrato::findOrFail($id);
        date_default_timezone_set('America/Sao_Paulo');        
        
        
        for( $i = 1;  $i <= $parc; $i++ ){
            
            $data = date('Y-m-d', strtotime("$i month", strtotime($contrato->data)));

            if ($i == 1 && $param == '"sim"'){
                /* Gero apenas a da adesão se for separada */
                $receber = [
                    'id'                => 0,
                    'created_at'        => date('Y-m-d H:i:s'),
                    'updated_at'        => date('Y-m-d H:i:s'),
                    'deleted_at'        => null,
                    'paciente_id'       => $contrato->paciente_id,
                    'vencimento'        => $data,
                    'pagamento'         => $data,
                    'valor'             => $contrato->valoradesao,
                    'numero'            => $i,
                    'numerodocumento'   => 0,
                    'descricao'         => 'Boleto gerado automaticamente pelo Contrato ' . $contrato->id . '. Adesão',
                    'status'            => 'Aberto',
                    'clinica_id'        => 1,
                    'tipopagto'         => $contrato->tipopagto,
                    'orcamento'         => null,
                    'remessa'           => null,
                    'retorno'           => null
                ];

                /* Atualizo o número do documento */
                $novoreceber = Contasreceber::create($receber);
                DB::update("update contasrecebers set numerodocumento = ? where id = ".$novoreceber['id'], [(1000 + $novoreceber['id'])]) ;

                /* Gero detalhe do contas a receber */
                DB::insert("insert into contasreceber_detalhes(contasreceber_id, tabela_servico_id, descricao, valor) values (?,?,?,?)", [$novoreceber['id'], 1, 'Boleto de Adesão', $contrato->valoradesao]) ;
                
            }
            
            $valor = 0;
            
            if($i == 1 && $param == '"nao"'){
                $valor = ($contrato->valormensalidade + $contrato->valoradesao);
            }elseif($i > 1){
                $valor = $contrato->valormensalidade;
            }
            
            $receber = [
                'id'                => 0,
                'created_at'        => date('Y-m-d H:i:s'),
                'updated_at'        => date('Y-m-d H:i:s'),
                'deleted_at'        => null,
                'paciente_id'       => $contrato->paciente_id,
                'vencimento'        => $data,
                'pagamento'         => $data,
                'valor'             => $valor,
                'numero'            => $i,
                'numerodocumento'   => 0,
                'descricao'         => 'Boleto gerado automaticamente por contrato',
                'status'            => 'Aberto',
                'clinica_id'        => 1,
                'tipopagto'         => $contrato->tipopagto,
                'orcamento'         => null,
                'remessa'           => null,
                'retorno'           => null
            ];
            
            /* Atualizo o número do documento */
            $novoreceber = Contasreceber::create($receber);
            DB::update("update contasrecebers set numerodocumento = ? where id = ".$novoreceber['id'], [(1000 + $novoreceber['id'])]) ;

            /* Gero detalhe do contas a receber */
            DB::insert("insert into contasreceber_detalhes(contasreceber_id, tabela_servico_id, descricao, valor) values (?,?,?,?)", [$novoreceber['id'], 1, 'Boleto', $contrato->valormensalidade]) ;
            
            if ($i == 1){
                DB::insert("insert into contasreceber_detalhes(contasreceber_id, tabela_servico_id, descricao, valor) values (?,?,?,?)", [$novoreceber['id'], 1, 'Adesao', $contrato->valoradesao]) ;
            }
        }
        
        /* Setar contrato como gerado receber */
        $res = DB::update("update contratos set gerado = 'Sim' where id = ".$id) ;
        
        /* Finaliza a verificação dos desconto do receber */

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/contasreceber');        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function retorno($str, Request $request) {
        $perPage = 10;
        $keyword = $request->get('search');
        
        $dados = DB::table('contasrecebers')->select("*")
                ->whereRaw("tipopagto = 'BO' and retorno IS NULL and deleted_at is NULL")
                ->orderBy("id", "desc")
                ->latest()
                ->paginate($perPage);
        
        return view('financeiro.contasreceber.retorno', compact('dados', 'request'));
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload(Request $request){

        $file =  $request->file('arqretorno');
                
        if(empty($file)){
            abort(404,"Erro ao carregar arquivo");
        }
        
        $path = $file->store('uploads');

        $retorno = \Eduardokum\LaravelBoleto\Cnab\Retorno\Factory::make($file);

        $retorno->processar();
        
        $retorno = $retorno->getDetalhes();

        foreach($retorno as $item){
            
            DB::update("update contasrecebers set "
                    . "retcarteira = ?,"
                    . "retnossonumero = ?,"
                    . "retnumerodocumento = ?,"
                    . "retnumerocontrole = ?,"
                    . "retocorrencia = ?,"
                    . "retocorrenciatipo = ?,"
                    . "retocorrenciadescricao  = ?,"
                    . "retdataocorrencia = ?,"
                    . "retdatavencimento = ?,"
                    . "retdatacredito = ?,"
                    . "retvalor = ?,"
                    . "retvalortarifa = ?,"
                    . "retvaloriof = ?,"
                    . "retvalorabatimento = ?,"
                    . "retvalordesconto = ?,"
                    . "retvalorrecebido = ?,"
                    . "retvalormora = ?,"
                    . "retvalormulta = ?,"
                    . "reterror = ?,"
                    . "remessa = ?,"
                    . "retorno = ?"
                . " where retorno IS NULL and numerodocumento = '$item->numeroDocumento'", 
                [
                    $item->carteira,
                    $item->nossoNumero,
                    $item->numeroDocumento,
                    $item->numeroControle,
                    $item->ocorrencia,
                    $item->ocorrenciaTipo,
                    $item->ocorrenciaDescricao,
                    $item->dataOcorrencia,
                    $item->dataVencimento,
                    $item->dataCredito,
                    $item->valor,
                    $item->valorTarifa,
                    $item->valorIOF,
                    $item->valorAbatimento,
                    $item->valorDesconto,
                    $item->valorRecebido,
                    $item->valorMora,
                    $item->valorMulta,
                    $item->error,
                    isset($item->error) ? 'BX' : 'ER',
                    'OK'
                ]) ;
        }


        return view('financeiro.contasreceber.retorno', compact('retorno'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function ajaxindexnomepaciente($str, $cob = null, $tip = null ) {

        $Sql = [];
        $query = '';
        
        if (is_numeric($str))
            array_push($Sql, "numerocarteira LIKE '%$str%'");
        else
            array_push($Sql, "nome LIKE '%$str%'");
        
        if ($cob != '*')
            array_push($Sql, "COALESCE(tipocobranca, 0) = $cob");

        if ($tip != '*')
            array_push($Sql, "COALESCE(tipopaciente, 0) = $tip");

        foreach ($Sql as $item) {
            if ($item == end($Sql)) {
                $query = $query . ' ' . $item;
            } else {
                $query = $query . ' ' . $item . ' AND';
            }
        }
        
        $query = $query . ' AND deleted_at IS NULL';


        $result = [];
        
        if (!empty($query)) {
            $pacientespesq = DB::select("SELECT id, COALESCE(numerocarteira, 'N/I') AS numerocarteira, nome, COALESCE(tipocobranca, 0) AS tipocobranca, COALESCE(tipopaciente, 0) AS tipopaciente "
                    . "FROM pacientes WHERE $query ORDER BY id DESC");
            
            $tipocobranca = '';
            $tipopaciente = '';
            
            foreach ($pacientespesq as $key => $res) {
                
                if($res->tipocobranca == 0){
                    $tipocobranca = '---' . 'Contrato';

                    if($res->tipopaciente == 0){
                        $tipopaciente = '---' . 'Dependente';
                    }else{
                        $tipopaciente = '---' . 'Titular';
                    }
                    
                }else{
                    $tipocobranca = '---' . 'Particular';
                }

                
                $result[] = $res->numerocarteira . '---' . $res->nome . $tipocobranca . $tipopaciente ;
            }
        }
        
        echo json_encode($result);
    }


    
}
