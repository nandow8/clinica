<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Contasreceber;
use App\Models\Orcamentos;
use App\Models\Clinica;
use App\Models\Paciente;
use App\Models\TabelaServico;
use App\Models\Examesrf;
use App\Models\OrcamentosDetalhes;
use App\Models\OrcamentosDescontos;
use App\Models\TabelaDescontoAcrescimo;
use App\Models\TiposPagamento;
use Illuminate\Http\Request;
use Session;
use PDF;

class OrcamentosController extends Controller {

    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request) {
        $pacientes = Paciente::all();
        $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();

        $keywordtipopagto = $request->get('schtipopagto');
        $keywordclienteid = $request->get('schclienteid');
        $keywordvencimentoa = $request->get('schvencimentoa');
        $keywordvencimentob = $request->get('schvencimentob');
        $keywordpagamentoa = $request->get('schpagamentoa');
        $keywordpagamentob = $request->get('schpagamentob');
        $keywordstatus = $request->get('schstatus');
        
        if (empty($keywordvencimentoa)){
            $keywordvencimentoa = date('Y/m') . '/01';
        }
        
        if (empty($keywordvencimentob)){
            $keywordvencimentob = date('Y/m/t');
        }

        $keyword = $request->get('search');
        $perPage = 10;

        $Sql = [];
        $query = '';

        if (!empty($keywordtipopagto))
            array_push($Sql, "tipopagto_id IN(".implode(',',$keywordtipopagto).")");

        if (!empty($keywordclienteid))
            array_push($Sql, "paciente_id = $keywordclienteid");

        if (!empty($keywordvencimentoa) && !empty($keywordvencimentob))
            array_push($Sql, "vencimento between '$keywordvencimentoa 00:00:00' and '$keywordvencimentob 23:59:59'");

        if (!empty($keywordpagamentoa) && !empty($keywordpagamentob))
            array_push($Sql, "pagamento between '$keywordpagamentoa 00:00:00' and '$keywordpagamentob 23:59:59'");

        if (!empty($keywordstatus))
            array_push($Sql, "status LIKE '%$keywordstatus%'");

        foreach ($Sql as $item) {
            if ($item == end($Sql)) {
                $query = $query . ' ' . $item;
            } else {
                $query = $query . ' ' . $item . ' and';
            }
        }

        if (!empty($query)) {
            $orcamento = DB::table('orcamentos')->select("*")
                    ->whereRaw("$query")
                    ->orderBy("id", "desc")
                    ->latest()
                    ->paginate($perPage);
        } else {
            $orcamento = Orcamentos::orderBy("id", "desc")->latest()->paginate($perPage);
        }
        
        return view('financeiro.orcamento.index', compact('orcamento', 'pacientes', 'tipospagamentos', 'keywordtipopagto'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        $pacientes = Paciente::all();
        $detalhes = OrcamentosDetalhes::all();
        $servicos = TabelaServico::all();
        $descontos = OrcamentosDescontos::all();
        $tabeladescontos = TabelaDescontoAcrescimo::where('tipo', '=', 'Desconto')->where('status', '=', 'Ativo')->get();
        $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();

        return view('financeiro.orcamento.create', compact('pacientes', 'servicos', 'detalhes', 'descontos', 'tabeladescontos', 'tipospagamentos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function createorcamento() {
        $pacientes = Paciente::all();
        $servicos = TabelaServico::all();
        $detalhes = OrcamentosDetalhes::all();
        $descontos = OrcamentosDescontos::all();
        $tabeladescontos = TabelaDescontoAcrescimo::where('tipo', '=', 'Desconto')->where('status', '=', 'Ativo')->get();
        $createorcamento = 'Orçamento';
        $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();

        return view('financeiro.orcamento.create', compact('pacientes', 'servicos', 'detalhes', 'descontos', 'tabeladescontos', 'createorcamento', 'tipospagamentos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request) {
        $requestData = $request->all();

        $novoreceber = Orcamentos::create($requestData);

        /* Inicia a verificação dos detalhes do receber */
        if (isset($requestData['id_detalhe'])) {
            // Insiro os novos Registros.
            foreach ($requestData['id_detalhe'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $orcamentodetalhe = new OrcamentosDetalhes;
                    $orcamentodetalhe->orcamento_id = $novoreceber->id;
                    $orcamentodetalhe->tabela_servico_id = $requestData['tabela_servico_id_detalhe'][$k];
                    $orcamentodetalhe->descricao = $requestData['descricao_detalhe'][$k];
                    $orcamentodetalhe->valor = $requestData['valor_detalhe'][$k];
                    $orcamentodetalhe->save();
                }
            }
        }
        /* Finaliza a verificação dos detalhes do receber */

        /* Inicia a verificação dos descontos do receber */
        if (isset($requestData['id_desconto'])) {
            // Insiro os novos Registros.
            foreach ($requestData['id_desconto'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $orcamentodesconto = new OrcamentosDescontos;
                    $orcamentodesconto->orcamento_id = $novoreceber->id;
                    $orcamentodesconto->tabela_descacre_id = $requestData['tabela_descacre_id_desconto'][$k];
                    $orcamentodesconto->descricao = $requestData['descricao_desconto'][$k];
                    $orcamentodesconto->valor = $requestData['valor_desconto'][$k];
                    $orcamentodesconto->save();
                }
            }
        }
        /* Finaliza a verificação dos descontos do receber */
       
        /* Update de campos */
        DB::update("update orcamentos set numerodocumento = ?, numero = ? where id = ".$novoreceber['id'], [(1000 + $novoreceber['id']), 1]) ;
        
        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/orcamento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id) {
        return $this->edit($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id) {
        $orcamento = Orcamentos::findOrFail($id);
        $pacientes = Paciente::all();
        $servicos = TabelaServico::all();
        $detalhes = OrcamentosDetalhes::all();
        $descontos = OrcamentosDescontos::all();
        $tabeladescontos = TabelaDescontoAcrescimo::where('tipo', '=', 'Desconto')->where('status', '=', 'Ativo')->get();
        $tipospagamentos = TiposPagamento::where('status', '=', 'Ativo')->get();
        
        return view('financeiro.orcamento.edit', compact('orcamento', 'pacientes', 'servicos', 'detalhes', 'descontos', 'tabeladescontos', 'tipospagamentos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id) {

        $requestData = $request->all();

        if ($requestData['numerodocumento'] === null)
            $requestData['numerodocumento'] = (1000 + $requestData['id']);
        if ($requestData['numero'] === null)
            $requestData['numero'] = 1;

        $orcamento = Orcamentos::findOrFail($id);
        $orcamento->update($requestData);

        /* Inicia a verificação dos detalhes do receber */
        if (!isset($requestData['id_detalhe'])) {
            // Verifico se existe apenas um detalhe para excluir
            foreach (OrcamentosDetalhes::where('orcamento_id', '=', $id)->get() as $k => $f) {
                $orcamentodetalhe = OrcamentosDetalhes::findOrFail($f->id);
                $orcamentodetalhe->deleted_at = date('Y-m-d H:i:s');
                $orcamentodetalhe->save();
            }
        } elseif (isset($requestData['id_detalhe'])) {
            // Verifico se houveram registros deletados.
            $deletados = OrcamentosDetalhes::where('orcamento_id', $requestData['id'])->whereNotIn('id', $requestData['id_detalhe'])->get();

            // Deleto os registros
            foreach ($deletados as $k => $r) {
                $orcamentodetalhe = OrcamentosDetalhes::findOrFail($r->id);
                $orcamentodetalhe->deleted_at = date('Y-m-d H:i:s');
                $orcamentodetalhe->save();
            }

            // Insiro os novos Registros.
            foreach ($requestData['id_detalhe'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $orcamentodetalhe = new OrcamentosDetalhes;
                    $orcamentodetalhe->orcamento_id = $requestData['orcamento_id_detalhe'][$k];
                    $orcamentodetalhe->tabela_servico_id = $requestData['tabela_servico_id_detalhe'][$k];
                    $orcamentodetalhe->descricao = $requestData['descricao_detalhe'][$k];
                    $orcamentodetalhe->valor = $requestData['valor_detalhe'][$k];
                    $orcamentodetalhe->save();
                }
            }
        }
        /* Finaliza a verificação dos detalhes do receber */

        /* Inicia a verificação dos descontos do receber */
        if (!isset($requestData['id_desconto'])) {
            // Verifico se existe apenas um desconto para excluir
            foreach (OrcamentosDescontos::where('orcamento_id', '=', $id)->get() as $k => $f) {
                $orcamentodesconto = OrcamentosDetalhes::findOrFail($f->id);
                $orcamentodesconto->deleted_at = date('Y-m-d H:i:s');
                $orcamentodesconto->save();
            }
        } elseif (isset($requestData['id_desconto'])) {
            // Verifico se houveram registros deletados.
            $deletados = OrcamentosDescontos::where('orcamento_id', $requestData['id'])->whereNotIn('id', $requestData['id_desconto'])->get();

            // Deleto os registros
            foreach ($deletados as $k => $r) {
                $orcamentodesconto = OrcamentosDescontos::findOrFail($r->id);
                $orcamentodesconto->deleted_at = date('Y-m-d H:i:s');
                $orcamentodesconto->save();
            }

            // Insiro os novos Registros.
            foreach ($requestData['id_desconto'] as $k => $v) {
                // Seleciono apenas os itens novos para gravar
                if ($v == 0) {
                    $orcamentodesconto = new OrcamentosDescontos;
                    $orcamentodesconto->orcamento_id = $requestData['orcamento_id_desconto'][$k];
                    $orcamentodesconto->tabela_descacre_id = $requestData['tabela_descacre_id_desconto'][$k];
                    $orcamentodesconto->descricao = $requestData['descricao_desconto'][$k];
                    $orcamentodesconto->valor = $requestData['valor_desconto'][$k];
                    $orcamentodesconto->save();
                }
            }
        }
        /* Finaliza a verificação dos desconto do receber */

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/orcamento');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id) {
        Orcamentos::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('financeiro/orcamento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return array $servico, informações cadastradas referente ao serviço.
     */
    public function retornadadosdescacre($id) {
        $descacre = TabelaDescontoAcrescimo::findOrFail($id);

        return compact('descacre');
    }

    /**
     * Show the form for boleting the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function ajaxexibirservicoexamerf($str) {

        $tabserv = TabelaServico::select('id', 'descricao as nome', 'valor as venda')
                        ->where('descricao', 'LIKE', "%$str%")
                        ->limit(10)->get();

        $examerf = Examesrf::select('id', 'mneumonico', 'codigo', 'nome', 'venda')
                        ->where('mneumonico', 'LIKE', "%$str%")
                        ->orwhere('codigo', 'LIKE', "%$str%")
                        ->orWhere('nome', 'LIKE', "%$str%")->limit(10)->get();

        $results = $tabserv->union($examerf);

        foreach ($results as $key => $res) {
            $result[] = $res->mneumonico . ' --- ' . $res->codigo . ' --- ' . $res->nome . ' --- ' . number_format($res->venda, 2, ',', '.') . ' --- ' . $res->id;
        }

        echo json_encode($result);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generatePDF($id) {
        $orcamento = Orcamentos::findOrFail($id);
        $pacientes = Paciente::where('id', '=', $orcamento->paciente_id)->get();
        $detalhes = OrcamentosDetalhes::where('orcamento_id', '=', $orcamento->id)->get();
        $descontos = OrcamentosDescontos::where('orcamento_id', '=', $orcamento->id)->get();

        $data = [
            'title' => 'Contas a Receber',
            'receber' => $orcamento,
            'paciente' => $pacientes,
            'detalhe' => $detalhes,
            'desconto' => $descontos
        ];

        //return view('financeiro.orcamento.receberPDF', $data);

        PDF::setOptions(['dpi' => 150, 'defaultFont' => 'sans-serif']);

        $pdf = PDF::loadView('financeiro.orcamento.receberPDF', $data);

        return $pdf->download('Receber-' . $id . '.pdf');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function ajaxindexnomepaciente($str, $cob = null, $tip = null ) {

        $Sql = [];
        $query = '';

        if (!empty($str))
            array_push($Sql, "nome LIKE '%$str%'");

        if ($cob != '*')
            array_push($Sql, "COALESCE(tipocobranca, 0) = $cob");

        if ($tip != '*')
            array_push($Sql, "COALESCE(tipopaciente, 0) = $tip");

        foreach ($Sql as $item) {
            if ($item == end($Sql)) {
                $query = $query . ' ' . $item;
            } else {
                $query = $query . ' ' . $item . ' and';
            }
        }

        $result = [];
        
        if (!empty($query)) {
            $pacientespesq = DB::select("SELECT id, nome, COALESCE(tipocobranca, 0) as tipocobranca, COALESCE(tipopaciente, 0) as tipopaciente "
                    . "FROM pacientes WHERE $query ORDER BY id DESC");
            
            $tipocobranca = '';
            $tipopaciente = '';
            
            foreach ($pacientespesq as $key => $res) {
                
                if($res->tipocobranca == 0){
                    $tipocobranca = '---' . 'Contrato';

                    if($res->tipopaciente == 0){
                        $tipopaciente = '---' . 'Dependente';
                    }else{
                        $tipopaciente = '---' . 'Titular';
                    }
                    
                }else{
                    $tipocobranca = '---' . 'Particular';
                }

                
                $result[] = $res->id . '---' . $res->nome . $tipocobranca . $tipopaciente ;
            }
        }

        echo json_encode($result);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function exportarReceber($id) {
        $orcamento = Orcamentos::findOrFail($id);
        $detalhes = OrcamentosDetalhes::where('orcamento_id', '=', $id)->get();
        $descontos = OrcamentosDescontos::where('orcamento_id', '=', $id)->get();
        
        if (!empty($orcamento)){
            $receber = [
                    'id'                => 0,
                    'created_at'        => $orcamento->created_at, 
                    'updated_at'        => $orcamento->updated_at, 
                    'paciente_id'       => $orcamento->paciente_id, 
                    'vencimento'        => $orcamento->vencimento, 
                    'pagamento'         => $orcamento->pagamento, 
                    'valor'             => $orcamento->valor, 
                    'numero'            => 1, 
                    'numerodocumento'   => null, 
                    'descricao'         => $orcamento->descricao, 
                    'status'            => 'Aberto', 
                    'clinica_id'        => $orcamento->clinica_id, 
                    'tipopagto_id'      => $orcamento->tipopagto_id,
                    'orcamento'         => $orcamento->orcamento
                ];

            $novoreceber = Contasreceber::create($receber);
            
            if (!empty($detalhes)){
                foreach ($detalhes as $k => $item){
                    DB::insert("insert into contasreceber_detalhes(created_at, updated_at, contasreceber_id, tabela_servico_id, descricao, valor) "
                              ."values (?,?,?,?,?,?)", 
                        [
                            $item->created_at,
                            $item->updated_at,
                            $novoreceber['id'], 
                            $item->tabela_servico_id, 
                            $item->descricao, 
                            $item->valor
                        ]) ;        
                }
            }

            if (!empty($descontos)){
                foreach ($descontos as $k => $item){
                    DB::insert("insert into contasreceber_descontos(created_at, updated_at, contasreceber_id, tabela_descacre_id, descricao, valor) "
                              ."values (?,?,?,?,?,?)", 
                        [
                            $item->created_at,
                            $item->updated_at,
                            $novoreceber['id'], 
                            $item->tabela_descacre_id, 
                            $item->descricao, 
                            $item->valor
                        ]) ;        
                }
            }
            
        }
        
        /* Update do orçamento */
        DB::update("update orcamentos set status = ? where id = ".$id, ['Baixado']);
        
        /* Update do Recebedr */
        DB::update("update contasrecebers set numerodocumento = ? where id = ".$novoreceber['id'], [(1000 + $novoreceber['id'])]) ;

        return redirect('financeiro/orcamento');        
        
    }
    
}
