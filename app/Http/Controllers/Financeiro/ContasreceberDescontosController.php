<?php

namespace App\Http\Controllers\Financeiro;

use App\Models\ContasreceberDescontos;
use Illuminate\Http\Request;

class ContasreceberDescontosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContasreceberDescontos  $contasreceberDescontos
     * @return \Illuminate\Http\Response
     */
    public function show(ContasreceberDescontos $contasreceberDescontos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContasreceberDescontos  $contasreceberDescontos
     * @return \Illuminate\Http\Response
     */
    public function edit(ContasreceberDescontos $contasreceberDescontos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContasreceberDescontos  $contasreceberDescontos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContasreceberDescontos $contasreceberDescontos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContasreceberDescontos  $contasreceberDescontos
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContasreceberDescontos $contasreceberDescontos)
    {
        //
    }
}
