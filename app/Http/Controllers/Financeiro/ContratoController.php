<?php

namespace App\Http\Controllers\Financeiro;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Contrato;
use App\Models\Paciente;
use App\Models\TabelaServico;
use App\Models\ModelosDocumento;

use Illuminate\Http\Request;
use Session;
use PDF;

class ContratoController extends Controller
{
    /**
     * Middleware de autenticação (AUTH)
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $contrato = Contrato::where('descricao', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $contrato = Contrato::latest()->paginate($perPage);
        }

        return view('financeiro.contrato.index', compact('contrato'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $pacientes = Paciente::where('tipopaciente','=',1)->where('status','=','Ativo')->orderBy('nome', 'ASC')->get();
        $servicos = TabelaServico::all();
        $modelos = ModelosDocumento::where('status','=','Ativo')->get();

        return view('financeiro.contrato.create', compact('pacientes', 'servicos', 'modelos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Contrato::create($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/contrato');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        return $this->edit($id);
        // $contrato = Contrato::findOrFail($id);
        //
        // return view('financeiro.contrato.show', compact('contrato'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $contrato = Contrato::findOrFail($id);
        $pacientes = Paciente::where('tipopaciente','=',1)->where('status','=','Ativo')->orderBy('nome', 'ASC')->get();
        $servicos = TabelaServico::all();
        $modelos = ModelosDocumento::where('status','=','Ativo')->get();

        return view('financeiro.contrato.edit', compact('contrato', 'pacientes', 'servicos', 'modelos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        $contrato = Contrato::findOrFail($id);
        $contrato->update($requestData);

        Session::flash('success', 'Salvo com sucesso');

        return redirect('financeiro/contrato');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Contrato::destroy($id);

        Session::flash('success', 'Excluido com sucesso');

        return redirect('financeiro/contrato');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return array $paciente, informações cadastradas referente ao paciente.
     */
    public function retornadadostitular($id)
    {
        $titular = Paciente::findOrFail($id);

        return compact('titular');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return array $dependentes, informações cadastradas referente aos dependentes.
     */
    public function retornadadosdependentes($id)
    {
        $dependentes = Paciente::where('paciente_titular_id', $id)->get();

        return compact('dependentes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return array $dependentes, informações cadastradas referente aos dependentes.
     */
    public function retornamodelodocumento($id)
    {
        $documento = ModelosDocumento::findOrFail($id);

        return compact('documento');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return array $dependentes, informações cadastradas referente aos dependentes.
     */
    public function generatePDF($id) {
        
        $contrato = Contrato::findOrFail($id);

        $pdf = PDF::loadHTML($contrato->descricao)->setPaper('a4', 'portable')->setWarnings(false);
        
        return $pdf->download('Contrato-' . $id . '.pdf');
    }

    public function copiacontrato($id){
        
        $contrato = Contrato::findOrFail($id);
        $novocontrato = $contrato->replicate();
        $novocontrato->data = date('Y-m-d');
        $novocontrato->gerado = 'Nao';
        $novocontrato->save();

        return redirect('financeiro/contrato');
        
    }
}
