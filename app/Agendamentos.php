<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agendamentos extends Model
{
    //
    protected $fillable = [
        'dataconsulta',
        'horaconsulta',
        'paciente',
        'medico',
        'sala',
        'atividade',
        'obs'
    ];
    protected $table = 'agendamentos';
    protected $primaryKey = 'id';
}
