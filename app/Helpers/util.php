<?php
/*
 * ** Comentários de Atualizações ***
  <IDENTIFICACAO_DE_MANUTENCAO>
  DATA              = "05/03/2019"
  NRO_OS            = ""
  PROGRAMADOR       = "JCSANTOS"
  CLIENTE           = "CAP"
  NOME_DA_ROTINA    = "util.php"
  MANUTENCAO        = "Criação do arquivo e disponibilização de funções."
  </IDENTIFICACAO_DE_MANUTENCAO>

 * ** Fim Atualizações ***
 */

  /**
   * Retorna tamanho do arquivo em unidades de medida da informática.
   *
   * @access public
   *
   * @param Integer $bytes - Número a ser formatado.
   * @param Integer $decimals - Quantidade de casas após a virgula.
   *
   * @return $return - Retorna número formatado.
   */
   if (! function_exists('formataBytes')) {
    function formataBytes($bytes, $decimals = 2) {
      if ($bytes == 0) {
          return '0 Bytes';
      }

      $k = 1024;
      $dm = $decimals;
      $sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

      $i = floor(log($bytes) / log($k));
      $num = ($bytes / pow($k, $i));

      return number_format($num, $dm, '.', '') . ' ' . $sizes[$i];
    }
  }

  /**
   * Retorna um número formatado.
   *
   * @access public
   *
   * @param String $numero - Número a ser formatado.
   * @param Integer $decimais - Quantidade de casas após a virgula.
   * @param String $separador_decimal - Caracter de separação de decimal, default ",".
   * @param String $separador_milhar - Caracter de separação de milhares, default ".".
   *
   * @return $return - Retorna número formatado.
   */
   if (! function_exists('formataMilhares')) {
    function formataMilhares($numero, $decimais = 0, $separador_decimal = ',', $separador_milhar = '.') {
        if (is_numeric($numero)) {
            return number_format($numero, $decimais, $separador_decimal, $separador_milhar);
        } else {
            return '';
        }
    }
   }

    /**
     * Retira caracteres especiais usando expressão regular
     *
     * @access public
     *
     * @param String $string - Conjunto de caracteres que será avalidado e alterado.
     * @return String - Conjunto sem caracteres especiais.
     */
    if (! function_exists('RetirarAcentos')) {
      function RetirarAcentos($string) {
          return preg_replace(
                  array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/",
              "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/",
              "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/",
              "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/",
              "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/",
              "/(ñ)/", "/(Ñ)/",
              "/(ç)/", "/(Ç)/"
                  ), explode(" ", "a A e E i I o O u U n N c C"), $string);
      }
    }

    /**
     * Inclui LOG.
     *
     * @access public
     *
     * @param String $dtatual - Data de execução do processo.
     * @param String $rotina - Nome da Rotina.
     * @param String $idusuario - Id do Usuário.
     * @param String $mensagem - Mensagem.
     * @param String $tipo - Tipo de log, E para erro, M para mensagem.
     *
     */
     if (! function_exists('geraLog')) {
       function geraLog(string $dtatual, string $rotina, string $idusuario, string $mensagem, string $tipo = 'M') {

        $usuario = Usuarios::getUsuarioByIdHelper($idusuario);

        $log = new Logs();

        try {
            $dados = array();
            $dados['lgid'] = 0;
            $dados['lgdtatual'] = $dtatual;
            $dados['lgsistema'] = substr("Santa Isabel", 0, 254);
            $dados['lgrotina'] = substr($rotina, 0, 63);
            $dados['lgnomeusuario'] = substr(($usuario['id'] . " - " . $usuario['nomerazao'] . " " . $usuario['sobrenomefantasia'] . " RGF: " . $usuario['rgf']), 0, 63);
            $dados['lgmensagem'] = substr($mensagem, 0, 511);
            $dados['lgtpmensagem'] = substr($tipo, 0, 1);
            $dados['lgbrowser'] = substr(($_SERVER['HTTP_USER_AGENT']), 0, 254);
            $dados['lgipusuario'] = substr(($_SERVER['REMOTE_ADDR'] . ':' . $_SERVER['REMOTE_PORT']), 0, 254);

            $log->save($dados);
        } catch (Exception $e) {
            echo 'Erro ao inserir log: ' . $e->getMessage();
            die();
        }
      }
    }

  /**
   * Altera as Iniciais de cada palavra em Maiusculas.
   *
   * @access public
   *
   * @param String $string - Texto a ser alterado.
   *
   */
    if (! function_exists('iniciaisMaiusculas')) {
      function iniciaisMaiusculas($string)
      {
        $words = explode(' ', strtolower(trim(preg_replace("/\s+/", ' ', $string))));
        $return[] = ucfirst($words[0]);

        unset($words[0]);

        foreach ($words as $word)
        {
          if (!preg_match("/^([dn]?[aeiou][s]?|em)$/i", $word))
          {
            $word = ucfirst($word);
          }
          $return[] = $word;
        }

        return implode(' ', $return);
      }
    }

?>
