<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-sm-2 col-form-label">{{ 'Nome' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($cidade->nome) ? $cidade->nome : ''}}" >
</div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('estados_id') ? 'has-error' : ''}}">
    <label for="estados_id" class="col-sm-2 col-form-label">{{ 'Estados Id' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="estados_id" type="number" id="estados_id" value="{{ isset($cidade->estados_id) ? $cidade->estados_id : ''}}" >
</div>
    {!! $errors->first('estados_id', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
</div>
