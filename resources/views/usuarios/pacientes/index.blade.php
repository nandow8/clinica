@extends('layouts.template')

@section('content')
    <div class="row"> 

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Pacientes</div>
                <div class="card-body">
                    <a href="{{ url('/usuarios/pacientes/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar Paciente">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Paciente
                    </a>

                    <form method="GET" action="{{ url('/usuarios/pacientes') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Tipo</th>
                                    <th>CPF</th>
                                    <th>Telefone</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
<!--                            
                            <i class="fa fa-male" aria-hidden="true"></i>
                            <i class="fa fa-female" aria-hidden="true"></i>
                            <i class="fa fa-child" aria-hidden="true"></i>
-->
                            <tbody>
                            @foreach($pacientes as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->nome }}</td>
                                    <td>{{ $item->tipopaciente == 0 ? 'Dependente' : 'Titular' }}</td>
                                    <td>{{ (isset($item->cpf)) ? $item->cpf : '--' }}</td>
                                    <td>{{ (isset($item->telefone)) ? $item->telefone : '--' }}</td>
                                    <td>
                                        <a href="{{ url('/usuarios/pacientes/' . $item->id) }}" title="Visualizar Paciente"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/usuarios/pacientes/' . $item->id . '/edit') }}" title="Editar Paciente"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        
                                        {{ Form::open(['url'=> '/usuarios/pacientes' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                            {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title' =>'Excluir', 'role' => 'button', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $pacientes->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')

@endsection