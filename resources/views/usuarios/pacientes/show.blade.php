@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Paciente</div>
                <div class="card-body">

                    <a href="{{ url('/usuarios/pacientes') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/> 
                    <br/> 

                    @include ('usuarios.pacientes.form', ['formMode' => 'create'])

                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script>
    jQuery(document).ready(function () {
        jQuery("#btnsubmit").hide();
    });
</script> 
@endsection

