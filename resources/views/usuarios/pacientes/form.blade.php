<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>

<style>
    .requerido{ font-style: italic; font-size: .8em; }
    #mapa{ width: 100%; height: 450px; border: 1px solid #ccc; display: block; }
    .total{ background: #fd6161; padding: 0px 10px; font-size: 1.1em; color: #fff; font-weight: bold; border-radius: 10px; }
    .info-title{ color: #040e13; font-size: 1.1em; font-weight: bold; }
</style>

@if( (request()->route()->getActionMethod() == 'create') || ((request()->route()->getActionMethod() == 'edit') && ($paciente->tipopaciente == 1)) )
    <div class="form-group row {{ $errors->has('empresa_id') ? 'has-error' : ''}}" id="empresarial">
        <label for="empresa_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">Empresarial</label>
        <div class="col-lg-5 col-md-6">
            <select name="empresa_id" class="form-control" id="empresa_id" >
                <option value="">Selecione...</option>
                @foreach ($empresa as $optionKey => $optionValue)
                <option value="{{ $optionValue->id }}" {{ (isset($paciente->empresa_id) && $paciente->empresa_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->razao }}</option>
                @endforeach
            </select>
        </div>
        {!! $errors->first('empresa_id', '<p class="help-block">:message</p>') !!}
    </div>
@else
    <div class="form-group row {{ $errors->has('empresa_id') ? 'has-error' : ''}}" id="empresarial">
            @php 
                if(isset($paciente->empresa_id)){ 
                    $grupoemp = DB::table('grupoempresarials')
                                ->select(DB::raw('razao'))
                                ->where('id', '=', $paciente->empresa_id)
                                ->get();
                    echo '<label for="empresa_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">Empresarial</label>';
                    echo '<div class="col-lg-5 col-md-6">';
                        echo isset($grupoemp) ? $grupoemp[0]->razao : '--'; 
                    echo '</div>';
                }
            @endphp
    </div>
@endif
<div class="form-group row {{ $errors->has('tipo') ? 'has-error' : ''}}">
  <label for="tipo" class="col-lg-2 col-md-3 col-sm-12 col-form-label">Cobrança <span class="requerido">(*)</span></label>
  <div class="col-lg-5 col-md-6">
    @if(request()->route()->getActionMethod() !== 'create')
        <strong> {{ ($paciente->tipocobranca == 0) ? 'Contrato' : 'Particular' }} </strong>
        <input type="hidden" name="tipocobranca" value="{{ $paciente->tipocobranca }}">
    @else
      <div class="custom-control custom-radio custom-control-inline">
        <input name="tipocobranca" class="custom-control-input" type="radio" value="0" id="tipocobrancaRadioInline1" onclick="exibetipopaciente(this)" checked>
        <label class="custom-control-label" for="tipocobrancaRadioInline1">Contrato</label>
      </div>
      <div class="custom-control custom-radio custom-control-inline">
        <input name="tipocobranca" class="custom-control-input" type="radio" value="1" id="tipocobrancaRadioInline2" onclick="exibetipopaciente(this)">
        <label class="custom-control-label" for="tipocobrancaRadioInline2">Particular</label>
      </div>
    @endif

  </div>
</div>


<div class="form-group row {{ $errors->has('tipopaciente') ? 'has-error' : ''}}" id="tipopaciente">
    <label for="tipopaciente" class="col-lg-2 col-md-3 col-sm-12 col-form-label">Tipo do paciente <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        @if(request()->route()->getActionMethod() !== 'create')
            <strong> {{ ($paciente->tipopaciente == 0) ? 'Dependente' : 'Titular' }} </strong>
            <input type="hidden" id="disparatipopaciente" name="tipopaciente" value="{{ $paciente->tipopaciente }}">
            @php
              if($paciente->tipopaciente == 0){
                echo '<script> window.onload = function() {exibetitular($("input[name=tipopaciente]"));}; </script>';
              }
            @endphp
        @else
            <div class="custom-control custom-radio custom-control-inline">
                <input class="custom-control-input" name="tipopaciente" type="radio" onclick="exibetitular(this)" id="tipopacienteRadioInline1" value="1" {{ (isset($paciente) && 1 == $paciente->tipopaciente) ? 'checked' : '' }} checked>
                <label class="custom-control-label" for="tipopacienteRadioInline1">Titular</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input class="custom-control-input" name="tipopaciente" type="radio" onclick="exibetitular(this)" id="tipopacienteRadioInline2" value="0" @if (isset($paciente)) {{ (0 == $paciente->tipopaciente) ? 'checked' : '' }} @endif>
                <label class="custom-control-label" for="tipopacienteRadioInline2">Dependente</label>
            </div>
        @endif
    </div>
    {!! $errors->first('tipopaciente', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('paciente_titular_id') ? 'has-error' : ''}}" id="titular">
    <label for="paciente_titular_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">Titular <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <select name="paciente_titular_id" class="form-control" id="paciente_titular_id" >
            <option value="">Selecione...</option>
            @foreach ($titulares as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($paciente->paciente_titular_id) && $paciente->paciente_titular_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nome }}</option>
            @endforeach
        </select>
    </div>
    {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row adicionarfoto">
        <label for="image" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Foto' }}</label>
        <div class="col-lg-5 col-md-6">
            @if (isset($paciente))
                <img src="/uploads/fotospacientes/<?php echo ($paciente->image) ? $paciente->image : 'defaultman.jpg' ; ?>" alt="{{ $paciente->nome }}" style="width:115px">
            @else
                <img src="/uploads/fotospacientes/defaultman.jpg" alt="" style="width:115px">
            @endif
        {{-- <input type="file" name="image"> --}}
    </div>
</div>

@if(request()->route()->getActionMethod() !== 'show')
    <div class="form-group row">
        <label for="image" class="col-lg-2 col-md-3 col-sm-12 col-form-label"></label>
        <p style="margin-left: 1%" class="btn btn-outline-success adicionarfoto">Adicionar Foto</p>
    </div>
@endif
<div class="form-group row" style="display: none;" id="tirarfoto">
    <label for="image" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Tirar Foto' }}</label>
    <div class="col-lg-5 col-md-6">
        <div class="col-md-6">
            <div id="my_camera"></div>
            <br/>
            <input type="button" class="btn btn-outline-success" value="Tirar Foto" onClick="take_snapshot()">
            <input type="hidden" name="imagecam" class="image-tag">
        </div>
        <div class="col-md-6">
            <div id="results"></div>
        </div>
    </div>
    {!! $errors->first('observacoes', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nome' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($paciente->nome) ? $paciente->nome : ''}}" required>
    </div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('sexo') ? 'has-error' : ''}}">
    <label for="sexo" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Sexo' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <div class="custom-control custom-radio custom-control-inline">
            <input class="custom-control-input" name="sexo" type="radio" id="customRadioInline1" value="1" {{ (isset($paciente) && 1 == $paciente->sexo) ? 'checked' : '' }}>
            <label class="custom-control-label" for="customRadioInline1">Masculino</label>
        </div>
        <div class="custom-control custom-radio custom-control-inline">
            <input class="custom-control-input" name="sexo" type="radio" id="customRadioInline2" value="0" @if (isset($paciente)) {{ (0 == $paciente->sexo) ? 'checked' : '' }} @endif>
            <label class="custom-control-label" for="customRadioInline2">Feminino</label>
        </div>
    </div>
    {!! $errors->first('sexo', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('indicacao') ? 'has-error' : ''}}" id="divindicacao">
    <label for="indicacao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Indicação' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="indicacao" type="text" id="indicacao" value="{{ isset($paciente->indicacao) ? $paciente->indicacao : ''}}" >
    </div>
    {!! $errors->first('indicacao', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group row {{ $errors->has('rg') ? 'has-error' : ''}}" id="divrg">
    <label for="rg" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'RG' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="rg" type="text" id="rg" value="{{ isset($paciente->rg) ? $paciente->rg : ''}}">
    </div>
    {!! $errors->first('rg', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('cpf') ? 'has-error' : ''}}" id="divcpf">
    <label for="cpf" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CPF' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="cpf" type="text" id="cpf" value="{{ isset($paciente->cpf) ? $paciente->cpf : ''}}">
    </div>
    {!! $errors->first('cpf', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('estadocivil') ? 'has-error' : ''}}" id="divestadocivil">
    <label for="estadocivil" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Estado Civil' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <!-- <input class="form-control" name="estadocivil" type="text" id="estadocivil" value="{{ isset($paciente->estadocivil) ? $paciente->estadocivil : ''}}" > -->
        <select name="estadocivil" id="estadocivil" class="form-control" required>
            <option value="">Selecione...</option>
            <option value="Solteiro" {{ (isset($paciente->estadocivil) && $paciente->estadocivil == 'Solteiro') ? 'selected' : ''}}>Solteiro</option>
            <option value="Casado" {{ (isset($paciente->estadocivil) && $paciente->estadocivil == 'Casado') ? 'selected' : ''}}>Casado</option>
            <option value="Divorciado" {{ (isset($paciente->estadocivil) && $paciente->estadocivil == 'Divorciado') ? 'selected' : ''}}>Divorciado</option>
            <option value="Outros" {{ (isset($paciente->estadocivil) && $paciente->estadocivil == 'Outros') ? 'selected' : ''}}>Outros</option>
        </select>
    </div>
    {!! $errors->first('estadocivil', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('datanascimento') ? 'has-error' : ''}}">
    <label for="datanascimento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Data Nascimento' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="datanascimento" type="text" id="datanascimento" value="{{ (isset($paciente->datanascimento)) ? $paciente->datanascimento : ''}}" >
    </div>
    {!! $errors->first('datanascimento', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('naturalidade') ? 'has-error' : ''}}" id="divnaturalidade">
    <label for="naturalidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Naturalidade' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="naturalidade" type="text" id="naturalidade" value="{{ isset($paciente->naturalidade) ? $paciente->naturalidade : ''}}" >
    </div>
    {!! $errors->first('naturalidade', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('nacionalidade') ? 'has-error' : ''}}" id="divnacionalidade">
    <label for="nacionalidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nacionalidade' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="nacionalidade" type="text" id="nacionalidade" value="{{ isset($paciente->nacionalidade) ? $paciente->nacionalidade : ''}}" >
    </div>
    {!! $errors->first('nacionalidade', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('telefone') ? 'has-error' : ''}}" id="divtelefone">
    <label for="telefone" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Telefone' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="telefone" type="text" id="telefone" value="{{ isset($paciente->telefone) ? $paciente->telefone : ''}}" required>
    </div>
    {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('celular') ? 'has-error' : ''}}" id="divcelular">
    <label for="celular" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Celular' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="celular" type="text" id="celular" value="{{ isset($paciente->celular) ? $paciente->celular : ''}}" >
    </div>
    {!! $errors->first('celular', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}" id="divemail">
    <label for="email" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Email' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="email" type="text" id="email" value="{{ isset($paciente->email) ? $paciente->email : ''}}" >
    </div>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('cep') ? 'has-error' : ''}}" id="divcep">
    <label for="cep" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CEP' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="cep" type="text" id="cep" value="{{ isset($paciente->cep) ? $paciente->cep : ''}}" >
    </div>
    {!! $errors->first('cep', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('estados_id') ? 'has-error' : ''}}" id="divestado_id">
    <label for="estados_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Estado' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <input type="text" name="estados_id" class="form-control" id="estados_id" value="{{isset($paciente->estados_id) ? $paciente->estados_id : '' }}" required>
        <!-- <select name="estados_id" class="form-control" id="estados_id" required onchange="setCidades(this)">
            <option value="">Selecione...</option>
            @foreach ($estados as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($paciente->estados_id) && $paciente->estados_id == $optionKey) ? 'selected' : ''}} class="{{ $optionValue->uf }}">{{ $optionValue->nome }}</option>
            @endforeach
        </select> -->
    </div>
    {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('cidades_id') ? 'has-error' : ''}}" id="divcidade_id">
    <label for="cidades_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Cidade' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <input type="text" name="cidades_id" id="cidades_id" value="{{ isset($paciente->cidades_id) ? $paciente->cidades_id : '' }}" class="form-control" required>
        <!-- <select name="cidades_id" class="form-control" id="cidades_id" required>
            <option value="">Selecione...</option>
            @foreach ($cidades as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($paciente->cidades_id) && $paciente->cidades_id == $optionKey) ? 'selected' : ''}} required>{{ $optionValue->nome }}</option>
            @endforeach
        </select> -->
    </div>
    {!! $errors->first('cidade', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('bairros_id') ? 'has-error' : ''}}" id="divbairro">
    <label for="bairros_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Bairro' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <input type="text" name="bairros_id" id="bairros_id" value="{{ isset($paciente->bairros_id) ? $paciente->bairros_id : '' }}" class="form-control" required>
        <!-- <select name="bairros_id" class="form-control" id="bairros_id" required>
            <option value="">Selecione...</option>
            @foreach ($bairros as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($paciente->bairros_id) && $paciente->bairros_id == $optionKey) ? 'selected' : ''}}>{{ $optionValue->nome }}</option>
            @endforeach
        </select> -->
    </div>
    {!! $errors->first('bairro', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('rua') ? 'has-error' : ''}}" id="divrua">
    <label for="rua" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Rua' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="rua" type="text" id="rua" value="{{ isset($paciente->rua) ? $paciente->rua : ''}}" required>
    </div>
    {!! $errors->first('rua', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}" id="divnumero">
    <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Número' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="numero" type="text" id="numero" value="{{ isset($paciente->numero) ? $paciente->numero : ''}}" required>
    </div>
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('complemento') ? 'has-error' : ''}}"id="divcomplemento">
    <label for="complemento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Complemento' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="complemento" type="text" id="complemento" value="{{ isset($paciente->complemento) ? $paciente->complemento : ''}}" >
    </div>
    {!! $errors->first('complemento', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('plano') ? 'has-error' : ''}}" id="divplano">
    <label for="plano" class="col-lg-2 col-md-3 col-sm-12 col-form-label" required>Plano <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <select name="plano" class="form-control" id="plano" onchange="buscaConvenio(this)" required>
            <option value="">Selecione...</option>
            @foreach ($planos as $key => $plano)
            <option value="{{ $plano->id }}" id="convenio_{{$plano->idconvenio}}" {{ (isset($paciente->plano) && $paciente->plano == $plano->id) ? 'selected' : ''}} required>{{ $plano->nomeplano }}</option>
            @endforeach
        </select>
        <div class="conveniocontainer shadow-lg p-3 bg-white rounded" style="display:none;"></div>
    </div>
    {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
</div>

<!-- <div class="form-group row {{ $errors->has('pacientesconvenio_id') ? 'has-error' : ''}}">
    <label for="pacientesconvenio_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">Convênio <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6">
        <select name="pacientesconvenio_id" class="form-control" id="pacientesconvenio_id" onchange="buscaConvenio(this)" required>
            <option value="">Selecione...</option>
            @foreach ($convenios as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($paciente->pacientesconvenio_id) && $paciente->pacientesconvenio_id == $optionValue->id) ? 'selected' : ''}} required>{{ $optionValue->convenio }}</option>
            @endforeach
        </select>
        <div class="conveniocontainer shadow-lg p-3 bg-white rounded"></div>
    </div>
    {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
</div> -->

<div class="form-group row {{ $errors->has('numerocarteira') ? 'has-error' : ''}}" id="divnumerocarteira">
    <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Número Carteira' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="numerocarteira" type="text" id="numerocarteira" value="{{ isset($paciente->numerocarteira) ? $paciente->numerocarteira : ''}}">
    </div>
    {!! $errors->first('numerocarteira', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('datavalidade') ? 'has-error' : ''}}" id="divdatavalidade">
    <label for="datavalidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Data Validade' }}</label>
    <div class="col-lg-5 col-md-6">
        <input class="form-control" name="datavalidade" type="text" id="datavalidade" value="{{ isset($paciente->datavalidade) ? $paciente->datavalidade : ''}}">
    </div>
    {!! $errors->first('datavalidade', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('observacoes') ? 'has-error' : ''}}" id="divobservacoes">
    <label for="observacoes" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Observações' }}</label>
    <div class="col-lg-5 col-md-6">
        <textarea class="form-control" rows="5" name="observacoes" type="textarea" id="observacoes" >{{ isset($paciente->observacoes) ? $paciente->observacoes : ''}}</textarea>
    </div>
    {!! $errors->first('observacoes', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6">
        <select name="status" class="form-control" id="status" required>
            <option value="Ativo" {{ (isset($paciente->status) && $paciente->status == 'Ativo') ? 'selected' : ''}} required>Ativo</option>
            <option value="Bloqueado" {{ (isset($paciente->status) && $paciente->status == 'Bloqueado') ? 'selected' : ''}} required>Bloqueado</option>
        </select>
    </div>
</div>
@if(request()->route()->getActionMethod() !== 'create')
    <div id="mapa"></div>
@endif

<br/>

<div class="form-group offset-md-5" id="btnsubmit">
    <a class="btn btn-warning rounded" href="{{ url('/usuarios/pacientes') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> Salvar</button>
</div>

@section('jqueryscript')

<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlHzImDCagrOppCrUjRU2FT__KHKsUYhs&callback=initMap"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>

<script>
    var metodo = "<?php echo request()->route()->getActionMethod(); ?>";
    var idpac = "<?php echo isset($paciente->id) ? $paciente->id : ''?>";

        //Funções MAPA
        function initMap(){

            var rua = jQuery("#rua").val();
            var numero = jQuery("#numero").val();
            var bairro = jQuery("#bairros_id").val();
            var cidade = jQuery("#cidades_id").val();
            var estado = jQuery("#estados_id").val();

            var endereco = rua+","+numero+"-"+bairro+"-"+cidade+"-"+estado;

            jQuery.ajax({
            type: "GET",
            dataType: 'json',
            data: {id: idpac },
            url: "/findpacientebyid",

            success: function(retorno){

                jQuery.each(retorno, function(index, dados) {
                    var titular = '';
                    if(dados.paciente_titular_id == null || dados.paciente_titular_id == ""){
                        titular = dados.nome;
                    }else{
                        titular = dados.titular;
                    }
                    endereco = dados.rua+' - '+dados.bairros_id+' - '+dados.estados_id;

                    var infos = '';
                    infos += "<span><b class='info-title'>Paciente:</b> "+dados.nome+"</span><br/><br/>";
                    infos += "<span><b class='info-title'>Número de dependentes:</b> <span class='total'>"+ dados.total +"</span></span><br/><br/>";
                    infos += "<span><b class='info-title'>Titular:</b> "+ titular +"</span><br/><br/>";
                    infos += "<span><b class='info-title'>Endereço:</b> "+endereco+"</span><br/><br/>";

                    GetLocation(endereco, infos);
                    });
                }
            });
        }


    function GetLocation(address, info) {

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {

                    var lat = results[0].geometry.location.lat();
                    var long = results[0].geometry.location.lng();

                    var options = {
                        zoom: 17,
                        zoomControl: true,
                        mapTypeControl: false,
                        scaleControl: false,
                        streetViewControl: false,
                        rotateControl: false,
                        fullscreenControl: false,
                        center: {lat: lat, lng: long},
                        styles: [{
                            "featureType": "poi",
                            "stylers": [{
                                "visibility": "off"
                            }]
                        }]
                    }

                    //Seta configuração do mapa
                    map = new google.maps.Map(document.getElementById('mapa'),{ options });

                    //se latitude e longitude estiverem OKAY
                    if(lat && long){

                        //cria marcador no mapa
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(lat, long),
                            icon: "{{ asset('/img/logos/cliente_icon.png') }}",
                            map: map
                        });

                        var infowindow = new google.maps.InfoWindow(), marker;

                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                                infowindow.setContent(info);
                                infowindow.open(map, marker);
                            }
                        })(marker));
                    }else{
                        alert('jif');
                        jQuery("#mapa").hide();
                    }
                }
            });
        }

    //Fim funções MAPA

     jQuery(document).ready(function () {
        jQuery('#telefone').mask('(00) 0000-0009');
        jQuery('#celular').mask('(00) 0000-00009');
        jQuery('#cep').mask('00000-000');
        jQuery('#rg').mask('00.000.000-0');
        jQuery('#cpf').mask('000.000.000-00');

        jQuery('#datanascimento, #datavalidade').datepicker({
            dateFormat: "dd/mm/yy",
            numberOfMonths: 1,
            monthNames: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
            monthNamesShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
            dayNames: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
            dayNamesShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
            dayNamesMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
            onSelect: function(selectedDate) {
            }
        });

        if(jQuery("#disparatipopaciente").val() == 0){
            jQuery("#titular").show()
        }else{
            if(jQuery("#tipopacienteRadioInline2").prop("checked")){
                jQuery("#titular").show()
            }else{
                jQuery("#titular").hide()
            }
        }

        jQuery(".adicionarfoto").click(function (e) {
            e.preventDefault();
            jQuery(".adicionarfoto").hide()
            jQuery("#tirarfoto").show()
        });

        buscaConvenio(jQuery("#plano"))
        // jQuery("#pacientesconvenio_id").on("change", function(e){

        //     var convenio_id = e.target.value
        //     if(convenio_id === ''){
        //         jQuery('.conveniocontainer').hide();
        //         return false;
        //     } else {
        //         jQuery('.conveniocontainer').show();
        //         jQuery.get('/usuarios/ajaxpacienteconvenio/' + convenio_id, function (data) {
        //             jQuery('.conveniocontainer').html(data.html);
        //         });
        //     }
        // })

     });

     function exibetipopaciente(obj){
        const tipo = jQuery(obj).val();
        if(tipo == 0){
            jQuery("#tipopaciente").show()
        }else{
            jQuery("#tipopacienteRadioInline1").prop("checked", true);
            jQuery("#tipopaciente").hide()
            jQuery("#titular").hide()
            jQuery("#paciente_titular_id").prop('selectedIndex', 0);
        }

     }

     function exibetitular(obj){
        const tipopaciente = jQuery(obj).val();
        if(tipopaciente == 0){
            jQuery("#paciente_titular_id").prop("required", true);
            jQuery("#titular").show();

            jQuery("#divindicacao").hide();
            jQuery("#divrg").hide();
            jQuery("#divcpf").hide();
            jQuery("#divestadocivil").hide();
            jQuery("#divnaturalidade").hide();
            jQuery("#divnacionalidade").hide();
            jQuery("#divtelefone").hide();
            jQuery("#divcelular").hide();
            jQuery("#divemail").hide();
            jQuery("#divcep").hide();
            jQuery("#divestado_id").hide();
            jQuery("#divcidade_id").hide();
            jQuery("#divbairro").hide();
            jQuery("#divrua").hide();
            jQuery("#divnumero").hide();
            jQuery("#divcomplemento").hide();
            jQuery("#divplano").hide();
            jQuery("#divnumerocarteira").hide();
            jQuery("#divdatavalidade").hide();
            jQuery("#divobservacoes").hide();
            jQuery("#mapa").hide();

            jQuery("#estadocivil").prop("required", false);
            jQuery("#telefone").prop("required", false);
            jQuery("#estados_id").prop("required", false);
            jQuery("#cidades_id").prop("required", false);
            jQuery("#bairros_id").prop("required", false);
            jQuery("#rua").prop("required", false);
            jQuery("#numero").prop("required", false);
            jQuery("#plano").prop("required", false);

            jQuery("#estados_id").prop('value', 0);
            jQuery("#cidades_id").prop('value', 0);
            jQuery("#bairros_id").prop('value', 0);
            jQuery("#datavalidade").prop('value', "31/12/2079");
        }

        if(tipopaciente == 1){
            jQuery("#paciente_titular_id").prop("required", false);
            jQuery("#titular").hide();

            jQuery("#divindicacao").show();
            jQuery("#divrg").show();
            jQuery("#divcpf").show();
            jQuery("#divestadocivil").show();
            jQuery("#divnaturalidade").show();
            jQuery("#divnacionalidade").show();
            jQuery("#divtelefone").show();
            jQuery("#divcelular").show();
            jQuery("#divemail").show();
            jQuery("#divcep").show();
            jQuery("#divestado_id").show();
            jQuery("#divcidade_id").show();
            jQuery("#divbairro").show();
            jQuery("#divrua").show();
            jQuery("#divnumero").show();
            jQuery("#divcomplemento").show();
            jQuery("#divplano").show();
            jQuery("#divnumerocarteira").show();
            jQuery("#divdatavalidade").show();
            jQuery("#divobservacoes").show();
            jQuery("#mapa").show();

            jQuery("#estadocivil").prop("required", true);
            jQuery("#telefone").prop("required", true);
            jQuery("#estados_id").prop("required", true);
            jQuery("#cidades_id").prop("required", true);
            jQuery("#bairros_id").prop("required", true);
            jQuery("#rua").prop("required", true);
            jQuery("#numero").prop("required", true);
            jQuery("#plano").prop("required", true);

        }

     }

     Webcam.set({
        width: 490,
        height: 390,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    Webcam.attach( '#my_camera' );

    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            jQuery(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        } );
    }

    function buscaConvenio(e){
        und = typeof(jQuery(e).val());

        if(jQuery(e).val() === ""){
            jQuery('.conveniocontainer').hide();
            return false;
        }
        if(und == 'undefined'){
            convenio_id = e
        }else {
            var option = jQuery("#plano :selected").attr('id');
            convSplit = option.split('_');
            convenio_id = convSplit[1];
        }
        if(convenio_id === ''){
            jQuery('.conveniocontainer').hide();
            return false;
        } else {
            jQuery('.conveniocontainer').show();
            jQuery.get('/cadastro/ajaxpacienteconvenio/' + convenio_id, function (data) {

                jQuery('.conveniocontainer').html(data.html);
            });
        }
    }

    function limpaCampos() {
        // Limpa valores do formulário de cep.
        jQuery("#rua").val("");
        jQuery("#bairros_id").val("");
        jQuery("#cidades_id").val("");
        jQuery("#estados_id").val("");
    }

    function buscarCEP(cep = ''){
        if(!cep){
            cep = jQuery('#cep').val();
        }
        //CEP - somente dígitos
        cep = cep.replace(/\D/g, '');

        jQuery("#rua").val("...");
        jQuery("#bairros_id").val("...");
        jQuery("#cidades_id").val("...");
        jQuery("#estados_id").val("...");

        jQuery.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

            if (!("erro" in dados)) {
                jQuery(".errorcep").html("").hide();
                //Atualiza os campos com os valores da consulta.
                jQuery("#rua").val(dados.logradouro);
                jQuery("#bairros_id").val(dados.bairro);
                jQuery("#cidades_id").val(dados.localidade);
                jQuery("#estados_id").val(dados.uf);

                if(false){
                  jQuery("#numero").focus();
                }
            } //fim do if
            else {
                //CEP pesquisado não foi encontrado.
                limpaCampos();
                jQuery(".errorcep").html("CEP não encontrado").show();
            }
        });

    }

    jQuery("#cep").keyup(function(){
        cep = jQuery(this).val();
        if(cep.length>=9){
            buscarCEP(cep);
        }else{
            // limpaCampos();
            jQuery(".errorcep").html("CEP não encontrado").show();
        }
    });

    function setCidades(obj){
        var id = jQuery(obj).val();

        if(id > 0){
            jQuery.ajax({

            beforeSend: function(xhr, type) {
                if (!type.crossDomain) {
                    xhr.setRequestHeader('X-CSRF-Token', jQuery('meta[name="csrf-token"]').attr('content'));
                }
            },
            url: '/getCidades',
            type: 'POST',
            dataType: 'json',
            data: {estado_id: id},
                success: function(cidades){
                    console.table(cidades);
                }
            });
        }
    }

    </script>
@endsection
