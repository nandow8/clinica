<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-sm-2 col-form-label">{{ 'Nome' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($estado->nome) ? $estado->nome : ''}}" >
</div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('uf') ? 'has-error' : ''}}">
    <label for="uf" class="col-sm-2 col-form-label">{{ 'Uf' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="uf" type="text" id="uf" value="{{ isset($estado->uf) ? $estado->uf : ''}}" >
</div>
    {!! $errors->first('uf', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
</div>
