@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Bairro</div>
                <div class="card-body">

                    <a href="{{ url('/usuarios/bairros') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $bairro->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Nome </th><td> {{ $bairro->nome }} </td></tr><tr><th> Cidades Id </th><td> {{ $bairro->cidades_id }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
