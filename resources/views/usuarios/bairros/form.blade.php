<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-sm-2 col-form-label">{{ 'Nome' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($bairro->nome) ? $bairro->nome : ''}}" >
</div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cidades_id') ? 'has-error' : ''}}">
    <label for="cidades_id" class="col-sm-2 col-form-label">{{ 'Cidades' }}</label>
    <div class="col-sm-5">
    <!-- <input class="form-control" name="cidades_id" type="number" id="cidades_id" value="{{ isset($bairro->cidades_id) ? $bairro->cidades_id : ''}}" > -->
    <select class="custom-select" id="inputGroupSelect01">
        <option selected>Selecione...</option>
        @foreach ($cidades as $cidade)
            <option value="{{ $cidade->id }}">{{ $cidade->nome }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('cidades_id', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
</div>
