@extends('layouts.template')

@section('content')
    <div class="row"> 

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Pré-Agendamento</div>
                <div class="card-body">
                    <a href="{{ url('/preagendamento/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar Preagendamento">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Pré Agendamento
                    </a>

                    <form method="GET" action="{{ url('/preagendamento') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Telefone</th>
                                    <th>Email</th>
                                    <!-- <th>CPF</th> -->
                                    <th>Data</th>
                                    <th>Horário</th>
                                    <th>Status</th>
                                    <th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($preagendamento as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nome }}</td><td>{{ $item->telefone }}</td>
                                    <td>{{ $item->email }}</td>
                                    <!-- <td>{{ $item->cpf }}</td> -->
                                    <td>{{ $item->dataconsulta }}</td>
                                    <td>{{ $item->horario }}</td>
                                    <td class="status">{{ $item->status }}</td>
                                    <td>
                                        <a href="{{ url('/preagendamento/' . $item->id) }}" title="Visualizar Preagendamento"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/preagendamento/' . $item->id . '/edit') }}" title="Editar Preagendamento"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        
                                        {{ Form::open(['url'=> '/preagendamento' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                            {{ Form::button('<i class="fa fa-pencil"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'role' => 'button', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $preagendamento->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/jquery.min.js')}}"></script>

    <script>
    $(document).ready(function(){
        
        $( ".status" ).each(function(index) {
            color = '';
            item = $(this).text();
            switch(item){
                case 'Agendado':
                    color = '#17a2b8';
                break;
                case 'Não agendado':
                    color = '#ffb200';
                break;
                case 'Cancelado':
                    color = '#dc3545';
                break;
                default:
                    color = '#000'
            }

            $(this).css("color",color);
        });

    });
    </script>
@endsection