@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Pré Agendamento</div>
                <div class="card-body">

                    <a href="{{ url('/preagendamento') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th> Nome </th><td> {{ $preagendamento->nome }} </td>
                                </tr>
                                <tr>
                                    <th> Telefone </th><td> {{ (isset($preagendamento->telefone)) ? $preagendamento->telefone : '--'}} </td>
                                </tr>
                                <tr>
                                    <th> Email </th><td> {{ (isset($preagendamento->email)) ? $preagendamento->email : '--'}} </td>
                                </tr>
                                <tr>
                                    <th> CPF </th><td> {{ (isset($preagendamento->cpf)) ? $preagendamento->cpf : '--' }} </td>
                                </tr>
                                <tr>
                                    <th> Data </th><td> {{ (isset($preagendamento->dataconsulta)) ? $preagendamento->dataconsulta : '--' }} </td>
                                </tr>
                                <tr>
                                    <th> Horário </th><td> {{ (isset($preagendamento->horario)) ? $preagendamento->horario : '--' }} </td>
                                </tr>
                                <tr>
                                    <th> Status </th><td> {{ (isset($preagendamento->status)) ? $preagendamento->status : '--' }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
