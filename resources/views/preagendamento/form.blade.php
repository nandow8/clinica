<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nome' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($preagendamento->nome) ? $preagendamento->nome : ''}}" required>
</div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('telefone') ? 'has-error' : ''}}">
    <label for="telefone" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Telefone' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="telefone" type="text" id="telefone" value="{{ isset($preagendamento->telefone) ? $preagendamento->telefone : ''}}" required>
</div>
    {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Email' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($preagendamento->email) ? $preagendamento->email : ''}}" >
</div>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cpf') ? 'has-error' : ''}}">
    <label for="cpf" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CPF' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="cpf" type="text" id="cpf" value="{{ isset($preagendamento->cpf) ? $preagendamento->cpf : ''}}" >
    </div>    
    {!! $errors->first('cpf', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('tipoconsulta') ? 'has-error' : ''}}">
    <label for="tipoconsulta" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Tipo Consulta' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <div class="radio">
        <label><input name="tipoconsulta" type="radio" onclick="changeOption(this)" value="consulta" {{ (isset($preagendamento) && 'consulta' == $preagendamento->tipoconsulta) ? 'checked' : '' }} checked> Consulta</label>
    </div>
    <div class="radio">
        <label><input name="tipoconsulta" type="radio" onclick="changeOption(this)" value="exame" {{ (isset($preagendamento) && 'exame' == $preagendamento->tipoconsulta) ? 'checked' : '' }}> Orçamento de Exame</label>
    </div>
</div>
    {!! $errors->first('tipoconsulta', '<p class="help-block">:message</p>') !!}
</div>

<div id="box-dataconsulta" {{ (isset($preagendamento)) && 'exame' === $preagendamento->tipoconsulta ? 'style=display:none' : '' }}>
    <div class="form-group row {{ $errors->has('dataconsulta') ? 'has-error' : ''}}">
        <label for="dataconsulta" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Data Consulta' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="dataconsulta" type="date" id="dataconsulta" value="{{ isset($preagendamento->dataconsulta) ? $preagendamento->dataconsulta : ''}}" checked>
    </div>
        {!! $errors->first('dataconsulta', '<p class="help-block">:message</p>') !!}
</div>
</div>


<div id="box-exame" {{ (isset($formMode)) && $formMode === 'create' ? 'style=display:none' : ''}} {{ (isset($preagendamento) && 'exame' == $preagendamento->tipoconsulta) ? '' : 'style=display:none' }}>
    <div class="form-group row {{ $errors->has('exame') ? 'has-error' : '' }}">
        <label for="exame" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Exame' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="exame" type="text" id="exame" value="{{ isset($preagendamento->exame) ? $preagendamento->exame : ''}}" >
    </div>
    {!! $errors->first('exame', '<p class="help-block">:message</p>') !!}
</div>
</div>

<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="horario" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Horário' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="horario" id="horario" class="form-control" placeholder="Horário">
        <option value="">Selecione</option>
        <option value="Entre 7h e 9h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 7h e 9h') ? 'selected' : ''}}>Entre 7h e 9h</option>
        <option value="Entre 9h e 11h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 9h e 11h') ? 'selected' : ''}}>Entre 9h e 11h</option>
        <option value="Entre 11h e 13h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 11h e 13h') ? 'selected' : ''}}>Entre 11h e 13h</option>
        <option value="Entre 13h e 15h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 13h e 15h') ? 'selected' : ''}}>Entre 13h e 15h</option>
        <option value="Entre 15h e 17h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 15h e 17h') ? 'selected' : ''}}>Entre 15h e 17h</option>
        <option value="Entre 17h e 19h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 17h e 19h') ? 'selected' : ''}}>Entre 17h e 19h</option>
        <option value="Entre 19h e 21h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 19h e 21h') ? 'selected' : ''}}>Entre 19h e 21h</option>
        <option value="Entre 21h e 23h" {{ (isset($preagendamento->horario) && $preagendamento->horario == 'Entre 21h e 23h') ? 'selected' : ''}}>Entre 21h e 23h</option>
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Não agendado":"Não agendado", "Agendado":"Agendado", "Cancelado" : "Cancelado", "Finalizado" : "Finalizado"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($preagendamento->status) && $preagendamento->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>
<div id="boxstatusregistro" class="form-group" style="display:none">
    <button type="button" class="close" data-dismiss="alert">x</button>
    <label id="statusregistro" class="alert" style="display:block"><label>
</div> 
<div class="form-group offset-md-4">
    <a href="{{ url('/preagendamento') }}" class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ 'Salvar' }}</button>
    <a href="{{ url('/novoregistro') }}" class="btn btn-success rounded" id="btn-registro" style="display:none;"><i class="fa fa-user-plus" aria-hidden="true"></i> Gerar registro</a>
</div>
@section('jqueryscript')    
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/frontend/jquery-mask.js') }}"></script> 
    <script type="text/javascript" src="{{asset('js/scripts/preagendamento_form.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection



