<div class="form-group row {{ $errors->has('modulo') ? 'has-error' : ''}}">
    <label for="modulo" class="col-sm-2 col-form-label">{{ 'Modulo' }}</label>
    <div class="col-sm-5">
    <select name="modulo" class="form-control" id="modulo" >
        @foreach (json_decode('{"Admin": "Admin","Financeiro": "Financeiro", "Cadastro": "Cadastro"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($tabelasparapopularperfil->modulo) && $tabelasparapopularperfil->modulo == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('modulo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('tabela') ? 'has-error' : ''}}">
    <label for="tabela" class="col-sm-2 col-form-label">{{ 'Tabela' }}</label>
    <div class="col-sm-5">
    <select name="tabela" class="form-control" id="tabela" >
        <option value="">Selecione...</option>
        @foreach ($tabelas as $optionKey => $optionValue)
            <option value="{{ $optionValue->Tables_in_cap }}" {{ (isset($tabelasparapopularperfil->tabela) && $tabelasparapopularperfil->tabela == $optionValue->Tables_in_cap) ? 'selected' : ''}}>{{ $optionValue->Tables_in_cap }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('tabela', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group  offset-md-6">
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection