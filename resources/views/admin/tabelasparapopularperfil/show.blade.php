@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Tabelasparapopularperfil</div>
                <div class="card-body">

                    <a href="{{ url('/admin/tabelasparapopularperfil') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $tabelasparapopularperfil->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Modulo </th><td> {{ $tabelasparapopularperfil->modulo }} </td></tr><tr><th> Tabela </th><td> {{ $tabelasparapopularperfil->tabela }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
