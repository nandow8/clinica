@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Perfil</div>
                <div class="card-body">

                    <a href="{{ url('/admin/perfil') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    @include ('admin.perfil.form', ['formMode' => 'edit'])

                </div>
            </div>
        </div>
    </div>
@endsection
