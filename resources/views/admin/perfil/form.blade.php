
 <div class="container">   
    <h1 style="font-size:5em; text-align: center;"></h1>
    <br>
    <br>

    @foreach ($perfillista as $key => $modulos) 
        <div class="card" style="width: 100%;">
            <h2 style="background-color: blue; color: white; text-align:center;" class="card-img-top" >{{ $key }}</h2>
            <div class="card-body">
                @foreach ($modulos as $key2 => $item) 
     
                    <input type="hidden" name="id[{{ $item->id }}]" value="{{ $item->id }}">
                    <input type="hidden" name="modulo[{{ $item->id }}]" value="{{ $item->modulo }}">
                    <input type="hidden" name="tabela[{{ $item->id }}]" value="{{ $item->tabela }}">

                    <div class="form-group row {{ $errors->has('excluir') ? 'has-error' : ''}}">
                        <label for="excluir" class="col-sm-6 col-form-label">{{ $item->tabela }}</label>
                        <div class="col-sm-6">
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" name="visualizar[{{ $item->id }}]" type="checkbox" id="{{ $item->tabela }}visualizar" value="{{ $item->tabela }}visualizar" @if (isset($perfils[0]->visualizar)) {{ ($item->tabela . 'visualizar' == $item->visualizar) ? 'checked' : '' }} @endif>  
                                <label class="custom-control-label" for="{{ $item->tabela }}visualizar">Visualizar</label>
                            </div> 
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" name="adicionar[{{ $item->id }}]" type="checkbox" id="{{ $item->tabela }}adicionar" value="{{ $item->tabela }}adicionar" @if (isset($perfils[0]->adicionar)) {{ ( $item->tabela . 'adicionar' == $item->adicionar) ? 'checked' : '' }} @endif>  
                                <label class="custom-control-label" for="{{ $item->tabela }}adicionar">Adicionar</label>
                            </div> 
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" name="editar[{{ $item->id }}]" type="checkbox" id="{{ $item->tabela }}editar" value="{{ $item->tabela }}editar" @if (isset($perfils[0]->editar)) {{ ($item->tabela . 'editar' == $item->editar) ? 'checked' : '' }} @endif>  
                                <label class="custom-control-label" for="{{ $item->tabela }}editar">Editar</label>
                            </div> 
                            <div class="custom-control custom-radio custom-control-inline">
                                <input class="custom-control-input" name="excluir[{{ $item->id }}]" type="checkbox" id="{{ $item->tabela }}excluir" value="{{ $item->tabela }}excluir" @if (isset($perfils[0]->excluir)) {{ ($item->tabela . 'excluir' == $item->excluir) ? 'checked' : '' }} @endif>  
                                <label class="custom-control-label" for="{{ $item->tabela }}excluir">Excluir</label>
                            </div>
                        </div>
                        {!! $errors->first('excluir', '<p class="help-block">:message</p>') !!}
                    </div>
                    <hr>
                @endforeach
            </div>
        </div>
    @endforeach
 
</div> 

<br/>
    
<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/admin/perfil') }}"><i class="fa fa-arrow-left fa-1" aria-hidden="true"></i> Voltar</a>
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
</div>


@section('jqueryscript')
    {{-- <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script> --}}
@endsection