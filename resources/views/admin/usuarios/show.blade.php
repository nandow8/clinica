@extends('layouts.template')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Visualizar Usuario </div>
                    <div class="card-body">

                        <a href="{{ url('/admin/usuarios') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                        
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr><th> Nome </th><td> {{ $usuario->name }} </td></tr><tr><th> Email </th><td> {{ $usuario->email }} </td></tr> 
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
