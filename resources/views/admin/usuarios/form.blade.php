<div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Name' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($usuario->name) ? $usuario->name : ''}}" >
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Email' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="email" type="email" id="email" value="{{ isset($usuario->email) ? $usuario->email : ''}}" >
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Password' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="password" type="password" id="password" >
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group offset-md-5">
    <input class="form-control" name="formMode" type="hidden" id="formMode" value="{{ $formMode }}" >
    @if($formMode === 'edita')
      <a class="btn btn-warning rounded" href="{{ url('/home') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Cancelar</a>
    @else
      <a class="btn btn-warning rounded" href="{{ url('/admin/usuarios') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @endif
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Salvar' : 'Salvar' }}</button>
</div>
