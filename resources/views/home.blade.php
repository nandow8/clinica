@extends('layouts.template')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    
                    
                    Seja bem vindo!
                    <br>
                    <!-- Apenas Administradores -->
                    @if (in_array(Auth::id(), [1,3,8,9]))
                    <div class="col-6">
                        <div class="card-title text-center" style="padding: 20px;">
                            <h4>Pacientes Registrados</h4>
                        </div>
                        <div class="card-img">
                            <canvas id="myChart" width="400" height="250"></canvas>
                        </div>
                        <div class="card-title text-center" style="padding: 10px;">
                            <p class="text-black">Total de pacientes: {{ $dependentes+$titulares+$particulares }}</p>
                        </div>
                    </div>
                    @endif
                    <!-- Fim Apenas Administradores -->
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Chart.js -->
<script src="{{ asset('/js/Chart.min.js') }}"></script>

<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    
    var myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: ['Particulares', 'Titulares', 'Dependentes'],
            datasets: [{
                label: '# Pacientes',
                data: [{{ $particulares }}, {{ $titulares }}, {{ $dependentes }}],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.5)',
                    'rgba(54, 162, 235, 0.5)',
                    'rgba(255, 206, 86, 0.5)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
//            scales: {
//                yAxes: [{
//                    ticks: {
//                        beginAtZero: true
//                    }
//                }]
//            }
        }
    });
</script>

@endsection
