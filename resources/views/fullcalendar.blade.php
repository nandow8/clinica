@extends('layouts.template')


<link href="{{ asset('css/fullcalendar/fullcalendar.min.css') }}" rel='stylesheet' />
<link href="{{ asset('css/fullcalendar/fullcalendar.print.min.css') }}" rel='stylesheet' media='print' />
<link href="{{ asset('css/fullcalendar/myfullcalendar.css') }}" rel='stylesheet'/>

@section('content')

<?php
	use App\Models\Funcionario;
  $iduser = Auth::user()->id;
	$tipoperfil = 'geral';
	if($iduser){
    $user = DB::table('users')->join('funcionarios', 'users.id', '=', 'funcionarios.user_id')->select()->where('users.id','=',$iduser)->get();

    if(count($user) > 0){
        $tipoperfil = ($user[0]->areaatuacao) ? $user[0]->areaatuacao : 'geral';
    }
  }
?>

<div class="container">
    <div class='row'>
      <div class="col-lg-3" >
		<div class='col-md-12'>
		<?php
			// $eventos = DB::table('agendamentos')
			// ->join('medicofichaclinicas', 'agendamentos.paciente', '=', 'medicofichaclinicas.paciente_id')
			// ->select()->where('agendamentos.id', '=', 1)
			// ->get();

			// dd($eventos);
		?>


			<h4 class="filtros-titulo">Filtros <i class="fa fa-filter"></i></h4>
			<div class="col-lg-12 filter-calendar" id="filter-rooms">
				<div class="filter-header">Salas <i class="fa fa-building-o"></i></div>
				<div id="rooms">
					<div class="each-room"><label for="todassalas"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="allsalas" value="todas" id="todassalas"/> <span class="room-title">Todas as salas</span> </label><i class="fa fa fa-tasks" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
					<?php
						$salas = DB::table('salas')->select('id','titulo', 'cor')->get();
						foreach($salas as $s => $room):
					?>
					<div class="each-room"><label for="sala_{{$room->id}}"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="salas[]" value="{{ $room->id}}" id="sala_{{$room->id}}" class="checksala"/> <span class="room-title">{{ $room->titulo}} </span></label><div class="room-color" style="background:{{$room->cor}}"></div></div>
					<?php endforeach; ?>
				</div>
			</div><br/>
			<?php
				if($tipoperfil !=='Médico'):
			?>
				<div class="col-lg-12 filter-calendar" id="filter-doctors">
					<div class="filter-header">Médicos <i class="fa fa-user-md"></i></div>
					<div id="doctors">
						<div class="each-doctor"><label for="todosmedicos"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="alldoutores" value="todos" id="todosmedicos"/> <span class="room-title">Todos os médicos</span> </label><i class="fa fa-tasks" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
							<?php
								$medicos = DB::table('funcionarios')->select('id','nome')->where('agenda', '=', 'sim')->get();
								foreach($medicos as $m => $medico):
							?>
							<div class="each-doctor"><label for="medico_{{$medico->id}}"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="medicos[]" value="{{ $medico->id }}" id="medico_{{$medico->id}}" class="checkmedico"/> <span class="medico-nome"><?php echo substr($medico->nome, 0, 20).'...' ?> </span></label></div>
							<?php endforeach; ?>
						</div>
				</div>
			<?php endif; ?>
        <div class="col-lg-12 filter-calendar" id="filter-status">
				<div class="filter-header">Status <i class="fa fa-hourglass"></i></div>
				<div id="status">
				<div class="each-status"><label for="todosStatus"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="allstatus" value="todos" id="todosStatus"/> <span class="status-title">Todos os status</span> </label><i class="fa fa-tasks" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
				<div class="each-status"><label for="status_2"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="status[]" value="aguardando" id="status_2" class="checkstatus"/> <span class="status-title">Aguardando</span> </label><i class="fa fa-clock-o" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
				<?php

					if($tipoperfil !=='Médico'):
          		?>
					<div class="each-status"><label for="status_1"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="status[]" value="agendado" id="status_1" class="checkstatus"/> <span class="status-title">Agendado</span> </label><i class="fa fa-calendar-check-o" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
					<div class="each-status"><label for="status_4"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="status[]" value="cancelado" id="status_4" class="checkstatus"/> <span class="status-title">Cancelada</span> </label><i class="fa fa-calendar-times-o" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
					<div class="each-status"><label for="status_5"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="status[]" value="remarcado" id="status_5" class="checkstatus"/> <span class="status-title">Remarcada</span> </label><i class="fa fa-refresh" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
				<?php endif;?>
				<div class="each-status"><label for="status_3"><i class="fa" style="color:#fff; margin-top:10px;"></i><input type="checkbox" name="status[]" value="finalizado" id="status_3" class="checkstatus"/> <span class="status-title">Finalizada</span> </label><i class="fa fa-check-square-o" style="float: right;margin-right: 1px; margin-top: 4px;"></i></div>
				</div>
			</div>

			</div><br/>
      </div>
      <div class='col-lg-9'>
        <div id='calendar'><!--o calendário vem aqui--></div>
      </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Agendamento</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <div class='row'>
        <input type="hidden" name="diasemana" id="diasemana">
        <div class='col-md-4'>
			<label>Data</label>
			<input id='_data' class='form-control' type='hidden'>
			<strong id='str_data'></strong>
        </div>
        <div class='col-md-4'>
			<label>Hora</label>
			<strong id='str_hora'>hora</strong>
        </div>
        <div class='col-md-4' id='boxredireciona' style="display:none;">
          <div class="dropdown">
            <button class="button dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-stethoscope fa-2" style="color:#13c713"></i> Atendimento
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#" ><i class="fa fa-television fa-2" style="color:#13c713"></i> Chamar no Telão</a>
        			<a class="dropdown-item" href="#" id="redireciona"><i class="fa fa-id-card-o fa-2" style="color:#13c713"></i> Ficha Clínica</a>
            </div>
          </div>
        </div>
      </div>
<hr>
      <div class='row'>
        <div class='col-md-10'>
          <label>Paciente</label>
          <select name='paciente' class='form-control'>
            <?php $pacientes = DB::select('select id, nome, cpf from pacientes where deleted_at is null ORDER BY nome ASC'); ?>
            <option value=''>Selecione</option>
            <?php foreach($pacientes as $k=>$aux): ?>
              <option value='<?= $aux->id ?>'><?= $aux->nome .' - '. $aux->cpf?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <?php if($tipoperfil!=="Médico"):?>
          <div class="col-md-2">
            <label >Adicionar</label>
            <a href="/usuarios/pacientes/create">
              <label class="new-register">
                <i class="fa fa-user-plus fa-2" style="color:#13c713"></i> Novo
              </label>
            </a>
          </div>
        <?php endif; ?>
      </div>
<hr>
      <div class='row'>
        <div class='col-md-6'>
          <label>Médico</label>
          <select name='medico' class='form-control'>
            <option value=''>Selecione</option>
            <?php foreach(DB::select('select id, nome from funcionarios where deleted_at is null and agenda = "sim"') as $k=>$aux): ?>
              <option value='<?= $aux->id ?>'> <?= $aux->nome ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class='col-md-6'>
        <label>Especialidades</label>
        <br>
        <strong id='mostrador'></strong>
        </div>
      </div>

<hr>

      <div class='row'>

        <div class='col-md-8'>
          <label>Clínica</label>
            <select name='clinica' class='form-control'>
                <option value=''>Selecione</option>
                <?php foreach(DB::select('select id, razao from clinicas where deleted_at is null') as $k=>$aux): ?>
                  <option value='<?= $aux->id ?>'> <?= $aux->razao ?></option>
                <?php endforeach; ?>
            </select>
        </div>

        <div class='col-md-4'>
        <label>Sala</label>
          <select name='sala' class='form-control' disabled>

          </select>
        </div>
      </div>
<hr>
      <div class='row'>
          <div class='col-md-4'>
          <label>Status</label>
            <select name='status' class='form-control'>
							<option value='agendado'>Agendado</option>
							<option value='aguardando'>Aguardando Atendimento</option>
							<option value='finalizado'>Consulta Finalizada</option>
							<option value='cancelado'>Consulta Cancelada</option>
							<option value='remarcado'>Consulta Remarcada</option>
            </select>
          </div>
          <div class='col-md-8'>
            <label>Atividade </label>
            <input name='atividade' class='form-control' type='text' value=".">
          </div>
        </div>
<br>
      <div class='row'>
        <div class='col-md-12'>
          <label>Observação</label>
          <input name='obs' class='form-control' type='text'>
        </div>
      </div>
<hr>
      <div class='row'>
          <div class='col-md-12'>
            <div id='alerta' class="alert alert-danger" role="alert" style='display: none; opacity: 0;'></div>
            <div id='alertamedico' class="alert alert-danger" role="alert" style='display: none; opacity: 0;'></div>
          </div>
      </div>

      </div>
      <div id='footer' class="modal-footer">
        <button type="button" class="btn btn-primary" onclick='salva()'>Salvar</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<!-- end modal -->

@endsection

<script src="{{ asset('js/fullcalendar/moment.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar/jqueryCalendar.min.js') }}"></script>

<script src="{{ asset('js/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ asset('js/fullcalendar/pt-br.js') }}"></script>
<!-- <script src="{{ asset('js/bootstrap.js') }}"></script> -->

@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/agendamentos_form.js')}}"></script>
    <script>
      var tipoperfil = '<?php echo $tipoperfil; ?>';
    </script>
@endsection
