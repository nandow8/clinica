<div class="form-group {{ $errors->has('clinica') ? 'has-error' : ''}}">
    <label for="clinica" class="control-label">{{ 'Clinica' }}</label>
    <?php
        $array = array();
        $row = DB::table('clinicas')->select('id','razao')->get();
    ?>
    <select name="clinica" class="form-control" id="clinica" required>
    <option value=''>Selecione uma clínica </option>
    @foreach($row as $k=>$aux)
    <option value='{{ $aux->id }}' {{ (isset($sala->clinica) && $sala->clinica == $aux->id) ? 'selected' : '' }}>{{ $aux->razao }}</option>
    @endforeach
    </select>
    {!! $errors->first('clinica', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('titulo') ? 'has-error' : ''}}">
    <label for="titulo" class="control-label">{{ 'Titulo' }}</label>
    <input class="form-control" name="titulo" type="text" id="titulo" value="{{ isset($sala->titulo) ? $sala->titulo : ''}}" required>
    {!! $errors->first('titulo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('cor') ? 'has-error' : ''}}">
    <label for="cor" class="control-label">{{ 'Cor' }}</label>
    <input class="form-control" name="cor" type="color" id="cor" value="{{ isset($sala->cor) ? $sala->cor : ''}}" >
    {!! $errors->first('cor', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="control-label">{{ 'Descricao' }}</label>
    <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" required>{{ isset($sala->descricao) ? $sala->descricao : ''}}</textarea>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"ativo":"Ativo","inativo": "Inativo"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($sala->status) && $sala->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
