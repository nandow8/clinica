@extends('layouts.template')
@section('content')
    
    <div class="container">
        <div class="row">
          

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Salas</div>
                    <div class="card-body">
                        <a href="{{ url('/cadastro/salas/create') }}" class="btn btn-success btn-sm" title="Add New Sala">
                            <i class="fa fa-plus" aria-hidden="true"></i> Add New
                        </a>
                        <form method="GET" action="{{ url('/cadastro/salas') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th><th>Clinica</th><th>Sala</th><th>Cor</th><th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($salas as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->clinica }}</td><td>{{ $item->titulo }}</td><td><div style='border-radius: 50%; width:30px; height: 30px; background-color:{{ $item->cor }}'>&nbsp</div></td>
                                        <td>
                                            <a href="{{ url('/cadastro/salas/' . $item->id) }}" title="View Sala"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                            <a href="{{ url('/cadastro/salas/' . $item->id . '/edit') }}" title="Edit Sala"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/cadastro/salas' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Sala" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $salas->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
