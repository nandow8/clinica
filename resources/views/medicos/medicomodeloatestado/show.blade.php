@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Medicomodeloatestado</div>
                <div class="card-body">

                    <a href="{{ url('/medicos/medicomodeloatestado') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $medicomodeloatestado->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Title </th><td> {{ $medicomodeloatestado->title }} </td></tr><tr><th> Texto </th><td> {{ $medicomodeloatestado->texto }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
