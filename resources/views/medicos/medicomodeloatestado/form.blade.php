<div class="form-group row {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Title' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($medicomodeloatestado->title) ? $medicomodeloatestado->title : ''}}" >
</div>
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('texto') ? 'has-error' : ''}}">
    <label for="texto" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Texto' }}</label>
    <div class="col-lg-8 col-md-8 col-sm-8">
    <textarea class="form-control" rows="20" name="texto" type="textarea" id="texto" >{{ isset($medicomodeloatestado->texto) ? $medicomodeloatestado->texto : ''}}</textarea>
</div>
    {!! $errors->first('texto', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/medicos/medicomodeloatestado') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection