<style> 
@media only screen and (max-width: 695px) {
    #escondemenu {
        display: none;
    }
}
@media only screen and (min-width: 695px) {
    #menu-vertical{
        position:fixed;
        z-index:999;  
        overflow:hidden;  
    }
    #principal{
        margin-left: 250px;
    }
    .tamanhocheck::before, .tamanhocheck::after {
        top: 0.1rem;
        width: 1.35rem;
        height: 1.35rem;
    }
}
    .espacocheck{ margin-top: 3%;}
    .form-controlz { width: 100%; color: #5e5e5e;}
    .form-control { width: 80%; }
    .espacotitle{margin: 3%;}
    .espacosubtitle{margin-top: 3%; margin-bottom: 1%; color: #5e5e5e;}
    .espacorow{ margin-top: 1%;}
    .espacorow2{ margin-bottom: 2.8%; }
    .input-linha,select.form-control {
        background: transparent;
        border: none;
        border-bottom: 1px solid #000000;
        -webkit-box-shadow: none;
        box-shadow: none;
        border-radius: 0;
    }

    .input-linha:focus, select.form-control:focus {
        -webkit-box-shadow: none;
        box-shadow: none;
    }
    .size-font-examefisico{
        font-size: 0.7em;
    }
    .espacamento-direita{
        right: 5%;
    }
</style>
{{-- Anamnese --}}
<h2 style="text-align: center; margin-top:3%;">Anamnese</h2>
<div class="tab-pane" id="anamnase" role="tabpanel" aria-labelledby="anamnase-tab">
        <a id="anamnese"></a>
        <div class="row" style="padding-top: 25px;">
            <div id="escondemenu">
                <div class="col-2" id="menu-vertical">
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a href="#anamnese" class="scrollSuave">Anamnese Dirigida</a><br/>
                        <a href="#historia" class="scrollSuave">História Patológica <br/ >Pregressa</a><br/>
                        <a href="#outrasdoencas" class="scrollSuave">Outras Doenças</a><br/>
                        <a href="#historiafisiologica" class="scrollSuave">História Fisiológica</a><br/>
                        <a href="#historiadapessoa" class="scrollSuave">História da Pessoa</a><br/>
                        <a href="#examefisico" class="scrollSuave">Exame Físico</a>
                    </div>
                </div>
            </div>
            
            <div class="col-8 col-md-7 col-xl-10 col-sm-12 col-lg-10" id="principal">
                
                <div class="tab-pane fade show active" id="v-pills-anamnese" role="tabpanel" aria-labelledby="v-pills-anamnese-tab">
                    <div class="container">
                        <div class="row">
                            <label for="" class="form-controlz" >Q.P</label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="qp" type="textarea" id="qp" >{{ isset($pacienteanamnese[0]->qp) ? $pacienteanamnese[0]->qp : ''}}</textarea>
                            <label for="" class="form-controlz" >H.D.A</label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="hda" type="textarea" id="hda" >{{ isset($pacienteanamnese[0]->hda) ? $pacienteanamnese[0]->hda : ''}}</textarea>
                        </div>
                        <h3 class="espacotitle"><strong>Anamnese Dirigida</strong></h3>
                        <h4 class="espacosubtitle"><strong>Geral</strong></h4>
                        <div class="row">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_emagrecimento" class="custom-control-input" id="geral_emagrecimento" value="1" {{ isset($pacienteanamnese[0]->geral_emagrecimento) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_emagrecimento">Emagrecimento</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_fadiga" class="custom-control-input" id="geral_fadiga" value="1" {{ isset($pacienteanamnese[0]->geral_fadiga) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_fadiga">Fadiga</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_insonia" class="custom-control-input" id="geral_insonia" value="1" {{ isset($pacienteanamnese[0]->geral_insonia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_insonia">Insônia</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_visao" class="custom-control-input" id="geral_visao" value="1" {{ isset($pacienteanamnese[0]->geral_visao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_visao">Visão</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_audicao" class="custom-control-input" id="geral_audicao" value="1" {{ isset($pacienteanamnese[0]->geral_audicao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_audicao">Audição</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_zumbido" class="custom-control-input" id="geral_zumbido" value="1" {{ isset($pacienteanamnese[0]->geral_zumbido) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_zumbido">Zumbido</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_cefaleia" class="custom-control-input" id="geral_cefaleia" value="1" {{ isset($pacienteanamnese[0]->geral_cefaleia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_cefaleia">Cefaléia</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_vertigens" class="custom-control-input" id="geral_vertigens" value="1" {{ isset($pacienteanamnese[0]->geral_vertigens) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_vertigens">Vestigens</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_tonteiras" class="custom-control-input" id="geral_tonteiras" value="1" {{ isset($pacienteanamnese[0]->geral_tonteiras) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_tonteiras">Tonteiras</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="geral_outros" id="geral_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->geral_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="geral_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="geral_outros_obs" placeholder="Outros" class="input-linha" id="geral_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->geral_outros_obs) ? $pacienteanamnese[0]->geral_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Pele, Fâneros e Mucosas</strong></h4>
                        
                        <div class="row espacorow">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="pele_alteracoesdacor" class="custom-control-input" id="pele_alteracoesdacor" value="1" {{ isset($pacienteanamnese[0]->pele_alteracoesdacor) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="pele_alteracoesdacor">Alterações da cor</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="pele_prurido" class="custom-control-input" id="pele_prurido" value="1" {{ isset($pacienteanamnese[0]->pele_prurido) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="pele_prurido">Prurido</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="pele_lesoes" class="custom-control-input" id="pele_lesoes" value="1" {{ isset($pacienteanamnese[0]->pele_lesoes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="pele_lesoes">Lesões</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="pele_outros" id="pele_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->pele_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="pele_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="pele_outros_obs" placeholder="Outros" class="input-linha" id="pele_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->pele_outros_obs) ? $pacienteanamnese[0]->pele_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Nariz e Seios Paranasais</strong></h4>
                        
                        <div class="row espacorow">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="nariz_dor" class="custom-control-input" id="nariz_dor" value="1" {{ isset($pacienteanamnese[0]->nariz_dor) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="nariz_dor">Dor</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="nariz_epistaxes" class="custom-control-input" id="nariz_epistaxes" value="1" {{ isset($pacienteanamnese[0]->nariz_epistaxes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="nariz_epistaxes">Epistaxes</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="nariz_obstrucao" class="custom-control-input" id="nariz_obstrucao" value="1" {{ isset($pacienteanamnese[0]->nariz_obstrucao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="nariz_obstrucao">Obstrução</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="nariz_outros" id="nariz_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->nariz_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="nariz_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="nariz_outros_obs" placeholder="Outros" class="input-linha" id="nariz_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->nariz_outros_obs) ? $pacienteanamnese[0]->nariz_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Boca e Garganta</strong></h4>
                        
                        <div class="row espacorow">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="dentes_dentes" class="custom-control-input" id="dentes_dentes" value="1" {{ isset($pacienteanamnese[0]->dentes_dentes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="dentes_dentes">Dentes</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="dentes_gengivas" class="custom-control-input" id="dentes_gengivas" value="1" {{ isset($pacienteanamnese[0]->dentes_gengivas) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="dentes_gengivas">Gengivas</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="dentes_rouquidao" class="custom-control-input" id="dentes_rouquidao" value="1" {{ isset($pacienteanamnese[0]->dentes_rouquidao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="dentes_rouquidao">Rouquidão</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="dentes_outros" id="dentes_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->dentes_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="dentes_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="dentes_outros_obs" placeholder="Outros" class="input-linha" id="dentes_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->dentes_outros_obs) ? $pacienteanamnese[0]->dentes_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Pescoço</strong></h4>
                        
                        <div class="row espacorow">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="pescoco_dor" class="custom-control-input" id="pescoco_dor" value="1" {{ isset($pacienteanamnese[0]->pescoco_dor) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="pescoco_dor">Dor</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="pescoco_tumoracoes" class="custom-control-input" id="pescoco_tumoracoes" value="1" {{ isset($pacienteanamnese[0]->pescoco_tumoracoes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="pescoco_tumoracoes">Tumorações</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="pescoco_outros" id="pescoco_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->pescoco_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="pescoco_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="pescoco_outros_obs" placeholder="Outros" class="input-linha" id="pescoco_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->pescoco_outros_obs) ? $pacienteanamnese[0]->pescoco_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Mamas</strong></h4>
                        
                        <div class="row espacorow">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="mamas_nodulos" class="custom-control-input" id="mamas_nodulos" value="1" {{ isset($pacienteanamnese[0]->mamas_nodulos) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="mamas_nodulos">Nodulos</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="mamas_dor" class="custom-control-input" id="mamas_dor" value="1" {{ isset($pacienteanamnese[0]->mamas_dor) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="mamas_dor">Dor</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="mamas_secrecao" class="custom-control-input" id="mamas_secrecao" value="1" {{ isset($pacienteanamnese[0]->mamas_secrecao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="mamas_secrecao">Secreção</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="mamas_outros" id="mamas_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->mamas_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="mamas_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="mamas_outros_obs" placeholder="Outros" class="input-linha" id="mamas_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->mamas_outros_obs) ? $pacienteanamnese[0]->mamas_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        
                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong> Anamnese dirigida obs...</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="geral_observacoes" type="textarea" id="geral_observacoes"> {{ isset($pacienteanamnese[0]->geral_observacoes) ? $pacienteanamnese[0]->geral_observacoes : ''}} </textarea>
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Aparelho Cardíaco: Dispinéia aos Esforços:</strong></h4>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_grandes" class="custom-control-input" id="apcardiaco_grandes" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_grandes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_grandes">Grandes</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_medios" class="custom-control-input" id="apcardiaco_medios" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_medios) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_medios">Médios</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_pequenos" class="custom-control-input" id="apcardiaco_pequenos" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_pequenos) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_pequenos">Pequenos</label>
                                </div>
                            </div> 
                        </div>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_repouso" class="custom-control-input" id="apcardiaco_repouso" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_repouso) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_repouso">Repouso</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_ortopneia" class="custom-control-input" id="apcardiaco_ortopneia" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_ortopneia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_ortopneia">Ortopneia</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_dispneia" class="custom-control-input" id="apcardiaco_dispneia" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_dispneia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_dispneia">Dispnéia</label>
                                </div>
                            </div> 
                        </div>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_paroxisticanoturna" class="custom-control-input" id="apcardiaco_paroxisticanoturna" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_paroxisticanoturna) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_paroxisticanoturna">Paroxistica noturna</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_suspinosa" class="custom-control-input" id="apcardiaco_suspinosa" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_suspinosa) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_suspinosa">Suspirosa</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_palpitacoes" class="custom-control-input" id="apcardiaco_palpitacoes" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_palpitacoes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_palpitacoes">Palpitações</label>
                                </div>
                            </div> 
                        </div>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_tosse" class="custom-control-input" id="apcardiaco_tosse" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_tosse) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_tosse">Tosse</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_cianosehemoptise" class="custom-control-input" id="apcardiaco_cianosehemoptise" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_cianosehemoptise) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_cianosehemoptise">Cianose-hemoptise</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_sincope" class="custom-control-input" id="apcardiaco_sincope" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_sincope) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_sincope">Síncope</label>
                                </div>
                            </div> 
                        </div>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_edema" class="custom-control-input" id="apcardiaco_edema" value="1" {{ isset($pacienteanamnese[0]->apcardiaco_edema) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_edema">Edema</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apcardiaco_outros" id="apcardiaco_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->apcardiaco_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apcardiaco_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="apcardiaco_outros_obs" placeholder="Outros" class="input-linha" id="apcardiaco_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->apcardiaco_outros_obs) ? $pacienteanamnese[0]->apcardiaco_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Aparelho Respiratório</strong></h4>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aprespiratorio_chiado" class="custom-control-input" id="aprespiratorio_chiado" value="1" {{ isset($pacienteanamnese[0]->aprespiratorio_chiado) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aprespiratorio_chiado">Chiado</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aprespiratorio_infeccaorespiratoria" class="custom-control-input" id="aprespiratorio_infeccaorespiratoria" value="1" {{ isset($pacienteanamnese[0]->aprespiratorio_infeccaorespiratoria) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aprespiratorio_infeccaorespiratoria">Infecção respiratoria</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aprespiratorio_tosse" class="custom-control-input" id="aprespiratorio_tosse" value="1" {{ isset($pacienteanamnese[0]->aprespiratorio_tosse) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aprespiratorio_tosse">Tosse</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aprespiratorio_expectoracao" class="custom-control-input" id="aprespiratorio_expectoracao" value="1" {{ isset($pacienteanamnese[0]->aprespiratorio_expectoracao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aprespiratorio_expectoracao">Expectoração</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aprespiratorio_outros" id="aprespiratorio_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->aprespiratorio_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aprespiratorio_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="aprespiratorio_outros_obs" placeholder="Outros" class="input-linha" id="aprespiratorio_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->aprespiratorio_outros_obs) ? $pacienteanamnese[0]->aprespiratorio_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Aparelho Digestivo</strong></h4>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_disfagia" class="custom-control-input" id="apdigestivo_disfagia" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_disfagia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_disfagia">Disfagia</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_pirose" class="custom-control-input" id="apdigestivo_pirose" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_pirose) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_pirose">Pirose</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_epigastralgia" class="custom-control-input" id="apdigestivo_epigastralgia" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_epigastralgia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_epigastralgia">Epigastralgia</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_dorabdominal" class="custom-control-input" id="apdigestivo_dorabdominal" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_dorabdominal) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_dorabdominal">Dor abdominal</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_funcionamentointestinal" class="custom-control-input" id="apdigestivo_funcionamentointestinal" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_funcionamentointestinal) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_funcionamentointestinal">Funcionamento intestinal</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_homorroidas" class="custom-control-input" id="apdigestivo_homorroidas" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_homorroidas) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_homorroidas">Hemorróidas</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_sangramentodigestivo" class="custom-control-input" id="apdigestivo_sangramentodigestivo" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_sangramentodigestivo) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_sangramentodigestivo">Sangramento digestivo</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_usodemedicacao" class="custom-control-input" id="apdigestivo_usodemedicacao" value="1" {{ isset($pacienteanamnese[0]->apdigestivo_usodemedicacao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_usodemedicacao">Uso de medicação</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apdigestivo_outros" id="apdigestivo_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->apdigestivo_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apdigestivo_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="apdigestivo_outros_obs" placeholder="Outros" class="input-linha" id="apdigestivo_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->apdigestivo_outros_obs) ? $pacienteanamnese[0]->apdigestivo_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Aparelho Gênito-Urinário</strong></h4>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_alteracoescorurina" class="custom-control-input" id="apurinario_alteracoescorurina" value="1" {{ isset($pacienteanamnese[0]->apurinario_alteracoescorurina) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_alteracoescorurina">Alterações cor de urina</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_volume" class="custom-control-input" id="apurinario_volume" value="1" {{ isset($pacienteanamnese[0]->apurinario_volume) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_volume">Volume</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_frequencia" class="custom-control-input" id="apurinario_frequencia" value="1" {{ isset($pacienteanamnese[0]->apurinario_frequencia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_frequencia">Frequência</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_disuria" class="custom-control-input" id="apurinario_disuria" value="1" {{ isset($pacienteanamnese[0]->apurinario_disuria) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_disuria">Disúria</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_nicturia" class="custom-control-input" id="apurinario_nicturia" value="1" {{ isset($pacienteanamnese[0]->apurinario_nicturia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_nicturia">Nictúria</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_secrecao" class="custom-control-input" id="apurinario_secrecao" value="1" {{ isset($pacienteanamnese[0]->apurinario_secrecao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_secrecao">Secreção</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_impotencia" class="custom-control-input" id="apurinario_impotencia" value="1" {{ isset($pacienteanamnese[0]->apurinario_impotencia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_impotencia">Impotência</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_alteracoesmenstruais" class="custom-control-input" id="apurinario_alteracoesmenstruais" value="1" {{ isset($pacienteanamnese[0]->apurinario_alteracoesmenstruais) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_alteracoesmenstruais">Alterações menstruais</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_sangramentoabdominal" class="custom-control-input" id="apurinario_sangramentoabdominal" value="1" {{ isset($pacienteanamnese[0]->apurinario_sangramentoabdominal) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_sangramentoabdominal">Sangramento abdominal</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="apurinario_outros" id="apurinario_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->apurinario_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="apurinario_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="apurinario_outros_obs" placeholder="Outros" class="input-linha" id="apurinario_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->apurinario_outros_obs) ? $pacienteanamnese[0]->apurinario_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Sistema nervoso</strong></h4>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="sisnervoso_paresias" class="custom-control-input" id="sisnervoso_paresias" value="1" {{ isset($pacienteanamnese[0]->sisnervoso_paresias) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="sisnervoso_paresias">Paresias</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="sisnervoso_parentesias" class="custom-control-input" id="sisnervoso_parentesias" value="1" {{ isset($pacienteanamnese[0]->sisnervoso_parentesias) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="sisnervoso_parentesias">Parentesias</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="sisnervoso_tremores" class="custom-control-input" id="sisnervoso_tremores" value="1" {{ isset($pacienteanamnese[0]->sisnervoso_tremores) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="sisnervoso_tremores">Tremores</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="sisnervoso_ausencias" class="custom-control-input" id="sisnervoso_ausencias" value="1" {{ isset($pacienteanamnese[0]->sisnervoso_ausencias) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="sisnervoso_ausencias">Ausências</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="sisnervoso_convulcoes" class="custom-control-input" id="sisnervoso_convulcoes" value="1" {{ isset($pacienteanamnese[0]->sisnervoso_convulcoes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="sisnervoso_convulcoes">Convulções</label>
                                </div>
                            </div>
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="sisnervoso_outros" id="sisnervoso_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->sisnervoso_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="sisnervoso_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="sisnervoso_outros_obs" placeholder="Outros" class="input-linha" id="sisnervoso_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->sisnervoso_outros_obs) ? $pacienteanamnese[0]->sisnervoso_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Aparelho locomotor</strong></h4>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aplocomotor_dor" class="custom-control-input" id="aplocomotor_dor" value="1" {{ isset($pacienteanamnese[0]->aplocomotor_dor) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aplocomotor_dor">Paresias</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aplocomotor_impotenciafuncional" class="custom-control-input" id="aplocomotor_impotenciafuncional" value="1" {{ isset($pacienteanamnese[0]->aplocomotor_impotenciafuncional) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aplocomotor_impotenciafuncional">Parentesias</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="aplocomotor_outros" id="aplocomotor_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->aplocomotor_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="aplocomotor_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="aplocomotor_outros_obs" placeholder="Outros" class="input-linha" id="aplocomotor_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->aplocomotor_outros_obs) ? $pacienteanamnese[0]->aplocomotor_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Psiquismo</strong></h4>
                        
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="psiquismo_ansiedade" class="custom-control-input" id="psiquismo_ansiedade" value="1" {{ isset($pacienteanamnese[0]->psiquismo_ansiedade) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="psiquismo_ansiedade">Ansiedade</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="psiquismo_depressao" class="custom-control-input" id="psiquismo_depressao" value="1" {{ isset($pacienteanamnese[0]->psiquismo_depressao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="psiquismo_depressao">Depressão</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="psiquismo_alucinacoes" class="custom-control-input" id="psiquismo_alucinacoes" value="1" {{ isset($pacienteanamnese[0]->psiquismo_alucinacoes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="psiquismo_alucinacoes">Alucinações</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="psiquismo_outros" id="psiquismo_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->psiquismo_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="psiquismo_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="psiquismo_outros_obs" placeholder="Outros" class="input-linha" id="psiquismo_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->psiquismo_outros_obs) ? $pacienteanamnese[0]->psiquismo_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <a id="historia"></a>
                        <h3 class="espacotitle"><strong>História Patológica Pregressa</strong></strong></h3>
                        
                        <h4 class="espacosubtitle"><strong>Doenças Infecciosas e Parasitárias</strong></h4>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_doencasinfancia" class="custom-control-input" id="doencasinfecciosas_doencasinfancia" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_doencasinfancia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_doencasinfancia">Doenças comuns durante a infância</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_doencasvenereas" class="custom-control-input" id="doencasinfecciosas_doencasvenereas" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_doencasvenereas) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_doencasvenereas">Doenças sexuamente trasmitiveis</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_febreindeterminada" class="custom-control-input" id="doencasinfecciosas_febreindeterminada" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_febreindeterminada) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_febreindeterminada">Febre de causa indeterminada</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_amigdalite" class="custom-control-input" id="doencasinfecciosas_amigdalite" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_amigdalite) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_amigdalite">Amigdalite</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_febrereumatica" class="custom-control-input" id="doencasinfecciosas_febrereumatica" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_febrereumatica) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_febrereumatica">Febre reumática</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_hepatite" class="custom-control-input" id="doencasinfecciosas_hepatite" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_hepatite) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_hepatite">Hepatite</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_infeccaourinaria" class="custom-control-input" id="doencasinfecciosas_infeccaourinaria" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_infeccaourinaria) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_infeccaourinaria">Infecção urinária</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_tuberculose" class="custom-control-input" id="doencasinfecciosas_tuberculose" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_tuberculose) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_tuberculose">Tuberculose</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_peneumonia" class="custom-control-input" id="doencasinfecciosas_peneumonia" value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_peneumonia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_peneumonia">Peneumonia</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="doencasinfecciosas_outros" id="doencasinfecciosas_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->doencasinfecciosas_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="custom-control">
                                    <input type="text" name="doencasinfecciosas_outros_obs" placeholder="Outros" class="input-linha" id="doencasinfecciosas_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->doencasinfecciosas_outros_obs) ? $pacienteanamnese[0]->doencasinfecciosas_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <h4 class="espacosubtitle"><strong>Manifestações alérgicas</strong></h4>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="manifalergicas_asma" class="custom-control-input" id="manifalergicas_asma" value="1" {{ isset($pacienteanamnese[0]->manifalergicas_asma) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="manifalergicas_asma">Asma</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="manifalergicas_rinite" class="custom-control-input" id="manifalergicas_rinite" value="1" {{ isset($pacienteanamnese[0]->manifalergicas_rinite) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="manifalergicas_rinite">Rinite</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="manifalergicas_urticaria" class="custom-control-input" id="manifalergicas_urticaria" value="1" {{ isset($pacienteanamnese[0]->manifalergicas_urticaria) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="manifalergicas_urticaria">Urticaria</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="manifalergicas_eczema" class="custom-control-input" id="manifalergicas_eczema" value="1" {{ isset($pacienteanamnese[0]->manifalergicas_eczema) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="manifalergicas_eczema">Eczema</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="manifalergicas_alergiamedicamentosaealimentar" class="custom-control-input" id="manifalergicas_alergiamedicamentosaealimentar" value="1" {{ isset($pacienteanamnese[0]->manifalergicas_alergiamedicamentosaealimentar) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="manifalergicas_alergiamedicamentosaealimentar">Alergia a medicamentos</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="manifalergicas_outros" id="manifalergicas_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->manifalergicas_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="manifalergicas_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="form-group">
                                    <input type="text" name="manifalergicas_outros_obs" placeholder="Outros" class="input-linha" id="manifalergicas_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->manifalergicas_outros_obs) ? $pacienteanamnese[0]->manifalergicas_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong> Cirurgias, traumas ou transfusões</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="obs_cirurgiastraumasconvulcoes" type="textarea" id="obs_cirurgiastraumasconvulcoes"> {{ isset($pacienteanamnese[0]->obs_cirurgiastraumasconvulcoes) ? $pacienteanamnese[0]->obs_cirurgiastraumasconvulcoes : ''}} </textarea>
                        </div>
                        
                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong> Uso prolongado de medicamentos</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="obs_usodemedicamentos" type="textarea" id="obs_usodemedicamentos"> {{ isset($pacienteanamnese[0]->obs_usodemedicamentos) ? $pacienteanamnese[0]->obs_usodemedicamentos : ''}} </textarea>
                        </div>
                        
                        <a id="outrasdoencas"></a>
                        <h3 class="espacotitle"><strong>Outras Doenças</strong></strong></h3>
                        
                        <h4 class="espacosubtitle"><strong></strong></h4>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_diabetes" class="custom-control-input" id="outrasdoencas_diabetes" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_diabetes) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_diabetes">Diabetes</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_hipertencao" class="custom-control-input" id="outrasdoencas_hipertencao" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_hipertencao) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_hipertencao">Hipertenção</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_cardiopatias" class="custom-control-input" id="outrasdoencas_cardiopatias" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_cardiopatias) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_cardiopatias">Cardiopatias</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_altlipidios" class="custom-control-input" id="outrasdoencas_altlipidios" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_altlipidios) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_altlipidios">Alt. lipidios</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_nefropatias" class="custom-control-input" id="outrasdoencas_nefropatias" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_nefropatias) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_nefropatias">Nefropatias</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_ulcerapeptica" class="custom-control-input" id="outrasdoencas_ulcerapeptica" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_ulcerapeptica) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_ulcerapeptica">Úlcera peptica</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_neoplastia" class="custom-control-input" id="outrasdoencas_neoplastia" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_neoplastia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_neoplastia">Neoplastia</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_epilepsia" class="custom-control-input" id="outrasdoencas_epilepsia" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_epilepsia) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_epilepsia">Epilepsia</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_neurose" class="custom-control-input" id="outrasdoencas_neurose" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_neurose) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_neurose">Neurose</label>
                                </div>
                            </div> 
                        </div>
                        <div class="row espacorow2">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_glaucoma" class="custom-control-input" id="outrasdoencas_glaucoma" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_glaucoma) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_glaucoma">Glaucoma</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_gota" class="custom-control-input" id="outrasdoencas_gota" value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_gota) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_gota">Gota</label>
                                </div>
                            </div>
                        </div>
                        <div class="row espacocheck">
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="outrasdoencas_outros" id="outrasdoencas_outros" class="custom-control-input"  value="1" {{ isset($pacienteanamnese[0]->outrasdoencas_outros) ? 'checked' : ''}}>
                                    <label class="custom-control-label tamanhocheck" for="outrasdoencas_outros">Outros</label>
                                </div>
                            </div> 
                            <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                <div class="custom-control">
                                    <input type="text" name="outrasdoencas_outros_obs" placeholder="Outros" class="input-linha" id="outrasdoencas_outros_obs" size="32" value="{{ isset($pacienteanamnese[0]->outrasdoencas_outros_obs) ? $pacienteanamnese[0]->outrasdoencas_outros_obs : ''}}">
                                </div>
                            </div>  
                        </div>
                        
                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Obs</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="outrasdoencas_obs" type="textarea" id="outrasdoencas_obs"> {{ isset($pacienteanamnese[0]->outrasdoencas_obs) ? $pacienteanamnese[0]->outrasdoencas_obs : ''}} </textarea>
                        </div>

                        <a id="historiafisiologica"></a>
                        <h3 class="espacotitle"><strong>História Fisiológica</strong></strong></h3>
                        
                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Nascimento - Crescimento - Puberdade - Menarca - Catamênios - Gestação - Climatério</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="obs_historiafisiologica" type="textarea" id="obs_historiafisiologica"> {{ isset($pacienteanamnese[0]->obs_historiafisiologica) ? $pacienteanamnese[0]->obs_historiafisiologica : ''}} </textarea>
                        </div>
                        
                        <a id="historiadapessoa"></a>
                        <h3 class="espacotitle"><strong>História da Pessoa</strong></strong></h3>
                        
                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Alimentação - Moradia - Hábitos (fumo - álcool - anticoncepcionais - tóxicos - exercícios)</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="obs_historiapessoa" type="textarea" id="obs_historiapessoa"> {{ isset($pacienteanamnese[0]->obs_historiapessoa) ? $pacienteanamnese[0]->obs_historiapessoa : ''}} </textarea>
                        </div>
                        
                        <a id="examefisico"></a>
                        <h3 class="espacotitle"><strong>Exame Físico</strong></strong></h3>
                        
                        <h4 class="espacosubtitle"><strong></strong></h4>
                        <div class="row">
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_peso" class="form-controlz" ><strong class="size-font-examefisico">Peso</strong></label>
                                <div class="input-group input-group-sm mb-12">
                                    <input onkeyup="calculaImc(this)" type=" width="10"text" name="examefisico_peso" id="examefisico_peso" value="{{ isset($pacienteanamnese[0]->examefisico_peso) ? $pacienteanamnese[0]->examefisico_peso : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_altura" class="form-controlz" ><strong class="size-font-examefisico">Altura</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input onkeyup="calculaImc(this)" type=" width="10"text" name="examefisico_altura" id="examefisico_altura" value="{{ isset($pacienteanamnese[0]->examefisico_altura) ? $pacienteanamnese[0]->examefisico_altura : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_imc" class="form-controlz" ><strong class="size-font-examefisico">IMC</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" id="examefisico_imc" width="10" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_tax" class="form-controlz" ><strong class="size-font-examefisico">Tax</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_tax" width="10" id="examefisico_tax" value="{{ isset($pacienteanamnese[0]->examefisico_tax) ? $pacienteanamnese[0]->examefisico_tax : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_respiracao" class="form-controlz" ><strong class="size-font-examefisico">Respiração</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_respiracao" width="10" id="examefisico_respiracao" value="{{ isset($pacienteanamnese[0]->examefisico_respiracao) ? $pacienteanamnese[0]->examefisico_respiracao : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_pulso" class="form-controlz" ><strong class="size-font-examefisico">Pulso</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_pulso" width="10" id="examefisico_pulso" value="{{ isset($pacienteanamnese[0]->examefisico_pulso) ? $pacienteanamnese[0]->examefisico_pulso : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_pabd_deitado" class="form-controlz" ><strong class="size-font-examefisico">PA - BD - Deitado</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_pabd_deitado" width="10" id="examefisico_pabd_deitado" value="{{ isset($pacienteanamnese[0]->examefisico_pabd_deitado) ? $pacienteanamnese[0]->examefisico_pabd_deitado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_pabd_sentado" class="form-controlz" ><strong class="size-font-examefisico">PA - BD - Sentado</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_pabd_sentado" width="10" id="examefisico_pabd_sentado" value="{{ isset($pacienteanamnese[0]->examefisico_pabd_sentado) ? $pacienteanamnese[0]->examefisico_pabd_sentado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                        </div>
                        <div class="row">
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_pabd_empe" class="form-controlz" ><strong class="size-font-examefisico">PA - BD - Em Pé</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_pabd_empe" width="10" id="examefisico_pabd_empe" value="{{ isset($pacienteanamnese[0]->examefisico_pabd_empe) ? $pacienteanamnese[0]->examefisico_pabd_empe : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_pabe_deitado" class="form-controlz" ><strong class="size-font-examefisico">PA - BE - Deitado</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_pabe_deitado" width="10" id="examefisico_pabe_deitado" value="{{ isset($pacienteanamnese[0]->examefisico_pabe_deitado) ? $pacienteanamnese[0]->examefisico_pabe_deitado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_pabe_sentado" class="form-controlz" ><strong class="size-font-examefisico">PA - BE - Sentado</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_pabe_sentado" width="10" id="examefisico_pabe_sentado" value="{{ isset($pacienteanamnese[0]->examefisico_pabe_sentado) ? $pacienteanamnese[0]->examefisico_pabe_sentado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                            <div class="col-10 col-sm-2 col-md-3 col-xl-2 col-lg-2">
                                <label for="examefisico_pabe_empe" class="form-controlz" ><strong class="size-font-examefisico">PA - BE - Em Pé</strong></label>
                                <div class="input-group input-group-sm mb-12 rounded">
                                    <input type="text" name="examefisico_pabe_empe" width="10" id="examefisico_pabe_empe" value="{{ isset($pacienteanamnese[0]->examefisico_pabe_empe) ? $pacienteanamnese[0]->examefisico_pabe_empe : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                </div>
                            </div> 
                        </div>


                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Exame Geral: Inspeção geral: Pele - Nutrição - Fâneros - Estado mental</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="pele_observacoes" type="textarea" id="pele_observacoes"> {{ isset($pacienteanamnese[0]->pele_observacoes) ? $pacienteanamnese[0]->pele_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Cabeça: Tireóide - Linfonodos - Vasos</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="nariz_observacoes" type="textarea" id="nariz_observacoes"> {{ isset($pacienteanamnese[0]->nariz_observacoes) ? $pacienteanamnese[0]->nariz_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Tórax: Inspeção geral - Fossa supraclavicular linfonodos</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="dentes_observacoes" type="textarea" id="dentes_observacoes"> {{ isset($pacienteanamnese[0]->dentes_observacoes) ? $pacienteanamnese[0]->dentes_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Ap. Respiratório: Dispnéia - Frêmitos - Percussão - Auscuta</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="pescoco_observacoes" type="textarea" id="pescoco_observacoes"> {{ isset($pacienteanamnese[0]->pescoco_observacoes) ? $pacienteanamnese[0]->pescoco_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Ap. Cardíaco: Abaulamento e retrações - Ritmo Ictus - Ritmo e frequência cardíaca </strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="mamas_observacoes" type="textarea" id="mamas_observacoes"> {{ isset($pacienteanamnese[0]->mamas_observacoes) ? $pacienteanamnese[0]->mamas_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Abdômen e Região Lombar: Inspeção geral - Sopros - Volume e forma - Macicês - Percussão</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="apcardiaco_observacoes" type="textarea" id="apcardiaco_observacoes"> {{ isset($pacienteanamnese[0]->apcardiaco_observacoes) ? $pacienteanamnese[0]->apcardiaco_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Fígado: Hepatimetria - Consistência - Borda - Sensibilidade - Superfície - Ponto cístico</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="aprespiratorio_observacoes" type="textarea" id="aprespiratorio_observacoes"> {{ isset($pacienteanamnese[0]->aprespiratorio_observacoes) ? $pacienteanamnese[0]->aprespiratorio_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Baço: Espaço de Traube - Consistência </strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="apdigestivo_observacoes" type="textarea" id="apdigestivo_observacoes"> {{ isset($pacienteanamnese[0]->apdigestivo_observacoes) ? $pacienteanamnese[0]->apdigestivo_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Gênito Urinária: Punho percussão lombar - Globo Vesical - Pontos Renoureterais</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="apurinario_observacoes" type="textarea" id="apurinario_observacoes"> {{ isset($pacienteanamnese[0]->apurinario_observacoes) ? $pacienteanamnese[0]->apurinario_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Membros: Inspeção geral - Varizes - Edema - Enchimento Capilar - P.A. no membro inferior</strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="sisnervoso_observacoes" type="textarea" id="sisnervoso_observacoes"> {{ isset($pacienteanamnese[0]->sisnervoso_observacoes) ? $pacienteanamnese[0]->sisnervoso_observacoes : ''}} </textarea>
                        </div>

                        <div class="row espacocheck">
                            <label for="" class="form-controlz" ><strong>Sistema Nervoso e Ósteo Articular: Estática - Reflexos - Marcha - Sensibilidade </strong></label>
                            <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2" name="aplocomotor_observacoes" type="textarea" id="aplocomotor_observacoes"> {{ isset($pacienteanamnese[0]->aplocomotor_observacoes) ? $pacienteanamnese[0]->aplocomotor_observacoes : ''}} </textarea>
                        </div>

                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    {{-- fim Anamnese --}}

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        jQuery(document).ready(function () {
            calculaImc()
        });

        function calculaImc(){
            jQuery('#examefisico_altura').mask('0.00');
            jQuery('#examefisico_peso').mask('00.00');
            var peso = jQuery('#examefisico_peso').val();
            var altura = jQuery('#examefisico_altura').val();

            if(peso != "" && altura != "" ){
                var quadrado = (altura * altura);
                var calculo = (peso/quadrado);
                jQuery('#examefisico_imc').val(Math.round(calculo));   
            }
        }
    </script>
    