<style>
    @media only screen and (max-width: 600px) {.celular{margin-left: 1%;}  }
    @media only screen and (min-width: 600px) { .celular{ margin-left: 35%; }}
</style>
<div class="container">
<h2 style="text-align: center; margin-top:3%;">Anexos</h2>
    <hr>
    <div id="adiciona-campo1">
        <div class="row justify-content-md-center">
            <div class="col-md-auto" >
                @if(request()->route()->getActionMethod() !== 'show') 
                    <button type="button" id="add-campo-anexo1" class="btn btn-primary rounded"> <i class="fa fa-plus-square fa-2"></i> Adicionar Anexos </button>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="mx-auto mt-5">
    <?php for ($i=0; $i < count($pacienteanexos) ; $i++) :  ?>    
        <?php  $image = explode(".", $pacienteanexos[$i]->nomeanexo); ?>
        <div class="row">
            <table class="table table-borderless table-responsive celular" >
                <tbody>
                    <tr>
                        <td><button type="button" class="btn btn-outline-success" id="excluir1_{{ $pacienteanexos[$i]->id }}" rounded" data-toggle="modal" data-target="#modal<?php echo $image[0]   ?>">{{ strtoupper($pacienteanexos[$i]->nomeanexo) }}</button></td>
                        <td><button type="button" class="btn btn-danger rounded" id="{{ $pacienteanexos[$i]->id }}" onclick="excluiAnexo(this.id)" ><i class="fa fa-trash-o"></i></button></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="modal fade bd-example-modal-lg" id="modal<?php echo $image[0]   ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <img class="img-fluid" src="/uploads/fichaclinicaanexos/<?php echo $pacienteanexos[$i]->nomeanexo; ?>" /> 
                        <div class="modal-footer" style="background-color: #FFFFFF;">
                            <button type="button" class="btn btn-danger rounded" data-dismiss="modal">Fechar</button>
                        </div>
                    </div>
                </div>
            </div>
    <?php endfor; ?>
</div>
    
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        var cont = 1;
        jQuery("#add-campo-anexo1").click(function () {
            cont++;
            var addcampoanexo = '<div class="row justify-content-md-center mt-3"  id="add-campo-anexo1' + cont +'"><div class="col-md-auto" ><input type="file" name="file[]" multiple class="btn btn-success rounded col-10" /><button style="margin-left: 2%;" type="button" id="' + cont + '" class="rounded btn btn-danger btn-apagar"><i class="fa fa-trash-o"></i></button></div></div>';
            jQuery("#adiciona-campo1").append(addcampoanexo);
        });

        jQuery('form').on('click', '.btn-apagar', function () {
            var button_id = jQuery(this).attr("id");
            jQuery('#add-campo-anexo1' + button_id + '').remove();
        });

        function excluiAnexo(anexo_id){
            console.log(anexo_id)

            jQuery.get('/medicos/ajaxexcluiranexo/' + anexo_id, function (data) {
                jQuery('#excluir1_' + anexo_id).remove();
                jQuery('#' + anexo_id).remove();
            });
            
        }
    </script>
