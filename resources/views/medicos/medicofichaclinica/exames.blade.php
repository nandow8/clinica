<!-- exames -->
<style>
    .margem-sufite{ margin-left: 4%;  }
    #exame-sufite{ font-size: 1.1em; color:gray; }
    .observacao-sufite{ font-size: 0.9em; color:gray; }

    #footer-exame {position: absolute; bottom: 53px; }
    #footer-historico-exame-data{ display: none    }
    #footer-historico-exame-linha{ display: none    }
    #footer-historico-exame-assinatura{ display: none    }
    @media print {
        @page { margin: 0; }
        #footer-exame {
            position: fixed;
            top: 1680px;
            bottom: 0;
        }

        #footer-historico-exame-data{
                display: block;
                position: fixed;
                top: 1630px;
                font-size: 25px;
        }
        #footer-historico-exame-linha{
                display: block;
                position: fixed;
                top: 1600px;
                left: 450px;
                font-size: 25px;
        }
        #footer-historico-exame-assinatura{
                display: block;
                position: fixed;
                top: 1630px;
                left: 620px;
                font-size: 25px;
        }

        #exame-sufite{ font-size: 1.6em; color:gray; }
    .observacao-sufite{ font-size: 1.2em; color:gray; }
    }
</style>
<div class="container">
    <h2 style="text-align: center; margin-top:3%;">Exames</h2>
    <hr>
    <div class="group-header">
        <strong>Dados Gerais do Pedido</strong>
    </div>
    <div class="row espacocheck offset-md-2">
        <label for="introducao_pedido" class="form-controlz" ><strong>Introdução do Pedido</strong></label>
        <input type="text" name="introducao_pedido" id="introducao_pedido" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
    </div>
    <div class="row espacocheck offset-md-2">
        <label for="info_adicional" class="form-controlz" ><strong> Info. adicional</strong></label>
        <textarea class="form-control" rows="2" name="info_adicional" type="textarea" id="info_adicional"></textarea>
    </div>

    <div class="group-header" style="margin-top: 3%;">
            <strong>Incluir Exames</strong>
        </div>
        <div class="row espacocheck offset-md-2">
            <label for="exame" class="form-controlz" ><strong>Exame</strong></label>
            <input type="text" name="exame" onkeypress="autocompletaexame(this)" id="exame" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <div class="row espacocheck offset-md-2">
            <label for="codigo" class="form-controlz" ><strong>Código</strong></label>
            <input type="text" name="codigo" id="codigo" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <div class="row espacocheck offset-md-2">
            <label for="observacoes" class="form-controlz" ><strong> Observações</strong></label>
            <textarea class="form-control summernote" rows="2" name="observacoes" type="textarea" id="observacoes"></textarea>
        </div>
        <div class="row md-3 offset-2" style="margin-top: 1%; margin-bottom: 4%;">
            <div class="col-3">
                <button type="button" class="btn rounded btn-success" id="add-exame-sufite"> Adicionar</button>
            </div>
            <div class="col-4">
                <button type="button" id="repetirultimareceita-exame" class="btn rounded btn-primary">Repetir ultima receita</button>
            </div>
            <div class="col-3">
                <button type="button" onclick="printme()" class="rounded btn btn-info">Imprimir</button>
            </div>
        </div>


        <div id="printable" class="row"  style="background-color: #f1f1f1; height: 900px;">
            <div class="col-12 mt-5">
                <div class="container" style="background-color: white; height: 800px; width: 80%;">
                    <img src="/img/medicofichaclinica/<?php echo 'saegs-exame.png' ; ?>" style="width:100%" id="img-header">
                    <div class="row">
                        <div class="col-12 mt-3" id="sufite-exame-add">
                            <div class="margem-sufite descricao-sufite"><strong>Paciente: </strong> {{ $dados_paciente['nome'] }}</div>
                        </div>
                    </div>
                    <p id="footer-historico-exame-data">{{ date('d/m/Y') }}</p>
                    <!-- <p id="footer-historico-exame-linha">__________________________________</p>
                    <p id="footer-historico-exame-assinatura" >Assinatura</p> -->
                    <img  src="/img/medicofichaclinica/<?php echo 'saegs-footer.jpeg' ; ?>" style="width:73%;" id="footer-exame">
                </div>
            <div>
        </div>
        </div>
        </div>
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        function printme() {
            window.print();
        }

        jQuery("#repetirultimareceita-exame").click(function () {
            var user_id = jQuery("input[name='user_id']").val()
            var medicofichaclinica_id = jQuery("input[name='medicofichaclinica_id']").val()

            jQuery.getJSON('/medicos/ajaxultimoexame/' + medicofichaclinica_id + '/' + user_id, function (data) {
                if(data){
                    var exame = data.exame.split("---")
                    var codigo = data.codigo.split("---")
                    var observacoes = data.observacoes.split("---")

                    for (let index = 0; index < exame.length; index++) {
                        var addultimoexame = '<div class="col-12 mt-3 container" id="add-exame-sufite' + index +'">'
                                    +'<div class="margem-sufite"> <strong id="medicamento-sufite"> Solicito <br />' + exame[index] + ' </strong><button type="button"  id="' + index + '" class="btn btn-light rounded pull-right btn-apagar-exame"> <i class="fa fa-times fa-2" id="icone-apagar"></i> </button></div>'
                                    +'<div class="margem-sufite descricao-sufite"> '+ codigo[index]  +' </div>'
                                    +'<div class="margem-sufite descricao-sufite">'+ observacoes[index] +'</div>'

                                    +'<input type="hidden" name="exame[]" value="'+ exame[index] +'" />'
                                    +'<input type="hidden" name="codigo[]" value="'+ codigo[index] +'" />'
                                    +'<input type="hidden" name="observacoes[]" value="'+ observacoes[index] +'" />'
                                +'<hr></div>';
                        jQuery("#sufite-exame-add").append(addultimoexame);

                    }
                }else{
                    Swal.fire({
                        position: 'center',
                        type: 'error',
                        title: 'Nenhum exame anterior foi encontrado para este paciente.',
                        animation: false,
                        customClass: 'animated tada',
                        showConfirmButton: false,
                        timer: 2500
                    })
                }

            });

        });

        var contexame = 1;
        jQuery("#add-exame-sufite").click(function () {
            var exame = jQuery('#exame').val()
            var codigo = jQuery('#codigo').val()
            var observacoes = jQuery('#observacoes').val()

            if(codigo){
                contexame++;

                var addcampoexame = '<div class="col-12 mt-3 container" id="add-exame-sufite' + contexame +'">'
                                +'<div class="margem-sufite"> <strong id="exame-sufite">Solicito <br />' + exame + ' </strong><button type="button"  id="' + contexame + '" class="btn btn-light rounded pull-right btn-apagar-exame"> <i class="fa fa-times fa-2" id="icone-apagar"></i> </button></div>'
                                +'<div class="margem-sufite observacao-sufite"> código: '+ codigo  +' </div>'
                                +'<div class="margem-sufite observacao-sufite">obs:'+ observacoes +'</div>'

                                +'<input type="hidden" name="exame[]" value="'+ exame +'" />'
                                +'<input type="hidden" name="codigo[]" value="'+ codigo +'" />'
                                +'<input type="hidden" name="observacoes[]" value="'+ observacoes +'" />'
                                +'<hr></div>';
                jQuery("#sufite-exame-add").append(addcampoexame);

                jQuery('#exame').val("");
                jQuery('#codigo').val("");
                jQuery('#observacoes').val("");
            }
        });

        jQuery('form').on('click', '.btn-apagar-exame', function () {
            var button_id = jQuery(this).attr("id");
            jQuery('#add-exame-sufite' + button_id + '').remove();
        });

        function autocompletaexame(obj){
            var val = jQuery(obj).val()

            jQuery.getJSON('/medicos/ajaxexibirexamerf/' + val , function (data) {
                jQuery("#exame").autocomplete({
                    source: function(request, response){
                        response(data)
                    },
                    select: function (e, ui) {
                        codigo = ui.item.label.split("---")[0]
                        jQuery('#codigo').val(codigo)
                    },
                    change: function (e, ui) {
                        exame = ui.item.label.split("---")[1]
                        jQuery('#exame').val(exame)
                    }
                });
            });
        }
    </script>
<!-- End exames -->
