@extends('layouts.template')

@section('content')
    <div class="row">
        
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <span class="float-left">Ficha clinica do Paciente</span>
                    <span class="float-right">Tempo do atendimento: <span id="minuto">00:</span><span id="segundo">00</span></span>
                </div>
                <div class="card-body">
                        
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" id="formedit" action="{{ url('/medicos/medicofichaclinica/' . $medicofichaclinica->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        @include ('medicos.medicofichaclinica.form', ['formMode' => 'edit'])

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
