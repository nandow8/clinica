<style>
    .timeline {
    list-style: none;
    padding: 20px 0 20px;
    position: relative;

    }

    .timeline:before {
    top: 0;
    bottom: 0;
    position: absolute;
    content: " ";
    width: 3px;
    background-color: #eeeeee;
    left: 10%;
    margin-left: -1.5px;
    }

    .timeline > li {
    margin-bottom: 20px;
    position: relative;
    }

    .timeline > li:before,
    .timeline > li:after {
    content: " ";
    display: table;
    }

    .timeline > li:after {
    clear: both;
    }

    .timeline > li:before,
    .timeline > li:after {
    content: " ";
    display: table;
    }

    .timeline > li:after {
    clear: both;
    }

    .timeline > li > .timeline-panel {
    width: 80%;
    float: left;
    border: 1px solid #d4d4d4;
    border-radius: 2px;
    padding: 20px;
    position: relative;
    background-color: #f9f9f9;
    -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
    }

    .timeline > li > .timeline-panel:before {
    position: absolute;
    top: 26px;
    right: -15px;
    display: inline-block;
    border-top: 15px solid transparent;
    border-left: 15px solid #ccc;
    border-right: 0 solid #ccc;
    border-bottom: 15px solid transparent;
    content: " ";
    }

    .timeline > li > .timeline-panel:after {
    position: absolute;
    top: 27px;
    right: -14px;
    display: inline-block;
    border-top: 14px solid transparent;
    border-left: 14px solid #fff;
    border-right: 0 solid #fff;
    border-bottom: 14px solid transparent;
    content: " ";
    }

    .timeline > li > .timeline-badge {
      color: #fff;
      width: 50px;
      height: 50px;
      line-height: 50px;
      font-size: 1.4em;
      text-align: center;
      position: absolute;
      top: 16px;
      left: 10%;
      margin-left: -25px;
      background-color: red;
      z-index: 100;
      border-top-right-radius: 50%;
      border-top-left-radius: 50%;
      border-bottom-right-radius: 50%;
      border-bottom-left-radius: 50%;
    }

    .timeline > li.timeline-inverted > .timeline-panel {
    margin-left: 15%;
    }

    .timeline > li.timeline-inverted > .timeline-panel:before {
    border-left-width: 0;
    border-right-width: 15px;
    left: -15px;
    right: auto;
    }

    .timeline > li.timeline-inverted > .timeline-panel:after {
    border-left-width: 0;
    border-right-width: 14px;
    left: -14px;
    right: auto;
    }

    .timeline-badge.primary {
    background-color: #2e6da4 !important;
    }

    .timeline-badge.success {
    background-color: #3f903f !important;
    }

    .timeline-badge.warning {
    background-color: #f0ad4e !important;
    }

    .timeline-badge.danger {
    background-color: #d9534f !important;
    }

    .timeline-badge.info {
    background-color: #5bc0de !important;
    }

    .timeline-title {
    margin-top: 0;
    color: inherit;
    }

    .timeline-body > p,
    .timeline-body > ul {
    margin-bottom: 0;
    }

    .timeline-body > p + p {
    margin-top: 5px;
    }

    .medicoborda{
      border-style: solid; border-color: lightgray; border-width: 1px; padding: 2%; background-color: white;
    }

    .font-print{
      color: purple; font-size: 1.4em; cursor: pointer;
    }


</style>
<div class="container">
  <h2 style="text-align: center; margin-top:3%;">Histórico</h2>
  <hr>
</div>

<div class="container shadow-lg p-3 mb-5 bg-white rounded">
  <ul class="timeline">

    @if($pacientehistorico->isEmpty())
      <p class="d-flex justify-content-center h5">Nenhum histórico encontrado para este paciente.</p>
    @endif

    @foreach($pacientehistorico as $key => $historico)
      <li class="timeline-inverted">
        <div class="timeline-badge success"><i class="glyphicon glyphicon-credit-card"></i></div>
        <div class="timeline-panel">
          <div class="timeline-heading">
            <h4 class="timeline-title">
              <span>Atendido por: {{ App\User::find($historico->user_id)['name'] }}</span>
              <span class="ml-5">No dia: {{ Carbon\Carbon::parse($historico->created_at)->format('d/m/Y') }}</span>
              <span class="ml-5">Consulta: {{ $historico['tempo_atendimento'] }}s</span>
                <span class="pull-right">
                  @if($key == 0 AND $historico->retificacao == "")
                    <button type="button" class="btn btn-outline-danger btn-sm rounded" data-toggle="modal" data-target="#exampleModalCenter">
                      Retificação
                    </button>
                  @endif
                  <button type="button" class="btn btn-outline-info btn-sm rounded anamnese_id" data-toggle="modal" data-target="#exampleModalCenterAnamnese" id="{{ App\Models\Medicoanamnese::find($historico->medicoanamnese_id)['id'] }}">
                    Anamnese
                  </button>
                </span>
                <!-- hidden input --> <input type="hidden" id="historico_id" value="{{ $historico->id }}"> <!-- end hidden input -->
            </h4>
          </div>
          <div class="timeline-body">
          <?php if ($historico->retificacao) :   ?>
              <br>
              <h5 class="medicoborda rounded shadow p-3 mb-2 bg-white rounded">
                <div>
                  <i class="fa fa-pencil-square-o fa-2" style="color: #FF4500; font-size: 1.2em;"></i>  <span>Retificação</span>
                </div>
                <p class="mt-2"> {{ $historico->retificacao }} </p>
              </h5>
            <?php endif; ?>

            <?php if ($historico->medicoevolucao_id) :   ?>
              <br>
              <h5 class="medicoborda rounded shadow p-3 mb-2 bg-white rounded">
                <div>
                  <i class="fa fa-line-chart mb-3" style="color: blue; font-size: 1.2em;"></i>  <span>Evolução</span>
                </div>
                <p>{{ App\Models\Medicoevolucao::find($historico->medicoevolucao_id)['evolucao'] }}</p>
              </h5>
            <?php endif; ?>

            <?php if ($historico->medicoresultado_id) :   ?>
              <br>
              <h5 class="medicoborda rounded shadow p-3 mb-2 bg-white rounded">
                <div>
                  <i class="fa fa-bar-chart fa-2" style="color: blue; font-size: 1.2em;"></i>  <span>Resultados</span>
                </div>
                <p>{{ App\Models\Medicoresultado::find($historico->medicoresultado_id)['resultados'] }}</p>
              </h5>
            <?php endif; ?>

            <?php if ($historico->medicoexames_id) :   ?>
              <br>
              <h5 class="rounded medicoborda shadow p-3 mb-2 bg-white rounded">
                <div>
                  <i class="fa fa-stethoscope mb-3" style="color: blue; font-size: 1.7em;"></i>
                  <span>Exames </span>
                  @if($key == 0)
                  <i class="fa fa-print fa-2 pull-right font-print exames_pegaid" id="{{ App\Models\Medicoexames::find($historico->medicoexames_id)['id'] }}"></i>
                  @endif
                </div>
                <p><b>Intro. pedido</b>: {{ App\Models\Medicoexames::find($historico->medicoexames_id)['introducao_pedido'] }}</p>

                <br>
                <p><b>Info. adicional</b>{{ App\Models\Medicoexames::find($historico->medicoexames_id)['info_adicional'] }}</p>

                <?php
                  $exames = explode("---", App\Models\Medicoexames::find($historico->medicoexames_id)['exame'] );
                  $codigos = explode("---", App\Models\Medicoexames::find($historico->medicoexames_id)['codigo'] );
                  $observacoes = explode("---", App\Models\Medicoexames::find($historico->medicoexames_id)['observacoes'] );
                ?>

                <br>
                <?php for ($i=0; $i < count($exames); $i++) : ?>
                  <p><b>exame</b>: {{ $exames[$i] }}</p>
                  <p><b>Código</b>: {{ $codigos[$i] }}</p>
                  <p><b>Observação</b>: {{ $observacoes[$i]}}</p>
                  <hr>
                <?php endfor; ?>
              </h5>
            <?php endif; ?>

            <?php if ($historico->medicoatestado_id) :   ?>
              <br>
              <h5 class="medicoborda shadow p-3 mb-2 bg-white rounded">
                <div>
                  <i class="fa fa-sticky-note-o mb-3" style="color: blue; font-size: 1.2em;"></i>
                  <span>Atestado</span>
                  @if($key == 0)
                  <i class="fa fa-print fa-2 pull-right font-print atestado_pegaid" id="{{ App\Models\Medicoatestado::find($historico->medicoatestado_id)['id'] }}"></i>
                  @endif
                </div>
                <p>{{ App\Models\Medicoatestado::find($historico->medicoatestado_id)['titulo'] }}</p>
                <p>{{ App\Models\Medicoatestado::find($historico->medicoatestado_id)['atestado'] }}</p>
              </h5>
            <?php endif; ?>

            <?php if ($historico->medicoprescricoes_id) :   ?>
              <br>
              <h5 class="rounded medicoborda shadow p-3 mb-2 bg-white rounded">
                <div>
                  <i class="fa fa-medkit fa-2" style="color: blue; font-size: 1.6em;"></i>
                  <span>Prescrições</span>
                  @if($key == 0)
                  <i class="fa fa-print fa-2 pull-right font-print prescricoes_pegaid" id="{{ App\Models\Medicoprescricoes::find($historico->medicoprescricoes_id)['id'] }}"></i>
                  @endif
                </div>

                <?php
                  $prescricoes_tipos = explode("---", App\Models\Medicoprescricoes::find($historico->medicoprescricoes_id)['prescricoes_tipo'] );
                  $tipo_receitas = explode("---", App\Models\Medicoprescricoes::find($historico->medicoprescricoes_id)['tipo_receita'] );
                  $medicamentos = explode("---", App\Models\Medicoprescricoes::find($historico->medicoprescricoes_id)['medicamento'] );
                  $descricoes = explode("---", App\Models\Medicoprescricoes::find($historico->medicoprescricoes_id)['descricao'] );
                  $posologias = explode("---", App\Models\Medicoprescricoes::find($historico->medicoprescricoes_id)['posologia'] );
                ?>

                <br>
                <?php for ($i=0; $i < count($prescricoes_tipos); $i++) : ?>
                  <p><b>tipo de prescrição</b>: {{ $prescricoes_tipos[$i] }} </p>
                  <p><b>Tipo de receita: </b><?php echo (isset($tipo_receitas[$i])) ? $tipo_receitas[$i] : ''; ?></p>
                  <p><b>medicamentos</b>: {{ $medicamentos[$i] }}</p>
                  <p><b>descricão</b>: {{ $descricoes[$i]}}</p>
                  <p><b>posologia</b>: {{ $posologias[$i]}}</p>
                  <hr>
                <?php endfor; ?>
              </h5>
            <?php endif; ?>

            <?php if ($historico->medicodiagnostico_id) : ?>
              <br>
              <h5 class="rounded medicoborda shadow p-3 mb-2 bg-white rounded">
                <div>
                  <i class="fa fa-exclamation fa-2" style="color: blue; font-size: 1.7em;"></i>  <span>Diagnostico</span>
                </div>

                <?php
                  $cids = explode("---", App\Models\Medicodiagnostico::find($historico->medicodiagnostico_id)['cid'] );
                ?>

                <br>
                <?php for ($i=0; $i < count($cids); $i++) : ?>
                  <p><b>CID</b>: {{ $cids[$i] }}</p>
                  <hr>
                <?php endfor; ?>

                <p><b>Info. adicional</b>: {{  App\Models\Medicodiagnostico::find($historico->medicodiagnostico_id)['informacoesadicionais'] }}</p>

              </h5>
            <?php endif; ?>

          </div>
        </div>
      </li>
    @endforeach

    <div id="printable-historico-prescricao" class="prescricaocontainer">
    <div id="printable-historico-exame" class="examecontainer">
    <div id="printable-historico-atestado" class="atestadocontainer">

    </div>
  </ul>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Retificação</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="form-group" style="margin-left: 15%;">
          <textarea class="form-control" name="retificacao" id="retificacao" rows="8"></textarea>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn rounded btn-danger" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn rounded btn-primary" id="saveretificacao">Salvar Alterações</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal  Anamnese-->
<div class="modal fade" id="exampleModalCenterAnamnese" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterexampleModalCenterAnamneseTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
    <div id="historico-anamnese"></div>
  </div>
</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script>

  jQuery(".prescricoes_pegaid").click(function(event){
    id_prescricao = this.id
    
    jQuery.get('/medicos/ajaxhistoricoprescricoespdf/' + id_prescricao, function (data) {
        jQuery('#printable-historico-prescricao').html(data.html);
        window.print();
        window.location.reload()
    });
  })

  jQuery(".exames_pegaid").click(function(event){
    id_exame = this.id
    
    jQuery.get('/medicos/ajaxhistoricoexamespdf/' + id_exame, function (data) {
        jQuery('#printable-historico-exame').html(data.html);
        window.print();
        window.location.reload()
    });
  })

  jQuery(".atestado_pegaid").click(function(event){
      id_atestado = this.id
      
    jQuery.get('/medicos/ajaxhistoricoatestadospdf/' + id_atestado , function (data) {
        jQuery('#printable-historico-atestado').html(data.html);
        window.print();
        window.location.reload()
    });
  })

  jQuery("#saveretificacao").click(function(event){
    var historico_id =$("#historico_id").val()
    var retificacao =$("#retificacao").val()

    jQuery.get('/medicos/ajaxhistoricoatestadosretificacao/' + historico_id + '/' +  retificacao, function (data) {
        window.location.reload()
    });


  })

  jQuery(".anamnese_id").click(function(event){
      id_anamnese = this.id
      
    jQuery.get('/medicos/buscaanamnese/' + id_anamnese , function (data) {
    
        jQuery('#historico-anamnese').html(data.html); 
    });
  })
</script>
