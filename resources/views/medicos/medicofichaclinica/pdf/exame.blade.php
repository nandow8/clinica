<style>
    .examecontainer{
      display: none;
    }
    
    @media print {
        @page { margin: 0; }
        
        #printable-historico-exame, #printable-historico-exame * {
            visibility: visible;
        }
        #printable-historico-exame {width: 100%;position: fixed;left: 0;top: 0; }
 
        .examecontainer{
          display: block;
        }
        footer {
            position: absolute;
            top: 1400px;
            width: 100%;
            bottom: 0;
        }

        #assinatura-exame{
            margin-left: 77%;
            font-size: 25px;
        }


        #linha-exame{
            margin-left: 70%; 
        }

        #data-exame{
            margin-top: 34px;
            margin-left: 20px;
            font-size: 25px;
        }

        .tamanho-exame {
            font-size: 30px;
        }
        .tamanho-ex {
            font-size: 20px;
        }
    }
    
    
     
</style>

<img src="/img/medicofichaclinica/<?php echo 'saegs-exame.png' ; ?>" style="width:100%"  >

<div class="container">
    <?php 
        $introducao_pedido = $exames[0]->introducao_pedido ;
        $info_adicional = $exames[0]->info_adicional ;
        $exames_ex = explode("---", $exames[0]->exame );
        $codigos = explode("---", $exames[0]->codigo );
        $observacoes = explode("---", $exames[0]->observacoes );
    ?>

    <br>
    <p class="mb-4 h3"><strong>Paciente: </strong>  {{ \App\Models\Paciente::find(\App\Models\Medicofichaclinica::find($exames[0]->medicofichaclinica_id)['paciente_id'])['nome'] }} </p>
    <?php for ($i=0; $i < count($exames_ex) ; $i++) : ?>
        <p class="tamanho-exame"><b>Solicito</b> </p>
        <p class="tamanho-exame"><b> {{ $exames_ex[$i]  }}</b></p>  <!-- . ' (' . $prescricoes_tipos[$i] . ')' -->
        <p class="tamanho-ex"> {{ $codigos[$i]}}</p>
        <p class="tamanho-ex"> {{ $observacoes[$i]}}</p>
        <hr>
    <?php endfor; ?>
</div>

<footer>
    <p id="data-exame">{{ date('d/m/Y') }}</p>
    <!-- <p id="linha-exame">__________________________________</p>
    <p  id="assinatura-exame">Assinatura</p> -->
    <img  src="/img/medicofichaclinica/<?php echo 'saegs-footer.jpeg' ; ?>" style="width:100%;" id="footer-historico-exame">
</footer>
