<style>
    .atestadocontainer{
      display: none;
    }

    @media print {
        .atestadocontainer{ display: block;}
        #printable-historico-atestado, #printable-historico-atestado * {
            visibility: visible;
        }
        #printable-historico-atestado {
            width: 100%;
            position: fixed;
            left: 0;
            top: 0;
        }
        footer {
            position: absolute;
            top: 1400px;
            width: 100%;
            bottom: 0;
        }

        #assinatura-atestado{
            margin-left: 77%;
            font-size: 25px;
        }


        #linha-atestado{
            margin-left: 70%; 
            margin-bottom: 15px;
        }

        #data-atestado{
            margin-top: 34px;
            margin-left: 20px;
            font-size: 25px;
        }
         
    }
    
   
     
    .tamanho-atestado{ font-size: 30px;  }
    .tamanho-ex{ font-size: 23px;  }
</style>

<img src="/img/medicofichaclinica/<?php echo 'atestadolaudo.png' ; ?>" style="width:100%" id="img-header">

<div class="container">
    

    <br>
    @foreach($atestados as $atestado)
        <p class="tamanho-atestado"><b> {{ $atestado->titulo  }}</b></p>
        <p class="tamanho-ex"> {{ $atestado->atestado }}</p>
        
        <hr>
    @endforeach
</div>

<footer>
    <p id="data-atestado">{{ date('d/m/Y') }}</p>
    <!-- <p id="linha-atestado">__________________________________</p>
    <p id="assinatura-atestado" class="mr-10">Assinatura</p> -->
    <img  src="/img/medicofichaclinica/<?php echo 'saegs-footer.jpeg' ; ?>" style="width:100%;" id="footer-historico-atestado">
</footer>