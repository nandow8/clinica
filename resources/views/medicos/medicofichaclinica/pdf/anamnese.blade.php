<!-- Modal  Anamnese-->

  
<div class="modal-content w-auto">
      <div class="modal-header">
        <h5 class="modal-title" >Anamnese</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h2 style="text-align: center; margin-top:3%;">Anamnese</h2>
 
            <div class="row" style="padding-top: 25px;">
       
                
                <div class="col-12">
                    
                    <div class="tab-pane fade show active" pills-anamnese" role="tabpanel" aria-labelledby="v-pills-anamnese-tab">
                        <div class="container">
                            <div class="row">
                                <label for="" class="form-controlz" >Q.P</label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea"  >{{ isset($buscaAnamnese[0]->qp) ? $buscaAnamnese[0]->qp : ''}}</textarea>
                                <label for="" class="form-controlz" >H.D.A</label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea"  >{{ isset($buscaAnamnese[0]->hda) ? $buscaAnamnese[0]->hda : ''}}</textarea>
                            </div>
                            <h3 class="espacotitle"><strong>Anamnese Dirigida</strong></h3>
                            <h4 class="espacosubtitle"><strong>Geral</strong></h4>
                            <div class="row">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_emagrecimento) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_emagrecimento">Emagrecimento</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_fadiga) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_fadiga">Fadiga</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_insonia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_insonia">Insônia</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_visao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_visao">Visão</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_audicao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_audicao">Audição</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_zumbido) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_zumbido">Zumbido</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_cefaleia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_cefaleia">Cefaléia</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_vertigens) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_vertigens">Vestigens</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_tonteiras) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_tonteiras">Tonteiras</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->geral_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="geral_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->geral_outros_obs) ? $buscaAnamnese[0]->geral_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Pele, Fâneros e Mucosas</strong></h4>
                            
                            <div class="row espacorow">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->pele_alteracoesdacor) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="pele_alteracoesdacor">Alterações da cor</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->pele_prurido) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="pele_prurido">Prurido</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->pele_lesoes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="pele_lesoes">Lesões</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->pele_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="pele_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->pele_outros_obs) ? $buscaAnamnese[0]->pele_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Nariz e Seios Paranasais</strong></h4>
                            
                            <div class="row espacorow">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->nariz_dor) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="nariz_dor">Dor</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->nariz_epistaxes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="nariz_epistaxes">Epistaxes</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->nariz_obstrucao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="nariz_obstrucao">Obstrução</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->nariz_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="nariz_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->nariz_outros_obs) ? $buscaAnamnese[0]->nariz_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Boca e Garganta</strong></h4>
                            
                            <div class="row espacorow">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->dentes_dentes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="dentes_dentes">Dentes</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->dentes_gengivas) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="dentes_gengivas">Gengivas</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->dentes_rouquidao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="dentes_rouquidao">Rouquidão</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->dentes_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="dentes_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->dentes_outros_obs) ? $buscaAnamnese[0]->dentes_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Pescoço</strong></h4>
                            
                            <div class="row espacorow">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->pescoco_dor) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="pescoco_dor">Dor</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->pescoco_tumoracoes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="pescoco_tumoracoes">Tumorações</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->pescoco_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="pescoco_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->pescoco_outros_obs) ? $buscaAnamnese[0]->pescoco_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Mamas</strong></h4>
                            
                            <div class="row espacorow">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->mamas_nodulos) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="mamas_nodulos">Nodulos</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->mamas_dor) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="mamas_dor">Dor</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->mamas_secrecao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="mamas_secrecao">Secreção</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->mamas_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="mamas_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->mamas_outros_obs) ? $buscaAnamnese[0]->mamas_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            
                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><strong> Anamnese dirigida obs...</strong></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->geral_observacoes) ? $buscaAnamnese[0]->geral_observacoes : ''}} </textarea>
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Aparelho Cardíaco: Dispinéia aos Esforços:</strong></h4>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_grandes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_grandes">Grandes</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_medios) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_medios">Médios</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_pequenos) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_pequenos">Pequenos</label>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_repouso) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_repouso">Repouso</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_ortopneia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_ortopneia">Ortopneia</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_dispneia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_dispneia">Dispnéia</label>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_paroxisticanoturna) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_paroxisticanoturna">Paroxistica noturna</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_suspinosa) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_suspinosa">Suspirosa</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_palpitacoes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_palpitacoes">Palpitações</label>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_tosse) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_tosse">Tosse</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_cianosehemoptise) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_cianosehemoptise">Cianose-hemoptise</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_sincope) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_sincope">Síncope</label>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_edema) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_edema">Edema</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apcardiaco_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apcardiaco_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->apcardiaco_outros_obs) ? $buscaAnamnese[0]->apcardiaco_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Aparelho Respiratório</strong></h4>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aprespiratorio_chiado) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aprespiratorio_chiado">Chiado</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aprespiratorio_infeccaorespiratoria) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aprespiratorio_infeccaorespiratoria">Infecção respiratoria</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aprespiratorio_tosse) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aprespiratorio_tosse">Tosse</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aprespiratorio_expectoracao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aprespiratorio_expectoracao">Expectoração</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aprespiratorio_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aprespiratorio_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->aprespiratorio_outros_obs) ? $buscaAnamnese[0]->aprespiratorio_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Aparelho Digestivo</strong></h4>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_disfagia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_disfagia">Disfagia</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_pirose) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_pirose">Pirose</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_epigastralgia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_epigastralgia">Epigastralgia</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_dorabdominal) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_dorabdominal">Dor abdominal</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_funcionamentointestinal) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_funcionamentointestinal">Funcionamento intestinal</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_homorroidas) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_homorroidas">Hemorróidas</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_sangramentodigestivo) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_sangramentodigestivo">Sangramento digestivo</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_usodemedicacao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_usodemedicacao">Uso de medicação</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apdigestivo_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apdigestivo_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->apdigestivo_outros_obs) ? $buscaAnamnese[0]->apdigestivo_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Aparelho Gênito-Urinário</strong></h4>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_alteracoescorurina) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_alteracoescorurina">Alterações cor de urina</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_volume) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_volume">Volume</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_frequencia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_frequencia">Frequência</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_disuria) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_disuria">Disúria</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_nicturia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_nicturia">Nictúria</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_secrecao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_secrecao">Secreção</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_impotencia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_impotencia">Impotência</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_alteracoesmenstruais) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_alteracoesmenstruais">Alterações menstruais</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_sangramentoabdominal) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_sangramentoabdominal">Sangramento abdominal</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->apurinario_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="apurinario_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->apurinario_outros_obs) ? $buscaAnamnese[0]->apurinario_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Sistema nervoso</strong></h4>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->sisnervoso_paresias) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="sisnervoso_paresias">Paresias</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->sisnervoso_parentesias) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="sisnervoso_parentesias">Parentesias</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->sisnervoso_tremores) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="sisnervoso_tremores">Tremores</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->sisnervoso_ausencias) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="sisnervoso_ausencias">Ausências</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->sisnervoso_convulcoes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="sisnervoso_convulcoes">Convulções</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->sisnervoso_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="sisnervoso_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->sisnervoso_outros_obs) ? $buscaAnamnese[0]->sisnervoso_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Aparelho locomotor</strong></h4>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aplocomotor_dor) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aplocomotor_dor">Paresias</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aplocomotor_impotenciafuncional) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aplocomotor_impotenciafuncional">Parentesias</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->aplocomotor_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="aplocomotor_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->aplocomotor_outros_obs) ? $buscaAnamnese[0]->aplocomotor_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Psiquismo</strong></h4>
                            
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->psiquismo_ansiedade) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="psiquismo_ansiedade">Ansiedade</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->psiquismo_depressao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="psiquismo_depressao">Depressão</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->psiquismo_alucinacoes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="psiquismo_alucinacoes">Alucinações</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->psiquismo_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="psiquismo_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->psiquismo_outros_obs) ? $buscaAnamnese[0]->psiquismo_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <a ></a>
                            <h3 class="espacotitle"><strong>História Patológica Pregressa</strong></strong></h3>
                            
                            <h4 class="espacosubtitle"><strong>Doenças Infecciosas e Parasitárias</strong></h4>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_doencasinfancia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_doencasinfancia">Doenças comuns durante a infância</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_doencasvenereas) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_doencasvenereas">Doenças sexuamente trasmitiveis</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_febreindeterminada) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_febreindeterminada">Febre de causa indeterminada</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_amigdalite) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_amigdalite">Amigdalite</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_febrereumatica) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_febrereumatica">Febre reumática</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_hepatite) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_hepatite">Hepatite</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_infeccaourinaria) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_infeccaourinaria">Infecção urinária</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_tuberculose) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_tuberculose">Tuberculose</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_peneumonia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_peneumonia">Peneumonia</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->doencasinfecciosas_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="doencasinfecciosas_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="custom-control">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->doencasinfecciosas_outros_obs) ? $buscaAnamnese[0]->doencasinfecciosas_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <h4 class="espacosubtitle"><strong>Manifestações alérgicas</strong></h4>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->manifalergicas_asma) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="manifalergicas_asma">Asma</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->manifalergicas_rinite) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="manifalergicas_rinite">Rinite</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->manifalergicas_urticaria) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="manifalergicas_urticaria">Urticaria</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->manifalergicas_eczema) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="manifalergicas_eczema">Eczema</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->manifalergicas_alergiamedicamentosaealimentar) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="manifalergicas_alergiamedicamentosaealimentar">Alergia a medicamentos</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->manifalergicas_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="manifalergicas_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="form-group">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->manifalergicas_outros_obs) ? $buscaAnamnese[0]->manifalergicas_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><strong> Cirurgias, traumas ou transfusões</strong></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->obs_cirurgiastraumasconvulcoes) ? $buscaAnamnese[0]->obs_cirurgiastraumasconvulcoes : ''}} </textarea>
                            </div>
                            
                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><strong> Uso prolongado de medicamentos</strong></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->obs_usodemedicamentos) ? $buscaAnamnese[0]->obs_usodemedicamentos : ''}} </textarea>
                            </div>
                            
                            <a ></a>
                            <h3 class="espacotitle"><strong>Outras Doenças</strong></strong></h3>
                            
                            <h4 class="espacosubtitle"><strong></strong></h4>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_diabetes) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_diabetes">Diabetes</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_hipertencao) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_hipertencao">Hipertenção</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_cardiopatias) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_cardiopatias">Cardiopatias</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_altlipidios) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_altlipidios">Alt. lipidios</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_nefropatias) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_nefropatias">Nefropatias</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_ulcerapeptica) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_ulcerapeptica">Úlcera peptica</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_neoplastia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_neoplastia">Neoplastia</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_epilepsia) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_epilepsia">Epilepsia</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_neurose) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_neurose">Neurose</label>
                                    </div>
                                </div> 
                            </div>
                            <div class="row espacorow2">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_glaucoma) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_glaucoma">Glaucoma</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"  class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_gota) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_gota">Gota</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row espacocheck">
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox"   class="custom-control-input"  value="1" {{ isset($buscaAnamnese[0]->outrasdoencas_outros) ? 'checked' : ''}}>
                                        <label class="custom-control-label tamanhocheck" for="outrasdoencas_outros">Outros</label>
                                    </div>
                                </div> 
                                <div class="col-12 col-sm-8 col-md-7 col-xl-3 col-lg-3 espacamento-direita">
                                    <div class="custom-control">
                                        <input type="text"  placeholder="Outros" class="input-linha"  size="32" value="{{ isset($buscaAnamnese[0]->outrasdoencas_outros_obs) ? $buscaAnamnese[0]->outrasdoencas_outros_obs : ''}}">
                                    </div>
                                </div>  
                            </div>
                            
                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><strong>Obs</strong></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->outrasdoencas_obs) ? $buscaAnamnese[0]->outrasdoencas_obs : ''}} </textarea>
                            </div>

                            <a ></a>
                            <h3 class="espacotitle"><strong>História Fisiológica</strong></strong></h3>
                            
                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Nascimento - Crescimento - Puberdade - Menarca - Catamênios - Gestação - Climatério</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->obs_historiafisiologica) ? $buscaAnamnese[0]->obs_historiafisiologica : ''}} </textarea>
                            </div>
                            
                            <a ></a>
                            <h3 class="espacotitle"><strong>História da Pessoa</strong></strong></h3>
                            
                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Alimentação - Moradia - Hábitos (fumo - álcool - anticoncepcionais - tóxicos - exercícios)</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->obs_historiapessoa) ? $buscaAnamnese[0]->obs_historiapessoa : ''}} </textarea>
                            </div>
                            
                            <a ></a>
                            <h3 class="espacotitle"><strong>Exame Físico</strong></strong></h3>
                            
                            <h4 class="espacosubtitle"><strong></h4>
                            <div class="row">
                                <div class="col-10 col-sm-3 col-md-3 col-xl-3 col-lg-3">
                                    <label for="examefisico_peso" class="form-controlz" ><strong class="size-font-examefisico">Peso</strong></label>
                                    <div class="input-group input-group-sm mb-12">
                                        <input onkeyup="calculaImc(this)" type=" width="10"text"   value="{{ isset($buscaAnamnese[0]->examefisico_peso) ? $buscaAnamnese[0]->examefisico_peso : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                        
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_altura" class="form-controlz" ><strong class="size-font-examefisico">Altura</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input onkeyup="calculaImc(this)" type=" width="10"text"   value="{{ isset($buscaAnamnese[0]->examefisico_altura) ? $buscaAnamnese[0]->examefisico_altura : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                        
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_imc" class="form-controlz" ><strong class="size-font-examefisico">IMC</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                        
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_tax" class="form-controlz" ><strong class="size-font-examefisico">Tax</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_tax) ? $buscaAnamnese[0]->examefisico_tax : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_respiracao" class="form-controlz" ><strong class="size-font-examefisico">Respiração</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_respiracao) ? $buscaAnamnese[0]->examefisico_respiracao : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_pulso" class="form-controlz" ><strong class="size-font-examefisico">Pulso</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_pulso) ? $buscaAnamnese[0]->examefisico_pulso : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_pabd_deitado" class="form-controlz" ><strong class="size-font-examefisico">PA - BD - Deitado</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_pabd_deitado) ? $buscaAnamnese[0]->examefisico_pabd_deitado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_pabd_sentado" class="form-controlz" ><strong class="size-font-examefisico">PA - BD - Sentado</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_pabd_sentado) ? $buscaAnamnese[0]->examefisico_pabd_sentado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_pabd_empe" class="form-controlz" ><strong class="size-font-examefisico">PA - BD - Em Pé</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_pabd_empe) ? $buscaAnamnese[0]->examefisico_pabd_empe : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_pabe_deitado" class="form-controlz" ><strong class="size-font-examefisico">PA - BE - Deitado</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_pabe_deitado) ? $buscaAnamnese[0]->examefisico_pabe_deitado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_pabe_sentado" class="form-controlz" ><strong class="size-font-examefisico">PA - BE - Sentado</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_pabe_sentado) ? $buscaAnamnese[0]->examefisico_pabe_sentado : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                                <div class="col-10 col-sm- col-md-3 col-xl- col-lg-">
                                    <label for="examefisico_pabe_empe" class="form-controlz" ><strong class="size-font-examefisico">PA - BE - Em Pé</strong></label>
                                    <div class="input-group input-group-sm mb-12 rounded">
                                        <input type="text"  width="10"  value="{{ isset($buscaAnamnese[0]->examefisico_pabe_empe) ? $buscaAnamnese[0]->examefisico_pabe_empe : ''}}" class="form-control" aria-describedby="inputGroup-sizing-sm">
                                    </div>
                                </div> 
                            </div>


                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Exame Geral: Inspeção geral: Pele - Nutrição - Fâneros - Estado mental</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->pele_observacoes) ? $buscaAnamnese[0]->pele_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Cabeça: Tireóide - Linfonodos - Vasos</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->nariz_observacoes) ? $buscaAnamnese[0]->nariz_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Tórax: Inspeção geral - Fossa supraclavicular linfonodos</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->dentes_observacoes) ? $buscaAnamnese[0]->dentes_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Ap. Respiratório: Dispnéia - Frêmitos - Percussão - Auscuta</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->pescoco_observacoes) ? $buscaAnamnese[0]->pescoco_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Ap. Cardíaco: Abaulamento e retrações - Ritmo Ictus - Ritmo e frequência cardíaca </h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->mamas_observacoes) ? $buscaAnamnese[0]->mamas_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Abdômen e Região Lombar: Inspeção geral - Sopros - Volume e forma - Macicês - Percussão</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->apcardiaco_observacoes) ? $buscaAnamnese[0]->apcardiaco_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Fígado: Hepatimetria - Consistência - Borda - Sensibilidade - Superfície - Ponto cístico</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->aprespiratorio_observacoes) ? $buscaAnamnese[0]->aprespiratorio_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Baço: Espaço de Traube - Consistência </h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->apdigestivo_observacoes) ? $buscaAnamnese[0]->apdigestivo_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Gênito Urinária: Punho percussão lombar - Globo Vesical - Pontos Renoureterais</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->apurinario_observacoes) ? $buscaAnamnese[0]->apurinario_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Membros: Inspeção geral - Varizes - Edema - Enchimento Capilar - P.A. no membro inferior</h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->sisnervoso_observacoes) ? $buscaAnamnese[0]->sisnervoso_observacoes : ''}} </textarea>
                            </div>

                            <div class="row espacocheck">
                                <label for="" class="form-controlz" ><h6>Sistema Nervoso e Ósteo Articular: Estática - Reflexos - Marcha - Sensibilidade </h6></label>
                                <textarea class="col-8 col-md-7 col-xl-10 col-sm-6 col-lg-9 form-control" rows="2"  type="textarea" > {{ isset($buscaAnamnese[0]->aplocomotor_observacoes) ? $buscaAnamnese[0]->aplocomotor_observacoes : ''}} </textarea>
                            </div>

                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    {{-- fim Anamnese --}}
   
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
 
