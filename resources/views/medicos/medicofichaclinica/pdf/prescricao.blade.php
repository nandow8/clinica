<style>
    .prescricaocontainer{
      display: none;
    }
    
    @media print {
        
        #printable-historico-prescricao, #printable-historico-prescricao * {
            visibility: visible;
        }
        #printable-historico-prescricao {
            width: 100%;
            position: fixed;
            left: 0;
            top: 0;
        }
    
     
        .prescricaocontainer{
          display: block;
        }

        footer {
            position: absolute;
            top: 1400px;
            width: 100%;
            bottom: 0;
        }

        #assinatura-prescricao{
            margin-left: 77%;
            font-size: 25px;
        }


        #linha-prescricao{
            margin-left: 70%; 
        }

        #data-prescricao{
            margin-top: 34px;
            margin-left: 20px;
            font-size: 25px;
        }
    }
    

    .tamanho-medicamento{ font-size: 30px;  }
    .tamanho{ font-size: 20px;  }
</style>

<img src="/img/medicofichaclinica/<?php echo 'saegs-receituario.png' ; ?>" style="width:100%" id="img-header">

<div class="container">
    <?php 
        $prescricoes_tipos = explode("---", $prescricoes[0]->prescricoes_tipo );
        $tipomedicamento = explode("---", $prescricoes[0]->tipomedicamento );
        $medicamentos = explode("---", $prescricoes[0]->medicamento );
        $descricoes = explode("---", $prescricoes[0]->descricao );
        $posologias = explode("---", $prescricoes[0]->posologia );
    ?>

    <br>
    <p class="mb-4 h3"><strong>Paciente: </strong>  {{ \App\Models\Paciente::find(\App\Models\Medicofichaclinica::find($prescricoes[0]->medicofichaclinica_id)['paciente_id'])['nome'] }} </p>
    <?php for ($i=0; $i < count($prescricoes_tipos); $i++) : ?>
        <p class="tamanho"> {{ $tipomedicamento[$i]}}</p>
        <p class="tamanho-medicamento"><b> {{ $medicamentos[$i]  }}</b></p>  <!-- . ' (' . $prescricoes_tipos[$i] . ')' -->
        <p class="tamanho"> {{ $descricoes[$i]}}</p>
        <p class="tamanho"> {{ $posologias[$i]}}</p>
        <hr>
    <?php endfor; ?>
</div>

<footer>
    <p id="data-prescricao">{{ date('d/m/Y') }}</p>
    <!-- <p id="linha-prescricao">__________________________________</p>
    <p id="assinatura-prescricao" class="mr-10">Assinatura</p> -->
    <img  src="/img/medicofichaclinica/<?php echo 'saegs-footer.jpeg' ; ?>" style="width:100%;" id="footer-historico-prescricao">
</footer>
    
