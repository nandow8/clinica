@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Medicofichaclinica</div>
                <div class="card-body">
                    
                    <br/>
                    <br/>

                    @include ('medicos.medicofichaclinica.form', ['formMode' => 'edit'])

                </div>
            </div>
        </div>
    </div>
@endsection
