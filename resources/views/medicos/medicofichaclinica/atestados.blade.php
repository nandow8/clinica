<!-- atestados -->
<style>
    #img-header-atestado{ display:none;}
    #img-footer-atestado{ display:none;}
    #footer-historico-atestado-data{ display: none    }
    #footer-historico-atestado-linha{ display: none    }
    #footer-historico-atestado-assinatura{ display: none    }

    @media print {
        @page { size: auto; margin: 0mm; }
        body 
        {
            background-color:#FFFFFF;  
            margin: 0px;  /* the margin on the content before printing */
       }
        .botoes, #label-titulo, #label-atestado{
            display: none;
        }

        #titulo{
            font-size: 2.5em;
            margin: 150px 0px 0px 435px;
        }

        #atestado{
            font-size: 1.7em;
            resize: none;
            width: 100%;
            margin-left: 180px;
            min-height: 1500px;
        }

        #printable_atestado, #printable_atestado * {
            visibility: visible;
        }

        #printable_atestado {
            width: 100%;
            position: fixed;
            left: 0;
            top: 0;
        }

        .input-linha-atestado{
            background: transparent;
            border: none;
            -webkit-box-shadow: none;
            box-shadow: none;
            border-radius: 0;
        }

        .input-linha-atestado:focus {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        #img-header-atestado{
            display:block;
            width:100%;
            margin-left: 100px;
        }

        #img-footer-atestado{
            display: block;
            position: absolute;
            bottom: 440px;
            margin-left: 100px;
        }

        #footer-historico-atestado-data{
                display: block;
                position: fixed;
                top: 1710px;
                font-size: 25px;
                margin-left: 220px
        }
        #footer-historico-atestado-linha{
                display: block;
                position: fixed;
                top: 1680px;
                left: 450px;
                font-size: 25px;
        }
        #footer-historico-atestado-assinatura{
                display: block;
                position: fixed;
                top: 1710px;
                left: 620px;
                font-size: 25px;
        }
    }
</style>
<div class="container-fluid">
    <h2 style="text-align: center; margin-top:3%;">Atestados / Laudo</h2>
    <hr>

    <div class="row" style="margin-top: 1%; margin-bottom: 4%;">
        <div class="col-2 col-xl-2 col-lg-2">
            @foreach($medico_modeloatestado as $modeloatestado)
                <button type="button" class="btn rounded btn-success btn-sm btn-block mb-1 medicomodelo" id="{{ $modeloatestado->id }}">{{ $modeloatestado->title }}</button>
                <input type="hidden" value="{{ $modeloatestado->title }}" id="title-atestado-{{ $modeloatestado->id }}">
                <input type="hidden" value="{{ $modeloatestado->texto }}" id="texto-atestado-{{ $modeloatestado->id }}">
            @endforeach
        </div>

        <div id="printable_atestado" class="col-10 col-xl-10 col-lg-10">
            <img src="/img/medicofichaclinica/<?php echo 'atestadolaudo.png' ; ?>" id="img-header-atestado">
            <p id="footer-historico-atestado-data">{{ date('d/m/Y') }}</p>
            <!-- <p id="footer-historico-atestado-linha">__________________________________</p>
            <p id="footer-historico-atestado-assinatura" >Assinatura</p> -->
            <img  src="/img/medicofichaclinica/<?php echo 'saegs-footer.jpeg' ; ?>" id="img-footer-atestado">
            <div class="row espacocheck">
                <label id="label-titulo" for="titulo" class="form-controlz" ><strong>Título</strong></label>
                <input type="text" name="titulo" id="titulo" class="input-linha-atestado form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
            </div>
            <div class="row espacocheck">
                <label id="label-atestado" for="atestado" class="form-controlz" ><strong>Atestado</strong></label>
                <textarea class="form-control input-linha-atestado summernote" rows="25" name="atestado" type="textarea" id="atestado"></textarea>
            </div>
            <button onclick="print_atestado()" type="button" style="margin-top: 1%; margin-bottom: 4%;" class="rounded botoes btn btn-info">Imprimir</button>

        </div>
    </div>

</div>

<script src="{{ asset('js/jquery.min.js') }}"></script>
<script>
    function print_atestado() {
        window.print();
    }

    $(".medicomodelo").click(function(event){
        now = new Date

        var nome_paciente = $("#nome_paciente").val()
        var title = $('#title-atestado-' + this.id).val()
        var texto = $('#texto-atestado-' + this.id).val()

        texto_paciente = texto.replace("&&PACIENTE&&", nome_paciente);

        var dataatual =  now.getDate() + "/" +  now.getDay() + "/" + now.getFullYear()
        texto_paciente_data = texto_paciente.replace("&&DATA&&", dataatual);

        $('#titulo').val(title)
        $('#atestado').val(texto_paciente_data)
    })
</script>
<!-- END atestados -->
