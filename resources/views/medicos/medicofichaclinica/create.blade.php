@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Adicionar Ficha clinica</div>
                <div class="card-body">
                        
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ url('/medicos/medicofichaclinica') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group row {{ $errors->has('paciente_id') ? 'has-error' : ''}}">
                                <label for="paciente_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Paciente ' }}</label>
                                <div class="col-lg-5 col-md-6 col-sm-12">
                                <select name="paciente_id" class="form-control" id="paciente_id" >
                                    <option value="0">Selecione...</option>
                                    @foreach ($listadepacientes as $optionKey => $optionValue)
                                        <option value="{{ $optionValue->id }}" {{ (isset($medicofichaclinica->paciente_id) && $medicofichaclinica->paciente_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nome }}</option>
                                    @endforeach
                                </select>
                            </div>
                                {!! $errors->first('paciente_id', '<p class="help-block">:message</p>') !!}
                            </div>
                            
                            <br/>
                                
                            <div class="form-group offset-md-5">
                                <a class="btn btn-warning rounded" href="{{ url('/medicos/medicofichaclinica') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
                                @if(request()->route()->getActionMethod() !== 'show') 
                                    <button class="btn btn-primary rounded" type="submit">
                                        <i class="fa fa-check fa-1" aria-hidden="true"></i>Salvar</button>
                                @else
                                    <br>
                                @endif
                            </div>
                            
                            
                           

                    </form>                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')

@endsection