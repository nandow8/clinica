<!-- prescricoes -->
<style>
    .margem-sufite{ margin-left: 4%;  }
    #medicamento-sufite{ font-size: 1.4em; color:gray; }
    .descricao-sufite{ font-size: 1.0em; color:gray; }
    #footer-prescricao {
        position: absolute;
        bottom: 53px;
    }


    #footer-historico-pre-data{
        display: none
    }
    #footer-historico-pre-linha{
        display: none
    }
    #footer-historico-pre-assinatura{
        display: none
    }

    @media print {
        @page { margin: 0; }
        body { margin: 1.3cm; }
        body * {
            visibility: hidden;
        }
        #footer-historico-pre-data{
                display: block;
                position: fixed;
                top: 1630px;
                font-size: 25px;
        }
        #footer-historico-pre-linha{
                display: block;
                position: fixed;
                top: 1600px;
                left: 450px;
                font-size: 25px;
        }
        #footer-historico-pre-assinatura{
                display: block;
                position: fixed;
                top: 1630px;
                left: 620px;
                font-size: 25px;
        }

        #icone-apagar{
            display: none;
        }
        #footer-prescricao {
            position: fixed;
             top: 1680px;
             bottom: 0;
        }
        #footer-exame {
            position: fixed;
             top: 1680px;
             bottom: 0;
        }

        .margem-sufite{ margin-left: 4%;  }
        #medicamento-sufite{ font-size: 2em; color:gray; }
        .descricao-sufite{ font-size: 1.4em; color:gray; }
        #printable, #printable * {
            visibility: visible;
        }
        #printable {
            width: 100%;
            position: fixed;
            left: 0;
            top: 0;
        }
    }


</style>
<div class="container">
    <h2 style="text-align: center; margin-top:3%;">Prescrições</h2>
    <hr>

    <div class="container">
        <h3 class="espacosubtitle offset-md-2">Tipo prescrição <span class="offset-md-3">Tipo receita</span></h3>
        <div class="row offset-md-2">

            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="industrializados" name="prescricoes_tipo" class="custom-control-input" value="industrializados" checked>
                <label class="custom-control-label tamanhocheck" for="industrializados">Industrializados</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="manipulados" name="prescricoes_tipo" class="custom-control-input" value="manipulados" >
                <label class="custom-control-label tamanhocheck" for="manipulados">Manipulados</label>
            </div>

            <div class="custom-control custom-radio custom-control-inline ml-5">
                <input type="radio" id="simples" name="tipo_receita" class="custom-control-input" value="simples" checked>
                <label class="custom-control-label tamanhocheck" for="simples">Simples</label>
            </div>
            <div class="custom-control custom-radio custom-control-inline">
                <input type="radio" id="controlado" name="tipo_receita" class="custom-control-input" value="controlado" >
                <label class="custom-control-label tamanhocheck" for="controlado">Controlado</label>
            </div>

        </div>
        <input type="hidden" name="id_medicamento" id="id_medicamento">
        <div class="row espacocheck offset-md-2">
            <label for="tipomedicamento" class="form-controlz" ><h3>Tipo</h3></label>
            <input type="text" name="tipomedicamento" id="tipomedicamento" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <div class="row espacocheck offset-md-2">
            <label for="medicamento" class="form-controlz" ><h3>Medicamento <span class="h6 ml-5">preço aproximado: R$ </span><input type="text" class="ml-6 rounded h5" size="4" id="preco_aproximado"></h3> </label>
            <input type="text" onkeyup="autocompletamedicamento(this)" onblur="atribuircampos(this)" name="medicamento" id="medicamento" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <div class="row espacocheck offset-md-2">
            <label for="descricao" class="form-controlz" ><h3>Descrição:</h3></label>
            <textarea class="form-control summernote" rows="2" name="descricao" type="textarea" id="descricao"> </textarea>
        </div>
        <div class="row espacocheck offset-md-2">
            <label for="posologia" class="form-controlz" ><h3>Posologia</h3></label>
            <input type="text" name="posologia" id="posologia" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>

        <div class="row md-3 offset-2" style="margin-top: 1%; margin-bottom: 4%;">
            <div class="col-3">
                <button type="button" class="btn rounded btn-success" id="add-medicamento-sufite"> Adicionar</button>
            </div>
            <div class="col-4">
                <button type="button" id="repetirultimareceita" class="btn rounded btn-primary">Repetir ultima receita</button>
            </div>
            <div class="col-3">
                <button type="button" onclick="printme()" class="rounded btn btn-info">Imprimir</button>
            </div>
        </div>

    </div>

        <div id="printable" class="row"  style="background-color: #f1f1f1; height: 1000px;">
            <div class="col-12 mt-5">
                <div class="container" style="background-color: white; height: 900px; width: 80%;">
                    <img src="/img/medicofichaclinica/<?php echo 'saegs-receituario.png' ; ?>" style="width:100%" id="img-header">
                    <div class="row">
                        <div class="col-12 mt-3" id="sufite-add">
                            <div class="margem-sufite descricao-sufite"><strong>Paciente: </strong> {{ $dados_paciente['nome'] }}</div>
                        </div>
                    </div>
                    <p id="footer-historico-pre-data">{{ date('d/m/Y') }}</p>
                    <!-- <p id="footer-historico-pre-linha">__________________________________</p>
                    <p id="footer-historico-pre-assinatura" >Assinatura</p> -->
                    <img  src="/img/medicofichaclinica/<?php echo 'saegs-footer.jpeg' ; ?>" style="width:73%;" id="footer-prescricao">
                </div>
            <div>
        </div>
    </div>

</div>
</div>

<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
<input type="hidden" name="medicofichaclinica_id" value="{{ $medicofichaclinica->id }}">

<!-- prescricoes -->
@section('jqueryscript')
<script>

    function printme() {
        window.print();
    }

    jQuery("#repetirultimareceita").click(function () {
        var user_id = jQuery("input[name='user_id']").val()
        var medicofichaclinica_id = jQuery("input[name='medicofichaclinica_id']").val()

        jQuery.getJSON('/medicos/ajaxultimareceita/' + medicofichaclinica_id + '/' + user_id, function (data) {
            if(data){
                var id_med = data.id_medicamento.split("---")
                var tipmed = data.tipomedicamento.split("---")
                var med = data.medicamento.split("---")
                var desc = data.descricao.split("---")
                var pres_tipo = data.prescricoes_tipo.split("---")
                var tipo_rece = data.tipo_receita.split("---")
                var poso = data.posologia.split("---")

                for (let index = 0; index < pres_tipo.length; index++) {
                    var addultima = '<div class="col-12 mt-3 container" id="add-medicamento-sufite' + index +'">'
                                +'<div class="margem-sufite descricao-sufite">' + tipmed[index] + '</div>'
                                +'<div class="margem-sufite"> <strong id="medicamento-sufite">' + med[index] + ' </strong><button type="button"  id="' + index + '" class="btn btn-light rounded pull-right btn-apagar"> <i class="fa fa-times fa-2" id="icone-apagar"></i> </button></div>'
                                +'<div class="margem-sufite descricao-sufite"> '+ desc[index]  +' </div>'
                                +'<div class="margem-sufite descricao-sufite">'+ poso[index] +'</div>'

                                +'<input type="hidden" name="id_medicamento[]" value="'+ id_med[index] +'" />'
                                +'<input type="hidden" name="prescricoes_tipo[]" value="'+ pres_tipo[index] +'" />'
                                +'<input type="hidden" name="tipo_receita[]" value="'+ pres_tipo[index] +'" />'
                                +'<input type="hidden" name="tipomedicamento[]" value="'+ tipmed[index] +'" />'
                                +'<input type="hidden" name="medicamento[]" value="'+ med[index] +'" />'
                                +'<input type="hidden" name="descricao[]" value="'+ desc[index] +'" />'
                                +'<input type="hidden" name="posologia[]" value="'+ poso[index] +'" />'
                            +'<hr></div>';
                    jQuery("#sufite-add").append(addultima);

                }
            }else{
                Swal.fire({
                    position: 'center',
                    type: 'error',
                    title: 'Nenhuma prescrição anterior foi encontrada para este paciente.',
                    animation: false,
                    customClass: 'animated tada',
                    showConfirmButton: false,
                    timer: 2500
                })
            }

        });

    });

    var cont = 1;
    jQuery("#add-medicamento-sufite").click(function () {
        var prescricoes_tipo = jQuery('#industrializados').prop('checked') ? 'Industrializado' : 'Manipulado'
        var tipo_receita = jQuery('#simples').prop('checked') ? 'Simples' : 'Controlado'
        var id_medicamento = jQuery('#id_medicamento').val()
        var medicamento = jQuery('#medicamento').val()
        var tipomedicamento = jQuery('#tipomedicamento').val()
        var descricao = jQuery('#descricao').val()
        var posologia = jQuery('#posologia').val()

        if(medicamento){
            cont++;

            var addcampo = '<div class="col-12 mt-3 container" id="add-medicamento-sufite' + cont +'">'
                            +'<div class="margem-sufite descricao-sufite"> '+ tipomedicamento  +' </div>'
                            +'<div class="margem-sufite"> <strong id="medicamento-sufite">' + medicamento + ' </strong><button type="button"  id="' + cont + '" class="btn btn-light rounded pull-right btn-apagar"> <i class="fa fa-times fa-2" id="icone-apagar"></i> </button></div>'
                            +'<div class="margem-sufite descricao-sufite"> '+ descricao  +' </div>'
                            +'<div class="margem-sufite descricao-sufite">'+ posologia +'</div>'

                            +'<input type="hidden" name="id_medicamento[]" value="'+ id_medicamento +'" />'
                            +'<input type="hidden" name="prescricoes_tipo[]" value="'+ prescricoes_tipo +'" />'
                            +'<input type="hidden" name="tipo_receita[]" value="'+ tipo_receita +'" />'
                            +'<input type="hidden" name="medicamento[]" value="'+ medicamento +'" />'
                            +'<input type="hidden" name="tipomedicamento[]" value="'+ tipomedicamento +'" />'
                            +'<input type="hidden" name="descricao[]" value="'+ descricao +'" />'
                            +'<input type="hidden" name="posologia[]" value="'+ posologia +'" />'
                            +'<hr></div>';
            jQuery("#sufite-add").append(addcampo);

            jQuery('#preco_aproximado').val("");
            jQuery('#id_medicamento').val("");
            jQuery('#medicamento').val("");
            jQuery('#tipomedicamento').val("");
            jQuery('#descricao').val("");
            jQuery('#posologia').val("");
        }
    });

    jQuery('form').on('click', '.btn-apagar', function () {
        var button_id = jQuery(this).attr("id");
        jQuery('#add-medicamento-sufite' + button_id + '').remove();
    });

    function autocompletamedicamento(obj){
        var val = jQuery(obj).val()
        var prescricoes_tipo = jQuery('#industrializados').prop('checked') ? 'Industrializado' : 'Manipulado'

        jQuery.getJSON('/medicos/ajaxexibirmedicamento/' + val + '/' + prescricoes_tipo, function (data) {
            jQuery("#medicamento").autocomplete({
                source: function(request, response){
                    response(data)
                },
                select: function (e, ui) {
                    descricao = ui.item.label.split("---")[2]
                    jQuery('#descricao').val(descricao)

                    preco_aproximado = ui.item.label.split("---")[3]
                    jQuery('#preco_aproximado').val(preco_aproximado)
                },
                change: function (e, ui) {
                    id_medicamento = ui.item.label.split("---")[0]
                    jQuery('#id_medicamento').val(id_medicamento)
                    medicamento = ui.item.label.split("---")[1]
                    jQuery('#medicamento').val(medicamento)
                }
            });
        });
    }

</script>
@endsection
<!-- end prescricoes -->
