 <!-- isset($pacientediagnostico[0]->cid) ? $pacientediagnostico[0]->cid : ''  -->
<div class="container">
    <h2 style="text-align: center; margin-top:3%;">Diagnóstico</h2>
    <hr>
    
    <div id="adiciona-campo" class="row espacocheck offset-md-2">
        <label for="" class="form-controlz" ><strong>CID</strong></label>
        
            <input type="text" onkeyup="autocompleta(this)" name="cid[]" id="cid" class="cid form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        
        <button type="button" id="add-campo-cid" class="btn btn-primary rounded"><i class="fa fa-plus-square fa-2"></i></button>
    </div>
    <div class="row espacocheck offset-md-2">
        <label for="" class="form-controlz" ><strong> Informações adicionais</strong></label>
        <textarea class="form-control summernote" rows="2" name="informacoesadicionais" type="textarea" id="informacoesadicionais"></textarea>
    </div>
</div>


<script src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        var cont = 1;
        jQuery("#add-campo-cid").click(function () {
            cont++;
            var addcampo = '<div id="add-campo-cid' + cont +'" class="row espacocheck col-12"><input type="text" onkeyup="autocompleta(this)" name="cid[]" id="cid" class="cid form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default"><button style="margin-left: 2%;" type="button" id="' + cont + '" class="rounded btn btn-danger btn-apagar"><i class="fa fa-trash-o"></i></button></div>';
            jQuery("#adiciona-campo").append(addcampo);
        });

        jQuery('form').on('click', '.btn-apagar', function () {
            var button_id = jQuery(this).attr("id");
            jQuery('#add-campo-cid' + button_id + '').remove();
        });

        // function excluicid(cid_id){
        //     console.log(cid_id)

        //     jQuery.get('/medicos/ajaxexcluircid/' + cid_id, function (data) {
        //         jQuery('#excluir_' + cid_id).remove();
        //         jQuery('#' + cid_id).remove();
        //     });
            
        // }
        
        function autocompleta(obj){
            var val = jQuery(obj).val()
            jQuery.getJSON('/medicos/ajaxexibirdescricaocid/' + val, function (data) {    
                jQuery(".cid").autocomplete({
                    source: data
                });
            });
        }
    </script> 