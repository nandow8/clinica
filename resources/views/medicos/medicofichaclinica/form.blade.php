<style>
    #fotopaciente{ width: 85px; height: 80px;}
</style>

<div class="mt-n4 mb-4">
    <div class="container">
        <div class="row">
            <div class="col">
                <img class="ml-4 img-thumbnail" src="/uploads/fotospacientes/<?php echo ($dados_paciente['image']) ? $dados_paciente['image'] : 'defaultman.jpg' ; ?>" alt="" id="fotopaciente">
                <a> {{ $dados_paciente['nome'] }}</a>
                <div class="float-right">
                    @if(request()->route()->getActionMethod() !== 'show') 
                        <span> Data: {{ date('d/m/Y') }} <br> Hora: {{ date('H:i:s', time()) }}</span> <br>
                    @endif
                        <span>
                            <button class="btn btn-outline-info btn-sm rounded" type="button"  data-toggle="modal" data-target="#modalHoraAgendada">
                                Horarios agendados
                            </button>
                        </span>
                </div>
                <input type="hidden" id="nome_paciente" value="{{ $dados_paciente['nome'] }}">
            </div> 
            <div class="col">
                @if(request()->route()->getActionMethod() !== 'show') 
                    <button class="btn btn-primary rounded pull-right" type="submit" id="finalizar_atendimento"><i class="fa fa-check fa-1" aria-hidden="true"></i> Finalizar Atendimento </button>
                @endif                    
                <a class="btn btn-light rounded mr-3 pull-right" href="{{ url('/eventos') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Cancelar</a>
            </div>
        </div>
    </div>
</div>


<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link" id="anamnase-tab" data-toggle="tab" href="#anamnase" role="tab" aria-controls="anamnase" aria-selected="false">Anamnase</a>
    </li>
    @if(request()->route()->getActionMethod() !== 'show') 
        <li class="nav-item">
            <a class="nav-link" id="evolucao-tab" data-toggle="tab" href="#evolucao" role="tab" aria-controls="evolucao" aria-selected="false">Evolução</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="diagnostico-tab" data-toggle="tab" href="#diagnostico" role="tab" aria-controls="diagnostico" aria-selected="false">Diagnóstico</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="prescricoes-tab" data-toggle="tab" href="#prescricoes" role="tab" aria-controls="prescricoes" aria-selected="false">Prescrições</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="exames-tab" data-toggle="tab" href="#exames" role="tab" aria-controls="exames" aria-selected="false">Exames</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="resultados-tab" data-toggle="tab" href="#resultados" role="tab" aria-controls="resultados" aria-selected="false">Resultados</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="atestados-tab" data-toggle="tab" href="#atestados" role="tab" aria-controls="atestados" aria-selected="false">Atestados / Laudo</a>
        </li>
    @endif
    <li class="nav-item">
        <a class="nav-link" id="anexos-tab" data-toggle="tab" href="#anexos" role="tab" aria-controls="anexos" aria-selected="false">Anexos</a>
    </li>
    <li class="nav-item">
        <a class="nav-link active" id="historico-tab" data-toggle="tab" href="#historico" role="tab" aria-controls="historico" aria-selected="false">Histórico</a>
    </li>
</ul>

<div class="tab-content">
    <div class="tab-pane active" id="historico" role="tabpanel" aria-labelledby="historico-tab">
        @include('medicos.medicofichaclinica.historico')
    </div>
    
    <div class="tab-pane" id="evolucao" role="tabpanel" aria-labelledby="evolucao-tab">
            @include('medicos.medicofichaclinica.evolucao')
    </div>
    <div class="tab-pane" id="anamnase" role="tabpanel" aria-labelledby="anamnase-tab">
        @include('medicos.medicofichaclinica.anamnase')
    </div>
    <div class="tab-pane" id="diagnostico" role="tabpanel" aria-labelledby="diagnostico-tab">
            @include('medicos.medicofichaclinica.diagnostico')
    </div>
    <div class="tab-pane" id="prescricoes" role="tabpanel" aria-labelledby="prescricoes-tab">
        @include('medicos.medicofichaclinica.prescricoes')
    </div>
    <div class="tab-pane" id="exames" role="tabpanel" aria-labelledby="exames-tab">
        @include('medicos.medicofichaclinica.exames')
    </div>
    <div class="tab-pane" id="resultados" role="tabpanel" aria-labelledby="resultados-tab">
        @include('medicos.medicofichaclinica.resultados')
    </div>
    <div class="tab-pane" id="atestados" role="tabpanel" aria-labelledby="atestados-tab">
        @include('medicos.medicofichaclinica.atestados')
    </div>
    <div class="tab-pane" id="anexos" role="tabpanel" aria-labelledby="anexos-tab">
        @include('medicos.medicofichaclinica.anexos')
    </div>
</div>

<br/>

<!-- hidden -->
    <input type="hidden" value="{{ (isset($agendamento->id)) ? $agendamento->id : ''}}" name="idagendamento">
    <input type="hidden" value="" name="tempo_atendimento">
<!-- end hidden -->

<div class="modal fade" id="modalHoraAgendada" tabindex="-1" role="dialog" aria-labelledby="modalHoraAgendadaTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalHoraAgendadaTitle">Agenda do paciente {{ $dados_paciente['nome'] }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @foreach($pacientehorariosagenda as $pacientehorarios)
            <h4 class="text-center">Data: {{ Carbon\Carbon::parse($pacientehorarios->dataconsulta)->format('d/m/Y') }} Hora: {{ $pacientehorarios->horaconsulta }}</h4>
        @endforeach
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger rounded" data-dismiss="modal">Fechar</button> 
      </div>
    </div>
  </div>
</div>


<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script>
   

    jQuery('form').on('submit', function (e) {
        e.preventDefault();
        var form = this;

        Swal.fire({
            title: 'Finalizar atendimento?',
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim!',
            cancelButtonText: 'Não',
            }).then((result) => {
            if (result.value) {
                jQuery("input[name='tempo_atendimento']").val(jQuery('#minuto').text() + jQuery('#segundo').text())
                form.submit()
            }
        })
    });
    @if(request()->route()->getActionMethod() !== 'show')
    var intervalo;

    function tempo(op) {
        if (op == 1) {
            document.getElementById('parar').style.display = "block";
            document.getElementById('comeca').style.display = "none";
        }
        var s = 1;
        var m = 0;
        var h = 0;
        intervalo = window.setInterval(function() {
            if (s == 60) { m++; s = 0; }
            if (m == 60) { h++; s = 0; m = 0; }
            // if (h < 10) document.getElementById("hora").innerHTML = "0" + h + ":"; else document.getElementById("hora").innerHTML = h + "h";
            if (m < 10) document.getElementById("minuto").innerHTML = "0" + m + ":"; else document.getElementById("minuto").innerHTML = m ;		
            if (s < 10) document.getElementById("segundo").innerHTML = "0" + s; else document.getElementById("segundo").innerHTML = s ;
            s++;
        },1000);
    }
    
    window.onload=tempo;
    @endif
</script>

    
