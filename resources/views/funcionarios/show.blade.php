<?php
    use Carbon\Carbon;
?>
@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Funcionário</div>
                <div class="card-body">

                    <a href="{{ url('/funcionarios/funcionarios') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>

                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr><th> Foto </th><td> <img src="/uploads/fotosfuncionarios/{{ $funcionario->image}}" style="width:130px; "> </td></tr>
                                <tr><th> Nome </th><td> {{ $funcionario->nome }} </td></tr>
                                <tr>
                                    <th> Especialidade </th>
                                    <td>
                                        <?php
                                            // criar array com especialidades
                                            $gx = explode(',', $funcionario->especialidade);
                                            foreach($gx as $key => $especialidade):
                                                $ramo = App\Models\Especialidade::find($especialidade);
                                        ?>
                                        <span class="especialidades">{{ isset($ramo->titulo) ? $ramo->titulo : '' }} </span>
                                        <?php
                                            endforeach;
                                        ?>
                                    </td>
                                </tr>
                                <tr><th> Área de atuação </th><td> {{ ($funcionario->areaatuacao) ? ucfirst($funcionario->areaatuacao) : '--' }} </td></tr>
                                <tr><th> Membro </th><td> {{ ($funcionario->membro) ? ucfirst($funcionario->membro) : '--' }} </td></tr>
                                <tr><th> CRM </th><td> {{ $funcionario->crm }} </td></tr>
                                <tr><th> CRM Emissão </th><td> {{ ($funcionario->crmemissao) ? Carbon::parse($funcionario->crmemissao)->format('d-m-Y') : '--'}} </td></tr>
                                <tr><th> Identidade </th><td> {{ $funcionario->identidade }} </td></tr>
                                <tr><th> CPF </th><td> {{ $funcionario->cpf }} </td></tr>
                                <tr><th> CEP </th><td> {{ $funcionario->cep }} </td></tr>
                                <tr><th> Endereço </th><td> {{ $funcionario->rua.', '. $funcionario->numero }} </td></tr>
                                <tr><th> Complemento </th><td> {{ $funcionario->complemento }} </td></tr>
                                <tr><th> Bairro </th><td> {{ $funcionario->bairro }} </td></tr>
                                <tr><th> Cidade </th><td> {{ $funcionario->cidade }} </td></tr>
                                <tr>
                                    <th>Estado</th>
                                    <td>
                                        <?php
                                            if($funcionario->estado) $estado = App\Models\Estado::find($funcionario->estado);
                                            echo ($estado) ? $estado->nome : '--';
                                        ?>
                                    </td>
                                </tr>
                                <tr><th> Telefone </th><td> {{ $funcionario->telefone }} </td></tr>
                                <tr><th> Recado </th><td> {{ $funcionario->recado }} </td></tr>
                                <tr><th> Celular </th><td> {{ $funcionario->celular }} </td></tr>
                                <tr><th> E-mail </th><td> {{ $funcionario->email }} </td></tr>
                                <tr><th> Nascimento </th><td> {{ ($funcionario->nascimento) ? Carbon::parse($funcionario->nascimento)->format('d-m-Y') : '--' }} </td></tr>
                                <tr><th> Nacionalidade </th><td> {{ $funcionario->nacionlalidade }} </td></tr>
                                <tr><th> Estado Civil </th><td> {{ ($funcionario->estadocivil) ? ucfirst($funcionario->estadocivil) : '--' }} </td></tr>
                                <tr><th> Tem filhos? </th><td> {{ ($funcionario->filhos) ? ucfirst($funcionario->filhos) : '--' }} </td></tr>
                                <tr><th> Total filhos </th><td> {{ $funcionario->totalfilhos }} </td></tr>
                                <tr><th> Observação </th><td> {{ $funcionario->obs }} </td></tr>
                                <tr><th> Status </th><td> {{ ucfirst($funcionario->status) }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
