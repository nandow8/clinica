
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />

<?php $acao = request()->route()->getActionMethod(); ?>

<div class="form-group row adicionarfoto">
        <label for="image" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Foto' }}</label>
        <div class="col-lg-5 col-md-6">
            @if (isset($funcionario))
                <img src="/uploads/fotosfuncionarios/<?php echo ($funcionario->image) ? $funcionario->image : 'defaultman.jpg' ; ?>" alt="{{ $funcionario->nome }}" style="width:120px">
            @else
                <img src="/uploads/fotosfuncionarios/defaultman.jpg" alt="" style="width:120px">
            @endif
        {{-- <input type="file" name="image"> --}}
    </div>
</div>

@if(request()->route()->getActionMethod() !== 'show')
    <div class="form-group row">
        <label for="image" class="col-lg-2 col-md-3 col-sm-12 col-form-label"></label>
        <p style="margin-left: 1%" class="btn btn-outline-success adicionarfoto">Adicionar Foto</p>
    </div>
@endif
<div class="form-group row" style="display: none;" id="tirarfoto">
    <label for="image" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Tirar Foto' }}</label>
    <div class="col-lg-5 col-md-6">
        <div class="col-md-6">
            <div id="my_camera"></div>
            <br/>
            <input type="button" class="btn btn-outline-success" value="Tirar Foto" onClick="take_snapshot()">
            <input type="hidden" name="imagecam" class="image-tag">
        </div>
        <div class="col-md-6">
            <div id="results"></div>
        </div>
    </div>
    {!! $errors->first('observacoes', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nome' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($funcionario->nome) ? $funcionario->nome : ''}}" required>
</div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('user_id') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Acessa?' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <select name="user_id" class="form-control" id="user_id" >
            <option value="0">Não Acessa</option>
            @foreach ($usuarios as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($funcionario->user_id) && $funcionario->user_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->name .' - '. $optionValue->email }}</option>
            @endforeach
        </select>
    </div>
    {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('membro') ? 'has-error' : ''}}">
    <label  for="membro" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Vínculo' }}<span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="membro" class="form-control" id="membro" required>
        <option value="">Selecione</option>
        @foreach (json_decode('{"honorario": "Honorário", "efetivo": "Efetivo", "assistente": "Assistente", "credenciado": "Credenciado", "contratado": "Contratado", "provisorio": "Provisório", "residente": "Residente", "aprimorado": "Aprimorado"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($funcionario->membro) && $funcionario->membro == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('membro', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('areaatuacao') ? 'has-error' : ''}}">
    <label for="areaatuacao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Area de Atuação' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <!-- <input class="form-control" name="areaatuacao" type="text" id="areaatuacao" value="{{ isset($funcionario->areaatuacao) ? $funcionario->areaatuacao : ''}}" > -->
    <select name="areaatuacao" class="form-control" id="areaatuacao" onchange="setProfissionalSaude(this)">
        @foreach (json_decode('{"Funcionário":"Funcionário","Médico": "Médico", "Dentista": "Dentista"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($funcionario->areaatuacao) && $funcionario->areaatuacao == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('areaatuacao', '<p class="help-block">:message</p>') !!}
</div>



<!-- <div class="form-group row {{ $errors->has('name') ? 'has-error' : ''}}">
    <label for="name" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Name' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($usuario->name) ? $usuario->name : ''}}" >
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Email' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="email" type="email" id="email" value="{{ isset($usuario->email) ? $usuario->email : ''}}" >
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group row {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Password' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <input class="form-control" name="password" type="password" id="password" >
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div> -->

<div id="ProfissionalSaude" style="display:none;">
  <div class="form-group row {{ $errors->has('especialidade') ? 'has-error' : ''}}">
      <label for="especialidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Especialidade' }} </label> <!--<span class="requerido">(*)</span></label>-->
      <div class="col-lg-5 col-md-6 col-sm-12">
      <?php
          $array = array();
          foreach((DB::table('especialidades')->select('id','titulo')->get()) as $k=>$aux):
              $array[$aux->id] = $aux->titulo;
          endforeach;
       ?>
      <?php
          if($acao !== 'create'){
              $selected = $funcionario->especialidade;
              $arrayselected = explode(',', $selected);
          }
      ?>
      <select name="especialidade[]" class="form-control" id="especialidade" multiple="multiple" >
          @foreach ($array as $optionKey => $optionValue)
              <option value="{{ $optionKey }}" {{ (isset($funcionario->especialidade)) && (in_array($optionKey, $arrayselected)) && $acao != 'create' ? 'selected' : ''}}>{{ $optionValue }}</option>
          @endforeach
      </select>
  </div>
      {!! $errors->first('especialidade', '<p class="help-block">:message</p>') !!}
  </div>

  <div class="form-group row {{ $errors->has('diastrabalho') ? 'has-error' : ''}}">
      <label  for="diastrabalho" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Dias de trabalho' }}</label>
      <div class="col-lg-5 col-md-6 col-sm-12">
          <?php
              $grupoDias = (isset($funcionario->diastrabalho)) ? explode(',', $funcionario->diastrabalho) : array();
          ?>
          <table>
              <tr>
                  <th width="2">Dom</th>
                  <th width="2">Seg</th>
                  <th width="2">Ter</th>
                  <th>Qua</th>
                  <th>Qui</th>
                  <th>Sex</th>
                  <th>Sab</th>
              </tr>
              <tr>
                  <td><input type="checkbox" name="diastrabalho[]" value="0" {{ (isset($grupoDias)) && in_array('0', $grupoDias) ? 'checked' : '' }}></td>
                  <td><input type="checkbox" name="diastrabalho[]" value="1" {{ (isset($grupoDias)) && in_array('1', $grupoDias) ? 'checked' : '' }}></td>
                  <td><input type="checkbox" name="diastrabalho[]" value="2" {{ (isset($grupoDias)) && in_array('2', $grupoDias) ? 'checked' : '' }}></td>
                  <td><input type="checkbox" name="diastrabalho[]" value="3" {{ (isset($grupoDias)) && in_array('3', $grupoDias) ? 'checked' : '' }}></td>
                  <td><input type="checkbox" name="diastrabalho[]" value="4" {{ (isset($grupoDias)) && in_array('4', $grupoDias) ? 'checked' : '' }}></td>
                  <td><input type="checkbox" name="diastrabalho[]" value="5" {{ (isset($grupoDias)) && in_array('5', $grupoDias) ? 'checked' : '' }}></td>
                  <td><input type="checkbox" name="diastrabalho[]" value="6" {{ (isset($grupoDias)) && in_array('6', $grupoDias) ? 'checked' : '' }}></td>
              </tr>
          </table>
      </div>
      {!! $errors->first('diastrabalho', '<p class="help-block">:message</p>') !!}
  </div>

  <div class="form-group row {{ $errors->has('crm') ? 'has-error' : ''}}">
      <label id='categoria' for="crm" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CRM/CRO' }} </label> <!--<span class="requerido">(*)</span></label>-->
      <div class="col-lg-5 col-md-6 col-sm-12">
      <input class="form-control" name="crm" type="text" id="crm" value="{{ isset($funcionario->crm) ? $funcionario->crm : ''}}" >
  </div>
      {!! $errors->first('crm', '<p class="help-block">:message</p>') !!}
  </div>
  <div class="form-group row {{ $errors->has('crmemissao') ? 'has-error' : ''}}">
      <label id='categoriaemissao' for="crmemissao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CRM/CRO Emissão' }} </label> <!--<span class="requerido">(*)</span></label>-->
      <div class="col-lg-5 col-md-6 col-sm-12">
      <input class="form-control" name="crmemissao" type="date" id="crmemissao" value="{{ isset($funcionario->crmemissao) ? $funcionario->crmemissao : ''}}" >
  </div>
      {!! $errors->first('crmemissao', '<p class="help-block">:message</p>') !!}
  </div>

  <div class="form-group row {{ $errors->has('agenda') ? 'has-error' : ''}}">
      <label for="agenda" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Usará a agenda?' }}</label>
      <div class="col-lg-5 col-md-6 col-sm-12">
          <input type="radio" name="agenda" value="nao" id="optionnao" {{ (isset($funcionario->agenda) && $funcionario->agenda == 'nao') ? 'checked' : '' }} checked> <label for="optionnao">Não</label>
          <input type="radio" name="agenda" value="sim" id="optionsim" {{ (isset($funcionario->agenda) && $funcionario->agenda == 'sim') ? 'checked' : ''}} ><label for="optionsim">Sim</label>
      </div>
      {!! $errors->first('agenda', '<p class="help-block">:message</p>') !!}
  </div>

</div>



<div class="form-group row {{ $errors->has('identidade') ? 'has-error' : ''}}">
    <label for="identidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Identidade' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="identidade" type="text" id="identidade" value="{{ isset($funcionario->identidade) ? $funcionario->identidade : ''}}" required>
</div>
    {!! $errors->first('identidade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cpf') ? 'has-error' : ''}}">
    <label for="cpf" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CPF' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cpf" type="text" id="cpf" value="{{ isset($funcionario->cpf) ? $funcionario->cpf : ''}}" required>
</div>
    {!! $errors->first('cpf', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cep') ? 'has-error' : ''}}">
    <label for="cep" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Cep' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cep" type="text" id="cep" value="{{ isset($funcionario->cep) ? $funcionario->cep : ''}}">
</div>
    <!-- <div class="col-lg-5 col-md-6 col-sm-12">
        <div style='cursor:pointer' class='btn btn-info' onclick='buscacep()'>Buscar CEP</div>
    </div> -->
    {!! $errors->first('cep', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('rua') ? 'has-error' : ''}}">
    <label for="rua" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Rua' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="rua" type="text" id="rua" value="{{ isset($funcionario->rua) ? $funcionario->rua : ''}}" required>
</div>
    {!! $errors->first('rua', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Número' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="numero" type="text" id="numero" value="{{ isset($funcionario->numero) ? $funcionario->numero : ''}}" required>
</div>
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('bairro') ? 'has-error' : ''}}">
    <label for="bairro" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Bairro' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="bairro" type="text" id="bairro" value="{{ isset($funcionario->bairro) ? $funcionario->bairro : ''}}" required>
</div>
    {!! $errors->first('bairro', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cidade') ? 'has-error' : ''}}">
    <label for="cidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Cidade' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cidade" type="text" id="cidade" value="{{ isset($funcionario->cidade) ? $funcionario->cidade : ''}}" required>
</div>
    {!! $errors->first('cidade', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('estado') ? 'has-error' : ''}}">
    <label for="estado" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Estado' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="estado" type="text" id="estado" value="{{ isset($funcionario->estado) ? $funcionario->estado : ''}}" required>
</div>
    {!! $errors->first('estado', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group row {{ $errors->has('complemento') ? 'has-error' : ''}}">
    <label for="complemento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Complemento' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="complemento" type="text" id="complemento" value="{{ isset($funcionario->complemento) ? $funcionario->complemento : ''}}" >
</div>
    {!! $errors->first('complemento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('telefone') ? 'has-error' : ''}}">
    <label for="telefone" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Telefone' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control telefone" name="telefone" type="text" id="telefone" value="{{ isset($funcionario->telefone) ? $funcionario->telefone : ''}}" >
</div>
    {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('recado') ? 'has-error' : ''}}">
    <label for="recado" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Recado' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control telefone" name="recado" type="text" id="recado" value="{{ isset($funcionario->recado) ? $funcionario->recado : ''}}" >
</div>
    {!! $errors->first('recado', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('celular') ? 'has-error' : ''}}">
    <label for="celular" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Celular' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control celular" name="celular" type="text" id="celular" value="{{ isset($funcionario->celular) ? $funcionario->celular : ''}}" >
</div>
    {!! $errors->first('celular', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('nascimento') ? 'has-error' : ''}}">
    <label for="nascimento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nascimento' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="nascimento" type="date" id="nascimento" value="{{ isset($funcionario->nascimento) ? $funcionario->nascimento : ''}}" required>
</div>
    {!! $errors->first('nascimento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('nacionlalidade') ? 'has-error' : ''}}">
    <label for="nacionlalidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nacionalidade' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="nacionlalidade" type="text" id="nacionlalidade" value="{{ isset($funcionario->nacionlalidade) ? $funcionario->nacionlalidade : ''}}" required>
</div>
    {!! $errors->first('nacionlalidade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('estadocivil') ? 'has-error' : ''}}">
    <label for="estadocivil" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Estado Civil' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="estadocivil" class="form-control" id="estadocivil" required >
        <option value="">Selecione...</option>
        @foreach (json_decode('{"solteiro":"Solteiro", "casado": "Casado", "divorciado":"Divorciado", "outros":"Outros"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($funcionario->estadocivil) && $funcionario->estadocivil == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('estadocivil', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('sexo') ? 'has-error' : ''}}">
    <label for="sexo" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Sexo' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="sexo" class="form-control" id="sexo" required>
        <option value="">Selecione...</option>
        @foreach (json_decode('{"masculino":"Masculino", "feminino": "Feminino"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($funcionario->sexo) && $funcionario->sexo == $optionKey) ? 'selected' : ''}} required>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('sexo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('filhos') ? 'has-error' : ''}}">
    <label for="filhos" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Filhos' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="filhos" class="form-control" id="filhos" >
        @foreach (json_decode('{"sim": "Sim", "nao": "Nao"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($funcionario->filhos) && $funcionario->filhos == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('filhos', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('totalfilhos') ? 'has-error' : ''}}">
    <label for="totalfilhos" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Quantidade de Filhos' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="totalfilhos" type="number" id="totalfilhos" value="{{ isset($funcionario->totalfilhos) ? $funcionario->totalfilhos : ''}}" >
</div>
    {!! $errors->first('totalfilhos', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('obs') ? 'has-error' : ''}}">
    <label for="obs" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Observações' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <textarea class="form-control" rows="5" name="obs" type="textarea" id="obs" >{{ isset($funcionario->obs) ? $funcionario->obs : ''}}</textarea>
</div>
    {!! $errors->first('obs', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"ativo":"Ativo","inativo":"Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($funcionario->status) && $funcionario->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>

<div class="form-group  offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/funcionarios/funcionarios') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    <button class="btn btn-success rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> Salvar</button>
</div>

@section('jqueryscript')
    <script>
        // Listagem de variáveis transferiadas para o javaScript
        var $form_especialidades = '<?php echo isset($funcionario) ? $funcionario->especialidade : '' ?>';

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/webcamjs/1.0.25/webcam.min.js"></script>
    <!-- <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script> -->
    <!-- <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script> -->
    <script>
      window.onload = function(){
        obj = jQuery('#areaatuacao');
        setProfissionalSaude(obj);
      }

        jQuery(".adicionarfoto").click(function (e) {
            e.preventDefault();
            jQuery(".adicionarfoto").hide();
            jQuery("#tirarfoto").show();
        });

        Webcam.set({
            width: 490,
            height: 390,
            image_format: 'jpeg',
            jpeg_quality: 90
        });

        Webcam.attach( '#my_camera' );

        function take_snapshot() {

            Webcam.snap( function(data_uri) {
                jQuery(".image-tag").val(data_uri);
                document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
            } );
        }

    //Máscaras
    var cpfMascara = function (val) {
       return val.replace(/\D/g, '').length > 11 ? '00.000.000/0000-00' : '000.000.000-009';
    };

    // jQuery('#telefone').mask('(00) 0000-0009');
    jQuery('#telefone, #telefonedois').mask('(00) 0000-00009');
    jQuery('#telefone, telefonedois').blur(function(event) {
        if(jQuery(this).val().length == 15){ // Celular com 9 dígitos + 2 dígitos DDD e 4 da máscara
            jQuery('#telefone, #telefonedois').mask('(00) 00000-0009');
        } else {
            jQuery('#telefone, #telefonedois').mask('(00) 0000-00009');
        }
    });

    jQuery('#celular').mask('(00) 00000-0009');
    jQuery('#cep').mask('00000-000');
    jQuery('#rg').mask('00.000.000-0');


    jQuery('#cpf').mask(cpfMascara);
    jQuery('#cnpj').mask(cpfMascara);
    jQuery('#cpfcnpj').mask(cpfMascara);

    function limpaCampos() {
        // Limpa valores do formulário de cep.
        jQuery("#rua").val("");
        jQuery("#bairro").val("");
        jQuery("#cidade").val("");
        jQuery("#estado").val("");
        jQuery("#numero").val("");
    }

    function buscacep(cep = ''){

        if(!cep){
            cep = jQuery('#cep').val();
        }
        //CEP - somente dígitos
        cep = cep.replace(/\D/g, '');

        jQuery("#rua").val("...");
        jQuery("#bairro").val("...");
        jQuery("#cidade").val("...");
        jQuery("#estado").val("...");
        jQuery("#numero").val("...");

        jQuery.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

            if (!("erro" in dados)) {

                jQuery(".errorcep").html("").hide();
                //Atualiza os campos com os valores da consulta.
                jQuery("#rua").val(dados.logradouro);
                jQuery("#bairro").val(dados.bairro);
                jQuery("#cidade").val(dados.localidade);
                jQuery("#estado").val(dados.uf);
                jQuery("#numero").val('');
                jQuery('.'+dados.uf).prop("selected", true);

                jQuery("#numero").focus();
            } //fim do if
            else {
                //CEP pesquisado não foi encontrado.
                limpaCampos();
                jQuery(".errorcep").html("CEP não encontrado").show();
            }
        });

    }

    jQuery('#cep').keyup(function(){
        cep = jQuery(this).val();
        if(cep.length>=9){
            buscacep(cep);
        }else{
            limpaCampos();
            jQuery(".errorcep").html("CEP não encontrado").show();
        }
    });

  function setProfissionalSaude(obj){
    val = jQuery(obj).val();
    if(val === 'Funcionário'){
      jQuery("#ProfissionalSaude").attr("style","display:none");
      jQuery("#especialidade").prop("required", false);
      jQuery("#crmemissao").prop("required", false);
      jQuery("#crm").prop("required", false);
      jQuery("#especialidade").val('');
      jQuery("#crmemissao").val('');
      jQuery("#crm").val('');
      jQuery("#optionnao").prop('checked',true);
    }else{
      jQuery("#ProfissionalSaude").attr("style","display:block");
      jQuery("#especialidade").prop("required", true);
      jQuery("#crmemissao").prop("required", true);
      jQuery("#crm").prop("required", true);
    }
  }
</script>


@endsection
