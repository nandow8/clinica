@extends('layouts.template')

@section('content')
<style>
    #mapa{ width: 100%; height: 450px; border: 1px solid #ccc; display: block; }
    .total{ background: #fd6161; padding: 0px 10px; font-size: 1.1em; color: #fff; font-weight: bold; border-radius: 10px; }
    .info-title{ color: #040e13; font-size: 1.1em; font-weight: bold; }
</style>
<div class="row">
    <div class="col-lg-12">
        <h3>Meus clientes</h3>
        <div id="mapa"></div>
    </div>
</div>
@endsection

@section('jqueryscript')
  
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAlHzImDCagrOppCrUjRU2FT__KHKsUYhs&callback=initMap"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/markerclustererplus/2.1.4/markerclusterer.js"></script>
<!-- <script src="{{ asset('js/scripts/mapa.js') }}" ></script> -->
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

<script>
    var map;
    var markers = [];

    function GetLocation(address, info) {

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var lat = results[0].geometry.location.lat();
                var long = results[0].geometry.location.lng();

                //se latitude e longitude estiverem OKAY
                if(lat && long){
                    
                    //cria marcador no mapa
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(lat, long),
                        icon: "img/logos/cliente_icon.png",
                        map: map
                    });
                    markers.push(marker);
                    
                    var infowindow = new google.maps.InfoWindow(), marker;
 
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(info);
                            infowindow.open(map, marker);
                        }
                    })(marker));

                }
            }
        });
    };

    function info(id){
        var cont = '';

        function getInformacoes(id){
            
            jQuery.ajax({
                type: "GET",
                dataType: 'json',
                data: {id: id},
                url: "mapa/findDependente"            
            }).done(function(retorno){
                
                cont = retorno;
                return cont;
                // total = retorno[0].total;
            });
        
        }
        return getInformacoes(id);
    
    }
    function findPacientes(){

        jQuery.ajax({
            type: "GET",
            dataType: 'json',
            data: {},
            url: "mapa/findpacientes",
            success: function(retorno){
                $.each(retorno, function(index, dados) {
                    var titular = '';
                    if(dados.paciente_titular_id == null || dados.paciente_titular_id == ""){
                        titular = dados.nome;                        
                    }else{
                        titular = dados.titular;
                    }
                    endereco = dados.rua+' - '+dados.bairros_id+' - '+dados.estados_id;
                    
                    var infos = '';
                    infos += "<span><b class='info-title'>Paciente:</b> "+dados.nome+"</span><br/><br/>";
                    infos += "<span><b class='info-title'>Número de dependentes:</b> <span class='total'>"+ dados.total +"</span></span><br/><br/>";
                    infos += "<span><b class='info-title'>Titular:</b> "+ titular +"</span><br/><br/>";
                    infos += "<span><b class='info-title'>Endereço:</b> "+endereco+"</span><br/><br/>";
                    
                    GetLocation(endereco, infos);
                });
               
                var markerCluster = new MarkerClusterer(map, markers);
               
            }
  	    }); // end ajax

    }

    findPacientes();
   

    function initMap(){
        //Latitude e Longitude do CAP    
        var la = parseFloat("-23.4836138");
        var lo = parseFloat("-46.3540317");
        
        var options = {
            zoom: 12,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: false,
            rotateControl: false,
            fullscreenControl: true,
            center: {lat: la, lng: lo}
        }

        //Seta configuração do mapa
        map = new google.maps.Map(document.getElementById('mapa'),{ options });
        
        //Marcar a localização do CAP
        var marker = new google.maps.Marker({
            position: map.getCenter(),
            title: "Centro de Atendimento Popular",
            icon: "img/logos/icon.png",               
            draggable: true,
            map: map
        });


        var infowindow = new google.maps.InfoWindow(), marker;
 
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infowindow.setContent("Centro de Atendimento Popular");
                infowindow.open(map, marker);
            }
        })(marker));
         
    }

</script>
@endsection