<?php

use PHPJasper\PHPJasper;

// Na linha abaixo utilize o .JRXML ao invés do .JASPER
$input  = public_path() . '/reports/relcontasreceber.jasper';
$output = public_path() . '/reports/'.time().'_contasreceber';
/*
$options = [
    'format' => ['pdf'],
    'locale' => 'pt_BR',
    'params' => [ ],
    'db_connection' => [
        'driver'   => env('DB_CONNECTION'),
        'host'     => env('DB_HOST'),
        'port'     => env('DB_PORT'),
        'database' => env('DB_DATABASE'),
        'username' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD'),
        'jdbc_dir' => base_path() . env('JDBC_DIR'),
    ]
];
*/

$jasper = new PHPJasper;
//$jasper->compile($input)->execute();


$jasper->process(
    $input,
    $output,
    ['pdf'],    
    [],
    [
        'driver'   => 'mysql',
        'host'     => env('DB_HOST'),
        'port'     => env('DB_PORT'),
        'database' => env('DB_DATABASE'),
        'username' => env('DB_USERNAME'),
        'password' => env('DB_PASSWORD'),
        'jdbc_dir' => base_path() . env('JDBC_DIR'),
    ]
)->execute();

$file = $output;
$path = $file;

if (!file_exists($file)) {
    //abort(404,'fodeu' );
    die('fodeu <br>'.$file);
}

//$file = file_get_contents($input);

return file_get_contents($output);

