@extends('layouts.template')

@section('content')
    <div class="row"> 

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Relatório do Contas a Receber</div>
                <div class="card-body">
                    <form method="GET" action="{{ url('/relatorios/contasreceberrel') }}">
                        <div class="tab-pane" style="border: 1px #606f7b solid; border-radius: 10px; padding: 5px 17px 8px 20px; box-shadow: 8px 8px grey" >
                            <div class="row" style="padding: 5px;">
                                <div class="col-sm-6" style="border: #6E6E6E 1px solid; padding: 2px; border-radius: 5px; box-shadow: 3px 3px grey; margin-left: -5px">
                                    <div class="card">
                                        <div class="card-header">Tipo de Relatório</div>
                                        <div class="card-body">
                                            <div class="form-inline">
                                                <input type="radio" name="schtiporel" value="A" class="form-horizontal" checked="checked">&nbsp;Analítico&nbsp;
                                                <input type="radio" name="schtiporel" value="S" class="form-horizontal" >&nbsp;Sintético&nbsp;
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="border: #6E6E6E 1px solid; padding: 3px; border-radius: 5px; box-shadow: 3px 3px grey; margin-left: 5px">
                                    <div class="card">
                                        <div class="card-header">Status</div>
                                        <div class="card-body">
                                            <select name="schstatus" class="form-control form-control-sm">
                                                @foreach (json_decode('{"":"Todos...","Aberto": "Aberto", "Recebido": "Recebido", "Atraso": "Atraso"}', true) as $optionKey => $optionValue)
                                                <option value="{{ $optionKey }}" {{ (request('schstatus') == $optionKey) ? 'selected' : '' }}>{{ $optionValue }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div class="row" style="padding: 5px;">
                                <div class="col-sm-12" style="border: #6E6E6E 1px solid; padding: 2px; border-radius: 5px; box-shadow: 3px 3px grey; margin-left: -2px">
                                    <div class="card">
                                        <div class="card-header">Origem do Recebimento</div>
                                        <div class="card-body">
                                            <select name="schorigem" class="form-control form-control-sm">
                                                @foreach (json_decode('{"":"Todos...","examesrf": "Laboratório RF","servicos": "Tabela de Serviços","manual": "Entrada Manual"}', true) as $optionKey => $optionValue)
                                                <option value="{{ $optionKey }}" {{ (request('schorigem') == $optionKey) ? 'selected' : '' }}>{{ $optionValue }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding: 5px;">
                                <div class="col-sm-6" style="border: #6E6E6E 1px solid; padding: 2px; border-radius: 5px; box-shadow: 3px 3px grey; margin-left: -5px">
                                    <div class="card">
                                        <div class="card-header">Por Data</div>
                                        <div class="card-body">
                                            <div class="form-inline">
                                                <input type="radio" name="schpordata" value="vecto" class="form-horizontal" checked="checked">&nbsp;Vencimento&nbsp;
                                                <input type="radio" name="schpordata" value="pagto" class="form-horizontal" >&nbsp;Pagamento&nbsp;
                                            </div>
                                            <br>
                                            <div class="form-inline">
                                                <input type="date" class="form-control form-control-sm" name="schdataini" value="{{ request('schdataini') }}">
                                                &nbsp;&agrave;&nbsp;
                                                <input type="date" class="form-control form-control-sm" name="schdatafim" value="{{ request('schdatafim') }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6" style="border: #6E6E6E 1px solid; padding: 3px; border-radius: 5px; box-shadow: 3px 3px grey; margin-left: 5px">
                                    <div class="card">
                                        <div class="card-header">Por Tipo de Pagamento</div>
                                        <div class="card-body">
                                            @foreach ($tipospagamentos as $optionKey => $optionValue)
                                                <div class="col-md-6">
                                                    <input type="checkbox" name="schtipopagto[]" value="{{ $optionValue->id }}" class="form-horizontal"
                                                        <?php
                                                            if(is_array(request('schtipopagto'))){
                                                                foreach(request('schtipopagto') as $k =>$i){
                                                                    if(request('schtipopagto')[$k] == $optionValue->id ) {
                                                                        echo 'checked';
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                    >&nbsp;{{ $optionValue->descricao }}&nbsp;<br>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <input type="submit" class="btn btn-success rounded" value="Atualizar Dados" />
                        </div>                    
                    </form>
                    <br/>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">    
            <div class="pre-scrollable">
                <table class="table table-striped">
                    <thead>
                        <tr style="font-size: 8pt;">
                            <th>#</th>
                            <th>Paciente</th>
                            <th>Status</th>
                            <th>Vencimento</th>
                            <th>Pagamento</th>
                            <th>Tipo</th>
                            <th>Descrição</th>
                            <th>Valor</th>
                            <th>Valor Pago</th>
                            <th>Lucro</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($relcontasreceber as $item)
                        <tr style="font-size: 8pt;">
                                <td>{{ $item->id }}</td>
                                <td>{{ $item->nome }}</td>
                                <td>{{ $item->status }}</td>
                                <td>{{ date('d/m/Y', strtotime($item->vencimento)) }}</td>
                                <td>{{ date('d/m/Y', strtotime($item->pagamento)) }}</td>
                                <td>{{ $item->tipo }}</td>
                                <td>{{ $item->descricao }}</td>
                                <td>{{ number_format($item->valor, 2, ',', '.') }}</td>
                                <td>{{ number_format($item->valorpago, 2, ',', '.') }}</td>
                                <td>{{ number_format($item->lucro, 2, ',', '.') }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

@section('jqueryscript')

<script type="text/javascript">
    
    
</script>


@endsection