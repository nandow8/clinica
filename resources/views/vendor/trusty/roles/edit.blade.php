@extends('layouts.template')

@section('content')
<form action="{{ route('roles.update', $role) }}" method="POST">
        @csrf
        @method('PUT')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Edit Details</div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="name">Name <span class="required text-danger small">*</span></label>
                                    <input type="text" name="name" id="name" class="form-control" value="{{ old('name', $role->name) }}" placeholder="Name" required>
                                </div>
                                <div class="form-group">
                                    <label for="display_name">Display Name</label>
                                    <input type="text" name="display_name" id="display_name" class="form-control" value="{{ old('display_name', $role->display_name) }}" placeholder="Display Name">
                                </div>
                                <div class="form-group">
                                    <label for="description">Description</label>
                                    <input type="text" name="description" id="description" class="form-control" value="{{ old('description', $role->description) }}" placeholder="Description">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card"> 

                    <div class="card-body">
                        <h1 class="d-flex justify-content-center">{{ $role->name }}</h1> 
                        <!-- <b>Display Name: </b> {{ $role->display_name }}<br>
                        <b>Description: </b> {{ $role->description }}<br>
                        <br> -->
                        <!-- <b>Created At: </b> {{ $role->created_at->format('m/d/Y') }}<br>
                        <b>Updated At: </b> {{ $role->updated_at->format('m/d/Y') }}<br> -->
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">Permissions</div>

                    <div class="card-body">

                        <table class="table" id="permissions-table">
                            <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>Display Name</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($permissions as $item)
                                <tr>
                                    <td>
                                        @if(in_array($item->id, $permission_ids))
                                            <input type="checkbox" name="permission_{{ $item->name }}" checked>
                                        @else
                                            <input type="checkbox" name="permission_{{ $item->name }}">
                                        @endif
                                    </td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->display_name }}</td>
                                    <td>{{ $item->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <div class="text-right">
                            <a href="{{ route('roles.index') }}" class="btn btn-secondary"><em class="fa fa-times"></em> Cancel</a>
                            <button class="btn btn-primary" type="submit"><em class="fa fa-save"></em> Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('footer')
    <script type="text/javascript">

        $('#permissions-table').dataTable();

    </script>
@endsection
