{{-- @extends('trusty::layouts.trusty') --}}
@extends('layouts.template')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">Dashboard</div>
                    <div class="card-body">
                        <ul>
                            <li><a href="{{ route('users.index') }}">Usuários</a></li>
                            <li><a href="{{ route('roles.index') }}">Regras</a></li>
                            <li><a href="{{ route('permissions.index') }}">Permisões</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
