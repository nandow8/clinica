<!DOCTYPE html>
<html>
    <body>

        <!-- <link rel="stylesheet" href="{{ asset('css/stylesheet.css')}}" > -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="   crossorigin="anonymous"></script>
        <style type="text/css">

            *{
                padding: 0;
                margin: 0;
                list-style: none;
                outline: none;
                border: none;

                --webkit-box-sizing: border-box;
                --moz-box-sizing: border-box;
                --box-sizing: border-box;
            }

            body{
                background: #f9f9f9;
            }

            /**
            * ======================================================================================= *
            * stylesheet of Painel Chamadas page
            * ======================================================================================= *
            */
            div.background{
                text-align: center;
                position: absolute;
                left: 0;
                width: calc(100% - 400px);
                height: 100%;
                background-color: white;
                background-size: cover;
            }

            section#conteudo-view{
                position: absolute;
                right: 0;
                padding: 10px;
                float: right;
                width: 380px;
                height: 100%;
                background: white;
            }

            #historico{
                float: left;
                background-color: white;
            }
            
            #principal{
                width: inherit;
                height: 100%;
                color: #2c2c36;
            }

            #principal ul,
            #principal ul li{
                float: left;
                width: 100%
            }

            #principal ul li i{
                float: left;
                height: 40px;
                width: 40px;
                color: inherit;
                text-align: center;
                line-height: 50px;
                font-size: 1.5em;
            }

            #principal ul li h1{
                float: left;
                height: 35px;
                color: inherit;
                font: 1.1em/50px cursive;
            }

            .titulo{
                float: left;
                height: 15px;
                width: 100%;
                color: #fff;
                font: 1.1em/20px cursive;
                background: #89b030;
                padding: 15px 5px 15px 5px;
                text-align: center;
            }

            #principal ul li h3{
                float: left;
                padding-left: 50px;
                height: 40px;
                color: inherit;
                font: 0.8em/50px cursive;
                line-height: 18px;
            }

            #propaganda{
                text-align: center;
            }

            .videoContainer {
                position: absolute;
                width: 100%;
                height: 100%;
                top: -75px;
                left: 0;
                bottom: 0;
                right: 0;
                display: flex;
                flex-direction: column;
                align-items: center;
                z-index: 0;
            }
            .splash {
                padding: 1px;
                background: rgb(0,0,100,0.2);
                width: 100%;
                height: 480px;
                padding: 60px 0;
                position: relative;
                left: 0;
            }

            #noticias {
                background-color: white;
                float: left;
                margin: 0px 0px -1px !important;
                min-height: 180px;
                padding: 11px 0 0;
                position: relative;
                width: 100%;
                z-index: 2;
            }

            #ultimochamado{
                float: left;
                min-height: 120px;
                position: relative;
                width: 100%;
            }
            
            #ultimochamado h1{
                float: left;
                padding-left: 10px;
                height: 40px;
                color: black;
                font: 1.8em/30px cursive;
                line-height: 18px;
            }

            #ultimochamado h3{
                float: left;
                padding-left: 10px;
                height: 40px;
                color: inherit;
                font: 1.2em/30px cursive;
                line-height: 18px;
            }

            .clock {
                font-size: 2em;
                font-weight: bold;
            }
            
            .chamadapornome{
                padding: 30px;
                background-color: wheat;
                margin: 50px;
            }
            
            input{
                padding: 9px;
                font-size: 18px;
                border-bottom: solid 1px #aaa; 
                width: 80%;
            }
            
            input[type="button"]{
                width: 15%;
            }
        </style>


        <div class="background">
            <div id="propaganda">
                <div class="videoContainer">
                    <iframe class="videoContainer__video" width="100%" height="750px" src="http://www.youtube.com/embed/v=811UGhkCemc&list=PLuwO6WnOd0q9YIla_tzynucJeLfTbvtQk?modestbranding=1&autoplay=1&controls=0&fs=0&loop=1&rel=0&showinfo=0&disablekb=1&playlist=v=811UGhkCemc&list=PLuwO6WnOd0q9YIla_tzynucJeLfTbvtQk" frameborder="0"></iframe>
                </div>      
                <!--
                        <div class="videoContainer">
                            <video autoplay loop sound preload poster="{{ asset('img/logos/logo.png') }}">
                                <source src="{{ asset('mov_bbb.mp4') }}" type='video/mp4'>
                            </video>
                        </div>      
                -->
            </div>
            <div class="splash">
            </div>    
            <div style="clear: both;"></div>    
            <div id="noticias">
                <div class="titulo">
                    <h4>Notícias</h4>
                </div>
            </div>
            <div id="chamadapornome">
                <input type="text" id="texto">
                <input type="button" id="falar" value="diga">
            </div>
        </div>

        <section id="conteudo-view">
            <div class="clock"><i class="fa fa-clock-o"></i></div>
            <div class="titulo">
                <h2> Atual </h2>
            </div>
            <div id="ultimochamado">
                <br>
                <div class="chamando col-md-12">
                    <table class="table-responsive">
                        @foreach($agendadia as $key => $itens)
                        @if($key == 0)
                        <tr>
                            <td>
                                @if(isset($itens->foto))
                                <img src="uploads/fotospacientes/{{ $itens->foto }}" width="140px"/>
                                @else
                                <img src="uploads/fotospacientes/defaultman.jpg" width="140px"/>
                                @endif
                            </td>
                            <td>
                                <h1>{{ $itens->paciente }}</h1>
                                <br>
                                <h3>
                                    {{ 'Sala: ' . $itens->sala . ' - ' . $itens->areaatuacao . ': ' . $itens->profissional }}
                                </h3>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="titulo">
                <h2> Histórico </h2>
            </div>
            <div id="historico">
                <nav id="principal">
                    <ul>
                        @foreach($agendadia as $key => $itens)

                        @if($key > 0)
                        <li>
                            <div class="chamado">
                                <h1>
                                    <i class="fa fa-user"></i>
                                    {{ $itens->paciente }}
                                </h1>
                                <h3>{{ 'Sala: ' . $itens->sala . ' - ' . $itens->areaatuacao . ': ' . $itens->profissional }}</h3>
                            </div>
                        </li>
                        @endif
                        @endforeach
                    </ul>
                </nav>
            </div>
        </section>


        <script>
var myVideo = document.getElementById("video1");

document.getElementById('falar').addEventListener("click", () => {
    chamarporvoz(document.getElementById("texto").value);
})


function playPause() {
    if (myVideo.paused)
        myVideo.play();
    else
        myVideo.pause();
}

function clock() {// We create a new Date object and assign it to a variable called "time".
    var time = new Date(),
            // Access the "getHours" method on the Date object with the dot accessor.
            hours = time.getHours(),
            // Access the "getMinutes" method with the dot accessor.
            minutes = time.getMinutes(),
            seconds = time.getSeconds();

    document.querySelectorAll('.clock')[0].innerHTML = harold(hours) + ":" + harold(minutes);

    function harold(standIn) {
        if (standIn < 10) {
            standIn = '0' + standIn
        }
        return standIn;
    }

}

function atualizaconteudos() {
    $('#ultimochamado').load(' #ultimochamado');
    $('#principal').load(' #principal');
}

function chamarporvoz(texto) {
   
    var meuSpeach = window.speechSynthesis;
    
    var utterThis = new SpeechSynthesisUtterance();
    utterThis.lang = 'pt-BR';
    utterThis.text = texto;
    utterThis.volume = 1.5;
    console.log(utterThis.onerror);
    
    meuSpeach.speak(utterThis);
    
    console.log(texto);
}



setInterval(clock, 1000);
setInterval(atualizaconteudos, 5000);

        </script>
    </body>
</html>

