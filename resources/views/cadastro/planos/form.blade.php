<?php
    // echo '<pre>';
    // print_r($convenios);

    // print_r($plano);
?>

<div class="form-group row {{ $errors->has('nomeplano') ? 'has-error' : ''}}">
    <label for="nomeplano" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nome do plano' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="nomeplano" type="text" id="nomeplano" value="{{ isset($plano->nomeplano) ? $plano->nomeplano : ''}}" required>
</div>
    {!! $errors->first('nomeplano', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('idconvenio') ? 'has-error' : ''}}">
    <label for="idconvenio" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Convênio' }} <span class="requerido">(*)</span></label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <!-- <input class="form-control" name="idconvenio" type="number" id="idconvenio" value="{{ isset($plano->idconvenio) ? $plano->idconvenio : ''}}" > -->

    <select name="idconvenio" class="form-control" required>
        <option value="">Selecione</option>
        @foreach ($convenios as $index => $convenio)
            <option value="{{ $convenio->id }}" {{ (isset($plano->idconvenio) AND $plano->idconvenio == $convenio->id) ? 'selected' : ''}} >{{ $convenio->convenio }}</option>
        @endforeach
    </select>

</div>
    {!! $errors->first('idconvenio', '<p class="help-block">:message</p>') !!}
</div>

<br/>

<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/cadastro/planos/') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection