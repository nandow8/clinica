@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Plano</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/planos') }}" title="Voltar">
                    <button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $plano->id }}</td>
                                    </tr>
                                */ ?>
                                <tr>
                                    <th> Nome do plano </th><td> {{ $plano->nomeplano }} </td>
                                </tr>
                                <tr>
                                    <th> Convênio </th>
                                    <td>
                                        <?php $convenio = \App\Models\Pacientesconvenio::find($plano->idconvenio);
                                            $conv = json_decode($convenio, true);
                                            echo ($conv['convenio']) ? $conv['convenio'] : '--';
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
