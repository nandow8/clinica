<div class="form-group row {{ $errors->has('convenio') ? 'has-error' : ''}}">
    <label for="convenio" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Convênio' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="convenio" type="text" id="convenio" value="{{ isset($pacientesconvenio->convenio) ? $pacientesconvenio->convenio : ''}}" >
</div>
    {!! $errors->first('convenio', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('cnpj') ? 'has-error' : ''}}">
    <label for="cnpj" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CNPJ' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cnpj" type="text" id="cnpj" value="{{ isset($pacientesconvenio->cnpj) ? $pacientesconvenio->cnpj : ''}}" >
</div>
    {!! $errors->first('cnpj', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('contato') ? 'has-error' : ''}}">
    <label for="contato" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Contato' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="contato" type="text" id="contato" value="{{ isset($pacientesconvenio->contato) ? $pacientesconvenio->contato : ''}}" >
</div>
    {!! $errors->first('numerocarteira', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('telefone') ? 'has-error' : ''}}">
    <label for="telefone" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Telefone 1' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="telefone" type="text" id="telefone" value="{{ isset($pacientesconvenio->telefone) ? $pacientesconvenio->telefone : ''}}" >
</div>
    {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('telefonedois') ? 'has-error' : ''}}">
    <label for="telefonedois" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Telefone 2' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="telefonedois" type="text" id="telefonedois" value="{{ isset($pacientesconvenio->telefonedois) ? $pacientesconvenio->telefonedois : ''}}" >
</div>
    {!! $errors->first('telefonedois', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Email' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($pacientesconvenio->email) ? $pacientesconvenio->email : ''}}" >
</div>
    {!! $errors->first('numerocarteira', '<p class="help-block">:message</p>') !!}
</div>


<!-- <div class="form-group row {{ $errors->has('numerocarteira') ? 'has-error' : ''}}">
    <label for="numerocarteira" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Preço' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="numerocarteira" type="text" id="numerocarteira" value="{{ isset($pacientesconvenio->numerocarteira) ? $pacientesconvenio->numerocarteira : ''}}" >
</div>
    {!! $errors->first('numerocarteira', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('datavalidade') ? 'has-error' : ''}}">
    <label for="datavalidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Datavalidade' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="datavalidade" type="date" id="datavalidade" value="{{ isset($pacientesconvenio->datavalidade) ? $pacientesconvenio->datavalidade : ''}}" >
</div>
    {!! $errors->first('datavalidade', '<p class="help-block">:message</p>') !!}
</div> -->
<br/>
    
<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/cadastro/pacientesconvenio/') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection