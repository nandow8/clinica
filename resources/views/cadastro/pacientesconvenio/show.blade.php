@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Convênio</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/pacientesconvenio') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $pacientesconvenio->id }}</td>
                                    </tr>
                                */ ?>
                                <tr>
                                    <th> Convênio </th><td> {{ $pacientesconvenio->convenio }} </td>
                                </tr>
                                <tr>
                                    <th> Contato </th><td> {{ ($pacientesconvenio->contato) ? $pacientesconvenio->contato : '--' }} </td>
                                </tr>
                                <tr>
                                    <th> CNPJ </th><td> {{ ($pacientesconvenio->cnpj) ? $pacientesconvenio->cnpj : '--' }} </td>
                                </tr>
                                <tr>
                                    <th> Telefone </th><td> {{ ($pacientesconvenio->telefone) ? $pacientesconvenio->telefone : '--' }} </td>
                                </tr>
                                <tr>
                                    <th> Telefone 2 </th><td> {{ ($pacientesconvenio->telefonedois) ? $pacientesconvenio->telefonedois : '--' }} </td>
                                </tr>
                                <tr>
                                    <th> E-mail </th><td> {{ ($pacientesconvenio->email) ? $pacientesconvenio->email : '--' }} </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
