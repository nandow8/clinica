@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Grupoempresarial</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/grupoempresarial') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $grupoempresarial->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Razao </th><td> {{ $grupoempresarial->razao }} </td></tr><tr><th> Fantasia </th><td> {{ $grupoempresarial->fantasia }} </td></tr><tr><th> Cpfcnpj </th><td> {{ $grupoempresarial->cpfcnpj }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
