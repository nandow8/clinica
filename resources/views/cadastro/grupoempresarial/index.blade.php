@extends('layouts.template')

@section('content')
    <div class="row"> 

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Grupos Empresariais</div>
                <div class="card-body">
                    <a href="{{ url('/cadastro/grupoempresarial/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar Grupoempresarial">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Grupo Empresarial
                    </a>

                    <form method="GET" action="{{ url('/cadastro/grupoempresarial') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th><th>Razão</th><th>Fantasia</th><th>CNPJ</th><th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($grupoempresarial as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->razao }}</td><td>{{ $item->fantasia }}</td><td>{{ $item->cpfcnpj }}</td>
                                    <td>
                                        <a href="{{ url('/cadastro/grupoempresarial/' . $item->id) }}" title="Visualizar Grupo Empresarial"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/cadastro/grupoempresarial/' . $item->id . '/edit') }}" title="Editar Grupo Empresarial"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        
                                        {{ Form::open(['url'=> '/cadastro/grupoempresarial' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                            {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title' =>'Excluir', 'role' => 'button', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $grupoempresarial->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')

@endsection