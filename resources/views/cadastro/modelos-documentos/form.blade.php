<script src="https://cloud.tinymce.com/5/tinymce.min.js?apiKey=9jfapv06bogu697vy9rivm8ks9m1tf73winctkye3lmdwdkx"></script>
<script>tinymce.init({ selector:'textarea' });</script>

<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Descrição' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="descricao" type="text" id="descricao" value="{{ isset($modelosdocumento->descricao) ? $modelosdocumento->descricao : ''}}" >
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('corpodocumento') ? 'has-error' : ''}}">
    <label for="corpodocumento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Documento' }}</label>
    <div class="col-lg-12 col-md-12 col-sm-12">
    <textarea class="form-control" rows="20" name="corpodocumento" type="textarea" id="corpodocumento" >{{ isset($modelosdocumento->corpodocumento) ? $modelosdocumento->corpodocumento : ''}}</textarea>
</div>
    {!! $errors->first('corpodocumento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($modelosdocumento->status) && $modelosdocumento->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>

<div class="form-group offset-md-5">
    <a href="{{ url('/cadastro/modelos-documentos') }}" title="Voltar" class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show')
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Salvar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection
