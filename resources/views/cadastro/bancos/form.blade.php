<div class="form-group row {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Codigo' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{ isset($banco->codigo) ? $banco->codigo : ''}}" >
</div>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('agencia') ? 'has-error' : ''}}">
    <label for="agencia" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Agencia' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="agencia" type="text" id="agencia" value="{{ isset($banco->agencia) ? $banco->agencia : ''}}" >
</div>
    {!! $errors->first('agencia', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('agenciadig') ? 'has-error' : ''}}">
    <label for="agenciadig" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Agenciadig' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="agenciadig" type="text" id="agenciadig" value="{{ isset($banco->agenciadig) ? $banco->agenciadig : ''}}" >
</div>
    {!! $errors->first('agenciadig', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('conta') ? 'has-error' : ''}}">
    <label for="conta" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Conta' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="conta" type="text" id="conta" value="{{ isset($banco->conta) ? $banco->conta : ''}}" >
</div>
    {!! $errors->first('conta', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('contadig') ? 'has-error' : ''}}">
    <label for="contadig" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Contadig' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="contadig" type="text" id="contadig" value="{{ isset($banco->contadig) ? $banco->contadig : ''}}" >
</div>
    {!! $errors->first('contadig', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cedente') ? 'has-error' : ''}}">
    <label for="cedente" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Cedente' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cedente" type="text" id="cedente" value="{{ isset($banco->cedente) ? $banco->cedente : ''}}" >
</div>
    {!! $errors->first('cedente', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cedentedig') ? 'has-error' : ''}}">
    <label for="cedentedig" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Cedentedig' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cedentedig" type="text" id="cedentedig" value="{{ isset($banco->cedentedig) ? $banco->cedentedig : ''}}" >
</div>
    {!! $errors->first('cedentedig', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nome' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($banco->nome) ? $banco->nome : ''}}" >
</div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('carteira') ? 'has-error' : ''}}">
    <label for="carteira" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Carteira' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="carteira" type="text" id="carteira" value="{{ isset($banco->carteira) ? $banco->carteira : ''}}" >
</div>
    {!! $errors->first('carteira', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('variacao') ? 'has-error' : ''}}">
    <label for="variacao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Variacao' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="variacao" type="text" id="variacao" value="{{ isset($banco->variacao) ? $banco->variacao : ''}}" >
</div>
    {!! $errors->first('variacao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Numero' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="numero" type="text" id="numero" value="{{ isset($banco->numero) ? $banco->numero : ''}}" >
</div>
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('convenio') ? 'has-error' : ''}}">
    <label for="convenio" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Convenio' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="convenio" type="text" id="convenio" value="{{ isset($banco->convenio) ? $banco->convenio : ''}}" >
</div>
    {!! $errors->first('convenio', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('instrucao1') ? 'has-error' : ''}}">
    <label for="instrucao1" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Instrucao1' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="instrucao1" type="text" id="instrucao1" value="{{ isset($banco->instrucao1) ? $banco->instrucao1 : ''}}" >
</div>
    {!! $errors->first('instrucao1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('instrucao2') ? 'has-error' : ''}}">
    <label for="instrucao2" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Instrucao2' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="instrucao2" type="text" id="instrucao2" value="{{ isset($banco->instrucao2) ? $banco->instrucao2 : ''}}" >
</div>
    {!! $errors->first('instrucao2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('instrucao3') ? 'has-error' : ''}}">
    <label for="instrucao3" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Instrucao3' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="instrucao3" type="text" id="instrucao3" value="{{ isset($banco->instrucao3) ? $banco->instrucao3 : ''}}" >
</div>
    {!! $errors->first('instrucao3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($banco->status) && $banco->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('geraboleto') ? 'has-error' : ''}}">
    <label for="geraboleto" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Geraboleto' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="geraboleto" class="form-control" id="geraboleto" >
        @foreach (json_decode('{"Sim": "Sim", "Não": "Não"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($banco->geraboleto) && $banco->geraboleto == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('geraboleto', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/cadastro/bancos') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection