@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Tabela de Descontos e Acréscimos</div>
                <div class="card-body">

                    <a href="{{ url('/tabeladescontoacrescimo/tabela-desconto-acrescimo') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $tabeladescontoacrescimo->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Titulo </th><td> {{ $tabeladescontoacrescimo->titulo }} </td></tr><tr><th> Descricao </th><td> {{ $tabeladescontoacrescimo->descricao }} </td></tr><tr><th> Valor </th><td> {{ $tabeladescontoacrescimo->valor }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
