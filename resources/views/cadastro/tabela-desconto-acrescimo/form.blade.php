<div class="form-group row {{ $errors->has('titulo') ? 'has-error' : ''}}">
    <label for="titulo" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Titulo *' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="titulo" type="text" id="titulo" required="true" value="{{ isset($tabeladescontoacrescimo->titulo) ? $tabeladescontoacrescimo->titulo : ''}}" >
</div>
    {!! $errors->first('titulo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Descricao' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" >{{ isset($tabeladescontoacrescimo->descricao) ? $tabeladescontoacrescimo->descricao : ''}}</textarea>
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('valor') ? 'has-error' : ''}}">
    <label for="valor" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Percentual *' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="valor" type="number" step="any" id="valor" required="true" value="{{ isset($tabeladescontoacrescimo->valor) ? $tabeladescontoacrescimo->valor : ''}}" >
</div>
    {!! $errors->first('valor', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('tipo') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Tipo' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="tipo" class="form-control" id="tipo" >
        @foreach (json_decode('{"Desconto":"Desconto", "Acrescimo":"Acréscimo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($tabeladescontoacrescimo->tipo) && $tabeladescontoacrescimo->tipo == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('tipo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Ativo":"Ativo", "Inativo":"Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($tabeladescontoacrescimo->status) && $tabeladescontoacrescimo->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/cadastro/tabela-desconto-acrescimo') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Salvar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection