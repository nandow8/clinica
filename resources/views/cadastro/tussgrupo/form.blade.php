<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-sm-2 col-form-label">{{ 'Descricao' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="descricao" type="text" id="descricao" value="{{ isset($tussgrupo->descricao) ? $tussgrupo->descricao : ''}}" >
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group  offset-md-6">
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection