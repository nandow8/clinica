@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Cid</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/cid') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $cid->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Codigo </th><td> {{ $cid->codigo }} </td></tr><tr><th> Descricao </th><td> {{ $cid->descricao }} </td></tr><tr><th> Status </th><td> {{ $cid->status }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
