<div class="form-group row {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="col-sm-2 col-form-label">{{ 'Codigo' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{ isset($cid->codigo) ? $cid->codigo : ''}}" >
</div>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-sm-2 col-form-label">{{ 'Descricao' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="descricao" type="text" id="descricao" value="{{ isset($cid->descricao) ? $cid->descricao : ''}}" >
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-sm-2 col-form-label">{{ 'Status' }}</label>
    <div class="col-sm-5">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($cid->status) && $cid->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group  offset-md-6">
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection