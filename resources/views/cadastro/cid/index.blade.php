@extends('layouts.template')

@section('content')
    <div class="row"> 

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Cid</div>
                <div class="card-body">
                    <a href="{{ url('/cadastro/cid/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar Cid">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Cid
                    </a>

                    <form method="GET" action="{{ url('/cadastro/cid') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th><th>Codigo</th><th>Descricao</th><th>Status</th><th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($cid as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->codigo }}</td><td>{{ $item->descricao }}</td><td>{{ $item->status }}</td>
                                    <td>
                                        <a href="{{ url('/cadastro/cid/' . $item->id) }}" title="Visualizar Cid"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/cadastro/cid/' . $item->id . '/edit') }}" title="Editar Cid"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        
                                        {{ Form::open(['url'=> '/cadastro/cid' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                            {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title' =>'Excluir', 'role' => 'button', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $cid->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')

@endsection