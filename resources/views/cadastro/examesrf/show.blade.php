@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Exames RF</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/examesrf') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $examesrf->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Tabela </th><td> {{ $examesrf->tabela }} </td></tr><tr><th> Mneumonico </th><td> {{ $examesrf->mneumonico }} </td></tr><tr><th> Codigo </th><td> {{ $examesrf->codigo }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
