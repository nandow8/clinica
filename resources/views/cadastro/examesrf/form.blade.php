<div class="form-group row {{ $errors->has('tabela') ? 'has-error' : ''}}">
    <label for="tabela" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Tabela' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="tabela" class="form-control" id="tabela" >
        @foreach (json_decode('{"AMB90":"Amb90"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($examesrf->tabela) && $examesrf->tabela == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('tabela', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('mneumonico') ? 'has-error' : ''}}">
    <label for="mneumonico" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Mnemônico' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="mneumonico" type="text" id="mneumonico" value="{{ isset($examesrf->mneumonico) ? $examesrf->mneumonico : ''}}" >
</div>
    {!! $errors->first('mneumonico', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Código' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{ isset($examesrf->codigo) ? $examesrf->codigo : ''}}" >
</div>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('nome') ? 'has-error' : ''}}">
    <label for="nome" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nome' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="nome" type="text" id="nome" value="{{ isset($examesrf->nome) ? $examesrf->nome : ''}}" >
</div>
    {!! $errors->first('nome', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('ch') ? 'has-error' : ''}}">
    <label for="ch" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CH' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="ch" type="number" id="ch" value="{{ isset($examesrf->ch) ? $examesrf->ch : ''}}" >
</div>
    {!! $errors->first('ch', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('preco') ? 'has-error' : ''}}">
    <label for="preco" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Preço' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="preco" type="number" id="preco" value="{{ isset($examesrf->preco) ? $examesrf->preco : ''}}" >
</div>
    {!! $errors->first('preco', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('percentual') ? 'has-error' : ''}}">
    <label for="percentual" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Percentual' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="percentual" type="number" id="percentual" value="{{ isset($examesrf->percentual) ? $examesrf->percentual : ''}}" >
</div>
    {!! $errors->first('percentual', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('venda') ? 'has-error' : ''}}">
    <label for="venda" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Venda' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="venda" type="number" id="venda" value="{{ isset($examesrf->venda) ? $examesrf->venda : ''}}" >
</div>
    {!! $errors->first('venda', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('lucro') ? 'has-error' : ''}}">
    <label for="lucro" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Lucro' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="lucro" type="number" id="lucro" value="{{ isset($examesrf->lucro) ? $examesrf->lucro : ''}}" >
</div>
    {!! $errors->first('lucro', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Ativo":"Ativo", "Inativo":"Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($examesrf->status) && $examesrf->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/cadastro/examesrf') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection