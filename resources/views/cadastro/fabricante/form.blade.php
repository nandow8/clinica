<div class="form-group {{ $errors->has('cnpj') ? 'has-error' : ''}}">
    <label for="cnpj" class="control-label">{{ 'Cnpj' }}</label>
    <input class="form-control" name="cnpj" type="text" id="cnpj" value="{{ isset($fabricante->cnpj) ? $fabricante->cnpj : ''}}" >
    {!! $errors->first('cnpj', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('razao') ? 'has-error' : ''}}">
    <label for="razao" class="control-label">{{ 'Razao' }}</label>
    <input class="form-control" name="razao" type="text" id="razao" value="{{ isset($fabricante->razao) ? $fabricante->razao : ''}}" >
    {!! $errors->first('razao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('fantasia') ? 'has-error' : ''}}">
    <label for="fantasia" class="control-label">{{ 'Fantasia' }}</label>
    <input class="form-control" name="fantasia" type="text" id="fantasia" value="{{ isset($fabricante->fantasia) ? $fabricante->fantasia : ''}}" >
    {!! $errors->first('fantasia', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($fabricante->status) && $fabricante->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
