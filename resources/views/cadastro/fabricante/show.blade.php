@extends('layouts.template')

@section('content')
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Fabricante {{ $fabricante->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/cadastro/fabricante') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/cadastro/fabricante/' . $fabricante->id . '/edit') }}" title="Edit Fabricante"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('cadastro/fabricante' . '/' . $fabricante->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Fabricante" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $fabricante->id }}</td>
                                    </tr>
                                    <tr><th> Cnpj </th><td> {{ $fabricante->cnpj }} </td></tr><tr><th> Razao </th><td> {{ $fabricante->razao }} </td></tr><tr><th> Fantasia </th><td> {{ $fabricante->fantasia }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
