@extends('layouts.template')

@section('content')
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Edit Fabricante #{{ $fabricante->id }}</div>
                    <div class="card-body">
                        <a href="{{ url('/cadastro/fabricante') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/cadastro/fabricante/' . $fabricante->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}

                            @include ('cadastro.fabricante.form', ['formMode' => 'edit'])

                        </form>

                    </div>
                </div>
            </div>
        </div>
@endsection
