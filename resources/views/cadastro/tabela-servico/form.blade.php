<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Descricao' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="descricao" type="text" id="descricao" value="{{ isset($tabelaservico->descricao) ? $tabelaservico->descricao : ''}}" >
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('valor') ? 'has-error' : ''}}">
    <label for="valor" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Valor' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="valor" type="number" step="any" id="valor" value="{{ isset($tabelaservico->valor) ? $tabelaservico->valor : ''}}" >
</div>
    {!! $errors->first('valor', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($tabelaservico->status) && $tabelaservico->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/cadastro/tabela-servico/') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Salvar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection