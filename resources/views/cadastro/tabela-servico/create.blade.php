@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Adicionar Tabela de Serviços</div>
                <div class="card-body">
                        
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ url('/cadastro/tabela-servico') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        @include ('cadastro.tabela-servico.form', ['formMode' => 'create'])

                    </form>                    
                </div>
            </div>
        </div>
    </div>
@endsection