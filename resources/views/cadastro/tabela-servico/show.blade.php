@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Tabela de Serviços</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/tabela-servico') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $tabelaservico->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Descricao </th><td> {{ $tabelaservico->descricao }} </td></tr><tr><th> Valor </th><td> {{ $tabelaservico->valor }} </td></tr><tr><th> Status </th><td> {{ $tabelaservico->status }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
