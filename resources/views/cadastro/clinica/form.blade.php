<div class="form-group row {{ $errors->has('razao') ? 'has-error' : ''}}">
    <label for="razao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Razão' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="razao" type="text" id="razao" required value="{{ isset($clinica->razao) ? $clinica->razao : ''}}" >
</div>
    {!! $errors->first('razao', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('fantasia') ? 'has-error' : ''}}">
    <label for="fantasia" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Fantasia' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="fantasia" type="text" id="fantasia" required value="{{ isset($clinica->fantasia) ? $clinica->fantasia : ''}}" >
</div>
    {!! $errors->first('fantasia', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cep') ? 'has-error' : ''}}">
    <label for="cep" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CEP' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cep" type="text" id="cep" required value="{{ isset($clinica->cep) ? $clinica->cep : ''}}" >
    <div class="alert alert-warning errorcep" style="padding:.1rem; text-align:center; margin-top:5px; display:none;"></div>
</div>
    {!! $errors->first('cep', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('endereco') ? 'has-error' : ''}}">
    <label for="endereco" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Endereço' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="endereco" type="text" id="endereco" required value="{{ isset($clinica->endereco) ? $clinica->endereco : ''}}" >
</div>
    {!! $errors->first('endereco', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Número' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="numero" type="text" id="numero" required value="{{ isset($clinica->cidade) ? $clinica->cidade : ''}}" >
</div>
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('uf') ? 'has-error' : ''}}">
    <label for="uf" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'UF' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="uf" type="text" id="uf" required value="{{ isset($clinica->uf) ? $clinica->uf : ''}}" >
</div>
    {!! $errors->first('uf', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('cidade') ? 'has-error' : ''}}">
    <label for="cidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Cidade' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cidade" type="text" id="cidade" required value="{{ isset($clinica->cidade) ? $clinica->cidade : ''}}" >
</div>
    {!! $errors->first('cidade', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('cnpj') ? 'has-error' : ''}}">
    <label for="cnpj" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CNPJ' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cnpj" type="text" id="cnpj" required value="{{ isset($clinica->cnpj) ? $clinica->cnpj : ''}}" >
</div>
    {!! $errors->first('cnpj', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('conta') ? 'has-error' : ''}}">
    <label for="conta" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Conta' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="conta" type="text" id="conta" value="{{ isset($clinica->conta) ? $clinica->conta : ''}}" >
</div>
    {!! $errors->first('conta', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('carteira') ? 'has-error' : ''}}">
    <label for="carteira" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Carteira' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="carteira" type="text" id="carteira" value="{{ isset($clinica->carteira) ? $clinica->carteira : ''}}" >
</div>
    {!! $errors->first('carteira', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('agencia') ? 'has-error' : ''}}">
    <label for="agencia" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Agencia' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="agencia" type="text" id="agencia" value="{{ isset($clinica->agencia) ? $clinica->agencia : ''}}" >
</div>
    {!! $errors->first('agencia', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('codigocliente') ? 'has-error' : ''}}">
    <label for="codigocliente" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Convênio' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="codigocliente" type="text" id="codigocliente" value="{{ isset($clinica->codigocliente) ? $clinica->codigocliente : ''}}" >
</div>
    {!! $errors->first('codigocliente', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('multa') ? 'has-error' : ''}}">
    <label for="multa" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Multa' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="multa" type="number" id="multa" value="{{ isset($clinica->multa) ? $clinica->multa : ''}}" >
</div>
    {!! $errors->first('multa', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('juros') ? 'has-error' : ''}}">
    <label for="juros" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Juros' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="juros" type="number" id="juros" value="{{ isset($clinica->juros) ? $clinica->juros : ''}}" >
</div>
    {!! $errors->first('juros', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('instrucao1') ? 'has-error' : ''}}">
    <label for="instrucao1" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Instrução 1' }}</label>
    <div class="col-lg-5 col-md-6">
    <input class="form-control" name="instrucao1" type="text" id="instrucao1" value="{{ isset($clinica->instrucao1) ? $clinica->instrucao1 : ''}}" >
</div>
    {!! $errors->first('instrucao1', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('instrucao2') ? 'has-error' : ''}}">
    <label for="instrucao2" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Instrução 2' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="instrucao2" type="text" id="instrucao2" value="{{ isset($clinica->instrucao2) ? $clinica->instrucao2 : ''}}" >
</div>
    {!! $errors->first('instrucao2', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('instrucao3') ? 'has-error' : ''}}">
    <label for="instrucao3" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Instrução 3' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="instrucao3" type="text" id="instrucao3" value="{{ isset($clinica->instrucao3) ? $clinica->instrucao3 : ''}}" >
</div>
    {!! $errors->first('instrucao3', '<p class="help-block">:message</p>') !!}
</div>

<br/>

<div class="form-group offset-md-5">
    <a  class="btn btn-warning rounded" href="{{ url('/cadastro/clinica') }}" title="Voltar"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show')
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Salvar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>

@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection
