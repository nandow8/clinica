@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Editar TUSS</div>
                <div class="card-body">
                        
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" action="{{ url('/cadastro/tuss/' . $tuss->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        @include ('cadastro.tuss.form', ['formMode' => 'edit'])

                    </form>
                    <a  style="position: relative; bottom: 54px; left: 40%;" href="{{ url('/cadastro/tuss') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                </div>
            </div>
        </div>
    </div>
@endsection
