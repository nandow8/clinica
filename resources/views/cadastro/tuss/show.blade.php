@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar TUSS</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/tuss') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $tuss->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Codigo </th><td> {{ $tuss->codigo }} </td></tr><tr><th> Descricao </th><td> {{ $tuss->descricao }} </td></tr><tr><th> Correlacao </th><td> {{ $tuss->correlacao }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
