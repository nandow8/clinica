@extends('layouts.template')

@section('content')
    <div class="row"> 

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Tuss</div>
                <div class="card-body">
                    <a href="{{ url('/cadastro/tuss/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar TUSS">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar TUSS
                    </a>

                    <form method="GET" action="{{ url('/cadastro/tuss') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th><th>Código</th><th>Descrição</th><th>Sub-Grupo</th><th>Grupo</th><th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($tuss as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->codigo }}</td><td>{{ $item->descricao }}</td>
                                    <td>
                                        @php
                                            $subgrupo = DB::table('tusssubgrupos')
                                                    ->select(DB::raw('descricao'))
                                                    ->where('id', '=',$item->subgrupo_id )
                                                    ->get();
                                            
                                            echo isset($subgrupo) ? $subgrupo[0]->descricao : '--';
                                            
                                        @endphp
                                    </td>
                                    <td>
                                        @php
                                            $grupo = DB::table('tussgrupos')
                                                    ->select(DB::raw('descricao'))
                                                    ->where('id', '=',$item->grupo_id )
                                                    ->get();
                                            
                                            echo isset($grupo) ? $grupo[0]->descricao : '--';
                                            
                                        @endphp
                                    </td>
                                    <td>
                                        <a href="{{ url('/cadastro/tuss/' . $item->id) }}" title="Visualizar TUSS"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/cadastro/tuss/' . $item->id . '/edit') }}" title="Editar TUSS"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
                                        
                                        {{ Form::open(['url'=> '/cadastro/tuss' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                            {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title' =>'Excluir', 'role' => 'button', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $tuss->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')

@endsection
