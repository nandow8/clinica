<div class="form-group row {{ $errors->has('codigo') ? 'has-error' : ''}}">
    <label for="codigo" class="col-sm-2 col-form-label">{{ 'Código' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="codigo" type="text" id="codigo" value="{{ isset($tuss->codigo) ? $tuss->codigo : ''}}" >
</div>
    {!! $errors->first('codigo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-sm-2 col-form-label">{{ 'Descrição' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="descricao" type="text" id="descricao" value="{{ isset($tuss->descricao) ? $tuss->descricao : ''}}" >
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('correlacao') ? 'has-error' : ''}}">
    <label for="correlacao" class="col-sm-2 col-form-label">{{ 'Correlação' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="correlacao" type="text" id="correlacao" value="{{ isset($tuss->correlacao) ? $tuss->correlacao : ''}}" >
</div>
    {!! $errors->first('correlacao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('rol') ? 'has-error' : ''}}">
    <label for="rol" class="col-sm-2 col-form-label">{{ 'Rol' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="rol" type="text" id="rol" value="{{ isset($tuss->rol) ? $tuss->rol : ''}}" >
</div>
    {!! $errors->first('rol', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('subgrupo_id') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Sub-Grupo' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="subgrupo_id" class="form-control" id="subgrupo_id" >
        @foreach ($subgrupos as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($tuss->subgrupo_id) && $tuss->subgrupo_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->descricao }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('grupo_id') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Grupo' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="grupo_id" class="form-control" id="grupo_id" >
        @foreach ($grupos as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($tuss->grupo_id) && $tuss->grupo_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->descricao }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('capitulo_id') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Capítulo' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="capitulo_id" class="form-control" id="capitulo_id" >
        @foreach ($capitulos as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($tuss->capitulo_id) && $tuss->capitulo_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->descricao }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('od') ? 'has-error' : ''}}">
    <label for="od" class="col-sm-2 col-form-label">{{ 'Od' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="od" type="text" id="od" value="{{ isset($tuss->od) ? $tuss->od : ''}}" >
</div>
    {!! $errors->first('od', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('amb') ? 'has-error' : ''}}">
    <label for="amb" class="col-sm-2 col-form-label">{{ 'Amb' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="amb" type="text" id="amb" value="{{ isset($tuss->amb) ? $tuss->amb : ''}}" >
</div>
    {!! $errors->first('amb', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('hco') ? 'has-error' : ''}}">
    <label for="hco" class="col-sm-2 col-form-label">{{ 'Hco' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="hco" type="text" id="hco" value="{{ isset($tuss->hco) ? $tuss->hco : ''}}" >
</div>
    {!! $errors->first('hco', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('hso') ? 'has-error' : ''}}">
    <label for="hso" class="col-sm-2 col-form-label">{{ 'Hso' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="hso" type="text" id="hso" value="{{ isset($tuss->hso) ? $tuss->hso : ''}}" >
</div>
    {!! $errors->first('hso', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('pac') ? 'has-error' : ''}}">
    <label for="pac" class="col-sm-2 col-form-label">{{ 'Pac' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="pac" type="text" id="pac" value="{{ isset($tuss->pac) ? $tuss->pac : ''}}" >
</div>
    {!! $errors->first('pac', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('dut') ? 'has-error' : ''}}">
    <label for="dut" class="col-sm-2 col-form-label">{{ 'Dut' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="dut" type="text" id="dut" value="{{ isset($tuss->dut) ? $tuss->dut : ''}}" >
</div>
    {!! $errors->first('dut', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-sm-2 col-form-label">{{ 'Status' }}</label>
    <div class="col-sm-5">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($tuss->status) && $tuss->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group  offset-md-6">
    @if(request()->route()->getActionMethod() !== 'show') 
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>


@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection
