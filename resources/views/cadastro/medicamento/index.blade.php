@extends('layouts.template')

@section('content')
    <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Medicamento</div>
                    <div class="card-body">
                        <!--<a href="{{ url('/cadastro/medicamento/create') }}" class="btn btn-success btn-sm" title="Add New Medicamento">
                            <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Medicamento
                        </a>-->

                        <form method="GET" action="{{ url('/cadastro/medicamento') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Pesquisa..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>

                        <br/>
                        <br/>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                        <th>Nome</th><th>Apresentação</th><th>Princípio Ativo</th><th>Tipo de Produto</th><th>Classe Terapeutica</th><th>Tipo</th>
                                        <!--<th>Actions</th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($medicamento as $item)
                                    <tr>
                                        <?php
                                          $imagem = '';

                                          if ($item->tarja === 'Tarja Vermelha' && $item->tipodeproduto !== 'Genérico')         { $imagem = 'img/tarja-vermelha.png'; }
                                          elseif ($item->tarja === 'Tarja Preta' && $item->tipodeproduto !== 'Genérico')        { $imagem = 'img/tarja-preta.png'; }
                                          elseif ($item->tarja === 'Venda Livre')                                               { $imagem = 'img/tarja-livre.png'; }
                                          elseif ($item->tarja === 'Tarja Vermelha' && $item->tipodeproduto === 'Genérico' )    { $imagem = 'img/tarja-vermelha-generico.png'; }
                                          elseif ($item->tarja === 'Tarja Preta'  && $item->tipodeproduto === 'Genérico' )      { $imagem = 'img/tarja-preta-generico.png'; }
                                          else                                                                                  { $imagem = 'img/tarja-livre.png'; }

                                          echo '<td>  <img class="rounded-circle" style="width: 40px;" src="', asset($imagem), '" alt="', $item->tarja, '"></td>';
                                        ?>

                                        <td>{{ $item->produto }}</td><td>{{ $item->apresentacao }}</td><td>{{ $item->princicipoativo }}</td><td>{{ $item->tipodeproduto }}</td> <td>{{ $item->classeterapeutica }}</td><td>{{ $item->tipomedicamento }}</td>
                                        <!--<td>
                                            <a href="{{ url('/cadastro/medicamento/' . $item->id) }}" title="Visualizar Medicamento"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                            <a href="{{ url('/cadastro/medicamento/' . $item->id . '/edit') }}" title="Editar Medicamento"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                            {{ Form::open(['url'=> '/cadastro/medicamento' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                                {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title'=>'Excluir' ,'role' => 'button', 'type' => 'submit']) }}
                                            {{ Form::close() }}
                                        </td>-->
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $medicamento->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>

    </div>
@endsection

@section('jqueryscript')

@endsection
