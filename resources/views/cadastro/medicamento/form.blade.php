<div class="form-group {{ $errors->has('princicipoativo') ? 'has-error' : ''}}">
    <label for="princicipoativo" class="control-label">{{ 'Princicipoativo' }}</label>
    <input class="form-control" name="princicipoativo" type="text" id="princicipoativo" value="{{ isset($medicamento->princicipoativo) ? $medicamento->princicipoativo : ''}}" >
    {!! $errors->first('princicipoativo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('laboratorio_id') ? 'has-error' : ''}}">
    <label for="laboratorio_id" class="control-label">{{ 'Laboratorio Id' }}</label>
    <input class="form-control" name="laboratorio_id" type="number" id="laboratorio_id" value="{{ isset($medicamento->laboratorio_id) ? $medicamento->laboratorio_id : ''}}" >
    {!! $errors->first('laboratorio_id', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('codigoggrem') ? 'has-error' : ''}}">
    <label for="codigoggrem" class="control-label">{{ 'Codigoggrem' }}</label>
    <input class="form-control" name="codigoggrem" type="text" id="codigoggrem" value="{{ isset($medicamento->codigoggrem) ? $medicamento->codigoggrem : ''}}" >
    {!! $errors->first('codigoggrem', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('registro') ? 'has-error' : ''}}">
    <label for="registro" class="control-label">{{ 'Registro' }}</label>
    <input class="form-control" name="registro" type="text" id="registro" value="{{ isset($medicamento->registro) ? $medicamento->registro : ''}}" >
    {!! $errors->first('registro', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ean1') ? 'has-error' : ''}}">
    <label for="ean1" class="control-label">{{ 'Ean1' }}</label>
    <input class="form-control" name="ean1" type="text" id="ean1" value="{{ isset($medicamento->ean1) ? $medicamento->ean1 : ''}}" >
    {!! $errors->first('ean1', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ean2') ? 'has-error' : ''}}">
    <label for="ean2" class="control-label">{{ 'Ean2' }}</label>
    <input class="form-control" name="ean2" type="text" id="ean2" value="{{ isset($medicamento->ean2) ? $medicamento->ean2 : ''}}" >
    {!! $errors->first('ean2', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('ean3') ? 'has-error' : ''}}">
    <label for="ean3" class="control-label">{{ 'Ean3' }}</label>
    <input class="form-control" name="ean3" type="text" id="ean3" value="{{ isset($medicamento->ean3) ? $medicamento->ean3 : ''}}" >
    {!! $errors->first('ean3', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('produto') ? 'has-error' : ''}}">
    <label for="produto" class="control-label">{{ 'Produto' }}</label>
    <input class="form-control" name="produto" type="text" id="produto" value="{{ isset($medicamento->produto) ? $medicamento->produto : ''}}" >
    {!! $errors->first('produto', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('apresentacao') ? 'has-error' : ''}}">
    <label for="apresentacao" class="control-label">{{ 'Apresentacao' }}</label>
    <input class="form-control" name="apresentacao" type="text" id="apresentacao" value="{{ isset($medicamento->apresentacao) ? $medicamento->apresentacao : ''}}" >
    {!! $errors->first('apresentacao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('classeterapeutica') ? 'has-error' : ''}}">
    <label for="classeterapeutica" class="control-label">{{ 'Classeterapeutica' }}</label>
    <input class="form-control" name="classeterapeutica" type="text" id="classeterapeutica" value="{{ isset($medicamento->classeterapeutica) ? $medicamento->classeterapeutica : ''}}" >
    {!! $errors->first('classeterapeutica', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tipodeproduto') ? 'has-error' : ''}}">
    <label for="tipodeproduto" class="control-label">{{ 'Tipodeproduto' }}</label>
    <input class="form-control" name="tipodeproduto" type="text" id="tipodeproduto" value="{{ isset($medicamento->tipodeproduto) ? $medicamento->tipodeproduto : ''}}" >
    {!! $errors->first('tipodeproduto', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pfsemimpostos') ? 'has-error' : ''}}">
    <label for="pfsemimpostos" class="control-label">{{ 'Pfsemimpostos' }}</label>
    <input class="form-control" name="pfsemimpostos" type="number" id="pfsemimpostos" value="{{ isset($medicamento->pfsemimpostos) ? $medicamento->pfsemimpostos : ''}}" >
    {!! $errors->first('pfsemimpostos', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf0') ? 'has-error' : ''}}">
    <label for="pf0" class="control-label">{{ 'Pf0' }}</label>
    <input class="form-control" name="pf0" type="number" id="pf0" value="{{ isset($medicamento->pf0) ? $medicamento->pf0 : ''}}" >
    {!! $errors->first('pf0', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf12') ? 'has-error' : ''}}">
    <label for="pf12" class="control-label">{{ 'Pf12' }}</label>
    <input class="form-control" name="pf12" type="number" id="pf12" value="{{ isset($medicamento->pf12) ? $medicamento->pf12 : ''}}" >
    {!! $errors->first('pf12', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf17') ? 'has-error' : ''}}">
    <label for="pf17" class="control-label">{{ 'Pf17' }}</label>
    <input class="form-control" name="pf17" type="number" id="pf17" value="{{ isset($medicamento->pf17) ? $medicamento->pf17 : ''}}" >
    {!! $errors->first('pf17', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf17alc') ? 'has-error' : ''}}">
    <label for="pf17alc" class="control-label">{{ 'Pf17alc' }}</label>
    <input class="form-control" name="pf17alc" type="number" id="pf17alc" value="{{ isset($medicamento->pf17alc) ? $medicamento->pf17alc : ''}}" >
    {!! $errors->first('pf17alc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf175') ? 'has-error' : ''}}">
    <label for="pf175" class="control-label">{{ 'Pf175' }}</label>
    <input class="form-control" name="pf175" type="number" id="pf175" value="{{ isset($medicamento->pf175) ? $medicamento->pf175 : ''}}" >
    {!! $errors->first('pf175', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf175alc') ? 'has-error' : ''}}">
    <label for="pf175alc" class="control-label">{{ 'Pf175alc' }}</label>
    <input class="form-control" name="pf175alc" type="number" id="pf175alc" value="{{ isset($medicamento->pf175alc) ? $medicamento->pf175alc : ''}}" >
    {!! $errors->first('pf175alc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf18') ? 'has-error' : ''}}">
    <label for="pf18" class="control-label">{{ 'Pf18' }}</label>
    <input class="form-control" name="pf18" type="number" id="pf18" value="{{ isset($medicamento->pf18) ? $medicamento->pf18 : ''}}" >
    {!! $errors->first('pf18', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf18alc') ? 'has-error' : ''}}">
    <label for="pf18alc" class="control-label">{{ 'Pf18alc' }}</label>
    <input class="form-control" name="pf18alc" type="number" id="pf18alc" value="{{ isset($medicamento->pf18alc) ? $medicamento->pf18alc : ''}}" >
    {!! $errors->first('pf18alc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pf20') ? 'has-error' : ''}}">
    <label for="pf20" class="control-label">{{ 'Pf20' }}</label>
    <input class="form-control" name="pf20" type="number" id="pf20" value="{{ isset($medicamento->pf20) ? $medicamento->pf20 : ''}}" >
    {!! $errors->first('pf20', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc0') ? 'has-error' : ''}}">
    <label for="pmc0" class="control-label">{{ 'Pmc0' }}</label>
    <input class="form-control" name="pmc0" type="number" id="pmc0" value="{{ isset($medicamento->pmc0) ? $medicamento->pmc0 : ''}}" >
    {!! $errors->first('pmc0', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc12') ? 'has-error' : ''}}">
    <label for="pmc12" class="control-label">{{ 'Pmc12' }}</label>
    <input class="form-control" name="pmc12" type="number" id="pmc12" value="{{ isset($medicamento->pmc12) ? $medicamento->pmc12 : ''}}" >
    {!! $errors->first('pmc12', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc17') ? 'has-error' : ''}}">
    <label for="pmc17" class="control-label">{{ 'Pmc17' }}</label>
    <input class="form-control" name="pmc17" type="number" id="pmc17" value="{{ isset($medicamento->pmc17) ? $medicamento->pmc17 : ''}}" >
    {!! $errors->first('pmc17', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc17alc') ? 'has-error' : ''}}">
    <label for="pmc17alc" class="control-label">{{ 'Pmc17alc' }}</label>
    <input class="form-control" name="pmc17alc" type="number" id="pmc17alc" value="{{ isset($medicamento->pmc17alc) ? $medicamento->pmc17alc : ''}}" >
    {!! $errors->first('pmc17alc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc175') ? 'has-error' : ''}}">
    <label for="pmc175" class="control-label">{{ 'Pmc175' }}</label>
    <input class="form-control" name="pmc175" type="number" id="pmc175" value="{{ isset($medicamento->pmc175) ? $medicamento->pmc175 : ''}}" >
    {!! $errors->first('pmc175', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc175alc') ? 'has-error' : ''}}">
    <label for="pmc175alc" class="control-label">{{ 'Pmc175alc' }}</label>
    <input class="form-control" name="pmc175alc" type="number" id="pmc175alc" value="{{ isset($medicamento->pmc175alc) ? $medicamento->pmc175alc : ''}}" >
    {!! $errors->first('pmc175alc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc18') ? 'has-error' : ''}}">
    <label for="pmc18" class="control-label">{{ 'Pmc18' }}</label>
    <input class="form-control" name="pmc18" type="number" id="pmc18" value="{{ isset($medicamento->pmc18) ? $medicamento->pmc18 : ''}}" >
    {!! $errors->first('pmc18', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc18alc') ? 'has-error' : ''}}">
    <label for="pmc18alc" class="control-label">{{ 'Pmc18alc' }}</label>
    <input class="form-control" name="pmc18alc" type="number" id="pmc18alc" value="{{ isset($medicamento->pmc18alc) ? $medicamento->pmc18alc : ''}}" >
    {!! $errors->first('pmc18alc', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('pmc20') ? 'has-error' : ''}}">
    <label for="pmc20" class="control-label">{{ 'Pmc20' }}</label>
    <input class="form-control" name="pmc20" type="number" id="pmc20" value="{{ isset($medicamento->pmc20) ? $medicamento->pmc20 : ''}}" >
    {!! $errors->first('pmc20', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('restricaohospitalar') ? 'has-error' : ''}}">
    <label for="restricaohospitalar" class="control-label">{{ 'Restricaohospitalar' }}</label>
    <input class="form-control" name="restricaohospitalar" type="text" id="restricaohospitalar" value="{{ isset($medicamento->restricaohospitalar) ? $medicamento->restricaohospitalar : ''}}" >
    {!! $errors->first('restricaohospitalar', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('cap') ? 'has-error' : ''}}">
    <label for="cap" class="control-label">{{ 'Cap' }}</label>
    <input class="form-control" name="cap" type="text" id="cap" value="{{ isset($medicamento->cap) ? $medicamento->cap : ''}}" >
    {!! $errors->first('cap', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('confaz87') ? 'has-error' : ''}}">
    <label for="confaz87" class="control-label">{{ 'Confaz87' }}</label>
    <input class="form-control" name="confaz87" type="text" id="confaz87" value="{{ isset($medicamento->confaz87) ? $medicamento->confaz87 : ''}}" >
    {!! $errors->first('confaz87', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('analiserecursal') ? 'has-error' : ''}}">
    <label for="analiserecursal" class="control-label">{{ 'Analiserecursal' }}</label>
    <input class="form-control" name="analiserecursal" type="text" id="analiserecursal" value="{{ isset($medicamento->analiserecursal) ? $medicamento->analiserecursal : ''}}" >
    {!! $errors->first('analiserecursal', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('listadeconcessaodecreditotributario') ? 'has-error' : ''}}">
    <label for="listadeconcessaodecreditotributario" class="control-label">{{ 'Listadeconcessaodecreditotributario' }}</label>
    <input class="form-control" name="listadeconcessaodecreditotributario" type="text" id="listadeconcessaodecreditotributario" value="{{ isset($medicamento->listadeconcessaodecreditotributario) ? $medicamento->listadeconcessaodecreditotributario : ''}}" >
    {!! $errors->first('listadeconcessaodecreditotributario', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('comercializacao2017') ? 'has-error' : ''}}">
    <label for="comercializacao2017" class="control-label">{{ 'Comercializacao2017' }}</label>
    <input class="form-control" name="comercializacao2017" type="text" id="comercializacao2017" value="{{ isset($medicamento->comercializacao2017) ? $medicamento->comercializacao2017 : ''}}" >
    {!! $errors->first('comercializacao2017', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tarja') ? 'has-error' : ''}}">
    <label for="tarja" class="control-label">{{ 'Tarja' }}</label>
    <select name="tarja" class="form-control" id="tarja" >
    @foreach (json_decode('{"": "N/I", "Tarja Vermelha": "Tarja Vermelha", "Tarja Preta": "Tarja Preta","Venda Livre": "Venda Livre"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($medicamento->tarja) && $medicamento->tarja == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('tarja', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('tipomedicamento') ? 'has-error' : ''}}">
    <label for="tipomedicamento" class="control-label">{{ 'Tipomedicamento' }}</label>
    <input class="form-control" name="tipomedicamento" type="text" id="tipomedicamento" value="{{ isset($medicamento->tipomedicamento) ? $medicamento->tipomedicamento : ''}}" >
    {!! $errors->first('tipomedicamento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($medicamento->status) && $medicamento->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
