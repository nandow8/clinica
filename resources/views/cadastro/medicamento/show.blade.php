@extends('layouts.template')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Medicamento {{ $medicamento->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/cadastro/medicamento') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/cadastro/medicamento/' . $medicamento->id . '/edit') }}" title="Edit Medicamento"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('cadastro/medicamento' . '/' . $medicamento->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Medicamento" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $medicamento->id }}</td>
                                    </tr>
                                    <tr><th> Princicipoativo </th><td> {{ $medicamento->princicipoativo }} </td></tr><tr><th> Laboratorio Id </th><td> {{ $medicamento->laboratorio_id }} </td></tr><tr><th> Codigoggrem </th><td> {{ $medicamento->codigoggrem }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
