@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Fornecedor</div>
                <div class="card-body">

                    <a href="{{ url('/cadastro/fornecedor') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $fornecedor->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Razao </th><td> {{ $fornecedor->razao }} </td></tr><tr><th> Fantasia </th><td> {{ $fornecedor->fantasia }} </td></tr><tr><th> Cpfcnpj </th><td> {{ $fornecedor->cpfcnpj }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
