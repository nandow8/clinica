<div class="form-group row {{ $errors->has('razao') ? 'has-error' : ''}}">
    <label for="razao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Razão' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="razao" type="text" id="razao" required value="{{ isset($fornecedor->razao) ? $fornecedor->razao : ''}}" >
</div>
    {!! $errors->first('razao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('fantasia') ? 'has-error' : ''}}">
    <label for="fantasia" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Fantasia' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="fantasia" type="text" id="fantasia" value="{{ isset($fornecedor->fantasia) ? $fornecedor->fantasia : ''}}" >
</div>
    {!! $errors->first('fantasia', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cpfcnpj') ? 'has-error' : ''}}">
    <label for="cpfcnpj" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CPF/CNPJ' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cpfcnpj" type="text" id="cpfcnpj" required value="{{ isset($fornecedor->cpfcnpj) ? $fornecedor->cpfcnpj : ''}}" >
</div>
    {!! $errors->first('cpfcnpj', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cep') ? 'has-error' : ''}}">
    <label for="cep" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'CEP' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cep" type="text" id="cep" required value="{{ isset($fornecedor->cep) ? $fornecedor->cep : ''}}" >
    <div class="alert alert-warning errorcep" style="padding:.1rem; text-align:center; margin-top:5px; display:none;"></div>
</div>
    {!! $errors->first('cep', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('endereco') ? 'has-error' : ''}}">
    <label for="endereco" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Endereço' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="endereco" type="text" id="endereco" value="{{ isset($fornecedor->endereco) ? $fornecedor->endereco : ''}}" >
</div>
    {!! $errors->first('endereco', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Número' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="numero" type="text" id="numero" value="{{ isset($fornecedor->numero) ? $fornecedor->numero : ''}}" >
</div>
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('complemento') ? 'has-error' : ''}}">
    <label for="complemento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Complemento' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="complemento" type="text" id="complemento" value="{{ isset($fornecedor->complemento) ? $fornecedor->complemento : ''}}" >
</div>
    {!! $errors->first('complemento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('bairro') ? 'has-error' : ''}}">
    <label for="bairro" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Bairro' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="bairro" type="text" id="bairro" value="{{ isset($fornecedor->bairro) ? $fornecedor->bairro : ''}}" >
</div>
    {!! $errors->first('bairro', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('cidade') ? 'has-error' : ''}}">
    <label for="cidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Cidade' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="cidade" type="text" id="cidade" value="{{ isset($fornecedor->cidade) ? $fornecedor->cidade : ''}}" >
</div>
    {!! $errors->first('cidade', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('uf') ? 'has-error' : ''}}">
    <label for="uf" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'UF' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="uf" type="text" id="uf" value="{{ isset($fornecedor->uf) ? $fornecedor->uf : ''}}" >
</div>
    {!! $errors->first('uf', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('telefone') ? 'has-error' : ''}}">
    <label for="telefone" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Telefone' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="telefone" type="text" id="telefone" value="{{ isset($fornecedor->telefone) ? $fornecedor->telefone : ''}}" >
</div>
    {!! $errors->first('telefone', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('celular') ? 'has-error' : ''}}">
    <label for="celular" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Celular' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="celular" type="text" id="celular" value="{{ isset($fornecedor->celular) ? $fornecedor->celular : ''}}" >
</div>
    {!! $errors->first('celular', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('ie') ? 'has-error' : ''}}">
    <label for="ie" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'I.E.' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="ie" type="text" id="ie" value="{{ isset($fornecedor->ie) ? $fornecedor->ie : ''}}" >
</div>
    {!! $errors->first('ie', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('im') ? 'has-error' : ''}}">
    <label for="im" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'I.M' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="im" type="text" id="im" value="{{ isset($fornecedor->im) ? $fornecedor->im : ''}}" >
</div>
    {!! $errors->first('im', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'E-mail' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($fornecedor->email) ? $fornecedor->email : ''}}" >
</div>
    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('contato') ? 'has-error' : ''}}">
    <label for="contato" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Contato' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="contato" type="text" id="contato" value="{{ isset($fornecedor->contato) ? $fornecedor->contato : ''}}" >
</div>
    {!! $errors->first('contato', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" required >
        @foreach (json_decode('{"Ativo": "Ativo", "Inativo": "Inativo"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($fornecedor->status) && $fornecedor->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>

<div class="form-group  offset-md-5">
    <a href="{{ url('/cadastro/fornecedor') }}" class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>

    @if(request()->route()->getActionMethod() !== 'show')
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Salvar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>

@section('jqueryscript')
    <script type="text/javascript" src="{{asset('js/scripts/base.js')}}"></script>
@endsection
