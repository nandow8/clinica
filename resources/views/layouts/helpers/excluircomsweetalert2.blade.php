<script>
    $(document).on('submit', '.form-delete', function(e){
        var form = this;
        e.preventDefault();

        const swalWithBootstrapButtons = Swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        })

        swalWithBootstrapButtons({ 
            title: "Deletar registro? ",
            // text: "Deseja realmente excluir ",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Não',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                form.submit()
            }  
        })
    })
</script>