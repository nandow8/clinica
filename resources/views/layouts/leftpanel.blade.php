<aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <!-- futuramente adicionar logo <a class="navbar-brand" href="./"><img src="{{ asset('img/logo.png') }}" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="{{ asset('img/logo2.png') }}" alt="Logo"></a> -->
                <a class="navbar-brand" href="/home">SAEGS  <img src="/img/logos/saegs.png" class="logo-saegs"></a>
                <a class="navbar-brand hidden" href="/home"><img src="/img/logos/saegs.png" class="logo-saegs"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/home"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <!-- <h3 class="menu-title">UI elements</h3> /.menu-title -->
                    @role('menuadmin')
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-user-circle-o fa-2"></i>Admin</a>
                        <ul class="sub-menu children dropdown-menu">
                        <!-- <li><i class="fa fa-id-card fa-2" style="color: lightgreen;"></i><a href="{{ url('admin/tabelasparapopularperfil') }}">Cadastro de tabelas</a></li> -->
                        <!-- <li><i class="fa fa-id-card fa-2" style="color: lightgreen;"></i><a href="{{ url('admin/perfil') }}">Perfil</a></li> -->
                        @role('trusts')
                          <li><i class="fa fa-pencil-square-o fa-2" style="color: lightgreen;"></i><a href="{{ url('/trust/roles') }}">Regras de Acessos</a></li>
                        @endrole
                        @role('usuarios')
                          <li><i class="fa fa-user-plus fa-2" style="color: lightgreen;"></i><a href="{{ url('admin/usuarios') }}">Cadastro de Usuários</a></li>
                        @endrole
                        </ul>
                    </li>
                    @endrole
                    @role('menucadastros')
                    <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-pencil-square fa-2"></i>Cadastros</a>
                        <ul class="sub-menu children dropdown-menu">
                          @role('fornecedores')
                            <li><i class="fa fa-cubes fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/fornecedor') }}"> Fornecedores</a></li>
                          @endrole
                          @role('clinicas')
                            <li><i class="fa fa-hospital-o fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/clinica') }}">Clínicas</a></li>
                          @endrole
                          @role('funcionarios')
                            <li><i class="fa fa-users fa-2" style="color:lightgreen"></i><a href="{{ url('funcionarios/funcionarios') }}">Funcionários</a></li>
                          @endrole
                          @role('especialidades')
                            <li><i class="fa fa-flag fa-2" style="color: lightgreen;"></i><a href="{{ url('especialidades/especialidades') }}">Especialidades Médicas</a></li>
                          @endrole
                          @role('tabelaservico')
                            <li><i class="fa fa-briefcase fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/tabela-servico') }}">Tabela de Serviços</a></li>
                          @endrole
                          @role('cid')
                            <li><i class="fa fa-list fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/cid') }}">CID-10</a></li>
                          @endrole
                          @role('tuss')
                            <li><i class="fa fa-medkit fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/tuss') }}">Tuss</a></li>
                          @endrole
                          @role('salas')
                            <li><i class="fa fa-building-o fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/salas') }}">Salas</a></li>
                          @endrole
                          @role('pacientesconvenio')
                            <li><i class="fa fa-book fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/pacientesconvenio') }}">Convênios</a></li>
                          @endrole
                          @role('planos')
                            <li><i class="fa fa-book fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/planos') }}">Planos</a></li>
                          @endrole
                          @role('medicamentos')
                            <li><i class="fa fa-star fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/medicamento') }}">Medicamentos</a></li>
                          @endrole
                          @role('fabricantes')
                            <li><i class="fa fa-flask fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/fabricante') }}">Laboratórios</a></li>
                          @endrole
                          @role('modelosdocumentos')
                            <li><i class="fa fa-leanpub fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/modelos-documentos') }}">&nbsp;Modelos Documentos</a></li>
                          @endrole
                          @role('exameslaboratorios')
                            <li><i class="fa fa-mixcloud fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/examesrf') }}">&nbsp;Exames</a></li>
                          @endrole
                          @role('tabeladescontoacrescimo')
                            <li><i class="fa fa-linode fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/tabela-desconto-acrescimo') }}">&nbsp;Descontos e Acréscimos</a></li>
                            <li><i class="fa fa-transgender-alt fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/grupoempresarial') }}">&nbsp;Grupos Empresariais</a></li>
                            <li><i class="fa fa-credit-card fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/tipos-pagamentos') }}">&nbsp;Tipos de Pagamentos</a></li>
                            <li><i class="fa fa-university fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/bancos') }}">&nbsp;Bancos</a></li>
                            <li><i class="fa fa-list-alt fa-2" style="color: lightgreen;"></i><a href="{{ url('cadastro/despesas') }}">&nbsp;Despesas</a></li>
                          @endrole
                        </ul>
                    </li>
                    @endrole
                    @role('menufinanceiros')
                      <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-money fa-2"></i>Financeiro</a>
                        <ul class="sub-menu children dropdown-menu">
                          @role('contaspagar')
                            <li><i class="fa fa-exclamation-triangle fa-2" style="color: lightgreen;"></i><a href="{{ url('financeiro/contaspagar') }}">Contas a Pagar</a></li>
                          @endrole
                          @role('contasreceber')
                            <li><i class="fa fa-list fa-2" style="color: lightgreen;"></i><a href="{{ url('financeiro/orcamento') }}">Orçamentos</a></li>
                            <li><i class="fa fa-usd fa-2" style="color: lightgreen;"></i><a href="{{ url('financeiro/contasreceber') }}">Contas a Receber</a></li>
                          @endrole
                          @role('contratos')
                            <li><i class="fa fa-wpforms fa-2" style="color: lightgreen;"></i><a href="{{ url('financeiro/contrato') }}">Contratos</a></li>
                          @endrole
                          @role('fluxocaixa')
                            <li><i class="fa fa-table fa-2" style="color: lightgreen;"></i><a href="{{ url('financeiro/fluxocaixa') }}">Fluxo de Caixa</a></li>
                          @endrole
                          @role('recibos')
                            <li><i class="fa fa-file-text-o fa-2" style="color: lightgreen;"></i><a href="{{ url('financeiro/recibo') }}">Recibos</a></li>
                          @endrole
                        </ul>
                      </li>
                    @endrole
                    @role('menumedicos')
                      <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-address-card-o fa-2"></i>Médicos</a>
                        <ul class="sub-menu children dropdown-menu">
                            @role('medicomodeloatestado')
                            <li><i class="fa fa-file-text-o fa-2" style="color: lightgreen;"></i><a href="{{ url('medicos/medicomodeloatestado') }}">Modelo dos Atestados</a></li>
                            @endrole
                            @role('medicofichaclinica')
                            <li><i class="fa fa-book fa-2" style="color: lightgreen;"></i><a href="{{ url('medicos/medicofichaclinica') }}">Ficha Clínica</a></li>
                            @endrole
                        </ul>
                      </li>
                    @endrole
                    @role('menupacientes')
                      <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-users fa-2"></i>Pacientes</a>
                        <ul class="sub-menu children dropdown-menu">
                          @role('pacientes')
                            <li><i class="fa fa-user-plus fa-2" style="color: lightgreen;"></i><a href="{{ url('usuarios/pacientes') }}">Registros</a></li>
                          @endrole
                        </ul>
                      </li>
                    @endrole
                    @role('menuagendamentos')
                      <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book fa-2"></i>Agendamentos</a>
                        <ul class="sub-menu children dropdown-menu">
                          @role('agendas')
                            <li><i class="fa fa-2 fa-calendar" style="color: lightgreen;"></i><a href="{{ url('eventos') }}">Agenda</a></li>
                          @endrole
                          @role('preagendamentos')
                            <li><i class="fa fa-2 fa-list-alt" style="color: lightgreen;" aria-hidden="true"></i><a href="{{ url('preagendamento') }}">Pré Agendamento</a></li>
                          @endrole
                        </ul>
                      </li>
                    @endrole
                    @role('menurelatorios')
                      <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-book fa-2"></i>Relatórios</a>
                        <ul class="sub-menu children dropdown-menu">
                          @role('localizacao')
                            <li><i class="fa fa-2 fa-calendar" style="color: lightgreen;"></i><a href="{{ url('mapa') }}">Localização</a></li>
                            <li><i class="fa fa-2 fa-usd" style="color: lightgreen;"></i><a href="{{ url('relatorios/contasreceberrel') }}">Contas a Receber</a></li>
                          @endrole
                        </ul>
                      </li>
                    @endrole
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>
