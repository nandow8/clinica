<div class="form-group row {{ $errors->has('titulo') ? 'has-error' : ''}}">
    <label for="titulo" class="col-sm-2 col-form-label">{{ 'Titulo' }}</label>
    <div class="col-sm-5">
    <input class="form-control" name="titulo" type="text" id="titulo" value="{{ isset($especialidade->titulo) ? $especialidade->titulo : ''}}" required>
</div>
    {!! $errors->first('titulo', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-sm-2 col-form-label">{{ 'Descricao' }}</label>
    <div class="col-sm-5">
    <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" required>{{ isset($especialidade->descricao) ? $especialidade->descricao : ''}}</textarea>
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group  offset-md-6">
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
</div>
