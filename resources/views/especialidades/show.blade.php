@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Especialidade</div>
                <div class="card-body">

                    <a href="{{ url('/especialidades/especialidades') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $especialidade->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Titulo </th><td> {{ $especialidade->titulo }} </td></tr><tr><th> Descricao </th><td> {{ $especialidade->descricao }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
