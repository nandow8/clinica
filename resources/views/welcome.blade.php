<!DOCTYPE html>
<html lang="pt-br">

  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="CAP - Centro de Atendimento Popular">
    <meta name="author" content="">
    
    <TITLE>CAP - Centro de Atendimento Popular</TITLE>
    <META NAME="DESCRIPTION" CONTENT="Clínica médica para atendimento popular na cidade de Itaquaquecetuba. Médicos preparados e alta qualidade nos nossos serviços">
    <META NAME="ABSTRACT" CONTENT="Clínica médica para atendimento popular na cidade de Itaquaquecetuba">
    <META NAME="KEYWORDS" CONTENT="clínica médica em itaquá, médicos, saúde, médicos preparados, saúde popular, centro de atendimento popular">
    <META NAME="RATING" CONTENT="general">
    <META NAME="DISTRIBUTION" CONTENT="local">
    <META NAME="LANGUAGE" CONTENT="PT">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset ('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Jquery UI CSS -->
    <link href="{{ asset ('vendor/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">

    <!-- Custom fonts for this template -->
    <link href="{{ asset ('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Custom styles for this template -->
    <link href="{{ asset ('css/frontend/agency.min.css') }}" rel="stylesheet">

  </head>

  <body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="img/logos/logomini.png" class="img-responsible" id="caplogo"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fas fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav text-uppercase ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#services">Serviços</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#agendamento">Agende uma consulta</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#portfolio">Portfólio</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#about">Sobre nós</a>
            </li>
            <!-- <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#team">Team</a>
            </li> -->
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contato</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    <!-- Header -->
    <header class="masthead">
      <div class="container">
        <div class="intro-text">
          <div class="intro-lead-in">Bem vindo ao CAP - Centro de Atendimento Popular</div>
          <!-- <div class="intro-heading text-uppercase">É um prazer recebê-lo</div> -->
          <a class="btn btn-green btn-md text-uppercase js-scroll-trigger" href="#services">Saiba mais</a>
        </div>
      </div>
    </header>

    <!-- Services -->
    <section id="services">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Serviços</h2>
            <h3 class="section-subheading text-muted">Confira alguns dos nossos serviços</h3>
          </div>
        </div>
        <div class="row text-center">
          <div class="col-md-4 each-service">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <!-- <i class="fas fa-shopping-cart fa-stack-1x fa-inverse"></i> -->
              <i class="fas fa-calendar-alt fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Agendamento</h4>
            <p class="text-muted">
              <div class="service-title">Agende ainda hoje! Sim, na CAP é possível.</div>
              Faça seu agendamento online agora mesmo.
            </p>
          </div>
          <div class="col-md-4 each-service">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <!-- <i class="fas fa-laptop fa-stack-1x fa-inverse"></i> -->
              <i class="fas fa-stethoscope fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Médicos</h4>
            <p class="text-muted">
              <div class="service-title">Médicos de confiança.</div>
              Nossa equipe de profissionais é escolhida a dedo para cuidar de você e da sua família.
            </p>
          </div>
          <div class="col-md-4 each-service">
            <span class="fa-stack fa-4x">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <!-- <i class="fas fa-lock fa-stack-1x fa-inverse"></i> -->
              <i class="fas fa-handshake fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">Qualidade</h4>
            <p class="text-muted">
              <div class="service-title">Milhares de pessoas já se surpreenderam com nossa qualidade e atendimento.</div>
              Venha cuidar da sua saúde no Centro de Atendimento Popular!
            </p>
          </div>
        </div>
      </div>
    </section>

    <section id="agendamento">
       <div class="container">       
          <div class="row">
              <div class="col-lg-8 col-md-12 offset-lg-2">
                  <h3 class="section-header">Pré agende uma consulta ou solicite um orçamento de exame</h3>
                  <form method="POST" id="agendaForm" name="agendamento" action="{{url('/preagendamento')}}">
                      {{csrf_field()}}
                      <div class="agendamento__radio-group" style="float: left">
                        <input type="radio" id="agendamento-tipo-consulta" value="consulta" name="opcao" class="agendamento__radio" data-vv-id="1" aria-required="true" aria-invalid="false" onclick="changeOption(this)" checked> 
                        <span class="tipocontato-titulo">Pré-agendar consulta</span>
                        <label for="agendamento-tipo-consulta" class="agendamento__label-radio">
                          <span class="agendamento__radio-button" name="agendamento-tipo-consulta"></span> 
                        </label> 
                      </div>
                      <div class="agendamento__radio-group">
                        <input type="radio" id="agendamento-tipo-exame" value="exame" name="opcao" class="agendamento__radio" onclick="changeOption(this)"> 
                        <span class="tipocontato-titulo">Orçamento de exame</span>
                        <label for="agendamento-tipo-exame" class="agendamento__label-radio">
                        <span class="agendamento__radio-button" name="agendamento-tipo-exame"></span> </label>
                      </div>
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group">
                              <label class="label-form" for="nome"><i class="fas fa-user-alt"></i> Nome <span required>(*)</span></label>
                              <input type="text" name="nome" id="nome" class="form-control" placeholder="Digite o seu nome" required/>
                           </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                               <label class="label-form" for="telefone"><i class="fas fa-phone-square"></i> Telefone <span required>(*)</span></label>
                               <input type="text" name="telefone" id="telefone" class="form-control" placeholder="Telefone para contato" required/>
                            </div>
                        </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="label-form" for="email"><i class="fas fa-at"></i> Email</label>
                          <input type="text" name="email" id="email" class="form-control" placeholder="Email - não obrigatório"/>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="label-form" for="cpf"><i class="fas fa-id-card"></i> CPF</label>
                          <input type="text" name="cpf" id="cpf" class="form-control" placeholder="Digite o CPF" data-mask="000.000.000-00">
                        </div>
                      </div>
                      <div class="col-lg-6" id="box-dataconsulta">								
                        <div class="form-group">
                          <label class="label-form" for="dataconsulta"><i class="fas fa-calendar-day"></i> Data</label>
                          <input type="text" name="dataconsulta" id="dataconsulta" class="form-control" placeholder="Data da consulta" autocomplete="off"/>
                        </div>                           
                      </div>
                      <div class="col-lg-6" id="box-exame" style="display:none;">
                        <div class="form-group">
                          <label class="label-form" for="exame"><i class="fas fa-clipboard-check"></i> Qual exame?</label>
                          <input type="text" name="exame" id="exame" class="form-control" placeholder="Exame"/>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label class="label-form" for="horario"><i class="fas fa-clock"></i> Horário de preferência</label>
                          <select name="horario" id="horario" class="form-control" placeholder="Horário">
                            <option value="">Selecione</option>
                            <option value="Entre 7h e 9h">Entre 7h e 9h</option>
                            <option value="Entre 9h e 11h">Entre 9h e 11h</option>
                            <option value="Entre 11h e 13h">Entre 11h e 13h</option>
                            <option value="Entre 13h e 15h">Entre 13h e 15h</option>
                            <option value="Entre 15h e 17h">Entre 15h e 17h</option>
                            <option value="Entre 17h e 19h">Entre 17h e 19h</option>
                            <option value="Entre 19h e 21h">Entre 19h e 21h</option>
                            <option value="Entre 21h e 23h">Entre 21h e 23h</option>
                          </select>
                        </div>
                      </div>						
                      <div class="col-lg-6">
                        <div class="form-group">
                          <button class="btn btn-primary" id="btn-send"><i class="fas fa-check"></i> Enviar</button>
                        </div>
                      </div>
                      <div class="col-lg-12"><span style="font-style:italic; color:#333; font-size:.7em;">(*) Campos obrigatórios</span></div>
                      <div class="col-lg-12" id="boxerror">
                          <span id="errormensagem" class="alert alert-danger"></span>
                      </div>
                      <div class="col-lg-12" id="boxsucesso">
                        <span id="sucessomensagem" class="alert alert-success"></span>
                      </div>
                  </form>
              </div>
          </div>
       </div>
    </section>
  
    <!-- Sobre -->
    <section id="about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Sobre nós</h2>
            <h3 class="section-subheading text-muted"></h3>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <ul class="timeline">
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/logos/logocap.jpg" alt="CAP" style="margin-left: .5px;">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 class="subheading">Sobre o CAP</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">
                    Somos uma clínica médica que surgiu com o objetivo de oferecer soluções completas de saúde com eficiência, qualidade, rapidez e baixo custo.
                    Para nós, saúde vai muito além dos serviços que oferecemos. 
                    Por isso, temos o compromisso de pensar na saúde de nossos pacientes desde o agendamento prático, até na localização de fácil acesso da clínica, nas formas de pagamento e no ambiente agradável e acolhedor. 
                    Não nos conformamos se algo não estiver da melhor forma para o paciente. Aqui nós fazemos acontecer. 
                    Pensamos em  soluções inovadoras, trabalhamos em equipe, com ética e foco em resultados.
                    </p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/logos/iconemissao.jpg" alt="Missão">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">                   
                    <h4 class="subheading">Missão</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Encantar com nosso cuidado, oferecendo bem estar, conforto e confiança. Saúde pra gente é isso.</p>
                  </div>
                </div>
              </li>
              <li>
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/logos/iconevisao.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">                   
                    <h4 class="subheading">Visão</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Ser a maior e melhor solução em saúde de forma sustentável e inovadora, encantando cada vez mais pessoas.</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <img class="rounded-circle img-fluid" src="img/logos/iconevalores.jpg" alt="">
                </div>
                <div class="timeline-panel">
                  <div class="timeline-heading">
                    <h4 class="subheading">Valores</h4>
                  </div>
                  <div class="timeline-body">
                    <p class="text-muted">Acolhimento. Comprometimento. Confiança. Espírito de dono. Ética. Fazer acontecer. Foco em resultado. Inconformidade. Inovação. Liderança pelo exemplo. Trabalho em equipe</p>
                  </div>
                </div>
              </li>
              <li class="timeline-inverted">
                <div class="timeline-image">
                  <h4>Faça parte
                    <br>da nossa
                    <br>história!</h4>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
   
    <!-- Parceiros/Clientes -->
    <section class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-3 col-sm-6">
            <a href="http://saegs.com.br">
              <img class="img-fluid d-block mx-auto" src="img/logos/saegsmini.png" alt="Saegs" style="width:60%">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="http://murillusmed.blogspot.com/">
              <img class="img-fluid d-block mx-auto" src="img/logos/murillusmed.png" alt="Murillus Med" style="width:100%">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="http://colegiotecnicopaulista.com.br">
              <img class="img-fluid d-block mx-auto" src="img/logos/ctp.png" alt="Centro Técnico Paulista" style="width:80%">
            </a>
          </div>
          <div class="col-md-3 col-sm-6">
            <a href="http://enfermedy.com.br">
              <img class="img-fluid d-block mx-auto" src="img/logos/enfermedy.png" alt="">
            </a>
          </div>
        </div>
      </div>
    </section>

    <!-- Contato -->
    <section id="contact">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 text-center">
            <h2 class="section-heading text-uppercase">Contate-nos</h2>
            <h3 class="section-subheading text-muted">Ficou com dúvida? Mande-nos uma mensagem</h3>
            <i class="section-subheading text-muted">Nossa equipe responderá o mais rápido possível</i>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
            <form id="contactForm" name="sentMessage" novalidate="novalidate" action="post">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input class="form-control" id="nome" name="nome" type="text" placeholder="Nome *" required="required" data-validation-required-message="Por favor informe seu nome">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="email" name="email" type="email" placeholder="Email *" required="required" data-validation-required-message="Por favor informe o seu email">
                    <p class="help-block text-danger"></p>
                  </div>
                  <div class="form-group">
                    <input class="form-control" id="phone" name="phone" type="tel" placeholder="Telefone *" required="required" data-validation-required-message="Por favor informe um telefone">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
					<textarea class="form-control" id="message" name="message" placeholder="Mensagem *" required="required" data-validation-required-message="Por favor insira a mensagem"></textarea>
					<p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-lg-12 text-center">
                  <div id="success"></div>
                  <div class="alert alert-danger" id="errormessage"></div>
                  <button id="sendMessageButton" class="btn btn-green btn-md text-uppercase" type="submit">Enviar</button>
                </div>
              </div>
			</form>

			<div class="row">
				<div class="boxcontato">
					<span><b>Endereço:</b> Rua Lavras, 99 - Vila Zeferina - 08576-060 - Itaquaquecetuba - São Paulo</span><br/>
					<span><b>Fone: </b> (11) 4753 - 2787 / (11) 4753 - 4524</span>
				</div>
			</div>

          </div>
        </div>
	  </div>
	  
    </section>
    
    <!-- Footer -->
    <footer>
      <div class="container">
        <div class="row">          
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item col-lg-6 col-md-4 col-sm-6">
                <a href="http://saegs.com.br"><img src="img/logos/saegsmini.png" class="img-responsible logosaegs" alt="Saegs Sistemas"></a>
              </li>
              <li class="list-inline-item col-lg-4 col-md-4 col-sm-6">
                <a href="http://fjmx.com.br/"><img src="img/logos/fjmx.png" class="img-responsible logofjmx" alt="FJMX"></a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="#" id="logotwitter">
                  <i class="fab fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#" id="logofacebook">
                  <i class="fab fa-facebook-f"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#" id="logolinkedin">
                  <i class="fab fa-linkedin-in"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <span class="copyright">Copyright &copy; CAP {{ date('Y')}}</span>
          </div>        
        </div>
      </div>
    </footer>

    <!-- Portfolio Modals -->
    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- Plugin JavaScript -->
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

    <!-- Plugin JQuery UI -->
    <script src="{{ asset('vendor/jquery-ui/jquery-ui.min.js') }}"></script>

    <!-- Contact form JavaScript -->
    <script src="{{ asset('js/frontend/jqBootstrapValidation.js') }}"></script>
    <script src="{{ asset('js/frontend/agency.min.js') }}"></script>
    <script>const baseUrl = {!! json_encode(url('/')) !!}</script>    

    <script src="{{ asset('js/frontend/eventos.js') }}"></script>
    <script src="{{ asset('js/frontend/jquery-mask.js') }}"></script>

  {{ TawkTo::widgetCode() }}
  </body>

</html>
