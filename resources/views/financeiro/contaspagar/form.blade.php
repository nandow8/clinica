<div class="form-group row {{ $errors->has('fornecedor_id') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Fornecedor (*)' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <select name="fornecedor_id" class="form-control" id="fornecedor_id" >
            @foreach ($fornecedores as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($contaspagar->fornecedor_id) && $contaspagar->fornecedor_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->fantasia }}</option>
            @endforeach
        </select>
    </div>
    {!! $errors->first('fornecedor_id', '<p class="help-block">:message</p>') !!}
</div>
<!-- <div class="form-group row {{ $errors->has('fornecedor') ? 'has-error' : ''}}">
    <label for="fornecedor" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Fornecedor' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="fornecedor" type="text" id="fornecedor" value="{{ isset($contaspagar->fornecedor) ? $contaspagar->fornecedor : ''}}" >
</div>
    {!! $errors->first('fornecedor', '<p class="help-block">:message</p>') !!}
</div> -->
<div class="form-group row {{ $errors->has('vencimento') ? 'has-error' : ''}}">
    <label for="vencimento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Vencimento' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="vencimento" type="date" id="vencimento" value="{{ isset($contaspagar->vencimento) ? $contaspagar->vencimento : ''}}" >
</div>
    {!! $errors->first('vencimento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('pagamento') ? 'has-error' : ''}}">
    <label for="pagamento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Pagamento' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="pagamento" type="date" id="pagamento" value="{{ isset($contaspagar->pagamento) ? $contaspagar->pagamento : ''}}" >
</div>
    {!! $errors->first('pagamento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('valor') ? 'has-error' : ''}}">
    <label for="valor" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Valor' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="valor" type="number" step="any" id="valor" value="{{ isset($contaspagar->valor) ? $contaspagar->valor : ''}}" >
</div>
    {!! $errors->first('valor', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('numerodocumento') ? 'has-error' : ''}}">
    <label for="numerodocumento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Número documento' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="numerodocumento" type="number" id="numerodocumento" value="{{ isset($contaspagar->numerodocumento) ? $contaspagar->numerodocumento : ''}}" >
</div>
    {!! $errors->first('numerodocumento', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}">
    <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Número' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <input class="form-control" name="numero" type="number" id="numero" value="{{ isset($contaspagar->numero) ? $contaspagar->numero : ''}}" >
</div>
    {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Descrição' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" >{{ isset($contaspagar->descricao) ? $contaspagar->descricao : ''}}</textarea>
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>
<div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <select name="status" class="form-control" id="status" >
        @foreach (json_decode('{"Aberto": "Em Aberto", "Pago": "Pago", "Atraso": "Em Atraso"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($contaspagar->status) && $contaspagar->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
</div>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
</div>

<br/>

<div class="form-group  offset-md-5">
    <a href="{{ url('/financeiro/contaspagar') }}" title="Voltar" class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    @if(request()->route()->getActionMethod() !== 'show')
        <button class="btn btn-primary rounded" type="submit">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Salvar' : 'Salvar' }}</button>
    @else
        <br>
    @endif
</div>
