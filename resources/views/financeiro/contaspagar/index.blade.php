@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Contas a pagar</div>
                <div class="card-body">
                    <a href="{{ url('/financeiro/contaspagar/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar Contaspagar">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Conta a pagar
                    </a>

                    <form method="GET" action="{{ url('/financeiro/contaspagar') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th><th>Fornecedor</th><th>Valor</th><th>Vencimento</th><th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($contaspagar as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                      @php
                                          $fornecedor = DB::table('fornecedors')
                                                  ->select(DB::raw('fantasia'))
                                                  ->where('id', '=', $item->fornecedor_id )
                                                  ->get();

                                          echo isset($fornecedor) ? $fornecedor[0]->fantasia : '--';
                                      @endphp
                                    </td>
                                    <td>{{ number_format($item->valor, 2, ',', '.') }}</td>
                                    <td>{{ date('d/m/Y', strtotime($item->vencimento)) }}</td>
                                    <td>
                                        <a href="{{ url('/financeiro/contaspagar/' . $item->id) }}" title="Visualizar Contaspagar"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        <a href="{{ url('/financeiro/contaspagar/' . $item->id . '/edit') }}" title="Editar Contaspagar"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                        {{ Form::open(['url'=> '/financeiro/contaspagar' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                            {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title' =>'Excluir', 'role' => 'button', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $contaspagar->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')

@endsection
