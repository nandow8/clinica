@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Contaspagar</div>
                <div class="card-body">

                    <a href="{{ url('/financeiro/contaspagar') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $contaspagar->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Fornecedor Id </th><td> {{ $contaspagar->fornecedor_id }} </td></tr><tr><th> Fornecedor </th><td> {{ $contaspagar->fornecedor }} </td></tr><tr><th> Vencimento </th><td> {{ $contaspagar->vencimento }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
