<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.4/jquery-confirm.min.css"></script>
<script src="https://code.jquery.com/jquery-1.9.1.js"></script>

<style type="text/css">

#loading{
    text-align: center;
    height: 450px;
}    
    
#loader {
    position: absolute;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 100px;
    height: 100px;
    margin: -50px 0 0 -50px;
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #4d6328;
    background: RGB(133,173,44,0.5);
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
}

@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

/* Add animation to "page content" */
.animate-bottom {
  position: relative;
  -webkit-animation-name: animatebottom;
  -webkit-animation-duration: 1s;
  animation-name: animatebottom;
  animation-duration: 1s
}

@-webkit-keyframes animatebottom {
  from { bottom:-100px; opacity:0 } 
  to { bottom:0px; opacity:1 }
}

@keyframes animatebottom { 
  from{ bottom:-100px; opacity:0 } 
  to{ bottom:0; opacity:1 }
}

#myDiv {
  display: none;
  text-align: center;
}
</style>

<div id="loading">
    <h1>Carregando...</h1>
    <div id="loader"></div>
</div>
<div id="myDiv">
<br>    
<input name="id" id="id" type="hidden" value="{{ isset($orcamento->id) ? $orcamento->id : '0'}}"> 
<ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="dados-tab" data-toggle="tab" href="#dados" role="tab" aria-controls="dados" aria-selected="true">Dados</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="detalhes-tab" data-toggle="tab" href="#detalhes" role="tab" aria-controls="detalhes" aria-selected="false">Detalhes</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="descontos-tab" data-toggle="tab" href="#descontos" role="tab" aria-controls="descontos" aria-selected="false">Descontos</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="dados" role="tabpanel" aria-labelledby="dados-tab">
        <br>
            @if (isset($createorcamento) && ($createorcamento == 'Orçamento'))
            <div class="form-group row {{ $errors->has('paciente_id') ? 'has-error' : ''}}">
                <label for="paciente_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Paciente *' }}</label>
                <div class="col-lg-5 col-md-6 col-sm-12">
                <select name="paciente_id" class="form-control" id="paciente_id" required="true">
                    <option value="1">Orçamento</option>
                </select>
                </div>
            </div>
            @else
            <div class="form-group row {{ $errors->has('paciente_id') ? 'has-error' : ''}}">
                <label for="paciente_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Paciente *' }}</label>
                <div class="col-lg-5 col-md-6 col-sm-12">
                <select name="paciente_id" class="form-control" id="paciente_id" required="true" onchange="verifOrcamento(this)">
                    @if (isset($orcamento->orcamento))
                    <option value="1">ORÇAMENTO</option>
                    @endif
                    <option value="">Selecione...</option>
                    @foreach ($pacientes as $optionKey => $optionValue)
                    <option value="{{ $optionValue->id }}" {{ (isset($orcamento->paciente_id) && $orcamento->paciente_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nome }}</option>
                    @endforeach
                </select>
                </div>
            </div>
            @endif
            {!! $errors->first('paciente_id', '<p class="help-block">:message</p>') !!}
            @if (isset($orcamento->orcamento) || isset($createorcamento))
            <div id='divorcamento'>
                <div class="form-group row {{ $errors->has('orcamento') ? 'has-error' : ''}}" >
                    <label for="orcamento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Nome' }}</label>
                    <div class="col-lg-5 col-md-6 col-sm-12">
                        <input class="form-control" name="orcamento" type="text" id="orcamento" value="{{ isset($orcamento->orcamento) ? $orcamento->orcamento : ''}}" >
                    </div>
                </div>
                {!! $errors->first('orcamento', '<p class="help-block">:message</p>') !!}
            </div>
            @endif
        
        <div class="form-group row {{ $errors->has('vencimento') ? 'has-error' : ''}}">
            <label for="vencimento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Vencimento *' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <input class="form-control" name="vencimento" type="date" id="vencimento" required="true" value="{{ isset($orcamento->vencimento) ? $orcamento->vencimento : date('Y-m-d') }}" >
            </div>
            {!! $errors->first('vencimento', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group row {{ $errors->has('pagamento') ? 'has-error' : ''}}">
            <label for="pagamento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Pagamento' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <input class="form-control" name="pagamento" type="date" id="pagamento" value="{{ isset($orcamento->pagamento) ? $orcamento->pagamento : date('Y-m-d') }}" >
            </div>
            {!! $errors->first('vencimento', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group row {{ $errors->has('valor') ? 'has-error' : ''}}">
            <label for="valor" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Valor' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <input class="form-control" name="valorview" type="text" id="valorview" readonly="true" required="true" value="{{ isset($orcamento->valor) ? $orcamento->valor : ''}}" >
                <input name="valor" type="hidden" step="any" id="valor" value="{{ isset($orcamento->valor) ? $orcamento->valor : ''}}" >
            </div>
            {!! $errors->first('valor', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group row {{ $errors->has('numerodocumento') ? 'has-error' : ''}}" style="display: none;">
            <label for="numerodocumento" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Documento *' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <input class="form-control" name="numerodocumento" type="number" id="numerodocumento" value="{{ isset($orcamento->numerodocumento) ? $orcamento->numerodocumento : ''}}" >
            </div>
            {!! $errors->first('numerodocumento', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group row {{ $errors->has('numero') ? 'has-error' : ''}}" style="display: none;">
            <label for="numero" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Parcela *' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <input class="form-control" name="numero" type="number" id="numero" value="{{ isset($orcamento->numero) ? $orcamento->numero : ''}}" >
            </div>
            {!! $errors->first('numero', '<p class="help-block">:message</p>') !!}
        </div>

        <div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
            <label for="descricao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Descrição' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" >{{ isset($orcamento->descricao) ? $orcamento->descricao : ''}}</textarea>
            </div>
            {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
        </div>
            
        <div class="form-group row {{ $errors->has('tipopagto_id') ? 'has-error' : ''}}">
            <label for="tipopagto_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Tipo *' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <select name="tipopagto_id" class="form-control" id="tipopagto_id" >
                    @foreach ($tipospagamentos as $optionKey => $optionValue)
                    <option value="{{ $optionValue->id }}" {{ (isset($orcamento->tipopagto_id) && $orcamento->tipopagto_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->descricao }}</option>
                    @endforeach
                </select>
            </div>
            {!! $errors->first('tipopagto_id', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group row {{ $errors->has('status') ? 'has-error' : ''}}">
            <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Status *' }}</label>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <select name="status" class="form-control" id="status" >
                    @foreach (json_decode('{"Aberto": "Em Aberto", "Recebido": "Recebido", "Atraso": "Em Atraso"}', true) as $optionKey => $optionValue)
                    <option value="{{ $optionKey }}" {{ (isset($orcamento->status) && $orcamento->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                    @endforeach
                </select>
            </div>
            {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
        </div>
        <div class="form-group row {{ $errors->has('clinica_id') ? 'has-error' : ''}}">
            <!--<label for="clinica_id" class="col-sm-2 col-form-label">{{ 'clinica_id' }}</label>-->
            <div class="col-lg-5 col-md-6 col-sm-12">
                <input class="form-control" name="clinica_id" type="hidden" id="clinica_id" value="{{ isset($orcamento->clinica_id) ? $orcamento->clinica_id : 1}}" >
            </div>
            {!! $errors->first('clinica_id', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="tab-pane fade" id="detalhes" role="tabpanel" aria-labelledby="detalhes-tab">
      <br>
        <div class="form-group row {{ $errors->has('servicovalor') ? 'has-error' : ''}}">
        <label for="exame" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Procedimentos' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
          <input type="hidden" name="campoididi" id="campoididi" >
          <input type="hidden" name="campoexame" id="campoexame" >
          <input type="hidden" name="campovenda" id="campovenda" >
          <input type="text" name="exame" onkeyup="autocompletaexame(this)" id="exame" class="form-control" aria-label="Sizing example input" aria-describedby="inputGroup-sizing-default">
        </div>
        <div class="divTableCell" style="text-align: center">
          <button id="BtnAddProcedimento" type="button" onclick="contareceberdetalheAddTr()" disabled class="btn btn-success" style="font-size: 25;" title="Adicionar"><i class="fa fa-plus fa-2"></i></button>
        </div>
      </div>
      <br>
        <div class="form-group row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <table class="table table-contareceberdetalhe col-lg-8 col-md-8 col-sm-8">
                <thead>
                    <th>&nbsp;</th>
                    <th style="text-align: left;">Descrição</th>
                    <th style="text-align: right;">Valor</th>
                    <th>&nbsp;</th>
                </thead>
                <tbody>
                    <script type="text/javascript">

                    $(document).ready(function() {

                        @php
                            if(isset($orcamento)):
                            foreach ($detalhes->where('orcamento_id','',$orcamento->id) as $optionKey => $optionValue):

                                $optionValue['var_db'] = true;

                        @endphp

                        detalhe = '@php echo str_replace("'", "\'", json_encode($optionValue)) @endphp';

                        contareceberdetalheAddTrInicio(detalhe);

                        @php endforeach; endif; @endphp

                    });

                    </script>
                </tbody>
                <tfoot>
                    <tr id="modelo-contareceberdetalhe" style="display:none;">
                        <td align="center" style="text-align:center !Important;">
                            <input name="_id_detalhe[]" type="hidden"/>
                            <input name="_orcamento_id_detalhe[]" type="hidden"/>
                            <input name="_tabela_servico_id_detalhe[]" type="hidden"/>
                        </td>

                        <td align="center" style="text-align:left !Important;">
                            <input name="_descricao_detalhe[]" type="hidden"/>
                            <div name="_descricao_detalhe[]">...</div>
                        </td>

                        <td align="center" style="text-align: right !Important;">
                            <input name="_valor_detalhe[]" type="hidden"/>
                            <div name="_valor_detalhe[]" id="_valor_detalhe[]" class="numbervalue">...</div>
                        </td>

                        <td style="text-align: center !important;" align="center">
                            @if(request()->route()->getActionMethod() !== 'show')
                                <a @php echo 'onclick="contareceberdetalheExcluirFromTable(this)"'; @endphp class="btn btn-danger"><span class="fa fa-minus fa-1"></span></a>
                            @endif
                        </td>

                    </tr>

                    <tr>
                        <td colspan="2" style="text-align: right;">Total</td>
                        <td style="text-align: left;"><div id='subtotdetalhevalor'>--</div></td>
                        <td>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
          </div>
        </div>

    </div>
    <div class="tab-pane fade" id="descontos" role="tabpanel" aria-labelledby="descontos-tab">
      <br>
      <div class="form-group row {{ $errors->has('descacre_id') ? 'has-error' : ''}}">
          <label for="descacre_id" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Descontos' }}</label>
          <div class="col-lg-5 col-md-6 col-sm-12">
              <select name="descacre" class="form-control" id="descacre" onchange="setValorDesconto(this)">
                  <option value="">Selecione...</option>
                  @foreach ($tabeladescontos as $optionKey => $optionValue)
                      <option value="{{ $optionValue->id }}">{{ $optionValue->titulo }}</option>
                  @endforeach
              </select>
          </div>
          {!! $errors->first('descacre_id', '<p class="help-block">:message</p>') !!}
      </div>
      <div class="form-group row {{ $errors->has('descontovalor') ? 'has-error' : ''}}">
          <label for="descontovalor" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Percentual' }}</label>
          <div class="col-lg-5 col-md-6 col-sm-12">
              <input class="form-control" name="descontovalor" type="hidden" id="descontovalor" value="" style="text-align: right;" >
              <input class="form-control" name="descontovalorview" type="text" readonly="true" id="descontovalorview" value="" style="text-align: right;" >
          </div>
          {!! $errors->first('descontovalor', '<p class="help-block">:message</p>') !!}
          <div class="divTableCell" style="text-align: center">
              <button id="BtnAddDesconto" type="button" onclick="contareceberdescontoAddTr()" class="btn btn-success" disabled="true" style="font-size: 25;" title="Adicionar"><i class="fa fa-plus fa-2"></i></button>
          </div>
        <br>
      </div>
        
        <div class="form-group row">
          <div class="col-lg-12 col-md-12 col-sm-12">
            <input name="percentualdesconto" id="percentualdesconto" value="0" type="hidden">
            <table class="table table-contareceberdesconto col-lg-8 col-md-8 col-sm-8">
                <thead>
                    <th>&nbsp;</th>
                    <th style="text-align: left;">Descrição</th>
                    <th style="text-align: right;">%</th>
                    <th>&nbsp;</th>
                </thead>
                <tbody>
                    <script type="text/javascript">

                    $(document).ready(function() {

                        @php
                            if(isset($orcamento)):
                            foreach ($descontos->where('orcamento_id','',$orcamento->id) as $optionKey => $optionValue):

                                $optionValue['var_db'] = true;

                        @endphp

                        detalhe = '@php echo str_replace("'", "\'", json_encode($optionValue)) @endphp';

                        contareceberdescontoAddTrInicio(detalhe);

                        @php endforeach; endif; @endphp

                    });

                    </script>
                </tbody>
                <tfoot>
                    <tr id="modelo-contareceberdesconto" style="display:none;">
                        <td align="center" style="text-align:center !Important;">
                            <input name="_id_desconto[]" type="hidden"/>
                            <input name="_orcamento_id_desconto[]" type="hidden"/>
                            <input name="_tabela_descacre_id_desconto[]" type="hidden"/>
                        </td>

                        <td align="center" style="text-align:left !Important;">
                            <input name="_descricao_desconto[]" type="hidden"/>
                            <div name="_descricao_desconto[]">...</div>
                        </td>

                        <td align="center" style="text-align:right !Important;">
                            <input name="_valor_desconto[]" type="hidden"/>
                            <div name="_valor_desconto[]" id="_valor_desconto[]" class="numbervalue">...</div>
                        </td>

                        <td style="text-align: center !important;" align="center">
                            @if(request()->route()->getActionMethod() !== 'show')
                            <a @php echo 'onclick="contareceberdescontoExcluirFromTable(this)"'; @endphp class="btn btn-danger"><span class="fa fa-minus fa-1"></span></a>
                            @endif
                        </td>

                    </tr>

                    <tr>
                        <td colspan="2" style="text-align: right;">Percentual</td>
                        <td style="text-align: right;"><div id='subtotdescontopercent'>--</div></td>
                        <td>&nbsp;</td>
                    </tr>
                </tfoot>
            </table>
          </div>
        </div>
      <br>
    </div>

    <br/>
    <div class="form-group offset-md-5">
        <a href="{{ url('/financeiro/orcamento') }}" title="Voltar" class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>

        @if(request()->route()->getActionMethod() !== 'show')
        <button class="btn btn-primary rounded" type="submit" id="btnenviar">
            <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ 'Salvar' }}</button>
        @else
        <br>
        @endif
    </div>
</div>
</div>

<script type="text/javascript">

    var myVar;
    var formID = document.getElementById("FormHorizontal");
    var send = $("#btnenviar");

    const formatoBR = new Intl.NumberFormat('pt-BR', {
      minimumFractionDigits: 2
    });

    /* Tempo para o loader da página */
    $(window).load(function(){
        myVar = setTimeout(showPage, 3000);
    });

    /* Desabilita botão enviar para evitar multiplas chamadas */
    $(formID).submit(function(event){
      if (formID.checkValidity()) {
        send.attr('disabled', 'disabled');
      }
    });

    $("#exame").on("dblclick", function(entry) {
        $("#exame").val("");
    });
    
    function showPage() {
      document.getElementById("loader").style.display = "none";
      document.getElementById("loading").style.display = "none";
      document.getElementById("myDiv").style.display = "block";
    }

    function contareceberdetalheAddTr(_values) {

        html = "";

        if (_values===false) {
                _values = {};
        }else if(_values === undefined){
                _values = {
                            "id":0,
                            "orcamento_id":$("#id").val(),
                            "tabela_servico_id":$('#campoididi').val(),
                            'descricao':$("#campoexame").val(),
                            "valor":$("#campovenda").val(),
                            "var_db":true
                          };
        } else {
                _values = jQuery.parseJSON(_values);
                itens = _values;
        }

        modelo = $("#modelo-contareceberdetalhe").html();
        modelo = '<tr>' + modelo + '</tr>';
        modelo = $(modelo);
        table = $("table.table-contareceberdetalhe");
        contareceberdetalheinner = table.find('tbody');

        modelo.find('p').html(html);
        modelo.find('input,select,textarea,div').each(function() {
            name = $(this).attr('name');
            id = $(this).attr('id');
            name = name.replace("_", "");
            
            if(_values && _values.var_db == true)name = name.replace("_detalhe[]", "");

            for (var property in _values) {
                if (property==name) {

                    $(this).attr('name', name + '_detalhe[]_array[]');

                    $(this).attr('id', id + '_detalhe[]_array[]');

                    if(_values.var_db){
                    }

                    $(this).attr('value', _values[property]);

                    if(name == 'valor'){
                        $(this).html(formatoBR.format(_values[property]));
                    }else{
                        $(this).html(_values[property]);
                    }

                    if ($(this).is("select")) {
                        $(this).parent().find('a').remove();
                    }
                }
            }
        });

        contareceberdetalheinner.append(modelo);

        $('.numbervalue').blur(function(){
                if($(this).val() <= 0){
                        $(this).val('');
                }
        });

        $('#servico').val('');
        $('#servicovalor').val('');
        $('#servicovalorview').val('');

        calcTotLinhasDetalhes();
    }

    function contareceberdetalheAddTrInicio(_values) {

        html = "";

        if (_values===false) {
                _values = {};
        } else {
                _values = $.parseJSON(_values);
                itens = _values;
        }

        modelo = $("#modelo-contareceberdetalhe").html();
        modelo = '<tr>' + modelo + '</tr>';
        modelo = $(modelo);
        table = $("table.table-contareceberdetalhe");
        orcamentodetalheinner = table.find('tbody');

        modelo.find('p').html(html);
        modelo.find('input,select,textarea,div').each(function() {
            name = $(this).attr('name');
            id = $(this).attr('id');
            name = name.replace("_", "");
            
            if(_values && _values.var_db == true)name = name.replace("_detalhe[]", "");

            for (var property in _values) {
                if (property==name) {

                    $(this).attr('name', name + '_detalhe[]_array[]');

                    $(this).attr('id', id + '_detalhe[]_array[]');

                    if(_values.var_db){
                    }

                    $(this).attr('value', _values[property]);

                    if(name == 'valor'){
                        $(this).html(formatoBR.format(_values[property]));
                    }else{
                        $(this).html(_values[property]);
                    }

                    if ($(this).is("select")) {
                        $(this).parent().find('a').remove();
                    }
                }
            }

        });

        orcamentodetalheinner.append(modelo);

        $('.numbervalue').blur(function(){
                if($(this).val() <= 0){
                        $(this).val('');
                }
        });

        calcTotLinhasDetalhes();
    }//end contareceberdetalhe do inicio

    function calcTotLinhasDetalhes(){
        var Total = 0.0;
        var Desc = 0.0;

        Desc = parseFloat($('#percentualdesconto').val());
        
        document.getElementsByName('valor_detalhe[]_array[]').forEach(function(entry) {
            if(!isNaN(parseFloat(entry.innerHTML)) && entry.id === '_valor_detalhe[]_detalhe[]_array[]'){
                    val = entry.innerHTML.replace('.', '');
                    Total = Total + parseFloat(val.replace(',', '.'));
                }
        });
        
        Total = Total - Total * Desc;

        $("#subtotdetalhevalor").html(formatoBR.format(parseFloat(Total).toFixed(2).toString()));
        $("#valor").val(parseFloat(Total).toFixed(2).toString());
        $("#valorview").val(formatoBR.format(parseFloat(Total).toFixed(2).toString()));
        
        calcTotLinhasDescontos;

    }

    function contareceberdetalheExcluirFromTable(obj) {

        var form = this;

        const swalWithBootstrapButtons = Swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        })

        obj = $(obj);
        content = obj.closest('tr');
        var idcode = 0;

        content.find('td').each(function(index, value){
            if(index == 0){
                idcode = $(value).find('input').val();
                console.log('id', idcode);
            }
        });

        swalWithBootstrapButtons({
            title: "Deletar registro!",
            text: "Deseja realmente excluir este registro?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: ' Sim ',
            cancelButtonText: ' Não ',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                content.find('input').remove();
                content.find('div').remove();
                content.hide();
                calcTotLinhasDetalhes();
            }
        })

    }

    function contareceberdescontoAddTr(_values) {

        html = "";

        if (_values===false) {
            _values = {};
        }else if(_values === undefined){
            _values = {
                        "id":0,
                        "orcamento_id":$("#id").val(),
                        "tabela_descacre_id":$('#descacre').val(),
                        'descricao':$("#descacre option:selected").html(),
                        "valor":$("#descontovalor").val(),
                        "var_db":true                            
                      };
        } else {
            _values = jQuery.parseJSON(_values);
            itens = _values;
        }

        modelo = $("#modelo-contareceberdesconto").html();
        modelo = '<tr>' + modelo + '</tr>';
        modelo = $(modelo);
        table = $("table.table-contareceberdesconto");
        contareceberdescontoinner = table.find('tbody');

        modelo.find('p').html(html);
        modelo.find('input,select,textarea,div').each(function() {
            name = $(this).attr('name');
            id = $(this).attr('id');
            name = name.replace("_", "");
            
            if(_values && _values.var_db == true)name = name.replace("_desconto[]", "");

            for (var property in _values) {
                if (property==name) {

                    $(this).attr('name', name + '_desconto[]_array[]');

                    $(this).attr('id', id + '_desconto[]_array[]');

                    if(_values.var_db){
                    }

                    $(this).attr('value', _values[property]);

                    if(name == 'valor'){
                        $(this).html(formatoBR.format(_values[property]));
                    }else{
                        $(this).html(_values[property]);
                    }

                    if ($(this).is("select")) {
                            $(this).parent().find('a').remove();
                    }
                }
            }
        });

        contareceberdescontoinner.append(modelo);

        $('.numbervalue').blur(function(){
                if($(this).val() <= 0){
                        $(this).val('');
                }
        });

        $('#descacre').val('');
        //$('#descontovalor').val('');
        $('#descontovalorview').val('');

        calcTotLinhasDescontos();
    }

    function contareceberdescontoAddTrInicio(_values) {

        html = "";

        if (_values===false) {
                _values = {};
        } else {
                _values = $.parseJSON(_values);
                itens = _values;
        }

        modelo = $("#modelo-contareceberdesconto").html();
        modelo = '<tr>' + modelo + '</tr>';
        modelo = $(modelo);
        table = $("table.table-contareceberdesconto");
        orcamentodescontoinner = table.find('tbody');

        modelo.find('p').html(html);
        modelo.find('input,select,textarea,div').each(function() {
            name = $(this).attr('name');
            id = $(this).attr('id');
            name = name.replace("_", "");
            
            if(_values && _values.var_db == true)name = name.replace("_desconto[]", "");

            for (var property in _values) {
                if (property==name) {

                    $(this).attr('name', name + '_desconto[]_array[]');

                    $(this).attr('id', id + '_desconto[]_array[]');

                    if(_values.var_db){
                    }

                    $(this).attr('value', _values[property]);

                    if(name == 'valor'){
                        $(this).html(formatoBR.format(_values[property]));
                    }else{
                        $(this).html(_values[property]);
                    }

                    if ($(this).is("select")) {
                            $(this).parent().find('a').remove();
                    }
                }
            }

        });

        orcamentodescontoinner.append(modelo);

        $('.numbervalue').blur(function(){
            if($(this).val() <= 0){
                $(this).val('');
            }
        });

        calcTotLinhasDescontos();
    }//end contareceberdetalhe do inicio

    function calcTotLinhasDescontos(){
        var Total = 0.0;
        var percent = 0.0;
        document.getElementsByName('valor_desconto[]_array[]').forEach(function(entry) {
            if(!isNaN(parseFloat(entry.innerHTML)) && entry.id === '_valor_desconto[]_desconto[]_array[]'){
                val = entry.innerHTML.replace('.', '');
                Total = Total + parseFloat(val.replace(',', '.'));
            }
        });

        percent = (parseFloat(Total).toFixed(2) / 100);

        $("#subtotdescontopercent").html(formatoBR.format(parseFloat(Total).toFixed(2).toString()));

        $("#percentualdesconto").val(parseFloat(percent).toFixed(2).toString());
        
        calcTotLinhasDetalhes();
    }

    function contareceberdescontoExcluirFromTable(obj) {

        var form = this;

        const swalWithBootstrapButtons = Swal.mixin({
        confirmButtonClass: 'btn btn-success',
        cancelButtonClass: 'btn btn-danger',
        buttonsStyling: false,
        })

        obj = $(obj);
        content = obj.closest('tr');
        var idcode = 0;

        content.find('td').each(function(index, value){
            if(index == 0){
                idcode = $(value).find('input').val();
            }
        });

        swalWithBootstrapButtons({
            title: "Deletar registro!",
            text: "Deseja realmente excluir este registro?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: ' Sim ',
            cancelButtonText: ' Não ',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
                content.find('input').remove();
                content.find('div').remove();
                content.hide();
                calcTotLinhasDescontos();
            }
        })

    }

    function setValorDesconto(obj){

        var Val = $(obj).val();

        if(Val !== ''){
            $.ajax({
                method: 'GET',
                url: '/financeiro/orcamento/' + Val + '/retornadadosdescacre',
                success: function(response){
                  valor = parseFloat(response.descacre.valor);
                  $('#descontovalor').val(valor.toFixed(2));
                  $('#descontovalorview').val(formatoBR.format(valor.toFixed(2)));
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log('Error '+xhr.status+' | '+thrownError);
                }
            });
        }else{
            $('#descontovalor').val('0');
            $('#descontovalorview').val('');
        }

        document.getElementById('BtnAddDesconto').disabled = (Val === "");

    }

    function autocompletaexame(obj){
        var val = jQuery(obj).val()

        document.getElementById('BtnAddProcedimento').disabled = (val === "");

        const isAlphaNumeric = ch => {
        	return ch.match(/^[a-z0-9]+$/i) !== null;
        }

        if(isAlphaNumeric(val)){
          jQuery.getJSON('/financeiro/orcamento/ajaxexibirservicoexamerf/' + val , function (data) {
            jQuery("#exame").autocomplete({
                source: function(request, response){
                    response(data)
                },
                select: function (e, ui) {
                    mneum = ui.item.label.split("---")[0].trim()
                    codig = ui.item.label.split("---")[1].trim()
                    exame = ui.item.label.split("---")[2].trim()
                    venda = ui.item.label.split("---")[3].trim()
                    ididi = ui.item.label.split("---")[4].trim()

                    mneum = mneum !== '' ? '(' + mneum + ') ' : ''
                    codig = codig !== '' ? codig + ' - ' : ''

                    jQuery('#campoididi').val(ididi)
                    jQuery('#campoexame').val(mneum  + codig +  exame)
                    jQuery('#campovenda').val(venda.replace('.','').replace(',','.'))
                },
                change: function (e, ui) {
                    mneum = ui.item.label.split("---")[0].trim()
                    codig = ui.item.label.split("---")[1].trim()
                    exame = ui.item.label.split("---")[2].trim()
                    venda = ui.item.label.split("---")[3].trim()
                    ididi = ui.item.label.split("---")[4].trim()

                    mneum = mneum !== '' ? '(' + mneum + ') ' : ''
                    codig = codig !== '' ? codig + ' - ' : ''

                    jQuery('#exame').val( mneum + codig + exame + ' R$ ' + venda )
                }
            });
        });
    }
  }

    /**
     * Comment
     */
    function verifOrcamento(obj) {
        
        var val = $(obj).val();
         
        console.log(val);
        
        if(val !== '1'){
            $('#divorcamento').css('display', 'none');
            $('#orcamento').val('');
        }else{
            $('#divorcamento').css('display', 'block');
            $('#orcamento').val('<?php echo isset($orcamento->orcamento) ? $orcamento->orcamento : ''?>');
        }
        
    }

</script>
