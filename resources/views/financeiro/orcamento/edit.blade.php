@extends('layouts.template')

@section('content')

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Editar Orçamento</div>
                <div class="card-body">
                        
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <form method="POST" id="FormHorizontal" action="{{ url('/financeiro/orcamento/' . $orcamento->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                        {{ method_field('PATCH') }}
                        {{ csrf_field() }}

                        @include ('financeiro.orcamento.form', ['formMode' => 'edit'])

                    </form>                   
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jqueryscript')

@endsection