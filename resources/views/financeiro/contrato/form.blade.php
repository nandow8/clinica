<style>
    @media all {
        .page-break { display: none; }
    }   

    @media print {
        @page { 
            margin-top:  20px;
            margin-left: 20px;
            margin-right: 20px;
            margin-bottom: 20px;
        }

        .page-break { display: block; page-break-before: always; }

        body { margin: 1.6cm; }

        body * {
            visibility: hidden;
        }

        .margem-sufite{ margin-left: 4%;  }
        #medicamento-sufite{ font-size: 2em; color:gray; }
        .descricao-sufite{ font-size: 1.4em; color:gray; }

        #corpodoc, #corpodoc * {
            visibility: visible;
        }

        #corpodoc {
            position: fixed;
            left: 0;
            top: 0;
        }
    }

</style>

<div class="form-group row {{ $errors->has('documento_id') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Modelo (*)' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <select name="documento_id" class="form-control search-form" id="documento_id" onchange="habilitarAtualizacao()">
            <option value="0">Selecione...</option>
            @foreach ($modelos as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($contrato->documento_id) && $contrato->documento_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->descricao }}</option>
            @endforeach
        </select>
    </div>
    {!! $errors->first('paciente_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('paciente_id') ? 'has-error' : ''}}">
    <label for="status" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Titular (*)' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <select name="paciente_id" class="form-control" id="paciente_id" onchange="habilitarAtualizacao()">
            <option value="0">Selecione...</option>
            @foreach ($pacientes as $optionKey => $optionValue)
            <option value="{{ $optionValue->id }}" {{ (isset($contrato->paciente_id) && $contrato->paciente_id == $optionValue->id) ? 'selected' : ''}}>{{ $optionValue->nome }}</option>
            @endforeach
        </select>
        <input type="hidden" id="titularrg">
        <input type="hidden" id="titularcpf">
        <input type="hidden" id="titularemail">
        <input type="hidden" id="titularnascto">
        <input type="hidden" id="titularlogradouro">
        <input type="hidden" id="titularnumero">
        <input type="hidden" id="titularbairro">
        <input type="hidden" id="titularcomplemento">
        <input type="hidden" id="titularcidade">
        <input type="hidden" id="titularcep">
    </div>
    {!! $errors->first('paciente_id', '<p class="help-block">:message</p>') !!}
</div>

<div class="form-group row {{ $errors->has('data') ? 'has-error' : ''}}">
    <label for="data" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Data (*)' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
        <div class="btn-group btn-group-toggle" data-toggle="buttons">
          <label onclick="setavencimento('10')" class="btn btn-info {{ isset($contrato->vencimento) && $contrato->vencimento == '10' ? 'active focus' : '' }}">
            <input type="radio" name="vencimento" id="option1" autocomplete="off" {{ isset($contrato->vencimento) && $contrato->vencimento == '10' ? 'checked' : '' }}> Dia 10
          </label>
          <label onclick="setavencimento('30')" class="btn btn-primary {{ isset($contrato->vencimento) && $contrato->vencimento == '30' ? 'active focus' : '' }}">
            <input type="radio" name="vencimento" id="option2" autocomplete="off" {{ isset($contrato->vencimento) && $contrato->vencimento == '30' ? 'checked' : '' }}> Dia 30
          </label>
<!--          <label onclick="setavencimento('OU')" class="btn btn-warning {{ isset($contrato->vencimento) && $contrato->vencimento == 'OU' ? 'active focus' : '' }}">
            <input type="radio" name="vencimento" id="option3" autocomplete="off" {{ isset($contrato->vencimento) && $contrato->vencimento == 'OU' ? 'checked' : '' }}> Outro
          </label>-->
        </div>      
        <input id="vencimento" type="hidden" name="vencimento" value="{{ isset($contrato->vencimento) ? $contrato->vencimento : '30'}}">
        <br>
        <input class="form-control" name="data" type="date" id="data" required="true" value="{{ isset($contrato->data) ? $contrato->data : date('Y-m-d')}}" onchange="habilitarAtualizacao()">
    </div>
    {!! $errors->first('data', '<p class="help-block">:message</p>') !!}
</div>

<fieldset class="border p-1">
    <legend class="w-auto">Valores:</legend>
    
    <div class="form-group row {{ $errors->has('tipopagto') ? 'has-error' : ''}}">
        <label for="tipopagto" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Tipo (*)' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <select name="tipopagto" class="form-control" id="tipopagto" onchange="habilitarAtualizacao()">
                @foreach (json_decode('{"":"Selecione...", "DI":"Dinheiro","BO":"Boleto","CC":"Cartao de Credito","CD":"Cartao Debito","CH":"Cheque"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($contrato->tipopagto) && $contrato->tipopagto == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
                @endforeach
            </select>
        </div>
        {!! $errors->first('tipopagto', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group row {{ $errors->has('valoradesao') ? 'has-error' : ''}}">
        <label for="valoradesao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Adesão (*)' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <input class="form-control" name="valoradesao" type="number" step="any" id="valoradesao" required="true" value="{{ isset($contrato->valoradesao) ? $contrato->valoradesao : ''}}" onchange="habilitarAtualizacao()">
        </div>
        {!! $errors->first('valoradesao', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group row {{ $errors->has('valormensalidade') ? 'has-error' : ''}}">
        <label for="valormensalidade" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Mensalidade (*)' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <input class="form-control" name="valormensalidade" type="number" step="any" id="valormensalidade" required="true" value="{{ isset($contrato->valormensalidade) ? $contrato->valormensalidade : ''}}" onchange="habilitarAtualizacao()">
        </div>
        {!! $errors->first('valormensalidade', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group row {{ $errors->has('valorconsulta') ? 'has-error' : ''}}">
        <label for="valorconsulta" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Consulta (*)' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <input class="form-control" name="valorconsulta" type="number" step="any" id="valorconsulta" required="true" value="{{ isset($contrato->valorconsulta) ? $contrato->valorconsulta : ''}}" onchange="habilitarAtualizacao()">
        </div>
        {!! $errors->first('valorconsulta', '<p class="help-block">:message</p>') !!}
    </div>

    <div class="form-group row {{ $errors->has('valorretorno') ? 'has-error' : ''}}">
        <label for="valorretorno" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Retorno (*)' }}</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <input class="form-control" name="valorretorno" type="number" step="any" id="valorretorno" required="true" value="{{ isset($contrato->valorretorno) ? $contrato->valorretorno : ''}}" onchange="habilitarAtualizacao()">
        </div>
        {!! $errors->first('valorretorno', '<p class="help-block">:message</p>') !!}
    </div>
</fieldset>

<p>&nbsp</p>

<div id="divproposta" style="display:none;" >
    <div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
        <label for="proposta" class="col-lg-2 col-md-3 col-sm-12 col-form-label">Proposta</label>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <textarea class="form-control" rows="5" name="proposta" type="textarea" id="proposta" >
            </textarea>
        </div>
    </div>
</div>

<div style="display:none;"> <!-- Modelos das tabelas que serão geradas  -->
    <table id="table-titular" class="table-titular" style="width:100%;"  border="1">
        <thead>
        <th colspan="2">NOME COMPLETO</th>
        <th>RG</th>
        <th>CPF</th>
        </thead>
        <tbody id="tbtitular">
        </tbody>
        <tfoot>
            <tr id="modelo-titular" style="display:none;">
                <td colspan="2" style="text-align:left !Important;">
                    <div name="nome[]" id="nome[]">...</div>
                </td>
                <td style="text-align:left !Important;">
                    <div name="rg[]" id="rg[]">...</div>
                </td>
                <td style="text-align:left !Important;">
                    <div name="cpf[]" id="cpf[]">...</div>
                </td>
            </tr>
        </tfoot>
    </table>

    <table id="table-dependentes" class="table-dependentes" style="width:100%;"  border="1">
        <thead>
        <th colspan="2">NOME COMPLETO</th>
        <th>D.N.</th>
        <th>PARENT.</th>
        </thead>
        <tbody id="tbdependentes">
        </tbody>
        <tfoot>
            <tr id="modelo-dependentes" style="display:none;">
                <td align="center" style="text-align:center !Important;">
                    <div name="num[]" id="num[]" class="numbervalue">...</div>
                </td>
                <td align="center" style="text-align:left !Important;">
                    <div name="nome[]" id="nome[]" class="numbervalue">...</div>
                </td>
                <td align="center" style="text-align:left !Important;">
                    <div name="datanascimento[]" id="datanascimento[]" class="numbervalue">...</div>
                </td>
                <td align="center" style="text-align:left !Important;">
                    <div name="cpf[]" id="cpf[]" class="numbervalue">...</div>
                </td>
            </tr>
        </tfoot>
    </table>
</div>

<div style="background-color: #f1f1f1; width: 100%;">
    <div class="card-header">
        @if(request()->route()->getActionMethod() !== 'show')
        <button id="btnAtualizar" disabled type="button" class="btn btn-primary" onclick="setAtualizaContrato()"><i class="fa fa-refresh"></i> Atualizar Proposta</button>
        @endif
<!--        
        <button type="button" class="btn btn-info" onclick="imprimir()"><i class="fa fa-print"></i> Imprimir</button>
        <button type="button" class="btn btn-danger" onclick="printme()"><i class="fa fa-print"></i> SSSS</button>
-->
    </div>

    <p>&nbsp</p>
    <div id="corpodoc" class="container" style="background-color: white; width: 94%; list-style:none; ">
        {{ isset($contrato->descricao) ? $contrato->descricao : ''}}
    </div>
    <div>

        <div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}" style="display:none;">
            <label for="descricao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Contrato' }}</label>
            <div class="col-lg-12 col-md-12 col-sm-12">
                <textarea class="form-control" rows="5" cols="20" name="descricao" type="textarea" id="descricao" >{{ isset($contrato->descricao) ? $contrato->descricao : ''}}</textarea>
            </div>
            {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
        </div>

        <br/>   

        <div class="form-group  offset-md-5">
            <a href="{{ url('/financeiro/contrato') }}" class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>

            @if(request()->route()->getActionMethod() !== 'show')
            <button class="btn btn-primary rounded" type="submit" id="btnenviar">
                <i class="fa fa-check fa-1" aria-hidden="true"></i> {{ 'Salvar' }}</button>
            @else
            <br>
            @endif

        </div>

        <script type="text/javascript">

            window.onload = iniciaContrato;

            const formatoBR = new Intl.NumberFormat('pt-BR', {
                minimumFractionDigits: 2
            });
            
            function iniciaContrato() {
                document.getElementById("corpodoc").innerHTML = document.getElementById("descricao").value;
            }

            function setAtualizaContrato() {

                var docHtml = document.getElementById("proposta").value;

                var paciente = document.getElementById("paciente_id");

                docHtml = docHtml.replace("&&NOME&&", paciente.options[paciente.selectedIndex].text);
                docHtml = docHtml.replace("&&CPF&&", document.getElementById("titularcpf").value);
                docHtml = docHtml.replace("&&RG&&", document.getElementById("titularrg").value);
                docHtml = docHtml.replace("&&EMAIL&&", document.getElementById("titularemail").value);
                docHtml = docHtml.replace("&&RUA&&", document.getElementById("titularlogradouro").value);
                docHtml = docHtml.replace("&&NUMERO&&", document.getElementById("titularnumero").value);
                docHtml = docHtml.replace("&&BAIRRO&&", document.getElementById("titularbairro").value);
                docHtml = docHtml.replace("&&COMPLEMENTO&&", document.getElementById("titularcomplemento").value);
                docHtml = docHtml.replace("&&CIDADE&&", document.getElementById("titularcidade").value);
                docHtml = docHtml.replace("&&CEP&&", document.getElementById("titularcep").value);
                docHtml = docHtml.replace("&&NASCTO&&", document.getElementById("titularnascto").value);
                
                docHtml = docHtml.replace("&&ADESAO&&", formatoBR.format(parseFloat(document.getElementById("valoradesao").value).toFixed(2).toString()));
                docHtml = docHtml.replace("&&MENSALIDADE&&", formatoBR.format(parseFloat(document.getElementById("valormensalidade").value).toFixed(2).toString()));
                docHtml = docHtml.replace("&&CONSULTA&&", formatoBR.format(parseFloat(document.getElementById("valorconsulta").value).toFixed(2).toString()));
                docHtml = docHtml.replace("&&RETORNO&&", formatoBR.format(parseFloat(document.getElementById("valorretorno").value).toFixed(2).toString()));

                docHtml = docHtml.replace("&&ADESAO&&", formatoBR.format(parseFloat(document.getElementById("valoradesao").value).toFixed(2).toString()));
                docHtml = docHtml.replace("&&MENSALIDADE&&", formatoBR.format(parseFloat(document.getElementById("valormensalidade").value).toFixed(2).toString()));

                if(tipopagto.options[tipopagto.selectedIndex].value == 'DI'){
                    docHtml = docHtml.replace("&&TPDI&&", "[&nbsp;X&nbsp;]" );
                }else{
                    docHtml = docHtml.replace("&&TPDI&&", "[&nbsp;&nbsp;&nbsp;]" );
                }

                if(tipopagto.options[tipopagto.selectedIndex].value == 'CD'){
                    docHtml = docHtml.replace("&&TPCD&&", "[&nbsp;X&nbsp;]" );
                }else{
                    docHtml = docHtml.replace("&&TPCD&&", "[&nbsp;&nbsp;&nbsp;]" );
                }

                if(tipopagto.options[tipopagto.selectedIndex].value == 'CC'){
                    docHtml = docHtml.replace("&&TPCC&&", "[&nbsp;X&nbsp;]" );
                }else{
                    docHtml = docHtml.replace("&&TPCC&&", "[&nbsp;&nbsp;&nbsp;]" );
                }

                if(tipopagto.options[tipopagto.selectedIndex].value == 'BO'){
                    docHtml = docHtml.replace("&&TPBO&&", "[&nbsp;X&nbsp;]" );
                }else{
                    docHtml = docHtml.replace("&&TPBO&&", "[&nbsp;&nbsp;&nbsp;]" );
                }

                if(tipopagto.options[tipopagto.selectedIndex].value == 'CH'){
                    docHtml = docHtml.replace("&&TPCH&&", "[&nbsp;X&nbsp;]" );
                }else{
                    docHtml = docHtml.replace("&&TPCH&&", "[&nbsp;&nbsp;&nbsp;]" );
                }
                
                if(document.getElementById("vencimento").value == '10'){
                    docHtml = docHtml.replace("&&DIA10&&", "[&nbsp;X&nbsp;]" );
                }else{
                    docHtml = docHtml.replace("&&DIA10&&", "[&nbsp;&nbsp;&nbsp;]" );
                }
                
                if(document.getElementById("vencimento").value == '30'){
                    docHtml = docHtml.replace("&&DIA30&&", "[&nbsp;X&nbsp;]" );
                }else{
                    docHtml = docHtml.replace("&&DIA30&&", "[&nbsp;&nbsp;&nbsp;]" );
                }

                docHtml = docHtml.replace("&&TABLETITULAR&&",
                        "<table style='width: 100%;' border='1' >" +
                        document.getElementById("table-titular").innerHTML +
                        "</table>");

                docHtml = docHtml.replace("&&TABLEDEPENDENTES&&",
                        "<table style='width: 100%;' border='1' >" +
                        document.getElementById("table-dependentes").innerHTML +
                        "</table>");

                document.getElementById("corpodoc").innerHTML = docHtml;

                document.getElementById("descricao").value = document.getElementById("corpodoc").innerHTML;
            }

            function imprimir() {
                var conteudo = document.getElementById('corpodoc').innerHTML;
                tela_impressao = window.open('about:blank');
                tela_impressao.document.write(conteudo);
                tela_impressao.window.print();
                tela_impressao.window.close();
            }

            function getTitularDependentes(obj) {

                var Val = jQuery(obj).val();

                jQuery("#tbtitular").empty();
                jQuery("#tbdependentes").empty();

                jQuery.ajax({
                    method: 'GET',
                    url: '/financeiro/contrato/' + Val + '/retornadadostitular',
                    success: function (response) {
                        var _values = response;
                        //console.table(_values);

                        html = "";

                        if (_values === false) {
                            _values = {};
                        } else {
                            _values = jQuery.parseJSON(JSON.stringify(_values));
                        }

                        document.getElementById("titularrg").value = _values.titular.rg; 
                        document.getElementById("titularcpf").value = _values.titular.cpf;
                        document.getElementById("titularemail").value = _values.titular.email;
                        document.getElementById("titularnascto").value = _values.titular.datanascimento.substr(8, 2) + "/" + _values.titular.datanascimento.substr(5, 2) + "/" + _values.titular.datanascimento.substr(0, 4);
                        document.getElementById("titularlogradouro").value = _values.titular.rua;
                        document.getElementById("titularnumero").value = _values.titular.numero;
                        document.getElementById("titularbairro").value = _values.titular.bairros_id;
                        document.getElementById("titularcomplemento").value = _values.titular.complemento;
                        document.getElementById("titularcidade").value = _values.titular.cidades_id;
                        document.getElementById("titularcep").value = _values.titular.cep;

                        modelo = jQuery("#modelo-titular").html();
                        modelo = '<tr>' + modelo + '</tr>';
                        modelo = jQuery(modelo);
                        table = jQuery("table.table-titular");
                        titularinner = table.find('tbody');

                        modelo.find('p').html(html);
                        modelo.find('input,select,textarea,div').each(function () {
                            name = jQuery(this).attr('name');
                            id = jQuery(this).attr('id');
                            name = name.replace("_", "");

                            name = name.replace("[]", "");

                            for (var property in _values.titular) {

                                if (property == name) {
                                    jQuery(this).attr('name', name + '[]');

                                    jQuery(this).attr('id', id);

                                    jQuery(this).html(_values.titular[property]);

                                    if (jQuery(this).is("select")) {
                                        jQuery(this).parent().find('a').remove();
                                    }
                                }
                            }
                        });

                        titularinner.append(modelo);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log('Error ' + xhr.status + ' | ' + thrownError);
                    }
                });

                jQuery.ajax({
                    method: 'GET',
                    url: '/financeiro/contrato/' + Val + '/retornadadosdependentes',
                    success: function (response) {
                        var _values = response;
                        console.table(response);

                        html = "";

                        if (_values === false) {
                            _values = {};
                        } else {
                            _values = jQuery.parseJSON(JSON.stringify(_values));
                        }
                        
                        let count = 0;
                        
                        for (i = 0; i < _values.dependentes.length; i += 1){ 
                            count++;
                            modelo = jQuery("#modelo-dependentes").html();
                            modelo = '<tr>' + modelo + '</tr>';
                            modelo = jQuery(modelo);
                            table = jQuery("table.table-dependentes");
                            dependentesinner = table.find('tbody');

                            modelo.find('p').html(html);
                            modelo.find('input,select,textarea,div').each(function () {
                                name = jQuery(this).attr('name');
                                id = jQuery(this).attr('id');
                                name = name.replace("_", "");

                                name = name.replace("[]", "");

                                for (var property in _values.dependentes[i]) {

                                    if (name == 'num' && property == 'id')
                                        jQuery(this).html(count);

                                    if (property == name) {
                                        jQuery(this).attr('name', name + '[]');

                                        jQuery(this).attr('id', id);


                                        if (name == 'datanascimento'){
                                            jQuery(this).html(_values.dependentes[i][property].substr(8, 2) + "/" + _values.dependentes[i][property].substr(5, 2) + "/" + _values.dependentes[i][property].substr(0, 4));
                                        }else{
                                            jQuery(this).html(_values.dependentes[i][property]);
                                        }
                                        
                                        if (jQuery(this).is("select")) {
                                            jQuery(this).parent().find('a').remove();
                                        }
                                        
                                    }
                                }
                            
                            });
                            dependentesinner.append(modelo);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log('Error ' + xhr.status + ' | ' + thrownError);
                    }
                });

            }

            function getModeloDocumento(obj) {

                var Val = jQuery(obj).val();
                if (Val > 0) {
                    jQuery.ajax({
                        method: 'GET',
                        url: '/financeiro/contrato/' + Val + '/retornamodelodocumento',
                        success: function (response) {
                            document.getElementById("proposta").innerHTML = response.documento.corpodocumento;
                            //console.log(response.documento.corpodocumento);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            console.log('Error ' + xhr.status + ' | ' + thrownError);
                        }
                    });
                }
            }

            function habilitarAtualizacao() {

                getModeloDocumento(document.getElementById("documento_id"));

                getTitularDependentes(document.getElementById("paciente_id"));

                let modelo = document.getElementById("documento_id").value;
                let paciente = document.getElementById("paciente_id").value;
                let data = document.getElementById("data").value;
                let adesao = document.getElementById("valoradesao").value;
                let mensal = document.getElementById("valormensalidade").value;
                let consulta = document.getElementById("valorconsulta").value;
                let retorno = document.getElementById("valorretorno").value;
                let tipo = document.getElementById("tipopagto").value;

                if (modelo > 0 && paciente > 0 && data > "" && adesao > 0 && mensal > 0 && consulta > 0 && retorno > 0 && tipo > '') {
                    jQuery('#btnAtualizar').prop('disabled', false);
                }else{
                    jQuery('#btnAtualizar').prop('disabled', true);
                }

            }

            function printme() {
                window.print();
            }
            
            function setavencimento(val){
                document.getElementById("vencimento").value = val;
                let dt = document.getElementById("data").value;
                if(val != 'OU'){
                    document.getElementById("data").value = dt.substr(0, 8)+val;
                }else{
                    document.getElementById("data").value = dt;
                }
                
                habilitarAtualizacao();
            }
            
        </script>
