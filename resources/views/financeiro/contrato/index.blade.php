@extends('layouts.template')
  
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Contrato</div>
                <div class="card-body">
                    <a href="{{ url('/financeiro/contrato/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar Contrato">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Contrato
                    </a>

                    <form method="GET" action="{{ url('/financeiro/contrato') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Buscar..." value="{{ request('search') }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th><th>Titular</th><th>Data</th><th>Mensalidade</th><th>Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($contrato as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>
                                        @php
                                            $paciente = DB::table('pacientes')
                                                    ->select(DB::raw('nome'))
                                                    ->where('id', '=', $item->paciente_id )
                                                    ->get();

                                            echo isset($paciente) ? $paciente[0]->nome : '--';

                                        @endphp
                                    </td>
                                    <td>{{ isset($item->data) ? date('d/m/Y', strtotime($item->data)) : '--' }}</td>
                                    <td style="text-align: right;">{{ number_format($item->valormensalidade, 2, ',', '.') }}</td>
                                    <td>
                                        <a href="{{ url('/financeiro/contrato/' . $item->id . '/contratoPDF') }}" title="Documento"><button class="btn btn-warning btn-sm rounded"><i class="fa fa-file-text-o fa-1" aria-hidden="true"></i></button></a>
                                        <!-- Button to Open the Modal -->
                                        @if($item->gerado == 'Nao')
                                        <button type="button" class="btn btn-success btn-sm rounded" data-toggle="modal" data-target="#myModal" onclick="setContrato({{$item->id}})">
                                          <i class="fa fa-money" aria-hidden="true"></i>
                                        </button>                                      
                                        @endif
                                        <!-- <a href="{{ url('/financeiro/contrato/' . $item->id) }}" title="Anexar Contrato"><button class="btn btn-warning btn-sm rounded"><i class="fa fa-paperclip" aria-hidden="true"></i></button></a> -->
                                        <a href="{{ url('/financeiro/contrato/' . $item->id) }}" title="Visualizar Contrato"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>
                                        @if($item->gerado == 'Nao')
                                        <a href="{{ url('/financeiro/contrato/' . $item->id . '/edit') }}" title="Editar Contrato"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>

                                        {{ Form::open(['url'=> '/financeiro/contrato' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                            {{ Form::button('<i class="fa fa-trash-o"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title' =>'Excluir', 'role' => 'button', 'type' => 'submit']) }}
                                        {{ Form::close() }}
                                        @else
                                        <a href="{{ url('/financeiro/contrato/' . $item->id . '/copiacontrato') }}" title="Replicar"><button class="btn btn-dark btn-sm rounded"><i class="fa fa-clone fa-1" aria-hidden="true"></i></button></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="pagination-wrapper"> {!! $contrato->appends(['search' => Request::get('search')])->render() !!} </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Gerar Contas a Receber</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
            <div class="row">
                <div class="table-responsive">
                    <input type="hidden" id="idcontrato">
                    <table class="table">
                        <tr>
                            <th>Quantidade de Parcelas</th>
                            <td>
                                <select class="form-control" id="numparcelas">
                                    <option value="12">12</option>
                                    <option value="11">11</option>
                                    <option value="10">10</option>
                                    <option value="9">09</option>
                                    <option value="8">08</option>
                                    <option value="7">07</option>
                                    <option value="6">06</option>
                                    <option value="5">05</option>
                                    <option value="4">04</option>
                                    <option value="3">03</option>
                                    <option value="2">02</option>
                                    <option value="1">01</option>
                                </select>
                            </td>
                        </tr>
                        <tr style="display: none;">
                            <th>Separar a Adesão em um Novo Receber?</th>
                            <td>
                                <input type="radio" value="nao" name="adesao" checked="true"> Não
                                <input type="radio" value="sim" name="adesao"> Sim
                            </td>
                        </tr>
                    </table>
                
                
            </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-sm rounded" data-dismiss="modal" onclick="gerarReceber()">Gerar</button>
        </div>
        
      </div>
    </div>
  </div>

@endsection

@section('jqueryscript')
<script type="text/javascript">
    function setContrato(id){
        document.getElementById("idcontrato").value = id;
    }
    
    function gerarReceber(){
        
        var contrato = document.getElementById("idcontrato").value;
        var parcelas = document.getElementById("numparcelas").value;
        var adesao   = document.querySelector('input[name = adesao]:checked').value;
        
        window.location.href = '/financeiro/contasreceber/'+contrato+','+parcelas+',"'+adesao+'"/contratogerareceber';
    }
</script>
@endsection
