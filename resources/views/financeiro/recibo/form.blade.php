<div class="form-group row {{ $errors->has('descricao') ? 'has-error' : ''}}">
    <label for="descricao" class="col-lg-2 col-md-3 col-sm-12 col-form-label">{{ 'Descricao' }}</label>
    <div class="col-lg-5 col-md-6 col-sm-12">
    <textarea class="form-control" rows="5" name="descricao" type="textarea" id="descricao" >{{ isset($recibo->descricao) ? $recibo->descricao : ''}}</textarea>
</div>
    {!! $errors->first('descricao', '<p class="help-block">:message</p>') !!}
</div>

<br/>
    
<div class="form-group  offset-md-5">
    <a class="btn btn-warning rounded" href="{{ url('/financeiro/recibo') }}"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</a>
    <button class="btn btn-primary rounded" type="submit"><i class="fa fa-check fa-1" aria-hidden="true"></i> {{ $formMode === 'edit' ? 'Editar' : 'Salvar' }}</button>
</div>
