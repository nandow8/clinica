@extends('layouts.template')

@section('content')
  @php
    date_default_timezone_set('America/Sao_Paulo');
    $hoje = request('dia') !== null ? request('dia') : date('Y-m-d');

    $d = strtotime($hoje);
    $d0 = date('d/m', strtotime('+0 days', $d));
    $d1 = date('d/m', strtotime('+1 days', $d));
    $d2 = date('d/m', strtotime('+2 days', $d));
    $d3 = date('d/m', strtotime('+3 days', $d));
    $d4 = date('d/m', strtotime('+4 days', $d));
    $d5 = date('d/m', strtotime('+5 days', $d));
    $d6 = date('d/m', strtotime('+6 days', $d));
    $d7 = date('d/m', strtotime('+7 days', $d));

  @endphp

    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Fluxo de Caixa</div>
                <div class="card-body">
                    <!-- <a href="{{ url('/financeiro/fluxocaixa/create') }}" class="btn btn-success btn-sm rounded" title="Adicionar Fluxocaixa">
                        <i class="fa fa-plus" aria-hidden="true"></i> Adicionar Fluxocaixa
                    </a> -->
                    <button id="btnImprimir" type="button" class="rounded btn btn-info">Imprimir</button>
                    <form method="GET" action="{{ url('/financeiro/fluxocaixa') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                        <div class="input-group">
                            <input type="date" class="form-control" name="dia" value="{{ $hoje }}">
                            <span class="input-group-append">
                                <button class="btn btn-secondary rounded" type="submit">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                    </form>

                    <br/>
                    <br/>

                    <div id="impressao">
                      <table class="table" border="1">
                          <thead>
                            <tr>
                              <td colspan="10">A Realizar</td>
                            </tr>
                            <tr style="font: 1.0em currier;">
                                <th>#</th>
                                <th width="250px">Descrição</th>
                                <th style="text-align: center;">{{$d0}}</th>
                                <th style="text-align: center;">{{$d1}}</th>
                                <th style="text-align: center;">{{$d2}}</th>
                                <th style="text-align: center;">{{$d3}}</th>
                                <th style="text-align: center;">{{$d4}}</th>
                                <th style="text-align: center;">{{$d5}}</th>
                                <th style="text-align: center;">{{$d6}}</th>
                                <th style="text-align: center;">{{$d7}}</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php
                              $Dia00 = 0;
                              $Dia01 = 0;
                              $Dia03 = 0;
                              $Dia02 = 0;
                              $Dia04 = 0;
                              $Dia05 = 0;
                              $Dia06 = 0;
                              $Dia07 = 0;
                            @endphp
                            @foreach($fluxocaixa as $item)
                              @if(in_array($item->status, ['Aberto', 'Atraso']))
                              <tr style="font: 0.9em currier; background-color: {{ $item->tipo == 'Pagar' ? '#fff2e6' : '#e6ffe6' }}">
                                  <td>{{ $loop->iteration }}</td>
                                  <td>{{ $item->grupo }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia00, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia01, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia02, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia03, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia04, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia05, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia06, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia07, 2, ',', '.') }}</td>
                              </tr>
                                @php
                                  $Dia00 = $Dia00 + $item->Dia00;
                                  $Dia01 = $Dia01 + $item->Dia01;
                                  $Dia02 = $Dia02 + $item->Dia02;
                                  $Dia03 = $Dia03 + $item->Dia03;
                                  $Dia04 = $Dia04 + $item->Dia04;
                                  $Dia05 = $Dia05 + $item->Dia05;
                                  $Dia06 = $Dia06 + $item->Dia06;
                                  $Dia07 = $Dia07 + $item->Dia07;
                                @endphp
                              @endif
                            @endforeach
                          </tbody>
                          <tfoot>
                            <tr style="font: 0.9em currier; font-weight: bold;">
                                <td colspan="2">Totais</td>
                                <td style="text-align: right;">{{ number_format($Dia00, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia01, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia02, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia03, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia04, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia05, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia06, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia07, 2, ',', '.') }}</td>
                            </tr>
                          </tfoot>
                      </table>
                      <table class="table" border="1">
                          <thead>
                            <tr>
                              <td colspan="10">Realizados</td>
                            </tr>
                            <tr style="font: 1.0em currier;">
                                <th>#</th>
                                <th width="250px">Descrição</th>
                                <th style="text-align: center;">{{$d0}}</th>
                                <th style="text-align: center;">{{$d1}}</th>
                                <th style="text-align: center;">{{$d2}}</th>
                                <th style="text-align: center;">{{$d3}}</th>
                                <th style="text-align: center;">{{$d4}}</th>
                                <th style="text-align: center;">{{$d5}}</th>
                                <th style="text-align: center;">{{$d6}}</th>
                                <th style="text-align: center;">{{$d7}}</th>
                            </tr>
                          </thead>
                          <tbody>
                            @php
                              $Dia00 = 0;
                              $Dia01 = 0;
                              $Dia03 = 0;
                              $Dia02 = 0;
                              $Dia04 = 0;
                              $Dia05 = 0;
                              $Dia06 = 0;
                              $Dia07 = 0;
                            @endphp
                            @foreach($fluxocaixa as $item)
                              @if(in_array($item->status, ['Pago', 'Recebido']))
                              <tr style="font: 0.9em currier; background-color: {{ $item->tipo == 'Pagar' ? '#fff2e6' : '#e6ffe6' }}">
                                  <td>{{ $loop->iteration }}</td>
                                  <td>{{ $item->grupo }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia00, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia01, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia02, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia03, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia04, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia05, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia06, 2, ',', '.') }}</td>
                                  <td style="text-align: right;">{{ number_format($item->Dia07, 2, ',', '.') }}</td>
                              </tr>
                                @php
                                  $Dia00 = $Dia00 + $item->Dia00;
                                  $Dia01 = $Dia01 + $item->Dia01;
                                  $Dia02 = $Dia02 + $item->Dia02;
                                  $Dia03 = $Dia03 + $item->Dia03;
                                  $Dia04 = $Dia04 + $item->Dia04;
                                  $Dia05 = $Dia05 + $item->Dia05;
                                  $Dia06 = $Dia06 + $item->Dia06;
                                  $Dia07 = $Dia07 + $item->Dia07;
                                @endphp
                              @endif
                            @endforeach
                          </tbody>
                          <tfoot>
                            <tr style="font: 0.9em currier; font-weight: bold;">
                                <td colspan="2">Totais</td>
                                <td style="text-align: right;">{{ number_format($Dia00, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia01, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia02, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia03, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia04, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia05, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia06, 2, ',', '.') }}</td>
                                <td style="text-align: right;">{{ number_format($Dia07, 2, ',', '.') }}</td>
                            </tr>
                          </tfoot>
                      </table>
                    </div>

                </div>
            </div>
        </div>
    </div>


@endsection

@section('jqueryscript')
<script>

  document.getElementById('btnImprimir').onclick = function() {
    window.print();
  };

</script>
@endsection
