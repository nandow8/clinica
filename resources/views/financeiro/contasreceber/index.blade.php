@extends('layouts.template')

@section('content')
<div class="row">

    <div class="col-md-12">
        <div class="card">
            <div class="card-header">Contas a Receber</div>
            <div class="card-body">
                <div class="card-header">
                    <a class="btn btn-success btn-sm rounded" href="{{ url('/financeiro/contasreceber/create') }}"  title="Adicionar Contas a receber">
                        <i class="fa fa-plus fa-2" aria-hidden="true"></i><br>Adicionar
                    </a>
<!--
                    <a class="btn btn-dark btn-sm rounded"href="{{ url('/financeiro/contasreceber/' . 'orcar' . '/createorcamento') }}" title="Adicionar Orçamento">
                        <i class="fa fa-percent fa-2" aria-hidden="true"></i><br>Orçamento
                    </a>
-->
                    <a class="btn btn-info btn-sm rounded" href="{{ url('/financeiro/contasreceber/' . 'envio' . '/remessa') }}" title="Remessa">
                        <i class="fa fa-share-square-o fa-2"></i><br>Remessa
                    </a>
                    <a class="btn btn-warning btn-sm rounded" href="{{ url('/financeiro/contasreceber/' . 'recebo' . '/retorno') }}" title="Retorno">
                        <i class="fa fa-arrow-circle-down fa-2"></i><br>Retorno
                    </a>
                    <button type="button" class="btn btn-dark btn-sm rounded" style="width: 100px" data-toggle="button" aria-pressed="false" autocomplete="off" onclick="pesquisa()" id="toglepesquisa">
                        <i class="fa fa-filter fa-2"></i><br>Filtros
                    </button>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <form method="GET" action="{{ url('/financeiro/contasreceber') }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <tr>
                                <th colspan="8">
                                    <div class="container" style="display: none;" id="tabpesquisa">
                                        <div class="row" style="border: #6E6E6E 1px solid; padding: 20px 0px 20px 0px; border-radius: 10px">
                                            <br>
                                            <div class="col-3">
                                                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                    <a class="nav-link active" id="v-pills-cliente-tab" data-toggle="pill" href="#v-pills-cliente" role="tab" aria-controls="v-pills-cliente" aria-selected="true">Clientes</a>
                                                    <a class="nav-link" id="v-pills-tipo-tab" data-toggle="pill" href="#v-pills-tipo" role="tab" aria-controls="v-pills-tipo" aria-selected="false">Tipo de Pagamento</a>
                                                    <a class="nav-link" id="v-pills-data-tab" data-toggle="pill" href="#v-pills-data" role="tab" aria-controls="v-pills-data" aria-selected="false">Data</a>
                                                    <a class="nav-link" id="v-pills-status-tab" data-toggle="pill" href="#v-pills-status" role="tab" aria-controls="v-pills-status" aria-selected="false">Status</a>
                                                </div>
                                            </div>
                                            <div class="col-9">
                                                <div class="tab-content" id="v-pills-tabContent">
                                                    <div class="tab-pane fade show active" id="v-pills-cliente" role="tabpanel" aria-labelledby="v-pills-cliente-tab">
                                                        <div class="form-check">                                                        
                                                            <input class="form-check-input" type="radio" name="tipopaciente" id="tipopaciente1" value="A" checked>
                                                            <label class="form-check-label" for="tipopaciente1">
                                                                Todos
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="tipopaciente" id="tipopaciente2" value="T">
                                                            <label class="form-check-label" for="tipopaciente2">
                                                                Titular
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="tipopaciente" id="tipopaciente3" value="D">
                                                            <label class="form-check-label" for="tipopaciente3">
                                                                Dependente
                                                            </label>
                                                        </div>
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="radio" name="tipopaciente" id="tipopaciente4" value="P">
                                                            <label class="form-check-label" for="tipopaciente4">
                                                                Particular
                                                            </label>
                                                        </div>
                                                        <br>
                                                        <input type="text" name="schnome" onkeyup="autocompletanomepaciente(this)" id="schnome" class="form-control" placeholder="Busca por (Nome ou Prontuário)..." value="{{ request('schnome') }}">
                                                        <input type="hidden" name="schclienteid" id="schclienteid" class="form-control" value="{{ request('schclienteid') }}">
                                                    </div>
                                                    <div class="tab-pane fade" id="v-pills-tipo" role="tabpanel" aria-labelledby="v-pills-tipo-tab">
                                                        <br>
                                                        @foreach ($tipospagamentos as $optionKey => $optionValue)
                                                            <div class="col-md-5 col-sm-5 col-xs-5">
                                                                <input type="checkbox" name="schtipopagto[]" value="{{ $optionValue->id }}" class="form-horizontal"
                                                                    <?php
                                                                        if(is_array(request('schtipopagto'))){
                                                                            foreach(request('schtipopagto') as $k =>$i){
                                                                                if(request('schtipopagto')[$k] == $optionValue->id ) {
                                                                                    echo 'checked';
                                                                                }
                                                                            }
                                                                        }
                                                                    ?>
                                                                >&nbsp;{{ $optionValue->descricao }}&nbsp;<br>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                    <div class="tab-pane fade" id="v-pills-data" role="tabpanel" aria-labelledby="v-pills-data-tab">
                                                        <div class="form-inline">
                                                            <input type="radio" name="schpordata" value="vecto" class="form-horizontal" {{ request('schpordata') == 'vecto' ? 'checked="checked"' : '' }} >&nbsp;Vencimento&nbsp;
                                                            <input type="radio" name="schpordata" value="pagto" class="form-horizontal" {{ request('schpordata') == 'pagto' ? 'checked="checked"' : '' }} >&nbsp;Pagamento&nbsp;
                                                        </div>
                                                        <br>
                                                        <div class="form-inline">
                                                            <input type="date" class="form-control form-control-sm" name="schdataini" value="{{ request('schdataini') }}">
                                                            &nbsp;&agrave;&nbsp;
                                                            <input type="date" class="form-control form-control-sm" name="schdatafim" value="{{ request('schdatafim') }}">
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="v-pills-status" role="tabpanel" aria-labelledby="v-pills-status-tab">
                                                        <br>
                                                        <select name="schstatus" class="form-control form-control-sm">
                                                            @foreach (json_decode('{"":"Todos...","Aberto": "Aberto", "Recebido": "Recebido", "Atraso": "Atraso"}', true) as $optionKey => $optionValue)
                                                            <option value="{{ $optionKey }}" {{ (request('schstatus') == $optionKey) ? 'selected' : '' }}>{{ $optionValue }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-12" style="text-align: left;">
                                                <span class="input-group-append">
                                                    <button class="btn btn-secondary rounded" type="submit">
                                                        <i class="fa fa-search"></i> Perquisar 
                                                    </button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </th>
                            </tr>
                        </form>
                        <tr role='row'>
                            <th class="sorting" tabindex="0" aria-controls="users-table" rowspan="1" colspan="1" aria-label="Id: activate to sort column ascending" style="width: 18px;">#</th>
                            <th>Cliente</th><th>Tipo<br>Valor</th><th>Numero<br>Parcela</th><th>Vencimento<br>Pagamento</th><th>Status</th><th>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($contasreceber as $item)
                            <tr>
                                <td>{{ $item->id }}</td>
                                <td>
                                    @php
                                        $paciente = DB::table('pacientes')
                                                    ->select(DB::raw('nome'))
                                                    ->where('id', '=', $item->paciente_id )
                                                    ->where('status', '=', 'Ativo' )
                                                    ->get();
                                        
                                        if ($item->paciente_id == 1){
                                            echo '<a href="/financeiro/contasreceber/' . $item->id . '" title="Visualizar Contas a Receber" class="text-info">ORÇAMENTO</a>';
                                        }else{
                                            echo '<a href="/financeiro/contasreceber/' . $item->id . '" title="Visualizar Contas a Receber" class="text-info">' . (isset($paciente[0]) ? $paciente[0]->nome : '--') .'</a>';
                                        }
                                    @endphp
                                    @if (isset($item->orcamento))
                                        <br><span class="content rounded btn-warning">{{ $item->orcamento }}</span>
                                    @endif
                                </td>
                                <td>
                                    @php
                                        $pagto = DB::table('tipos_pagamentos')
                                                    ->select(DB::raw('descricao'))
                                                    ->where('id', '=', $item->tipopagto_id  )
                                                    ->where('status', '=', 'Ativo' )
                                                    ->get();

                                        echo isset($pagto[0]) ? $pagto[0]->descricao : '---';
                                    @endphp
                                    <br>
                                    {{ number_format($item->valor, 2, ',', '.') }}
                                </td>
                                <td>
                                    {{ $item->numerodocumento }} 
                                    &nbsp;/&nbsp;
                                    {{ $item->numero }}
                                </td>
                                <td>{{ date('d/m/Y', strtotime($item->vencimento)) }}<br>{{ isset($item->pagamento) ? date('d/m/Y', strtotime($item->pagamento)) : '--' }}</td>
                                <td>{{ $item->status }}</td>
                                <td >
                                    <a href="{{ url('/financeiro/contasreceber/' . $item->id . '/receberPDF') }}" title="Documento"><button class="btn btn-warning btn-sm rounded"><i class="fa fa-file-text-o fa-1" aria-hidden="true"></i></button></a>
                                    @if($item->tipopagto == 'BO')
                                    <a href="{{ url('/financeiro/contasreceber/' . $item->id . '/boleto') }}" title="Boleto Contas a Receber"><button class="btn btn-success btn-sm rounded"><i class="fa fa-file-pdf-o fa-1" aria-hidden="true"></i></button></a>
                                    <!--<a href="{{ url('/financeiro/contasreceber/wssantander/' . $item->id . '/boleto') }}" title="Boleto Contas a Receber"><button class="btn btn-warning btn-sm rounded"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></button></a>-->        
                                    @endif    
                                    <!--<a href="{{ url('/financeiro/contasreceber/' . $item->id) }}" title="Visualizar Contas a Receber"><button class="btn btn-info btn-sm rounded"><i class="fa fa-eye" aria-hidden="true"></i></button></a>-->
                                    <a href="{{ url('/financeiro/contasreceber/' . $item->id . '/edit') }}" title="Editar Contas a Receber"><button class="btn btn-primary btn-sm rounded"><i class="fa fa-pencil-square-o fa-1" aria-hidden="true"></i></button></a>

                                    {{ Form::open(['url'=> '/financeiro/contasreceber' . '/' . $item->id, 'method' => 'delete', 'class' => 'form-delete','style'=> 'display:inline']) }}
                                    {{ Form::button('<i class="fa fa-trash-o fa-1"></i>', ['class' => 'btn btn-danger btn-sm rounded', 'title' =>'Excluir', 'role' => 'button', 'type' => 'submit']) }}
                                    {{ Form::close() }}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination-wrapper"> 
                        {!! $contasreceber->appends([
                                'schnome'       => Request::get('schnome'),
                                'schclienteid'  => Request::get('schclienteid'),
                                'schtipopagto'  => Request::get('schtipopagto'),
                                'schpordata'    => Request::get('schpordata'),
                                'schdataini'    => Request::get('schdataini'),
                                'schdatafim'    => Request::get('schdatafim'),
                                'schstatus'     => Request::get('schstatus')
                            ])->render() 
                        !!} 
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
&ac
@section('jqueryscript')

<script>
    
    jQuery("#schnome").on("dblclick", function(entry) {
        jQuery("#schnome").val("")
        jQuery("#schclienteid").val("")
    });
    
    function pesquisa() {
        var botao = document.getElementById("toglepesquisa").getAttribute('aria-pressed');

        if (botao == 'true') {
            document.getElementById("tabpesquisa").style.display = "none";
        } else {
            document.getElementById("tabpesquisa").style.display = "block";
        }
    }

    function autocompletanomepaciente(obj){
        let val = jQuery(obj).val()
        
        let tod = jQuery('#tipopaciente1').is(":checked")
        let tit = jQuery('#tipopaciente2').is(":checked")
        let dep = jQuery('#tipopaciente3').is(":checked")
        let par = jQuery('#tipopaciente4').is(":checked")
        
        let cob = 0 
        let tip = 0 
        
        if(tod){
            cob = '*'
            tip = '*'
        }
        
        if(tit){
            cob = '0'
            tip = '1'
        }
        
        if(dep){
            cob = '0'
            tip = '0'
        }

        if(par){
            cob = '1'
            tip = '*'
        }
        
        const isAlphaNumeric = ch => {
        	return ch.match(/^[a-z0-9]+$/i) !== null;
        }
        
        if(isAlphaNumeric(val)){
          jQuery.getJSON('/financeiro/contasreceber/ajaxindexnomepaciente/' + val + '/' + cob+ '/' + tip , function (data) {
            jQuery("#schnome").autocomplete({
                source: function(request, response){
                    response(data)
                },
                select: function (e, ui) {
                    cod = ui.item.label.split("---")[0].trim()
                    nom = ui.item.label.split("---")[1].trim()
                    nom = nom !== '' ? nom : ''
                    jQuery('#schclienteid').val(cod)
                    jQuery('#schnome').val(nom)
                    
                },
                change: function (e, ui) {
                    cod = ui.item.label.split("---")[0].trim()
                    nom = ui.item.label.split("---")[1].trim()
                    nom = nom !== '' ? nom : ''
                    jQuery('#schclienteid').val(cod)
                    jQuery('#schnome').val(nom)
                }
            });
        });
    }
  }

</script>
@endsection
