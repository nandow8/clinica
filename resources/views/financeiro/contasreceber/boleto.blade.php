@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Boleto do Contas a Receber</div>
                <div class="card-body">

                    <a href="{{ url('/financeiro/contasreceber') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>

                    <br/>
                    <br/>
                    <h3>{{ $contasreceber->cliente }}</h3>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                @php

                                    $beneficiario = new  \Eduardokum\LaravelBoleto\Pessoa;

                                    // Santander
                                    $beneficiario = new \Eduardokum\LaravelBoleto\Pessoa(
                                        [
                                            'nome'      => $clinica->razao,
                                            'endereco'  => $clinica->endereco,
                                            'cep'       => $clinica->cep,
                                            'uf'        => $clinica->uf,
                                            'cidade'    => $clinica->cidade,
                                            'documento' => $clinica->cnpj,
                                        ]
                                    );

                                    $instrucao1 = $clinica->instrucao1;
                                    $instrucao2 = $clinica->instrucao2;
                                    $instrucao3 = $clinica->instrucao3;

                                    $pagador = new \Eduardokum\LaravelBoleto\Pessoa(
                                        [
                                            'nome'      => $paciente->nome,
                                            'endereco'  => $paciente->rua . (isset( $paciente->numero) ? ', ' . $paciente->numero : '') . (isset($paciente->complemento) ? ' - ' . $paciente->complemento : ''),
                                            'bairro'    => $paciente->bairros_id,
                                            'cep'       => $paciente->cep,
                                            'uf'        => $paciente->estados_id,
                                            'cidade'    => $paciente->cidades_id,
                                            'documento' => $paciente->cpf,
                                        ]
                                    );

                                    // Pega linhas do campo descricao.
                                    $lines = explode(PHP_EOL, $contasreceber->descricao);

                                    $demonstrativo1 = isset($lines[0]) ? $lines[0] : '';
                                    $demonstrativo2 = '__________________________________________________________________________________________________________';
                                    $demonstrativo3 = 'Detalhe da Conta:';
                                    $demonstrativo4 = '| ';
                                    $demonstrativo5 = '__________________________________________________________________________________________________________';

                                    foreach($detalhes as $k => $r){
                                        $demonstrativo4 = $demonstrativo4 . $r['descricao'] . ' - R$: ' . $r['valor'] . ' | ';
                                    }
                                    
                                    $boleto = new Eduardokum\LaravelBoleto\Boleto\Banco\Santander(
                                        [
                                            'logo'                   => 'img/logoboleto-fundobranco.jpeg',
                                            'dataVencimento'         => new \Carbon\Carbon(\Carbon\Carbon::createFromFormat('Y-m-d', $contasreceber->vencimento)->toDateTimeString()),
                                            'valor'                  => $contasreceber->valor,
                                            'multa'                  => $contasreceber->multa,
                                            'juros'                  => $contasreceber->juros,
                                            'numero'                 => $contasreceber->numero,
                                            'numeroDocumento'        => $contasreceber->numerodocumento.'',
                                            'pagador'                => $pagador,
                                            'beneficiario'           => $beneficiario,
                                            'diasBaixaAutomatica'    => 15,
                                            'carteira'               => $clinica->carteira,
                                            'agencia'                => $clinica->agencia,
                                            'conta'                  => $clinica->conta,
                                            'codigoCliente'          => $clinica->codigocliente,
                                            'descricaoDemonstrativo' => [$demonstrativo1, $demonstrativo2, $demonstrativo3, $demonstrativo4, $demonstrativo5],
                                            'instrucoes'             => [$clinica->instrucao1, $clinica->instrucao2, $clinica->instrucao3],
                                            'aceite'                 => 'N',
                                            'especieDoc'             => 'DM',
                                        ]
                                    );

                                    $pdf = new Eduardokum\LaravelBoleto\Boleto\Render\Pdf();
                                    $pdf->addBoleto($boleto);

                                    date_default_timezone_set('America/Sao_Paulo');
                                    $date = date('YmdHisU');
                                    
                                    $arquivo = 'boletos/santander_boleto_'.$date.'_nro-' . $contasreceber->id . '.pdf';

                                    if(file_exists($arquivo)){
                                        unlink($arquivo);
                                    }
                                    
                                    $pdf->gerarBoleto($pdf::OUTPUT_SAVE, $arquivo);

                                @endphp

                                <!--<tr><th>&nbsp;</th><td> <a href="{{ asset($arquivo) }}" title="Boleto"><i class="fa fa fa-barcode fa-2x"> <img src="{{ asset('boletos/logos/033.png') }}"/></i></a> </td></tr>-->
                                <tr><th>&nbsp;</th><td> <embed src="{{ asset($arquivo) }}" frameborder="1" width="100%" height="500px"> </td></tr>

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
