<?php

  use TIExpert\WSBoletoSantander\Boleto;
  use TIExpert\WSBoletoSantander\BoletoSantanderServico;
  use TIExpert\WSBoletoSantander\ComunicadorCurlSOAP;
  use TIExpert\WSBoletoSantander\Convenio;
  use TIExpert\WSBoletoSantander\InstrucoesDeTitulo;
  use TIExpert\WSBoletoSantander\Pagador;
  use TIExpert\WSBoletoSantander\Titulo;

  /* Informações do Convênio */
  $convenio = new Convenio();
  $convenio->setCodigoBanco("0033");
  $convenio->setCodigoConvenio($clinica->codigocliente);

  /* informações do Pagador */
  $pagador = new Pagador(
      $contasreceber->tipopagto,
      $contasreceber->numerodocumento,
      $paciente->nome,
      $paciente->rua . ', ' . $paciente->numero . (isset($paciente->complemento)? ' - ' . $paciente->complemento : ''),
      $paciente->bairros_id,
      $paciente->cidades_id,
      $paciente->estados_id,
      $paciente->cep
  );

  /* Instruções do Titulo */
  $instrucoes = new InstrucoesDeTitulo();
  $instrucoes->setMulta($clinica->multa);
  $instrucoes->setmultarApos(null);
  $instrucoes->setJuros($clinica->juros);
  $instrucoes->setTipoDesconto("0");
  $instrucoes->setValorDesconto("0");
  $instrucoes->setDataLimiteDesconto(null);
  $instrucoes->setValorAbatimento(null);
  $instrucoes->setTipoProtesto("0");
  $instrucoes->setProtestarApos(null);
  $instrucoes->setBaixarApos("0");
  $instrucoes->setTipoPagamento(1);
  $instrucoes->setQtdParciais(0);
  $instrucoes->setTipoValor(1);
  $instrucoes->setPercentualMinimo(0);
  $instrucoes->setPercentualMaximo(0);

  /* Cria o Titulo */
  $titulo = new Titulo();
  $titulo->setValor($contasreceber->valor);
  $titulo->setNossoNumero($contasreceber->numerodocumento);
  $titulo->setSeuNumero(null);
  $titulo->setDataVencimento(date('dmY', strtotime($contasreceber->vencimento)));
  $titulo->setDataEmissao(date('dmY', strtotime($contasreceber->created_at)));
  $titulo->setMensagem('$contasreceber->numerodocumento');
  $titulo->setEspecie($contasreceber->numerodocumento);
  $titulo->setInstrucoes($instrucoes);

  /* Monta o boleto */
  $boleto = new Boleto($convenio, $pagador, $titulo);

  /* Registra o Boleto */
  $comunicador = new ComunicadorCurlSOAP ();
  $svc = new BoletoSantanderServico($comunicador);

  /* Gera um Tícket */
  $ticket = $svc->solicitarTicketInclusao($boleto);

  //Remover
  //dd($convenio, $clinica, $paciente, $contasreceber, $detalhes, $pagador, $instrucoes, $titulo, $boleto, $ticket);

?>
