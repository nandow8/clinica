@extends('layouts.template')

@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Visualizar Contas a Receber</div>
                <div class="card-body">

                    <a href="{{ url('/financeiro/contasreceber') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    
                    <br/>
                    <br/>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <?php /*
                                    <tr>
                                        <th>ID</th><td>{{ $contasreceber->id }}</td>
                                    </tr>
                                */ ?>
                                <tr><th> Cliente Id </th><td> {{ $contasreceber->cliente_id }} </td></tr><tr><th> Cliente </th><td> {{ $contasreceber->cliente }} </td></tr><tr><th> Vencimento </th><td> {{ $contasreceber->vencimento }} </td></tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
