<!doctype html>
<html>

<head>
    <title>{{ $title }}</title>
    <style>
        .page-break {
            page-break-after: always;
        }

        body {
            margin-top: 40px;
            font-size: 14px;
            font-family: Arial, Helvetica, sans-serif;
        }

        table { 
            border-spacing: 0px;
        }

        table, th, td {
            border: 1px solid #f1f1f1;
            text-align: left;
            height: 25px;
            vertical-align: central;
        }
    </style>    
</head>
<body>
    
    <body>
        <table class="table">
            <tbody>
                <tr>
                    <th colspan="2">
                        <p>
                        <img src="{{ public_path('/img/contasreceber/orcamento.jpeg') }}" width="100%">
                        </p>
                    </th>
                </tr>
                <tr>
                    <th>Paciente:</th><td>{{ isset($paciente[0]->nome) ? $paciente[0]->nome : 'Orçamento' }}</td>
                </tr>
                @if(isset($receber->orcamento))
                <tr>
                    <th>Nome:</th><td>{{ $receber->orcamento }}</td>
                </tr>
                @endif
                <tr>
                    <th>Data:</th><td>{{ date('d/m/Y', strtotime($receber->vencimento)) }}</td>
                </tr>
                <tr>
                    <th>Valor:</th><td style="text-align: left;">{{ number_format($receber->valor, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    <th>Descrição:</th><td> {{ $receber->descricao }}</td>
                </tr>
            </tbody>
        </table>
        <?php 
            $valores = 0; 
            $descontos = 0;
        ?>
        <p>&nbsp;</p>
        <table class="table">
            <thead>
                <tr>
                    <th>Procedimento:</th><th>Valor</th>
                </tr>
            </thead>
            <tbody>

                @foreach($detalhe as $item)
                <tr>
                    <td>{{ $item->descricao }}</td><td style="text-align: right;">{{ number_format($item->valor, 2, ',', '.') }}</td>
                    <?php $valores += $item->valor; ?>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <td>Subtotal</td><td style="text-align: right;">{{ number_format($valores, 2, ',', '.') }}</td>
                </tr>
                <tr>
                    @foreach($desconto as $item)
                        <?php $descontos += $item->valor; ?>
                    @endforeach    
                    @if ($descontos > 0)
                        <td>Descontos:</td><td style="text-align: right;">{{ number_format($descontos, 2, ',', '.') }}%</td>
                    @endif
                </tr>
                <tr>
                    @if ($descontos > 0)
                        <td>Total</td><td style="text-align: right;">{{ number_format(($valores - $valores * $descontos / 100), 2, ',', '.') }}</td>
                    @else
                        <td>Total</td><td style="text-align: right;">{{ number_format($valores, 2, ',', '.') }}</td>
                    @endif
                </tr>
            </tfoot>
        </table>
    </body>
</html>