<?php
    //require 'autoload.php';

    $clinica = DB::select("SELECT * FROM clinicas WHERE id = 1");

    $clinica = $clinica[0];    
    
    $dados = DB::select(
            "SELECT 
                r.vencimento, r.pagamento, r.valor, r.numero, r.numerodocumento, 
                c.razao, c.endereco, c.cep, c.uf, c.cidade, c.cnpj, c.instrucao1, c.instrucao2, c.instrucao3, 
                c.agencia, c.carteira, c.conta, c.codigocliente, 
                p.nome, p.rua, p.numero, p.complemento, p.bairros_id, p.cep, p.estados_id, p.cidades_id, p.cpf 
            FROM contasrecebers  r
            INNER JOIN clinicas  c on (r.clinica_id = c.id)
            INNER JOIN pacientes p on (r.paciente_id = p.id)
            WHERE r.tipopagto = 'BO' and r.remessa IS NULL and r.retorno IS NULL
            ");
  
    $remessa = new \Eduardokum\LaravelBoleto\Cnab\Remessa\Cnab400\Banco\Santander(
        [
        'agencia'       => $clinica->agencia,
        'carteira'      => $clinica->carteira,
        'conta'         => $clinica->conta,
        'codigoCliente' => $clinica->codigocliente,
        'beneficiario'  => ['nome'      => $clinica->razao,
                            'endereco'  => $clinica->endereco,
                            'cep'       => $clinica->cep,
                            'uf'        => $clinica->uf,
                            'cidade'    => $clinica->cidade,
                            'documento' => $clinica->cnpj,
                           ],
        ]
    );
        
    foreach($dados as $item){
        
        // Santander
        $beneficiario = new \Eduardokum\LaravelBoleto\Pessoa(
            [
                'nome'      => $item->razao,
                'endereco'  => $item->endereco,
                'cep'       => $item->cep,
                'uf'        => $item->uf,
                'cidade'    => $item->cidade,
                'documento' => $item->cnpj,
            ]
        );

        $instrucao1 = $item->instrucao1;
        $instrucao2 = $item->instrucao2;
        $instrucao3 = $item->instrucao3;

        $pagador = new \Eduardokum\LaravelBoleto\Pessoa(
            [
                'nome'      => $item->nome,
                'endereco'  => $item->rua . (isset( $item->numero) ? ', ' . $item->numero : '') . (isset($item->complemento) ? ' - ' . $item->complemento : ''),
                'bairro'    => $item->bairros_id,
                'cep'       => $item->cep,
                'uf'        => $item->estados_id,
                'cidade'    => $item->cidades_id,
                'documento' => $item->cpf,
            ]
        );
        
        $boleto = new Eduardokum\LaravelBoleto\Boleto\Banco\Santander(
            [
                'logo'                   => realpath(__DIR__ . '/../logos/') . DIRECTORY_SEPARATOR . '033.png',
                'dataVencimento'         => new \Carbon\Carbon(),
                'valor'                  => $item->valor,
                'multa'                  => false, //$item->multa,
                'juros'                  => false, //$item->juros,
                'numero'                 => $item->numero,
                'numeroDocumento'        => $item->numerodocumento.'',
                'pagador'                => $pagador,
                'beneficiario'           => $beneficiario,
                'diasBaixaAutomatica'    => 15,
                'carteira'               => $item->carteira,
                'agencia'                => $item->agencia,
                'conta'                  => $item->conta,
                'descricaoDemonstrativo' => ['demonstrativo 1', 'demonstrativo 2', 'demonstrativo 3'],
                'instrucoes'             => [$instrucao1, $instrucao2, $instrucao3],
                'aceite'                 => 'N',
                'especieDoc'             => 'DM',
            ]
        );

        $remessa->addBoleto($boleto);
        
    }

    echo $remessa->save(__DIR__ . DIRECTORY_SEPARATOR . 'arquivos' . DIRECTORY_SEPARATOR . 'santander0001.txt');
