<script src="https://code.jquery.com/jquery-1.9.1.js"></script>


@extends('layouts.template')

@section('content')
<?php
    $arquivo = 'remessa/CAP-Remessa' . date('Y-m-d') . '.txt';

    $clinica = DB::select("SELECT * FROM clinicas WHERE id = 1");

    $clinica = $clinica[0];    
    
    $result = DB::select(
            "SELECT 
                r.vencimento, r.pagamento, r.valor, r.numero, r.numerodocumento, 
                c.razao, c.endereco, c.cep, c.uf, c.cidade, c.cnpj, c.instrucao1, c.instrucao2, c.instrucao3, 
                c.agencia, c.carteira, c.conta, c.codigocliente, 
                p.nome, p.rua, p.numero, p.complemento, p.bairros_id, p.cep, p.estados_id, p.cidades_id, p.cpf 
            FROM contasrecebers  r
            INNER JOIN clinicas  c on (r.clinica_id = c.id)
            INNER JOIN pacientes p on (r.paciente_id = p.id)
            WHERE r.tipopagto = 'BO' and r.remessa = 'CH' 
            ");
  
    if(!empty($result)){

        $remessa = new \Eduardokum\LaravelBoleto\Cnab\Remessa\Cnab400\Banco\Santander(
            [
            'agencia'       => $clinica->agencia,
            'carteira'      => $clinica->carteira,
            'conta'         => $clinica->conta,
            'codigoCliente' => $clinica->codigocliente,
            'beneficiario'  => ['nome'      => $clinica->razao,
                                'endereco'  => $clinica->endereco,
                                'cep'       => $clinica->cep,
                                'uf'        => $clinica->uf,
                                'cidade'    => $clinica->cidade,
                                'documento' => $clinica->cnpj,
                               ],
            ]
        );


        foreach($result as $item){

            // Santander
            $beneficiario = new \Eduardokum\LaravelBoleto\Pessoa(
                [
                    'nome'      => $item->razao,
                    'endereco'  => $item->endereco,
                    'cep'       => $item->cep,
                    'uf'        => $item->uf,
                    'cidade'    => $item->cidade,
                    'documento' => $item->cnpj,
                ]
            );

            $instrucao1 = $item->instrucao1;
            $instrucao2 = $item->instrucao2;
            $instrucao3 = $item->instrucao3;

            $pagador = new \Eduardokum\LaravelBoleto\Pessoa(
                [
                    'nome'      => $item->nome,
                    'endereco'  => $item->rua . (isset( $item->numero) ? ', ' . $item->numero : '') . (isset($item->complemento) ? ' - ' . $item->complemento : ''),
                    'bairro'    => $item->bairros_id,
                    'cep'       => $item->cep,
                    'uf'        => $item->estados_id,
                    'cidade'    => $item->cidades_id,
                    'documento' => $item->cpf,
                ]
            );
            
            //$datas = \Carbon\Carbon::createFromFormat('Y-m-d', $contasreceber->vencimento)->toDateTimeString();
            
            $boleto = new Eduardokum\LaravelBoleto\Boleto\Banco\Santander(
                [
                    'logo'                   => realpath(__DIR__ . '/../logos/') . DIRECTORY_SEPARATOR . '033.png',
                    'dataVencimento'         => new \Carbon\Carbon(\Carbon\Carbon::createFromFormat('Y-m-d', $item->vencimento)->toDateTimeString()),
                    'valor'                  => $item->valor,
                    'multa'                  => false, //$item->multa,
                    'juros'                  => false, //$item->juros,
                    'numero'                 => $item->numero,
                    'numeroDocumento'        => $item->numerodocumento.'',
                    'pagador'                => $pagador,
                    'beneficiario'           => $beneficiario,
                    'diasBaixaAutomatica'    => 15,
                    'carteira'               => $item->carteira,
                    'agencia'                => $item->agencia,
                    'conta'                  => $item->conta,
                    'descricaoDemonstrativo' => ['demonstrativo 1', 'demonstrativo 2', 'demonstrativo 3'],
                    'instrucoes'             => [$instrucao1, $instrucao2, $instrucao3],
                    'aceite'                 => 'N',
                    'especieDoc'             => 'DM',
                ]
            );

            $remessa->addBoleto($boleto);

        }

        $remessa->save($arquivo);
    
    }else{
        $fp = fopen($arquivo, 'w');
        fwrite($fp, '');
        fclose($fp);
    }
?>
    <div class="row">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Remessas do Contas a Receber</div>
                <div class="card-body">
                    <a href="{{ url('/financeiro/contasreceber') }}" title="Voltar"><button class="btn btn-warning rounded"><i class="fa fa-arrow-left" aria-hidden="true"></i> Voltar</button></a>
                    <!--<input name="search" type="text" />-->

                    <br/>
                    <br />

                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>#</th><th>Cliente</th><th>Documento</th><th>Parcela</th><th>Vencimento</th><th>Valor</th>
                                    <th>
                                        <!--<a href="{{ url('/financeiro/contasreceber/0,"CH"/marcarremessaajax') }}"><i class="fa fa-object-group text-success"></i>&nbsp;Remessa</a>-->
                                        Remessa
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($dados as $dado)
                                <tr>
                                    <td>{{ $dado->id }}</td>
                                    <td>
                                        @php
                                            $paciente = DB::table('pacientes')
                                                    ->select(DB::raw('nome'))
                                                    ->where('id', '=', $dado->paciente_id )
                                                    ->get();

                                            echo isset($paciente) ? $paciente[0]->nome : '--';
                                        @endphp
                                    </td>
                                    <td>{{ $dado->numerodocumento }}</td>
                                    <td>{{ $dado->numero }}</td>
                                    <td>{{ date('d/m/Y', strtotime($dado->vencimento)) }}</td>
                                    <td style="text-align: right;">{{ number_format($dado->valor, 2, ',', '.') }}</td>
                                    <td style="text-align: center;">
                                        @if ($dado->remessa === null)
                                            <a href="{{ url('/financeiro/contasreceber/' . $dado->id.',"CH"/marcarremessaajax') }}"><i class="fa fa-circle-o text-secondary"></i></a>
                                        @elseif ($dado->remessa === 'CH')
                                            <a href="{{ url('/financeiro/contasreceber/' . $dado->id.',"RM"/marcarremessaajax') }}""><i class="fa fa-check text-success"></i></a>
                                        @elseif ($dado->remessa === 'RM')
                                            <a href="{{ url('/financeiro/contasreceber/' . $dado->id.',null/marcarremessaajax') }}"><i class="fa fa-remove text-danger"></i></a>
                                        @elseif ($dado->remessa === 'ER')
                                            <i class="fa fa-warning text-warning"></i>
                                        @elseif ($dado->remessa === 'BX')
                                            <i class="fa fa-smile-o text-primary"></i>
                                        @else
                                            <i class="fa fa-asterisk text-info"></i>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td colspan="7">
                                        <?php
                                            $count = 0;
                                            foreach($dados as $item){
                                                if($item->remessa === 'CH') $count += 1;
                                            }
                                            echo 'Selecionados: '.$count;
                                        ?>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    <div class="pagination-wrapper"> {!! $dados->appends(['search' => Request::get('search')])->render() !!} </div>
                    <div>
                        @if(file_exists($arquivo))
                        <button class="btn btn-danger rounded" onclick="arquivoRemessa('{{ asset($arquivo) }}')"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                        <span class="text-primary">{{ asset($arquivo) }}</span>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('jqueryscript')
<script type="text/javascript">
    
    function arquivoRemessa(file){
        
        var form = this;

        const swalWithBootstrapButtons = Swal.mixin({
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
        })
        
        swalWithBootstrapButtons({
            title: "Emitir Remessa!",
            text: "Deseja emitir o arquivo de remessa e marcar os boletos como enviados?",
            type: 'info',
            showCancelButton: true,
            confirmButtonText: ' Sim ',
            cancelButtonText: ' Não ',
            reverseButtons: true
            }).then((result) => {
            if (result.value) {
               window.open(file, 'Download');
               //window.location.href = '/financeiro/contasreceber/-1,"BX"/marcarremessaajax';
            }
        })

    }
    
</script>
@endsection