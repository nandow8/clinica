<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SAEGS - Página não encontrada</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link href="{{ asset ('vendor/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">

    <style>
        ::-moz-selection { background: #fff; color: #3490dc; text-shadow: none; }
        ::selection { background: #fff; color: #3490dc; text-shadow: none; }
        #boxerror{
            width:100%;
            height:100%;
            position: absolute;
            background: #f6f6f6;
        }
        .nopadding{ margin:0px !important; padding:0px !important;}
        #img-erro{ width:100%; }
        #leftside{ 
            background:#f6f6f6;
            padding:50px;
            text-align: center;
            height:100%;
            display:block;
        }
        .erroheader{ 
            font-weight: bold;
            font-family: 'Nunito', Arial, sans-serif;
            font-size: 3.5em;
            color: #565656;
            margin-top: 100px;
            text-shadow: 1px 1px 2px #898989;
        }
        .titleerro{ 
            font-weight: bold;
            font-family: 'Nunito', Arial, sans-serif;
            font-size: 3.5em;
            color: #c10000;
            margin-top: 100px;
            text-shadow: 1px 4px 6px #4b4b4b, 0 0 0 #9a9a9a, 1px 4px 6px #000;
        }
        .descricaoerro{
            font-weight: bold;
            font-family: 'Nunito', Arial, sans-serif;
            font-size: 1.5em;
            color: #565656;
            width: 100%;
            padding: 10px;     
        }
        .btn-back{
            border:2px solid #ccc;
            background:#fff;
            color:#6f6f6f;
            font-weight:bold;
            margin:20px;
        }
        .btn-back:hover{ background:#007bff; color:#fff; border-color:#fff; }
        
        #rightside{
            text-align:center;
        }
        #rightside #img-erro{
            margin-top:50px;
            margin-bottom:20px;            
        }
        @media (min-width: 992px) and (max-width: 1368px) {
            #leftside{ 
                background:#fff;
                height:100vh;
            }
            #rightside{ 
                background:#ccc;
                height:100vh;
            }
        }
        
    </style>
</head>
<body>
    <div id="boxerror">        
        <div class="row nopadding">
            <div class="col-lg-6 nopadding" id="leftside">
                <h3 class="erroheader">OPS!!! <span class="titleerro">404</span></h3>
                
                <label class="descricaoerro">Desculpe-nos, mas a página que está procurando não foi encontrada</label> 
            
                <div class="retornar">
                    <a href="/home" class="btn btn-back">Ir para HOME <i class="fa fa-home"></i></a>
                </div>
            </div>
            <div class="col-lg-6 nopadding" id="rightside">
                <img src="/img/errors/404.png" alt="Erro 404" class="img-responsible" id="img-erro">
            </div>
        </div>        
    </div>
</body>
</html>