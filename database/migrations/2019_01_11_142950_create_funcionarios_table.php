<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            // $table->string('nome')->nullable();
            $table->string('especialidade');
            $table->string('areaatuacao')->nullable();
            $table->string('membro');
            $table->string('crm')->nullable();
            $table->date('crmemissao')->nullable();
            $table->string('identidade')->nullable();
            $table->string('cpf')->nullable();
            $table->string('cep')->nullable();
            $table->string('rua')->nullable();
            $table->string('numero')->nullable();
            $table->string('bairro')->nullable();
            $table->string('cidade')->nullable();
            $table->string('estado')->nullable();
            $table->string('complemento')->nullable();
            $table->string('telefone')->nullable();
            $table->string('recado')->nullable();
            $table->string('celular')->nullable();
            $table->string('email')->nullable();
            $table->date('nascimento')->nullable();
            $table->string('nacionlalidade')->nullable();
            $table->string('estadocivil');
            $table->string('sexo');
            $table->string('filhos');
            $table->integer('totalfilhos')->nullable();
            $table->text('obs')->nullable();
            $table->string('status');
            $table->string('image')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('funcionarios');
    }
}
