<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContaspagarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contaspagars', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->integer('fornecedor_id')->nullable();
            $table->string('fornecedor')->nullable();
            $table->date('vencimento')->nullable();
            $table->date('pagamento')->nullable();
            $table->integer('valor')->nullable();
            $table->integer('numerodocumento')->nullable();
            $table->integer('numero')->nullable();
            $table->text('descricao')->nullable();
            $table->string('status');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contaspagars');
    }
}
