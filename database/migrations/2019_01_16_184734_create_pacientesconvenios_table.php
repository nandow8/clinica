<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePacientesconveniosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientesconvenios', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('convenio')->nullable();
            $table->string('plano')->nullable();
            $table->string('numerocarteira')->nullable();
            $table->date('datavalidade')->nullable();
            $table->char('cnpj', 18)->nullable();
            $table->string('contato')->nullable();
            $table->string('telefone')->nullable();
            $table->string('telefonedois')->nullable();
            $table->string('email')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pacientesconvenios');
    }
}
