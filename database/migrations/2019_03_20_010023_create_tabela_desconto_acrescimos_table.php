<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTabelaDescontoAcrescimosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tabela_desconto_acrescimos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('titulo')->nullable();
            $table->text('descricao')->nullable();
            $table->integer('valor')->nullable();
            $table->string('tipo')->default('Desconto');
            $table->string('status')->default('Ativo');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tabela_desconto_acrescimos');
    }
}
