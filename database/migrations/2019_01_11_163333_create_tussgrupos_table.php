<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTUSSGruposTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tussgrupos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('descricao')->nullable();
        });

        DB::table('tussgrupos')->insert([
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'N/I'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'ANATOMIA PATOLÓGICA E CITOPATOLOGIA'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'CABEÇA E PESCOÇO'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'ELETROFISIOLÓGICOS / MECÂNICOS E FUNCIONAIS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'ENDOSCÓPICOS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'EXAMES ESPECÍFICOS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'GENÉTICA'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'MEDICINA NUCLEAR'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'MEDICINA TRANSFUSIONAL'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'MÉTODOS DIAGNÓSTICOS POR IMAGEM'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'NARIZ E SEIOS PARANASAIS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'OLHOS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'ORELHA'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'OUTROS PROCEDIMENTOS INVASIVOS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PAREDE TORÁCICA'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PELE E TECIDO CELULAR SUBCUTÂNEO, MUCOSAS E ANEXOS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PROCEDIMENTOS CLÍNICOS AMBULATORIAIS E HOSPITALARES'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PROCEDIMENTOS GERAIS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PROCEDIMENTOS LABORATORIAIS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'RADIOTERAPIA'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA CÁRDIO-CIRCULATÓRIO'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA DIGESTIVO E ANEXOS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA GENITAL E REPRODUTOR FEMININO'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA GENITAL E REPRODUTOR MASCULINO'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA MÚSCULO-ESQUELÉTICO E ARTICULAÇÕES'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA NERVOSO - CENTRAL E PERIFÉRICO'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA RESPIRATÓRIO E MEDIASTINO'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'SISTEMA URINÁRIO'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'TRANSPLANTES DE ÓRGÃOS OU TECIDOS'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tussgrupos');
    }

}
