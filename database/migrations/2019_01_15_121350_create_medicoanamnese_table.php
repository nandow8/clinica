<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicoanamneseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicoanamnese', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('medicofichaclinica_id')->nullable();
            $table->foreign('medicofichaclinica_id')->references('id')->on('medicofichaclinicas')->onDelete('cascade');
            $table->text('qp', 500)->nullable();
            $table->text('hda', 500)->nullable();
            $table->boolean('geral_emagrecimento')->nullable();
            $table->boolean('geral_visao')->nullable();
            $table->boolean('geral_cefaleia')->nullable();
            $table->boolean('geral_fadiga')->nullable();
            $table->boolean('geral_audicao')->nullable();
            $table->boolean('geral_vertigens')->nullable();
            $table->boolean('geral_insonia')->nullable();
            $table->boolean('geral_zumbido')->nullable();
            $table->boolean('geral_tonteiras')->nullable();
            $table->text('geral_observacoes', 500)->nullable();
            $table->boolean('geral_outros')->nullable();
            $table->string('geral_outros_obs')->nullable();
            $table->boolean('pele_alteracoesdacor')->nullable();
            $table->boolean('pele_prurido')->nullable();
            $table->boolean('pele_lesoes')->nullable();
            $table->text('pele_observacoes', 500)->nullable();
            $table->boolean('pele_outros')->nullable();
            $table->string('pele_outros_obs')->nullable();
            $table->boolean('nariz_dor')->nullable();
            $table->boolean('nariz_epistaxes')->nullable();
            $table->text('nariz_obstrucao', 500)->nullable();
            $table->text('nariz_observacoes', 500)->nullable();
            $table->boolean('nariz_outros')->nullable();
            $table->string('nariz_outros_obs')->nullable();
            $table->boolean('dentes_dentes')->nullable();
            $table->boolean('dentes_gengivas')->nullable();
            $table->boolean('dentes_rouquidao')->nullable();
            $table->text('dentes_observacoes', 500)->nullable();
            $table->boolean('dentes_outros')->nullable();
            $table->string('dentes_outros_obs')->nullable();
            $table->boolean('pescoco_dor')->nullable();
            $table->boolean('pescoco_tumoracoes')->nullable();
            $table->text('pescoco_observacoes', 500)->nullable();
            $table->boolean('pescoco_outros')->nullable();
            $table->string('pescoco_outros_obs')->nullable();
            $table->boolean('mamas_nodulos')->nullable();
            $table->boolean('mamas_dor')->nullable();
            $table->boolean('mamas_secrecao')->nullable();
            $table->text('mamas_observacoes', 500)->nullable();
            $table->boolean('mamas_outros')->nullable();
            $table->string('mamas_outros_obs')->nullable();
            $table->boolean('apcardiaco_grandes')->nullable();
            $table->boolean('apcardiaco_medios')->nullable();
            $table->boolean('apcardiaco_pequenos')->nullable();
            $table->boolean('apcardiaco_repouso')->nullable();
            $table->boolean('apcardiaco_ortopneia')->nullable();
            $table->boolean('apcardiaco_dispneia')->nullable();
            $table->boolean('apcardiaco_paroxisticanoturna')->nullable();
            $table->boolean('apcardiaco_suspinosa')->nullable();
            $table->boolean('apcardiaco_palpitacoes')->nullable();
            $table->boolean('apcardiaco_tosse')->nullable();
            $table->boolean('apcardiaco_cianosehemoptise')->nullable();
            $table->boolean('apcardiaco_sincope')->nullable();
            $table->boolean('apcardiaco_edema')->nullable();
            $table->text('apcardiaco_observacoes', 500)->nullable();
            $table->boolean('apcardiaco_outros')->nullable();
            $table->string('apcardiaco_outros_obs')->nullable();
            $table->boolean('aprespiratorio_chiado')->nullable();
            $table->boolean('aprespiratorio_infeccaorespiratoria')->nullable();
            $table->boolean('aprespiratorio_tosse')->nullable();
            $table->boolean('aprespiratorio_expectoracao')->nullable();
            $table->text('aprespiratorio_observacoes', 500)->nullable();
            $table->boolean('aprespiratorio_outros')->nullable();
            $table->string('aprespiratorio_outros_obs')->nullable();
            $table->boolean('apdigestivo_disfagia')->nullable();
            $table->boolean('apdigestivo_pirose')->nullable();
            $table->boolean('apdigestivo_epigastralgia')->nullable();
            $table->boolean('apdigestivo_dorabdominal')->nullable();
            $table->boolean('apdigestivo_funcionamentointestinal')->nullable();
            $table->boolean('apdigestivo_homorroidas')->nullable();
            $table->boolean('apdigestivo_sangramentodigestivo')->nullable();
            $table->boolean('apdigestivo_usodemedicacao')->nullable();
            $table->text('apdigestivo_observacoes', 500)->nullable();
            $table->boolean('apdigestivo_outros')->nullable();
            $table->string('apdigestivo_outros_obs')->nullable();
            $table->boolean('apurinario_alteracoescorurina')->nullable();
            $table->boolean('apurinario_volume')->nullable();
            $table->boolean('apurinario_frequencia')->nullable();
            $table->boolean('apurinario_disuria')->nullable();
            $table->boolean('apurinario_nicturia')->nullable();
            $table->boolean('apurinario_secrecao')->nullable();
            $table->boolean('apurinario_impotencia')->nullable();
            $table->boolean('apurinario_alteracoesmenstruais')->nullable();
            $table->boolean('apurinario_sangramentoabdominal')->nullable();
            $table->text('apurinario_observacoes', 500)->nullable();
            $table->boolean('apurinario_outros')->nullable();
            $table->string('apurinario_outros_obs')->nullable();
            $table->boolean('sisnervoso_paresias')->nullable();
            $table->boolean('sisnervoso_parentesias')->nullable();
            $table->boolean('sisnervoso_tremores')->nullable();
            $table->boolean('sisnervoso_ausencias')->nullable();
            $table->boolean('sisnervoso_convulcoes')->nullable();
            $table->text('sisnervoso_observacoes', 500)->nullable();
            $table->boolean('sisnervoso_outros')->nullable();
            $table->string('sisnervoso_outros_obs')->nullable();
            $table->boolean('aplocomotor_dor')->nullable();
            $table->boolean('aplocomotor_impotenciafuncional')->nullable();
            $table->text('aplocomotor_observacoes', 500)->nullable();
            $table->boolean('aplocomotor_outros')->nullable();
            $table->string('aplocomotor_outros_obs')->nullable();
            $table->boolean('psiquismo_ansiedade')->nullable();
            $table->boolean('psiquismo_depressao')->nullable();
            $table->boolean('psiquismo_alucinacoes')->nullable();
            $table->text('psiquismo_observacoes', 500)->nullable();
            $table->boolean('psiquismo_outros')->nullable();
            $table->string('psiquismo_outros_obs')->nullable();
            $table->boolean('doencasinfecciosas_doencasinfancia')->nullable();
            $table->boolean('doencasinfecciosas_doencasvenereas')->nullable();
            $table->boolean('doencasinfecciosas_febreindeterminada')->nullable();
            $table->boolean('doencasinfecciosas_amigdalite')->nullable();
            $table->boolean('doencasinfecciosas_febrereumatica')->nullable();
            $table->boolean('doencasinfecciosas_hepatite')->nullable();
            $table->boolean('doencasinfecciosas_infeccaourinaria')->nullable();
            $table->boolean('doencasinfecciosas_tuberculose')->nullable();
            $table->boolean('doencasinfecciosas_peneumonia')->nullable();
            $table->text('doencasinfecciosas_observacoes', 500)->nullable();
            $table->boolean('doencasinfecciosas_outros')->nullable();
            $table->string('doencasinfecciosas_outros_obs')->nullable();
            $table->boolean('manifalergicas_asma')->nullable();
            $table->boolean('manifalergicas_rinite')->nullable();
            $table->boolean('manifalergicas_urticaria')->nullable();
            $table->boolean('manifalergicas_eczema')->nullable();
            $table->boolean('manifalergicas_alergiamedicamentosaealimentar')->nullable();
            $table->text('manifalergicas_observacoes', 500)->nullable();
            $table->boolean('manifalergicas_outros')->nullable();
            $table->string('manifalergicas_outros_obs')->nullable();
            $table->boolean('outrasdoencas_diabetes')->nullable();
            $table->boolean('outrasdoencas_hipertencao')->nullable();
            $table->boolean('outrasdoencas_cardiopatias')->nullable();
            $table->boolean('outrasdoencas_altlipidios')->nullable();
            $table->boolean('outrasdoencas_nefropatias')->nullable();
            $table->boolean('outrasdoencas_ulcerapeptica')->nullable();
            $table->boolean('outrasdoencas_neoplastia')->nullable();
            $table->boolean('outrasdoencas_epilepsia')->nullable();
            $table->boolean('outrasdoencas_neurose')->nullable();
            $table->boolean('outrasdoencas_glaucoma')->nullable();
            $table->boolean('outrasdoencas_gota')->nullable();
            $table->text('outrasdoencas_obs', 500)->nullable();
            $table->boolean('outrasdoencas_outros')->nullable();
            $table->string('outrasdoencas_outros_obs')->nullable();
            $table->text('obs_cirurgiastraumasconvulcoes', 500)->nullable();
            $table->text('obs_usodemedicamentos', 500)->nullable();
            $table->text('obs_historiafisiologica', 500)->nullable();
            $table->text('obs_historiapessoa', 500)->nullable();
            $table->float('examefisico_peso', 8, 2)->nullable();
            $table->float('examefisico_altura', 8, 2)->nullable();
            $table->float('examefisico_tax', 8, 2)->nullable();
            $table->float('examefisico_respiracao', 8, 2)->nullable();
            $table->float('examefisico_pulso', 8, 2)->nullable();
            $table->float('examefisico_pabd_deitado', 8, 2)->nullable();
            $table->float('examefisico_pabd_sentado', 8, 2)->nullable();
            $table->float('examefisico_pabd_empe', 8, 2)->nullable();
            $table->float('examefisico_pabe_deitado', 8, 2)->nullable();
            $table->float('examefisico_pabe_sentado', 8, 2)->nullable();
            $table->float('examefisico_pabe_empe', 8, 2)->nullable();
            $table->text('examefisico_observacoes', 500)->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicoanamnese');
    }
}
