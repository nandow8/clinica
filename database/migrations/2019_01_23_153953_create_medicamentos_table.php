<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicamentos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('princicipoativo', 1500)->nullable();
            $table->bigInteger('laboratorio_id')->unsigned();
            $table->string('codigoggrem')->nullable();
            $table->string('registro')->nullable();
            $table->string('ean1')->nullable();
            $table->string('ean2')->nullable();
            $table->string('ean3')->nullable();
            $table->string('produto', 1500)->nullable();
            $table->string('apresentacao', 1500)->nullable();
            $table->string('classeterapeutica')->nullable();
            $table->string('tipodeproduto')->nullable();
            $table->decimal('pfsemimpostos', 8, 2)->nullable();
            $table->decimal('pf0', 10, 2)->nullable();
            $table->decimal('pf12', 8, 2)->nullable();
            $table->decimal('pf17', 8, 2)->nullable();
            $table->decimal('pf17alc', 8, 2)->nullable();
            $table->decimal('pf175', 8, 2)->nullable();
            $table->decimal('pf175alc', 8, 2)->nullable();
            $table->decimal('pf18', 8, 2)->nullable();
            $table->decimal('pf18alc', 8, 2)->nullable();
            $table->decimal('pf20', 8, 2)->nullable();
            $table->decimal('pmc0', 8, 2)->nullable();
            $table->decimal('pmc12', 8, 2)->nullable();
            $table->decimal('pmc17', 8, 2)->nullable();
            $table->decimal('pmc17alc', 8, 2)->nullable();
            $table->decimal('pmc175', 8, 2)->nullable();
            $table->decimal('pmc175alc', 8, 2)->nullable();
            $table->decimal('pmc18', 8, 2)->nullable();
            $table->decimal('pmc18alc', 8, 2)->nullable();
            $table->decimal('pmc20', 8, 2)->nullable();
            $table->string('restricaohospitalar')->nullable();
            $table->string('cap')->nullable();
            $table->string('confaz87')->nullable();
            $table->string('analiserecursal')->nullable();
            $table->string('listadeconcessaodecreditotributario')->nullable();
            $table->string('comercializacao2017')->nullable();
            $table->string('tarja')->default('N/I');
            $table->string('tipomedicamento')->nullable();
            $table->string('status')->default('Ativo');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicamentos');
    }
}
