<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('nome')->nullable();
            $table->string('indicacao')->nullable();
            $table->string('sexo')->nullable();
            $table->string('rg')->nullable();
            $table->string('cpf')->nullable();
            $table->string('estadocivil')->nullable();
            $table->date('datanascimento')->nullable();
            $table->string('naturalidade')->nullable();
            $table->string('nacionalidade')->nullable();
            $table->string('telefone')->nullable();
            $table->string('celular')->nullable();
            $table->string('email')->nullable();
            $table->string('cep')->nullable();
            $table->string('estados_id');
            $table->string('cidades_id');
            $table->string('bairros_id');
            $table->string('rua')->nullable();
            $table->string('numero')->nullable();
            $table->string('complemento')->nullable();
            $table->text('observacoes')->nullable();
            $table->boolean('tipopaciente');
            $table->bigInteger('paciente_titular_id')->nullable();
            $table->string('image')->nullable();
            $table->integer('pacientesconvenio_id')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pacientes');
    }
}
