<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTelachamados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
      Schema::create('telachamados', function (Blueprint $table) {
        $table->BigIncrements('id');
        $table->timestamps();
        $table->softDeletes();
        $table->unsignedBigInteger('paciente_id')->nullable();
        $table->unsignedBigInteger('funcionario_id')->nullable();
        $table->unsignedBigInteger('sala_id')->nullable();
        $table->unsignedBigInteger('clinica_id')->nullable();
        $table->date('dataconsulta');
        $table->string('status')->default('Chamado');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('telachamados');
    }
}
