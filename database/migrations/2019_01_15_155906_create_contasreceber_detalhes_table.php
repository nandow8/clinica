<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContasreceberDetalhesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contasreceber_detalhes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('contasreceber_id')->unsigned();
            $table->bigInteger('tabela_servico_id')->unsigned();
            $table->string('descricao')->nullable();
            $table->decimal('valor', 8, 2)->nullable();

            $table->foreign('contasreceber_id')->references('id')->on('contasrecebers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contasreceber_detalhes', function (Blueprint $table) {
            $table->dropForeign('contasreceber_detalhes_contasreceber_id_foreign');
        });
        
        Schema::dropIfExists('contasreceber_detalhes');
    }
}
