<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgendamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agendamentos', function (Blueprint $table) {
            $table->increments('id');
            $table->date('dataconsulta')->nullable();
            $table->time('horaconsulta')->nullable();
            $table->dateTime('startconsulta')->nullable();
            $table->dateTime('endconsulta')->nullable();
            $table->bigInteger('paciente')->nullable();
            $table->bigInteger('medico')->nullable();
            $table->bigInteger('sala')->nullable();

            $table->bigInteger('idclinica')->nullable();
            $table->string('nomeclinica')->nullable();

            $table->string('nomepaciente', 80)->nullable();
            $table->string('nomemedico', 80)->nullable();
            $table->string('atividade', 100);
            $table->string('obs', 200);
            $table->string('status', 80);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agendamentos');
    }
}
