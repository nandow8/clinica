<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicohistoricoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicohistoricos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('paciente_id')->nullable();
            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->unsignedBigInteger('medicofichaclinica_id')->nullable();
            $table->foreign('medicofichaclinica_id')->references('id')->on('medicofichaclinicas');
            $table->unsignedBigInteger('medicoanamnese_id')->nullable();
            $table->foreign('medicoanamnese_id')->references('id')->on('medicoanamnese');
            $table->unsignedBigInteger('medicoevolucao_id')->nullable();
            $table->foreign('medicoevolucao_id')->references('id')->on('medicoevolucao');
            $table->unsignedBigInteger('medicoexames_id')->nullable();
            $table->foreign('medicoexames_id')->references('id')->on('medicoexames');
            $table->unsignedBigInteger('medicoatestado_id')->nullable();
            $table->foreign('medicoatestado_id')->references('id')->on('medicoatestado');
            $table->unsignedBigInteger('medicoanexos_id')->nullable();
            $table->foreign('medicoanexos_id')->references('id')->on('medicoanexos');
            $table->unsignedBigInteger('medicoprescricoes_id')->nullable();
            $table->foreign('medicoprescricoes_id')->references('id')->on('medicoprescricoes');
            $table->unsignedBigInteger('medicodiagnostico_id')->nullable();
            $table->foreign('medicodiagnostico_id')->references('id')->on('medicodiagnostico');
            $table->unsignedBigInteger('medicoresultado_id')->nullable();
            $table->string('retificacao')->nullable();
            $table->string('tempo_atendimento')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicohistoricos');
    }
}
