<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClinicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('razao')->nullable();
            $table->string('fantasia')->nullable();
            $table->string('endereco')->nullable();
            $table->string('numero')->nullable();
            $table->string('cep')->nullable();
            $table->string('uf')->nullable();
            $table->string('cidade')->nullable();
            $table->string('cnpj')->nullable();
            $table->string('conta')->nullable();
            $table->string('carteira')->nullable();
            $table->string('agencia')->nullable();
            $table->string('codigocliente')->nullable();
            $table->integer('multa')->nullable();
            $table->integer('juros')->nullable();
            $table->string('instrucao1')->nullable();
            $table->string('instrucao2')->nullable();
            $table->string('instrucao3')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clinicas');
    }
}
