<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTUSSCapitulosTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tusscapitulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('descricao')->nullable();
        });

        DB::table('tusscapitulos')->insert([
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'N/I'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PROCEDIMENTOS CIRÚRGICOS E INVASIVOS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PROCEDIMENTOS CLÍNICOS AMBULATORIAIS E HOSPITALARES'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PROCEDIMENTOS GERAIS'],
            ['created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'descricao' => 'PROCEDIMENTOS DIAGNÓSTICOS E TERAPÊUTICOS'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('tusscapitulos');
    }

}
