<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicoexamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicoexames', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->unsignedBigInteger('medicofichaclinica_id')->nullable();
            $table->foreign('medicofichaclinica_id')->references('id')->on('medicofichaclinicas');
            $table->text('introducao_pedido')->nullable();
            $table->longText('info_adicional')->nullable();
            $table->longText('exame')->nullable();
            $table->text('codigo')->nullable();
            $table->longText('observacoes')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medicoexames');
    }
}
