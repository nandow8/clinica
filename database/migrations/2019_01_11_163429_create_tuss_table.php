<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTUSSTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('tuss', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('codigo', 50)->nullable();
            $table->string('descricao', 500)->nullable();
            $table->string('correlacao', 3)->nullable();
            $table->string('rol', 500)->nullable();
            $table->bigInteger('subgrupo_id')->unsigned();
            $table->bigInteger('grupo_id')->unsigned();
            $table->bigInteger('capitulo_id')->unsigned();
            $table->string('od', 30)->nullable();
            $table->string('amb', 30)->nullable();
            $table->string('hco', 30)->nullable();
            $table->string('hso', 30)->nullable();
            $table->string('pac', 30)->nullable();
            $table->string('dut', 30)->nullable();
            $table->string('status')->default('Ativo');

            $table->foreign('subgrupo_id')->references('id')->on('tusssubgrupos');
            $table->foreign('grupo_id')->references('id')->on('tussgrupos');
            $table->foreign('capitulo_id')->references('id')->on('tusscapitulos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('tuss', function (Blueprint $table) {
            $table->dropForeign('tuss_subgrupo_id_foreign');
            $table->dropForeign('tuss_grupo_id_foreign');
            $table->dropForeign('tuss_capitulo_id_foreign');
        });

        Schema::drop('tuss');
    }

}
