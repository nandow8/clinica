<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bancos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('codigo')->nullable();
            $table->string('agencia')->nullable();
            $table->string('agenciadig')->nullable();
            $table->string('conta')->nullable();
            $table->string('contadig')->nullable();
            $table->string('cedente')->nullable();
            $table->string('cedentedig')->nullable();
            $table->string('nome')->nullable();
            $table->string('carteira')->nullable();
            $table->string('variacao')->nullable();
            $table->string('numero')->nullable();
            $table->string('convenio')->nullable();
            $table->string('instrucao1')->nullable();
            $table->string('instrucao2')->nullable();
            $table->string('instrucao3')->nullable();
            $table->string('status');
            $table->string('geraboleto');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bancos');
    }
}
