<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContratosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contratos', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->longText('descricao')->nullable();
            $table->bigInteger('documento_id')->unsigned();
            $table->bigInteger('paciente_id')->unsigned();
            $table->date('data')->nullable();
            $table->decimal('valoradesao', 8, 2)->nullable();
            $table->decimal('valormensalidade', 8, 2)->nullable();
            $table->decimal('valorretorno', 8, 2)->nullable();
            $table->decimal('valorconsulta', 8, 2)->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contratos');
    }
}
