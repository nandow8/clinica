<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrcamentosDetalhesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orcamentos_detalhes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('orcamento_id')->unsigned();
            $table->bigInteger('tabela_servico_id')->unsigned();
            $table->string('descricao')->nullable();
            $table->decimal('valor', 8, 2)->nullable();

            $table->foreign('orcamento_id')->references('id')->on('orcamentos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orcamentos_detalhes', function (Blueprint $table) {
            $table->dropForeign('orcamentos_detalhes_orcamento_id_foreign');
        });
        
        Schema::dropIfExists('orcamentos_detalhes');
    }
}
