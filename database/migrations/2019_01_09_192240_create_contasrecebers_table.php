<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContasrecebersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('contasrecebers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->bigInteger('paciente_id')->unsigned();
            $table->date('vencimento')->nullable();
            $table->date('pagamento')->nullable();
            $table->decimal('valor', 8, 2)->nullable();
            $table->integer('numero')->nullable();
            $table->integer('numerodocumento')->nullable();
            $table->text('descricao')->nullable();
            $table->string('status')->default('Receber');
            $table->bigInteger('clinica_id')->unsigned();
            $table->char('tipopagto', 2)->default('DI'); // 'DI':'Dinheiro'|'Bo':'Boleto'|'CC':'Cartao Credito'|'CD':'Cartao Debito'|'CH':'Cheque'

            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->foreign('clinica_id')->references('id')->on('clinicas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      
        Schema::table('contasrecebers', function (Blueprint $table) {
            $table->dropForeign('contasreceber_paciente_id_foreign');
            $table->dropForeign('contasreceber_clinica_id_foreign');
        });

        Schema::drop('contasrecebers');
    }

}
