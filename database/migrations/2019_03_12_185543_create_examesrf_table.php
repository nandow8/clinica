<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExamesrfTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examesrf', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('tabela', 150)->nullable();
            $table->string('mneumonico', 150)->nullable();
            $table->string('codigo', 150)->nullable();
            $table->string('nome', 254)->nullable();
            $table->decimal('ch', 10, 2)->nullable();
            $table->decimal('preco', 10, 2)->nullable();
            $table->decimal('percentual', 10, 2)->nullable();
            $table->decimal('venda', 10, 2)->nullable();
            $table->decimal('lucro', 10, 2)->nullable();
            $table->string('status', 50)->default('Ativo');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('examesrf');
    }
}
