<?php

use Illuminate\Database\Seeder;

class AddDummyEvent extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $data = [
            ['title'=>'Finacial forum', 'start_date'=>'2019-01-12', 'end_date'=>'2019-01-15'],
            ['title'=>'Devtalk', 'start_date'=>'2019-01-13', 'end_date'=>'2019-01-20'],
            ['title'=>'Super Event', 'start_date'=>'2019-01-23', 'end_date'=>'2019-01-24'],
            ['title'=>'wtf event', 'start_date'=>'2019-01-15', 'end_date'=>'2019-01-27'],
           ];
           \DB::table('events')->insert($data);
    }
}
