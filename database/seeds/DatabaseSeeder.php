<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        

        
        DB::table('users')->insert([
            'name' => 'Usuario fake',
            'email' => 'usuariofake@gmail.com',
            'password' => bcrypt('123123'),
        ]);

        for ($i=0; $i < 10; $i++) { 
            DB::table('pacientes')->insert([
                'nome' => str_random(10),
                'email' => str_random(10) . '@gmail.com',
                'indicacao' => str_random(10),
                'sexo' => rand(1, 2),
                'rg' => rand(100, 999) . '.' . rand(100, 999) . '.' . rand(100, 999) . '-' . rand(1, 9),
                'cpf' => rand(100, 999) . '.' . rand(100, 999) . '.' . rand(100, 999) . '-' . rand(10, 99),
                'estadocivil' => 'Solteiro',
                'datanascimento' => date('Y-m-d'),
                'naturalidade' => 'ssp',
                'nacionalidade' => 'brasileiro',
                'telefone' => '(' . rand(10, 99) . ')' . ' '.  rand(1000, 9999) . '-' . rand(1000, 9999),
                'celular' => '(' . rand(10, 99) . ')' . ' '.  rand(1000, 9999) . '-' . rand(10000, 99999),
                'cep' => rand(10000, 99999) . '-' . rand(100, 999),
                'numero' => rand(2, 50),
                'tipopaciente' => 1,
                'estados_id' => rand(1, 27),
                'cidades_id' => rand(1, 50),
                'bairros_id' => rand(1, 50),
                'created_at' => date('Y-m-d H:m:s'),
                'updated_at' => date('Y-m-d H:m:s'),
            ]);
        }
    }
}
